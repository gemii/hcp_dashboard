$(function(){
	getNotReadMail();
	getNotReadMessage();
	loadTalkMessageWindow();
	setInterval('getNotReadMail()',30000);
	setInterval('getNotReadMessage()',30000);
	var path = window.location.href;
	if(path.indexOf("mail") >= 0)
		$('#mail_li').attr("class","active");
	if(path.indexOf("chat") >= 0)
		$('#chat_li').attr("class","active");
	
	//talk 窗口移动
	 var clicked = "Nope.";
	var mausx = "0";
	var mausy = "0";
	var winx = "0";
	var winy = "0";
	var difx = mausx - winx;
	var dify = mausy - winy;

	$("html").mousemove(
			function(event) {
				if(event.pageX < 50)
					return false;
				if(event.pageY < 50)
					return false;
				if(event.pageY > 700)
					return false;
				mausx = event.pageX;
				mausy = event.pageY;
				winx = $(".talk_window").offset().left;
				winy = $(".talk_window").offset().top;
				if (clicked == "Nope.") {
					difx = mausx - winx;
					dify = mausy - winy;
				}
				var newx = event.pageX - difx
						- $(".talk_window").css("marginLeft").replace('px', '');
				var newy = event.pageY - dify
						- $(".talk_window").css("marginTop").replace('px', '');
				$(".talk_window").css({
					top : newy,
					left : newx
				});
			});
	$("#pew").mousedown(function(event) {
		clicked = "Yeah.";
	});
	$("html").mouseup(function(event) {

		clicked = "Nope.";
	});
});
//得到未读邮件数据,并填充
function getNotReadMail(){
	$.ajax({
		   type: "GET",
		   url: "/SX2/mail/getNotRead",
		   success: function(data){
			   if(data.status == 200){
				   var mail = eval("("+data.data+")");
				   $('#notRead').html(mail["count"]);
				   var notRead = mail.notReadList;
				   var notReadList = eval("("+notRead+")");
				   var lis = "";
				   lis += "<li>";
				   lis += "	<div class='notification_header'>";
				   lis += "		<h3>"+ "你有" + mail["count"] + "封未读邮件" +"</h3>";
				   lis += "	</div>";
				   lis += "</li>";
				   for(var i = 0;i < notReadList.length; i++){
					   lis += "<li><a href='/SX2/mail/details/"+ notReadList[i].mailId +"/in'>";
					   lis += "		<div class='user_img'>";
					   lis += "			<img src='/SX2/user/getPhoto?photoPath=" + notReadList[i].photo + "' alt=''>";
					   lis += "		</div>";
					   lis += "		<div class='notification_desc'>";
					   lis += "			<p>" + notReadList[i].title + "</p>";
					   lis += "			<p>";
					   lis += "				<span>" + notReadList[i].dateString + "</span>";
					   lis += "			</p>";
					   lis += "		</div>";
					   lis += "		<div class='clearfix'></div>";
					   lis += "	</a>";
					   lis += "</li>";
				   }
				   lis += "<li>";
				   lis += "	<div class='notification_bottom'>";
				   lis += "		<a href='/SX2/mail/inbox'>查看所有邮件</a>";
				   lis += "	</div>";
				   lis += "</li>";
				   $('#notRead_ul').html("");
				   $('#notRead_ul').append(lis);
			   }
		}
	});
}

//非chat.jsp 页面接收单聊信息
function getTalkMessage2(){
	if($('#chatName_common').val() == "" || $('#chatName_common').val() == null)
		return;
	$.ajax({
		   type: "post",
		   url: "/SX2/chat/getMessage?chatName=" +$('#chatName_common').val() ,
		   success: function(data){
			   if(data.status == 200){
				   $.each(data.data,function(index,msg){
					   var talk_each = "";
					   talk_each += "<div class='talk_each_box'>";
					   talk_each += "	<div class='sanjiao_left'></div>";
					   talk_each += "	<div class='talk_each_left'>"+ msg.content +"</div>";
					   talk_each += "<div class='talk_each_time'>" + moment(msg.created).format("YYYY-MM-DD HH:mm:ss") + "</div>";
					   talk_each += "</div>";
					   $('#talk_content_common').append(talk_each);
					   var box = document.getElementById("talk_content_box_common");
					   box.scrollTop = box.scrollHeight;
				   });
			   }
		   }
		});
}
function loadTalkMessageWindow(){
	var window = "";
	var chatName = "";
	window += "<div class='talk_window noselect talk_window_common'>";
	window += "	<form id='talkForm_common'>";
	window += "		<div class='pew' id='pew'>";
	window += "			<span>与<span id='talk_username_common'></span>聊天中";
	window += "			</span><input type='hidden' id='chatName_common' name='chatName' value=''>";
	window += "		</div>";
	window += "		<div class='talk_container'>";
	window += "			<div class='talk_content_box talk_content_box_common' id='talk_content_box_common'>";
	window += "				<div style='text-align:center;' id='historyDivCommon'>";
	window += "					<a href='javascript:getHistoryCommon();'>查看历史纪录</a>";
	window += "				</div>";
	window += "				<div id='talk_content_common'>";
	window += "					";
	window += "				</div>";
	window += "			</div>";
	window += "			<div class='talk_sendMsg'>";
	var getTalkMessage = setInterval('getTalkMessage2()',3000);
	window += "				<textarea style='height:50px;width:330px;resize:none;font-size:12px;' name='talkContent' id='talkContent_common'></textarea>";
	window += "				<button class='btn btn-default btn-xs talk_close_btn' onclick='close_talk2("+ getTalkMessage +");return false;'>关闭</button>";
	window += "				<button class='btn btn-default btn-xs talk_send_btn' id='sendTalkMessage_btn' onclick='sendTalkMessage2();return false;'>发送</button>";
	window += "			</div>";
	window += "		</div>";
	window += "	</form>";
	window += "</div>";
	$('body').append(window);
}
function showTalkMessageWindow(chatName){
	$('#talk_username_common').html(chatName);
	$('#chatName_common').val(chatName);
	$('.talk_window').css("z-index","999");
	document.getElementById('talkContent_common').onkeydown = function(event) {
		if (event.keyCode == 13) {
			sendTalkMessage2();
		}
	};
}
//得到未读消息数据,并填充
function getNotReadMessage(){
	$.ajax({
		   type: "GET",
		   url: "/SX2/chat/getNotRead",
		   success: function(data){
			   if(data.status == 200){
				   var message = eval("("+data.data+")");
				   $('#notReadMessageCount').html(message["count"]);
				   var notRead = message.notReadList;
				   var notReadList = eval("("+notRead+")");
				   var lis = "";
				   lis += "<li>";
				   lis += "	<div class='notification_header'>";
				   lis += "		<h3>"+ "你有" + message["count"] + "封未读消息" +"</h3>";
				   lis += "	</div>";
				   lis += "</li>";
				   for(var i = 0;i < notReadList.length; i++){
					   lis += "<li><a href='javascript:void(0);' onclick='showTalkMessageWindow(\""+ notReadList[i].senderName +"\");'>";
					   lis += "		<div class='user_img'>";
					   lis += "			<img src='/SX2/user/getPhoto?photoPath=" + notReadList[i].photo + "' alt=''>";
					   lis += "		</div>";
					   lis += "		<div class='notification_desc'>";
					   lis += "			<p>" + notReadList[i].senderName + "<span class='message_count' style='color:#fff !important'>"+ notReadList[i].count +"</span></p>";
					   lis += "			<p>";
					   lis += "				<span>" + notReadList[i].dateString + "</span>";
					   lis += "			</p>";
					   lis += "		</div>";
					   lis += "		<div class='clearfix'></div>";
					   lis += "	</a>";
					   lis += "</li>";
				   }
				   lis += "<li>";
				   lis += "	<div class='notification_bottom'>";
				   lis += "		<a href='javascript:void(0);' style='cursor:default;'>-----------------------</a>";
				   lis += "	</div>";
				   lis += "</li>";
				   $('#notReadMessageCount_ul').html("");
				   $('#notReadMessageCount_ul').append(lis);
			   }
		}
	});
}

//单聊
function sendTalkMessage2(){
	if($('#talkContent_common').val() == "" || $('#talkContent_common').val() == null){
		return ;
	}
	var regEx_script = "<script[^>]*?>[\\s\\S]*?<\\/script>"; 				    	// 定义script的正则表达式
	var regEx_style = "<style[^>]*?>[\\s\\S]*?<\\/style>";					   	 // 定义style的正则表达式
	var regEx_html = "<[^>]+>"; 																	// 定义HTML标签的正则表达式
	var talkContent_common = $('#talkContent_common').val().replace(regEx_script, "")
		.replace(regEx_style, "").replace(regEx_html, "");
	$('#talkContent_common').val(talkContent_common)
	$.ajax({
		   type: "POST",
		   url: "/SX2/chat/sendMessage",
		   data:$('#talkForm_common').serialize(),
		   success: function(data){
			   if(data.status==200){
				   var talk_each = "";
				   talk_each += "<div class='talk_each_box'>";
				   talk_each += "	<div class='sanjiao_right pull-right'></div>";
				   talk_each += "	<div class='talk_each_right pull-right'>"+ talkContent_common +"</div>";
				   talk_each += "<div class='clearfix'></div>";
				   talk_each += "<div class='talk_each_time pull-right'>" + moment().format("YYYY-MM-DD HH:mm:ss") + "</div>";
				   talk_each += "</div>";
				   $('#talk_content_common').append(talk_each);
				   $('#talkContent_common').val("");
				   $('#talkContent_common').focus();
				   var box = document.getElementById("talk_content_box_common");
				   box.scrollTop = box.scrollHeight;
			   }
		   }
		});
}
//关闭单聊窗口
function close_talk2(getTalkMessage){
	$('.talk_window_common').css("z-index","-1");
	clearInterval(getTalkMessage);
	return false;
}

function getHistoryCommon(){
	$.ajax({
		   type: "post",
		   url: "/SX2/chat/getHistory?chatName=" +$('#chatName_common').val() ,
		   success: function(data){
			   if(data.status == 200){
				   if(data.data == ""){
					   $('#historyDivCommon').html("<small>无历史纪录</small>");
				   }else{
					   $('#historyDivCommon').html("");
					   var userId = data.data.userId;
					   $('#talk_content_common').html("");
					   $.each(data.data.historyList,function(index,msg){
						   if(msg.senderId == userId){
							   var talk_each = "";
							   talk_each += "<div class='talk_each_box'>";
							   talk_each += "	<div class='sanjiao_right pull-right'></div>";
							   talk_each += "	<div class='talk_each_right pull-right'>"+ msg.content +"</div>";
							   talk_each += "<div class='clearfix'></div>";
							   talk_each += "<div class='talk_each_time pull-right'>" + moment(msg.created).format("YYYY-MM-DD HH:mm:ss") + "</div>";
							   talk_each += "</div>";
							   $('#talk_content_common').append(talk_each);
						   }else{
							   var talk_each = "";
							   talk_each += "<div class='talk_each_box'>";
							   talk_each += "	<div class='sanjiao_left'></div>";
							   talk_each += "	<div class='talk_each_left'>"+ msg.content +"</div>";
							   talk_each += "<div class='talk_each_time'>" + moment(msg.created).format("YYYY-MM-DD HH:mm:ss") + "</div>";
							   talk_each += "</div>";
							   $('#talk_content_common').append(talk_each);
						   }
					   });
				   }
			   }
		   }
		});
}