<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <link rel="SHORTCUT ICON" href="<%=path%>/image/index/logo.ico" type="image/x-icon" >
    <base href="<%=basePath%>">
    <title>HCP</title>
	<link rel="stylesheet" type="text/css" href="<%=path %>/css/login_styles.css">
  </head>
  
  <body>
    <div class="htmleaf-container">
	<div class="wrapper">
		<div class="container">
			<h1>Welcome</h1>
			
			<form action="<%=path %>/user/login" method="post" class="form" onsubmit="return check()" >
				<input id="id" name="id" type="text" placeholder="请输入用户ID">
				<input id="password" name="password" type="password" placeholder="请输入密码">
				<button type="submit" id="login-button">Login</button><br>
				<label id="label"><font color="#FF0000">${sessionScope.error }</font></label>
				 <%
				   request.getSession().removeAttribute("error");
				 %>
			</form>
		</div>
		
		<ul class="bg-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
</div>
  </body>
	<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
	<script type="text/javascript">
	/* 	$('#login-button').click(function (event) {
			event.preventDefault();
			$('form').fadeOut(500);
			$('.wrapper').addClass('form-success');
			$('form').submit();
		}); */
		$(document).ready(function() {
		
		});
	function check(){
	  var id = $('#id').val();
	  var password = $('#password').val();
	  if(id==null||id==""){
	    alert("请输入用户ID");
	    return false;
	  }else if(password==null||password==""){
	    alert("请输入密码");
	    return false;
	  }else{
	  return true;
	  }
	}
	</script>
</html>
