<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<link rel="SHORTCUT ICON" href="<%=path%>/image/index/logo.ico" type="image/x-icon" >

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>页面找不到啦</title>
	<link rel="stylesheet" href="<%=path %>/css/bootstrap.css">
	<link rel="stylesheet" href="<%=path %>/css/error.css">
	<meta http-equiv="refresh" content="3;url=<%=path%>/">
</head>
<body>
	<!-- 代码 开始 -->
	<div id="container" style="padding-top:100px">
		<img class="png" src="<%=path %>/image/error/404.png" /> <img class="png msg"
			src="<%=path %>/image/error/404_msg.png" />
		<p>
			<a href="<%=path%>"><img class="png" src="<%=path %>/image/error/404_to_index.png" /></a>
		</p>
	</div>
	<div id="cloud" class="png"></div>
	<!-- 代码 结束 -->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<%=path %>/js/bootstrap.js"></script>
</body>
</html>