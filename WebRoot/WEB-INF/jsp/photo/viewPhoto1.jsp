<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>HCP-图片预览</title>
    <link rel="shortcut icon" href="<%=path%>/image/index/logo.ico">
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all">
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css"
	media="all" />
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
  </head>
  
  <body>
  <nav class="navbar navbar-default" role="navigation" style="height:60px">
   <div class="navbar-header">
      <a class="navbar-brand" href="#"><img id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" /></a>
   </div>
</nav>
    <div class="panel panel-default">
					   <div class="panel-heading">
					      <h3  class="panel-title">
					         图片名称：<label id="title"></label>
					      </h3>
					   </div>
					   <div class="panel-body">
					   	  <div style="width:900px; height:700px; border: 1px solid #sss; overflow: hidden;">
					          <img id="myImg" src="<%=path%>/viewPhoto${path}/" style="max-width:1000px; position:relative; top: 5px; left: 5px;">
					      </div>
					      <button onclick="function left(){alert('000');$('#myImg').css('transform','rotate(90deg)');}">left</button>
					   </div>
					</div>
  </body>
  <script src="<%=path%>/js/bootstrap.js"></script>
  <script type="text/javascript">
  	    $(document).ready(function() {
        var path = '${path}';
        var fileName = spitString(path);
        $("#title").html(fileName);
        $('#myImg').attr("alt",fileName);
   	 });
   	 	function spitString(fileName){
		  var index = fileName.indexOf("_");
		  return fileName.substring(index+1);
		}
  </script>
</html>
