<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML>
<html>
<head>
<link rel="SHORTCUT ICON" href="<%=path%>/image/index/logo.ico" type="image/x-icon" >
<title>HCP-审核员</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet"	type="text/css" media="all">
<link href="<%=path%>/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
<!-- Custom Theme files -->
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--js-->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="<%=path%>/css/font-awesome.css" rel="stylesheet">

<!--skycons-icons-->
<script src="<%=path%>/js/skycons.js"></script>
<!--//skycons-icons-->
</head>
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main" style="z-index:999;">
					<div class="header-left">
						<div class="logo-name">
							<a href="<%=path%>"> <!-- <h1>KPI Of College</h1> -->
								<img id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" />
							</a>
						</div>

					</div>
					<div class="header-right" >
						<div class="profile_details_left">
							<div class="clearfix"></div>
						</div>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img">
												</span>
												<div class="user-name">
												  <p>${sessionScope.user.name }</p>
												  <span>${sessionScope.user.type }</span>
												</div>
											<i class="fa fa-angle-down lnr"></i> <i
												class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
								</a>
									<ul class="dropdown-menu drp-mnu">
										<li><a href="<%=path%>/user/quit"><i class="fa fa-sign-out"></i> 退出</a></li>
									</ul></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script>
					$(document).ready(function() {
						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});

					});
				</script>
				<!-- /script-for sticky-nav -->
				<!--inner block start here-->
				<div class="inner-block" style="min-height: 570px; overflow:auto;">
				     	<div class="panel panel-info" >
							<div class="panel-heading">
								<h3 class="panel-title">微信群列表</h3>
							</div>
							<div class="panel-body">
						   	  <div class="col-md-4 col-sm-4 col-xs-4"  >
						         <input type="text" id="groupName" class="form-control" required="required" placeholder="请输入微信群名">
						      </div>
						  	 <button type="button" class="btn btn-primary" onclick="chaxun()">查询</button>
							    <hr><div id="loading" align="center" ></div>
								<div id="contentDIV">
				                   <div id="tishi" class="alert alert-danger" hidden="hidden" style="clear: both;">空空如也...</div>
									 <div id="table">
										 <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
										        <thead>
										            <tr>
										                <th>惠氏ID</th>
										                <th>群名</th>
										                <th>城市</th>
										                <th>关联医院</th>
										                <th>群属性</th>
										                <th>群人数</th>
										                <th class="oper">操作</th> 
										            </tr>
										        </thead>
										        <tbody id="tbody">
													
										        </tbody>
										    </table>
									 </div> 
								 </div>
							</div>
						</div>
						</div>
				</div>
				<!--inner block end here-->
				<!--copy rights start here-->
				<div class="copyrights">
					<jsp:include page="/menu/foot.jsp"></jsp:include>
				</div>
				<!--COPY rights end here-->
		</div>
		<!--slider menu-->
		<div class="sidebar-menu">
			<div class="logo">
				<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span>
				</a> <a href="#"> <span id="logo"></span> <!--<img id="logo" src="" alt="Logo"/>-->
				</a>
			</div>
			<div class="menu">
			<ul id="menu">
				<li id="menu-home"><a href="<%=path%>/admin"> <i
						class="fa fa-home"></i> <span>审核员</span></a></li>
				<li ><a><i class="fa fa-user-md"></i><span>HCP管理</span><span
							class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hcp/admin/verify">HCP审核</a></li>
						<li><a href="<%=path%>/hcp/admin/view">HCP列表</a></li>
					</ul></li>
				<li ><a href="<%=path%>/activity/admin/view">
					<i class="fa fa-leaf"></i><span>活动管理</span></a></li>
				<li><a><i class="fa fa-hospital-o"></i><span>医院管理</span> <span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hospital/view">查看列表</a></li>
						<li><a href="<%=path%>/hospital/add">添加医院</a></li>
					</ul></li>
				<li ><a href="<%=path%>/contract/admin/view"><i class="fa  fa-file-text"></i><span>合同管理</span> </a></li>
				<li><a><i class="fa fa-jpy"></i><span>支付管理</span> <span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<%-- <li><a href="<%=path%>/payment/admin/view/5">付款管理</a></li> --%>
						<li><a href="<%=path%>/payment/admin/view/0">待支付</a></li>
						<li><a href="<%=path%>/payment/admin/view/1">已支付</a></li>
						<li><a href="<%=path%>/payment/admin/view/2">支付失败</a></li>
					</ul></li>
				<li class="active"><a href="<%=path%>/group/admin/view"><i class="fa  fa-wechat"></i><span>微信群管理</span> </a></li>
			</ul>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<!--slide bar menu end here-->
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="height: 530px; overflow:auto;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">医院详细信息</h4>
				</div>
				<div class="modal-body">
					<div class="panel-body">
						<table class="table">
							<tbody>
								<tr>
									<td>
										<div class="col-md-5 col-sm-5 col-xs-5">
											群编号：<label id="wyethId"></label>
										</div>
										<div class="col-md-7 col-sm-7 col-xs-7">
											群名称：<label id="groupName1"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-5 col-sm-5 col-xs-5">
											医院ID：<label id="hsptId"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											名称：<label id="hsptName"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-4 col-sm-4 col-xs-4">
											大区：<label id="areaName"></label>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-4">
											省份：<label id="province"></label>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-4">
											城市：<label id="cityName"></label>
										</div>
										</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-4 col-sm-4 col-xs-4">
											团队：<label id="team"></label>
										</div>
										<div class="col-md-7 col-sm-7 col-xs-7">
											创建时间：<label id="ctime"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											anchorCity：<label id="anchorCity"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											clusterCity：<label id="clusterCity"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-3 col-sm-3 col-xs-3">
											班长：<label id="squadNumber"></label>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3">
											小助手：<label id="helperNumber"></label>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3">
											机器人：<label id="botNumber"></label>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-3">
											KOM：<label id="KOM"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											新增用户+白名单人数：<label id="awn"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											退群用户+白名单人数：<label id="ewn"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											当前用户+白名单人数：<label id="nwn"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											总计群成员数：<label id="tn"></label>
										</div>
									</td>
								</tr>
								<tr>
								   <td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											备注：<label id="remark"></label>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	
	<script>
		var toggle = true;

		$(".sidebar-icon").click(
				function() {
					if (toggle) {
						$(".page-container").addClass("sidebar-collapsed")
								.removeClass("sidebar-collapsed-back");
						$("#menu span").css({
							"position" : "absolute"
						});
					} else {
						$(".page-container").removeClass("sidebar-collapsed")
								.addClass("sidebar-collapsed-back");
						setTimeout(function() {
							$("#menu span").css({
								"position" : "relative"
							});
						}, 400);
					}
					toggle = !toggle;
				});
	</script>
	<!--scrolling js-->
	<script src="<%=path%>/js/jquery.nicescroll.js"></script>
	<script src="<%=path%>/js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="<%=path%>/js/bootstrap.js"></script>
	<script src="<%=path%>/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<%=path%>/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    var table = null;
	$(document).ready(function() {
		table = $('#example').DataTable();
    	$('#loading').hide();
		$('#contentDIV').hide();
    });
    $.ajaxSetup({
 contentType: "application/x-www-form-urlencoded; charset=utf-8"
});
       function chaxun(){
         var groupName = $("#groupName").val();
         /* if(groupName==null||$.trim(groupName)==""){
           alert("请输入微信群名进行查询！");
           return false;
         }  */
		   $.ajax({  //绑定数据
			   type: "POST",
			   url: "<%=path%>/group/admin/list",
			   data: "groupName="+groupName,
			   dataType:"json",  
			   beforeSend:function(XMLHttpRequest){ 
				   $('#loading').show();
				   $('#contentDIV').hide();
		           $("#loading").html("<img src='<%=path%>/image/loading.gif' />"); 
		       },
		       error:function(e) { 
					alert("查询出错，请重试！"); 
					$('#loading').hide();
				},
			   success: function(data){
				   $('#contentDIV').show();
				   $('#loading').hide();
				   if(data==""){
					   $('#tishi').show();
					   $('#table').hide();
				   }else{
					   $('#table').show();
					   $('#tishi').hide();
				   }
				   table.destroy(false);
				   $('#tbody').html("");
				   $(data.group).each(function(index,item){
					   var row ="<tr>";
					   row +="<td>"+item.wyethId +"</td>";
					   row +="<td>"+item.groupName +"</td>";
					   row +="<td>"+item.city +"</td>";
					   row +="<td>"+item.hsptName +"</td>";
					   row +="<td>"+item.groupType +"</td>";
					   row +="<td>"+item.totalNumber +"</td>";
					   row += "<td><button class=\"btn btn-primary btn-sm\" onclick=\"view('" + item.id + "')\">详情</button></td>";
						row +="</tr>";										
						 $('#tbody').append(row);
					});
				   table=$('#example').DataTable();
			   }
			});
	   };
	function view(id) {
		   $.ajax({
				   type: "POST",
				   url: "<%=path%>/group/admin/show/",
				   data:"id="+id,
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				     $("#wyethId").html(data.group.wyethId);
				     $("#groupName1").html(data.group.groupName);
				     $("#areaName").html(data.group.areaName);
				     $("#province").html(data.group.province);
				     $("#cityName").html(data.group.city);
				     $("#team").html(data.group.department);
				     $("#hsptId").html(data.group.hsptId);
				     $("#hsptName").html(data.group.hsptName);
				     $("#anchorCity").html(data.group.anchorcity);
				     $("#clusterCity").html(data.group.clustercity);
				     $("#ctime").html(FormatDate(data.group.ctime,1));
				     $("#squadNumber").html(data.group.squadNumber);
				     $("#helperNumber").html(data.group.helperNumber);
				     $("#botNumber").html(data.group.helperNumber);
				     $("#KOM").html(data.group.komNumber);
				     $("#awn").html(data.group.addWhitelistNumber);
				     $("#ewn").html(data.group.exitWhitelistNumber);
				     $("#nwn").html(data.group.nowWhitelistNumber);
				     $("#tn").html(data.group.totalNumber);
				     $("#remark").html(data.group.remark);
				     $("#myModal").modal('show');
				   }
				 }); 
		};
    </script>
	<!-- mother grid end here-->
</body>
</html>
