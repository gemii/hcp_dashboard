<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<head>
<link rel="shortcut icon" href="<%=path%>/image/index/logo.ico">
<title>HCP-审核员</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all">
<!-- Custom Theme files -->
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link type="text/css" rel="stylesheet" href="<%=path %>/js/jedate/skin/jedate.css">
<link href="<%=path%>/css/dataTables.bootstrap.min.css" type="text/css"	rel="stylesheet">
<!--js-->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="<%=path%>/css/font-awesome.css" rel="stylesheet">

<!--skycons-icons-->
<script src="<%=path%>/js/skycons.js"></script>
<!--//skycons-icons-->

</head>
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main">
					<div class="header-left">
						<div class="logo-name">
							<a href="<%=path%>"> <!-- <h1>KPI Of College</h1> -->
								<img id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" />
							</a>
						</div>

					</div>
					<div class="header-right">
						<div class="profile_details_left">
							<!--notifications of menu start -->
							<div class="clearfix"></div>
						</div>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img">
												</span>
												<div class="user-name">
												  <p>${sessionScope.user.name }</p>
												  <span>${sessionScope.user.type }</span>
												</div>
											<i class="fa fa-angle-down lnr"></i> <i
												class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
								</a>
									<ul class="dropdown-menu drp-mnu">
										<li><a href="<%=path%>/user/quit"><i class="fa fa-sign-out"></i> 退出</a></li>
									</ul></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script>
					$(document).ready(function() {
						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});

					});
				</script>
				<!-- /script-for sticky-nav -->
				<!--inner block start here-->
				<div class="inner-block" style="min-height:560px">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 id="div_h3" class="panel-title">${div_h3 }</h3>
						</div>
						<div class="panel-body">
								<div class="row clearfix">
									<div class="col-md-3 col-sm-3 col-xs-3">
										 <div  class="input-group " style="height:30px;">
								              <span class="hcpClass input-group-addon">公司名</span>
								                <select  id="tp_name" class="hcpClass form-control" > 
											    </select>
										   </div>
									</div>
								<div class=" col-md-2 col-sm-2 col-xs-2">
									<input class="form-control datainp" id="dateinfo1" type="text" 
									placeholder="开始时间" readonly name="startTime"  >
								</div>
								 <div class=" col-md-2 col-sm-2 col-xs-2">
									<input class=" form-control datainp" id="dateinfo2" type="text" 
									placeholder="结束时间" readonly name="endTime"  >
								</div>
						  	 	<button type="button" class="btn btn-primary" onclick="view()">查询</button>
						  	 	<label id="totalCount"></label>
							    <hr>
								<c:if test="${sessionScope.state==5}">
									<div class="col-md-4 column">
									付款状态：
										 <label class="checkbox-inline">
										      <input type="radio" name="payState" 
										         value="1"  onClick="pay1()" checked="checked">待付款
										   </label>
										<label class="checkbox-inline">
									      <input type="radio" name="payState" 
									         value="2"  onClick="pay2()">已付款
									   </label>
									</div>
									</c:if>
								</div>
						   	<div id="table">
									<table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
										<thead>
											<tr>
											<c:if test="${sessionScope.state==5}">
												<th class="th1"><input  type="checkbox" onclick="AllCheck_click(this)"/></th>
											</c:if>
												<th>活动ID</th>
												<th>医生</th>
												<th>业务员</th>
												<th>活动主题</th>
												<th>金额</th>
												<th>活动审核时间</th>
												<th>操作</th>
											</tr>
										</thead>
										<tbody id="tbody">
		
										</tbody>
									<c:if test="${sessionScope.state==5}">
										<tfoot class="th1">
											<tr>
												<td colspan="8">
													<button type="button" class="btn btn-success" onclick="button_click()">确认已付款</button>
												</td>
											</tr>
										</tfoot>
									</c:if>
									</table>
							</div>
						</div>
					</div>
				 </div>
				<!--inner block end here-->
				
			</div>
			<!--copy rights start here-->
				<div class="copyrights">
					<jsp:include page="/menu/foot.jsp"></jsp:include>
				</div>
				<!--COPY rights end here-->
		</div>
		<!--slider menu-->
		<div class="sidebar-menu">
			<div class="logo">
				<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span>
				</a> <a href="#"> <span id="logo"></span> <!--<img id="logo" src="" alt="Logo"/>-->
				</a>
			</div>
			<div class="menu">
			<ul id="menu">
				<li id="menu-home"><a href="<%=path%>/admin"> <i
						class="fa fa-home"></i> <span>审核员</span></a></li>
				<li ><a><i class="fa fa-user-md"></i><span>HCP管理</span><span
							class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hcp/admin/verify">HCP审核</a></li>
						<li><a href="<%=path%>/hcp/admin/view">HCP列表</a></li>
					</ul></li>
				<li ><a href="<%=path%>/activity/admin/view">
					<i class="fa fa-leaf"></i><span>活动管理</span></a></li>
				<li><a><i class="fa fa-hospital-o"></i><span>医院管理</span> <span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hospital/view">查看列表</a></li>
						<li><a href="<%=path%>/hospital/add">添加医院</a></li>
					</ul></li>
				<li ><a href="<%=path%>/contract/admin/view"><i class="fa  fa-file-text"></i><span>合同管理</span> </a></li>
				<li class="active"><a><i class="fa fa-jpy"></i><span>支付管理</span> <span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<%-- <li><a href="<%=path%>/payment/admin/view/5">付款管理</a></li> --%>
						<li><a href="<%=path%>/payment/admin/view/0">待支付</a></li>
						<li><a href="<%=path%>/payment/admin/view/1">已支付</a></li>
						<li><a href="<%=path%>/payment/admin/view/2">支付失败</a></li>
					</ul></li>
				<li ><a href="<%=path%>/group/admin/view"><i class="fa  fa-wechat"></i><span>微信群管理</span> </a></li>
			</ul>
			</div>
		</div>
		<div class="clearfix"></div>
		</div>
	</div>
	<!--slide bar menu end here-->
	
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="height: 570px; overflow:auto;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">活动支付信息</h4>
				</div>
				<div class="modal-body">
					<div class="panel-body">
					<form action="<%=path%>/payment/third/save" method="post">
					<table class="table">
						<tbody>
							<tr>
								<td>
									<div class="col-md-12 col-sm-12 col-xs-12">
										活动ID：<label id="id"></label>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="col-md-6 col-sm-6 col-xs-6">
										医生：<label id="hcpName"></label>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										业务员：<label id="staffName"></label>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="col-md-12 col-sm-12 col-xs-12">
										活动主题：<label id="actSubject"></label>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="col-md-4 col-sm-4 col-xs-4">
										金额：<label id="payment"></label>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-8">
										开户行：<label id="bank"></label>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="col-md-12 col-sm-12 col-xs-12">
										城市：<label id="cityId"></label>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="col-md-4 col-sm-4 col-xs-4">
										户名：<label id="bankCardName"></label>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-8">
										银行卡号：<label id="bankCardNumber"></label>
									</div>
								</td>
							</tr>
							<tr class="pay_tr1">
							   <td>
							   		<div class="col-md-8 col-sm-8 col-xs-8">
										支付ID：<label id="payID"></label>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4">
										状态：<label id="pay_state"></label>
									</div>
							   </td>
							 </tr>
								 <tr id="tr_payFile" >
									<td>
										<div class="panel panel-default col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        支付凭证照片
										      </h3>
										   </div>
										   <div id="fileId2"  class="panel-body">
										   </div>
										</div>
									</td>
								</tr>
								 <tr class="pay_tr3">
								   <td>
								   备注：<label id="pay_remark"></label>
								   </td>
								 </tr>
							</tbody>
						 </table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	</div>
	
	<script>
		var toggle = true;

		$(".sidebar-icon").click(
				function() {
					if (toggle) {
						$(".page-container").addClass("sidebar-collapsed")
								.removeClass("sidebar-collapsed-back");
						$("#menu span").css({
							"position" : "absolute"
						});
					} else {
						$(".page-container").removeClass("sidebar-collapsed")
								.addClass("sidebar-collapsed-back");
						setTimeout(function() {
							$("#menu span").css({
								"position" : "relative"
							});
						}, 400);
					}
					toggle = !toggle;
				});
	</script>
<!--scrolling js-->
	<script src="<%=path%>/js/jquery.nicescroll.js"></script>
	<script src="<%=path%>/js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="<%=path%>/js/bootstrap.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jedate/jedate.js"></script>
	<script src="<%=path%>/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="<%=path%>/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		 jeDate({
			dateCell:"#dateinfo1",
			format:"YYYY-MM-DD",
			isinitVal:false,
			isTime:true //isClear:false,
		});
	  jeDate({
			dateCell:"#dateinfo2",
			format:"YYYY-MM-DD",
			isinitVal:false,
			isTime:true //isClear:false,
		});
	var pay_to_third = 0,allState = ${state};
	var table = null;
	$(document).ready(function() {
	table = $('#example').DataTable();
	  $.ajax({
			   type: "POST",
			   url: "<%=path%>/user/getUser/",
			   dataType:"json",
			   error : function(e) {
						alert("数据加载失败，请刷新重试！");
					},
			   success: function(data){
			     	$(data).each(
							function(index,item) {
							  $("#tp_name").append("<option value='" + item.id + "'>"+item.name+"</option>");
							}
						);
					/*  view(); */
				   	}
			 }); 
		});
		function view(){
			var id = $("#tp_name").val();
			if(allState==5){
				search1(id,pay_to_third);
			}else{
				search(id);
			}
		};
		function AllCheck_click(obj){
			$(".Check").prop('checked', $(obj).prop('checked'));
		};
		function button_click(){
		var pay = parseFloat(0.0);
		 var ids = $(".Check:checked").map(
		 	function(){
		 		pay += parseFloat($(this).attr("xyz"));
		 		return $(this).attr("abc");
		 	}).get().join(',')
		 if(ids==null||ids==""){
		   alert("请选择要支付的活动！");
		 }else{
		 	 but_check(ids,pay);
		 }
		};
		function but_check(ids,pay){
			if(confirm("确认已经付款？")){
				$.ajax({
					   type: "POST",
					   url: "<%=path%>/payment/admin/pay/",
					   data:"ids="+ids+"&tp_id="+$("#tp_name").val(),
					   success: function(){
					   	window.location="<%=path%>/payment/admin/view/5";
					   }
				 });
			}else{
				return false;
			}
		};
		function search(id){
			  var state = ${state};
			  var date1 = $('#dateinfo1').val();
			  var date2 = $('#dateinfo2').val();
			  if(date1!=""&&date2==""){
			  	alert("开始日期必须选择！");
			  	return false;
			  }else if(date2!=""&&date1==""){
			  	alert("结束日期必须选择！");
			  	return false;
			  }else if(date1!=""&&date2!=""&&date1>=date2){
				  alert("开始日期晚于结束日期！");
				  return false;
			  }
			  $.ajax({
				   type: "POST",
				   url: "<%=path%>/payment/admin/getList/"+id,
				   data:"startTime="+date1+"&endTime="+date2,
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				   		$("#totalCount").html("总金额:"+data.totalCount);
				   		 table.destroy(false);
				         $('#tbody').html("");
				 		 $(data.payment).each(
								function(index,item) {
									var row = "<tr>";
								if(state==5){
									if(allState==0)
									   row += "<td><input class=\"Check\" type=\"checkbox\" abc=\""+item.activityId+"\" xyz=\""+item.feedPaid+"\"/></td>";
								}
									row += "<td>" + item.activityId + "</td>";
									row += "<td>" + item.hcpName + "</td>";
									row += "<td>" + item.staffName + "</td>";
									row +="<td>"+item.actSubject+"</td>"; 
									row += "<td>" + item.feedPaid + "</td>";
									row += "<td>" + FormatDate(item.verfiryTime,1) + "</td>";
									row += "<td><button class=\"btn btn-primary btn-sm\" onclick=\"detail('" + item.activityId + "')\">详情</button></td>";
									row += "</tr>";

									$('#tbody').append(row);
								});
						 	table=$('#example').DataTable();
						}
				 }); 
		};
		function pay1(){
			var id = $("#tp_name").val();
			pay_to_third = 0;
			search1(id,0);
			$(".th1").show();
		};
		function pay2(){
			var id = $("#tp_name").val();
			pay_to_third = 1;
			search1(id,1);
			$(".th1").hide();
		};
		function search1(id,state){
			  $.ajax({
				   type: "POST",
				   url: "<%=path%>/payment/admin/getList/"+id+"/"+state,
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				   		 table.destroy(false);
				         $('#tbody').html("");
				 		 $(data.payment).each(
								function(index,item) {
									var row = "<tr>";
									if(pay_to_third==0){
										row += "<td><input class=\"Check\" type=\"checkbox\" abc=\""+item.activityId+"\" xyz=\""+item.feedPaid+"\"/></td>";
									}
									row += "<td>" + item.activityId + "</td>";
									row += "<td>" + item.hcpName + "</td>";
									row += "<td>" + item.staffName + "</td>";
									row +="<td><a title='"+item.actSubject+"' style='color:#333;'>"; 
									   if(item.actSubject.length>8){
										   row +=item.actSubject.substring(0,8) + "...";
									   }else{
										   row += item.actSubject;
									   } 
									row +="</a></td>";
									row += "<td>" + item.feedPaid + "</td>";
									var str = "";
									if(item.payState==0){
									  str = "待支付";
									}else if(item.payState==1){
									  str = "支付成功";
									}else if(item.payState==2){
									  str = "支付失败";
									}
									row += "<td>" + str + "</td>";
									if(pay_to_third==0){
									$("#oper").html("操作");
									  row += "<td><button class=\"btn btn-primary btn-sm\" onclick=\"but_check('" + item.activityId + "','"+item.feedPaid+"')\">确认已付款</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
									 }else{
									    	$("#oper").html("付款时间");
									    	row += "<td>"+FormatDate(item.payToThirdTime,1)+"</td>";
									    }
									row += "</tr>";

									$('#tbody').append(row);
								});
						 	table=$('#example').DataTable();
						}
				 }); 
		};
		function detail(id){
			 $.ajax({
				   type: "POST",
				   url: "<%=path%>/payment/admin/getList/",
				   data:"activityId="+id,
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				 		 $(data.payment).each(
								function(index,item) {
								if(index==0){
								 	$("#id").html(item.activityId);
									$("#hcpName").html(item.hcpName);
									$("#staffName").html(item.staffName);
									$("#actSubject").html(item.actSubject); 
									$("#payment").html(item.feedPaid); 
									$("#bank").html(item.bank); 
									$("#bankCardName").html(item.bankCardName); 
									$("#bankCardNumber").html(item.bankCardNumber); 
									$("#payID").html(item.paymentId); 
									$("#cityId").html(item.cityId); 
									$(".pay_tr1").show();
									if(item.payState==0){
										$(".pay_tr1").hide();
										$(".pay_tr3").hide();
										$("#tr_payFile").hide();
									}else if(item.payState==1){
									   $(".pay_tr3").hide();
										$("#tr_payFile").show();
										$("#pay_state").html("支付成功"); 
										$("#fileId2").html("");
										$(data.file).each(function(index,item){
									         var row = "";
									         if(index!=0)
									            row += "<hr>";
									        row += "<a href=\"<%=path%>/view/photo"+item.path+"/\" target='_blank' title='"+spitStringTitle(item.fileName)+"'>"+spitString(item.fileName)+"</a>";
									        $("#fileId2").append(row);
									     });
									     if($("#fileId2").html()==""){
									     	$("#fileId2").html("未上传付款凭证");
									     }
									}else if(item.payState==2){
									   $("#pay_state").html("支付失败"); 
									   $("#tr_payFile").hide();
									   $("#pay_remark").html(item.payRemark); 
									   $(".pay_tr3").show();
									}
								}
							}); 
							$("#myModal").modal('show');
						}
				 }); 	
		}
	</script>
	<!-- mother grid end here-->
</body>
</html>
