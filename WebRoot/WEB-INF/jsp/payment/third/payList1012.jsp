<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML>
<html>
<head>
<link rel="shortcut icon" href="<%=path%>/image/index/logo.ico">
<title>HCP-第三方</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href="<%=path %>/js/zyupload/skins/zyupload-1.0.0.min.css" type="text/css">
<!-- Custom Theme files -->
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="<%=path%>/css/dataTables.bootstrap.min.css" type="text/css"	rel="stylesheet">
<!--icons-css-->
<link href="<%=path%>/css/font-awesome.css" rel="stylesheet">
<!--js-->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<!--skycons-icons-->
<script src="<%=path%>/js/skycons.js"></script>

</head>
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main">
					<div class="header-left">
						<div class="logo-name">
							<a href="<%=path%>"> <!-- <h1>KPI Of College</h1> -->
								<img id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" />
							</a>
						</div>

					</div>
					<div class="header-right">
						<div class="profile_details_left">
							<!--notifications of menu start -->
							<div class="clearfix"></div>
						</div>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img"> </span>
												<div class="user-name">
												  <p>${sessionScope.user.name }</p>
												  <span>${sessionScope.user.type }</span>
												</div>
											<i class="fa fa-angle-down lnr"></i> 
											<i class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
									</a>
									<ul class="dropdown-menu drp-mnu">
										<li><a href="<%=path%>/user/quit"><i class="fa fa-sign-out"></i> 退出</a></li>
									</ul>
								</li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script>
					$(document).ready(function() {
						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});
					});
				</script>
				<!-- /script-for sticky-nav -->
				<!--inner block start here-->
				<div class="inner-block" style="min-height:560px">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">${div_h3 }</h3>
						</div>
						<div class="panel-body">
							<table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>活动ID</th>
										<th>医生</th>
										<th>业务员</th>
										<th>活动主题</th>
										<th>金额</th>
										<th class="oper">操作</th>
									</tr>
								</thead>
								<tbody id="tbody">

								</tbody>
							</table>
						</div>
					</div>
				 </div>
			</div>
				<!--inner block end here-->
				<!--copy rights start here-->
				<div class="copyrights">
					<jsp:include page="/menu/foot.jsp"></jsp:include>
				</div>
				<!--COPY rights end here-->
		</div>
		<!--slider menu-->
		<div class="sidebar-menu">
			<div class="logo">
				<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span></a> 
				<a href="#"> <span id="logo"></span> <!--<img id="logo" src="" alt="Logo"/>--></a>
			</div>
			<div class="menu">
				<ul id="menu">
					<li id="menu-home"><a href="#"> <i class="fa fa-home"></i> <span>第三方</span></a></li>
					<li ><a href="<%=path%>/contract/third/view"><i class="fa  fa-file-text"></i><span>合同管理</span> </a></li>
					<li class="active"><a><i class="fa fa-jpy"></i><span>支付管理</span> 
						<span class="fa fa-angle-right" style="float: right"></span></a>
						<ul>
							<li><a href="<%=path%>/payment/third/view/0">待支付</a></li>
							<li><a href="<%=path%>/payment/third/view/1">支付成功</a></li>
							<li><a href="<%=path%>/payment/third/view/2">支付失败</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div class="clearfix"></div>
		</div>
	</div>
	
	<!--slide bar menu end here-->
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="height: 570px; overflow:auto;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">活动支付信息</h4>
				</div>
				<div class="modal-body">
					<div class="panel-body">
					<form action="<%=path%>/payment/third/save" method="post" onSubmit="return _check()">
					<table class="table">
						<tbody>
							<tr>
								<td>
									<div class="col-md-12 col-sm-12 col-xs-12">
										活动ID：<label id="id"></label><input id="aId" name="id" type="hidden">
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="col-md-6 col-sm-6 col-xs-6">
										医生：<label id="hcpName"></label>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										业务员：<label id="staffName"></label>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="col-md-12 col-sm-12 col-xs-12">
										活动主题：<label id="actSubject"></label>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="col-md-4 col-sm-4 col-xs-4">
										金额：<label id="payment"></label>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-8">
										开户行：<label id="bank"></label>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="col-md-4 col-sm-4 col-xs-4">
										户名：<label id="bankCardName"></label>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-8">
										银行卡号：<label id="bankCardNumber"></label>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="col-md-12 col-sm-12 col-xs-12">
										城市：<label id="cityId"></label>
									</div>
								</td>
							</tr>
							<tr class="pay_tr1">
								<td>
									<div class="col-md-3 col-sm-3 col-xs-3">
										支付情况：
									</div>
									<div class="col-md-9 col-sm-9 col-xs-9">
									   <label class="checkbox-inline">
										      <input type="radio" name="payState" 
										         value="1"  onClick="pass()" checked="checked">&nbsp;成功
										   </label>
										<label class="checkbox-inline">
									      <input type="radio" name="payState" 
									         value="2"  onClick="nopass()">&nbsp;失败
									   </label>
									</div>
								</td>
							</tr>
							<tr id="tr_file">
								   <td>
								   		 <div class="input-group col-md-12 col-sm-12 col-xs-12">
									         <button type="button" class="btn btn-success"
													data-toggle="modal" data-target=".bs-example-modal-lg">
													上传付款凭证</button><br>
											   <label id="label_filepath"></label>
											   <input id="fileId" name="payFile" type="hidden"/>
									      </div>
								   </td>
							</tr>
							<tr class="pay_tr pay_tr1">
								<td>
									<div class="input-group col-md-12 col-sm-12 col-xs-12">
								         <span id="span_remark" class="input-group-addon">备注</span>
								         <textarea id="textarea_remark" name="payRemark" style="width:300px;height:100px;" class="form-control" ></textarea>
								      </div>
								</td>
							</tr>
							
							<tr class="pay_tr1">
								<td>
									<div class="col-md-3 col-sm-3 col-xs-3">
								        <button type="submit" class="btn btn-primary" >提交</button>
								     </div>
								  </td>
								</tr>
								<tr class="pay_tr2">
								   <td>
								   	支付情况：<label id="pay_state"></label>
								   </td>
								 </tr>
								 <tr class="tr_payFile">
									<td>
										<div class="panel panel-default col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        支付凭证照片
										      </h3>
										   </div>
										   <div id="fileId2"  class="panel-body">
										   </div>
										</div>
									</td>
								</tr>
								 <tr class="pay_tr3">
								   <td>
								   备注：<label id="pay_remark"></label>
								   </td>
								 </tr>
							</tbody>
						 </table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	</div>
	
	<!-- 添加附件模态框 -->
	<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
		aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">添加附件
						<font size="2.5" color="blue">&nbsp;&nbsp;附件仅支持jpg,png,jpeg,gif类型的图片</font>
					</h4>
					
				</div>
				<div class="modal-body">
					<div id="zyupload" class="zyupload">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"
						onclick="save()">确定</button>
				</div>
			</div>
		</div>
	</div>
	
	<script>
		var toggle = true;

		$(".sidebar-icon").click(
				function() {
					if (toggle) {
						$(".page-container").addClass("sidebar-collapsed")
								.removeClass("sidebar-collapsed-back");
						$("#menu span").css({
							"position" : "absolute"
						});
					} else {
						$(".page-container").removeClass("sidebar-collapsed")
								.addClass("sidebar-collapsed-back");
						setTimeout(function() {
							$("#menu span").css({
								"position" : "relative"
							});
						}, 400);
					}
					toggle = !toggle;
				});
	</script>
<!--scrolling js-->
	<script src="<%=path%>/js/jquery.nicescroll.js"></script>
	<script src="<%=path%>/js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="<%=path%>/js/bootstrap.js"></script>
	<script src="<%=path%>/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="<%=path%>/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script type="text/javascript" 	src="<%=path %>/js/zyupload/zyupload-1.0.0.min.js"></script>
	<script type="text/javascript">
	var filePath = "",allPath="";
	$(document).ready(function() {
		   $.ajax({
				   type: "POST",
				   url: "<%=path%>/payment/third/getList/",
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				 		 $(data.payment).each(
								function(index,item) {
									var row = "<tr><td>"+item.activityId+"</td>";
									row += "<td>" + item.hcpName + "</td>";
									row += "<td>" + item.staffName + "</td>";
									row +="<td>"+item.actSubject+"</td>"; 
									row += "<td>" + item.feedPaid + "</td>";
									row += "<td><button class=\"btn btn-primary btn-sm\" onclick=\"view('" + item.activityId + "')\">操作</button>&nbsp;";
									row += "</td></tr>";

									$('#tbody').append(row);
								});
							$('#example').dataTable({
								"aaSorting": [[ 0, "desc" ]]
							}); 
						}
				 }); 
				 
			// 初始化插件
			$("#zyupload").zyUpload({
				width            : "650px",           // 宽度
				height           : "160px",           // 宽度
				itemWidth        : "80px",            // 文件项的宽度
				itemHeight       : "65px",            // 文件项的高度
				url              : "<%=path%>/uploadFile", // 上传文件的路径
				fileType         : [ "jpg", "png","jpeg","gif","JPG","PNG"], // 上传文件的类型
				fileSize         : 51200000,          // 上传文件的大小
				multiple         : true,            // 是否可以多个文件上传
				dragDrop         : false,            // 是否可以拖动上传文件
				tailor           : false,            // 是否可以裁剪图片
				del              : false,             // 是否可以删除文件
				finishDel        : false,            // 是否在上传文件完成后删除预览
				/* 外部获得的回调接口 */
				onSelect : function(selectFiles, allFiles) { // 选择文件的回调方法  selectFile:当前选中的文件  allFiles:还没上传的全部文件
				},
				onDelete : function(file, files) { // 删除一个文件的回调方法 file:当前删除的文件  files:删除之后的文件
				},
				onSuccess : function(file, response) {
					 fileName = file.name;// 文件上传成功的回调方法
					if (file.name.indexOf("+") >= 0) {
						fileName = file.name.replace("+", "_");
					}
					if (filePath == "") {
					    filePath = fileName;
					    allPath = response;
						$('#label_filepath').html(filePath);
					} else {
						filePath += "<br>" + fileName;
						allPath += "||" + response;
						$('#label_filepath').html(filePath);
					}
					
				},
				onFailure : function(file, response) { // 文件上传失败的回调方法
				},
				onComplete : function(response) { // 上传完成的回调方法
				}
			});
		});
		function view(id){
			   $.ajax({
				   type: "POST",
				   url: "<%=path%>/payment/third/getList/",
				   data:"activityId="+id,
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				 		 $(data.payment).each(
								function(index,item) {
								if(index==0){
								 	$("#id").html(item.activityId);
								 	$("#aId").val(item.activityId);
									$("#hcpName").html(item.hcpName);
									$("#staffName").html(item.staffName);
									$("#actSubject").html(item.actSubject); 
									$("#payment").html(item.feedPaid); 
									$("#bank").html(item.bank); 
									$("#bankCardName").html(item.bankCardName); 
									$("#bankCardNumber").html(item.bankCardNumber); 
									$("#cityId").html(item.cityId); 
									
									$(".pay_tr").hide();
									if(item.payState==0){
										$(".pay_tr2").hide();
										$(".pay_tr3").hide();
										$(".tr_payFile").hide();
										$(".pay_tr1").show();
										$(".pay_tr").hide();
										$("#tr_file").show();
									}else if(item.payState==1){
									   $(".pay_tr1").hide();
									   $(".pay_tr3").hide();
										$(".pay_tr2").show();
										$(".tr_payFile").show();
										$("#tr_file").hide();
										$("#pay_state").html("支付成功"); 
										$("#fileId2").html("");
										$(data.file).each(function(index,item){
									         var row = "";
									         if(index!=0)
									            row += "<hr>";
									        row += "<a href=\"<%=path%>/view/photo"+item.path+"/\" target='_blank' title='"+spitStringTitle(item.fileName)+"'>"+spitString(item.fileName)+"</a>";
									        $("#fileId2").append(row);
									     });
									     if($("#fileId2").html()==""){
									     	$("#fileId2").html("未上传付款凭证   <button type=\"button\" class=\"btn btn-success\" onclick=\"update_photo()\">点击上传</button>");
									     }
									}else if(item.payState==2){
									   $(".pay_tr1").hide();
									   $(".pay_tr2").show();
									   $("#pay_state").html("支付失败"); 
									   $("#tr_file").hide();
									   $(".tr_payFile").hide();
									   $("#pay_remark").html(item.payRemark); 
									   $(".pay_tr3").show();
									}
								}
							}); 
							$("#myModal").modal('show');
						}
				 }); 	
		};
		function update_photo(){
			$(".pay_tr2").hide();
			$(".pay_tr3").hide();
			$(".tr_payFile").hide();
			$(".pay_tr1").show();
			$(".pay_tr").hide();
			$("#tr_file").show();
		}
		function nopass(){
			$(".pay_tr").show();
			$("#tr_file").hide();
			$("#span_remark").html("原因");
			$("#textarea_remark").attr("required","required");
			$("#textarea_remark").attr("placeholder","必填项");
		}
		function pass(){
		     $(".pay_tr").hide();
		     $("#tr_file").show();
		}
		function save(){
		      $.ajax({  //绑定数据
			   type: "POST",
			   url: "<%=path%>/upload/save",
			   data:"allPath="+allPath,
			   dataType:"text",  
		       error:function(e) { 
					alert("保存文件出错，请重试！"); 
				},
			   success: function(data){
			      $("#fileId").val(data);
			   }
			});
		};
		function _check(){
			if(confirm("确认提交吗？")){
				return true;
			}else{
				return false;
			}
		};
	</script>
	<!-- mother grid end here-->
</body>
</html>
