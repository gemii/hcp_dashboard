<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>
<html>
<head>
<link rel="SHORTCUT ICON" href="<%=path%>/image/index/logo.ico" type="image/x-icon" >
<title>HCP-审核员</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all">
<link href="<%=path%>/css/dataTables.bootstrap.min.css" type="text/css"	rel="stylesheet">
<!-- Custom Theme files -->
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css"
	media="all" />
<!--js-->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="<%=path%>/css/font-awesome.css" rel="stylesheet">

<!--skycons-icons-->
<script src="<%=path%>/js/skycons.js"></script>
<!--//skycons-icons-->
</head>
<body >
	<div class="page-container">
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main" style="z-index:999;">
					<div class="header-left">
						<div class="logo-name">
							<a href="<%=path%>"> <!-- <h1>KPI Of College</h1> --> <img
								id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" />
							</a>
						</div>

					</div>
					<div class="header-right">
						<div class="profile_details_left">
							<div class="clearfix"></div>
						</div>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img"> 
											</span>
											<div class="user-name">
												<p>${sessionScope.user.name }</p>
												<span>${sessionScope.user.type }</span>
											</div>
											<i class="fa fa-angle-down lnr"></i> <i
												class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
								</a>
									<ul class="dropdown-menu drp-mnu">
										<li><a href="<%=path%>/user/quit"><i
												class="fa fa-sign-out"></i> 退出</a></li>
									</ul></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script>
					$(document).ready(function() {
						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});

					});
				</script>
				<!-- /script-for sticky-nav -->
				<!--inner block start here-->
				<div class="inner-block" style="overflow-y :auto;">
					  <div class="panel panel-info">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" 
					          href="#collapseOne">
					          活动信息
					        </a>
					      </h4>
					    </div>
					    <div id="collapseOne" class="panel-collapse collapse in">
					      <div class="panel-body">
					        <table class="table">
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											活动ID：<label id="id"></label>
										</div>
										<div id="state" class="col-md-6 col-sm-6 col-xs-6">
											
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											活动主题：<label id="actSubject1"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											微信群：<label id="groupId"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
										<td>
											<div class="col-md-6 col-sm-6 col-xs-6">
												业务员：<label id="staffId"></label>
											</div>
											<div class="col-md-3 col-sm-3 col-xs-3">
												医生姓名：<label id="hcpName"></label>
											</div>	
											<div class="col-md-3 col-sm-3 col-xs-3">
												级别：<label id="hcpClass"></label>
											</div>	
										</td>
									</tr>
									<tr>
									   <td>
											<div class="col-md-6 col-sm-6 col-xs-6">
												合同ID：<label id="contractId"></label>
											</div>
											<div id="div_state" class="col-md-6 col-sm-6 col-xs-6">
												合同状态：<label id="contractState"></label>&nbsp;
												<button id="button_add" type="button" class="btn btn-primary">审核合同</button>
											</div>
										</td>
									</tr>
									<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											申请时间：<label id="actCtime"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											活动时长：<label id="actPeriod"></label>小时
										</div>
									</td>
								</tr>
								<tr>
								   <td>	
								   		<div class="col-md-6 col-sm-6 col-xs-6">
											活动开始时间：<label id="actStartTime"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											活动结束时间：<label id="actEndTime"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											审核意见：<label id="actReview"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											活动薪酬：<label id="actPaid"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											活动课件名：<label id="actCourseware"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											hcp微信名：<label id="hcpWxName"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											hcp微信昵称：<label id="hcpWxNick"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											备注：<label id="remark"></label>
										</div>
									</td>
								</tr>
						</table>
					      </div>
					    </div>
					  </div><br>
					
				<!-- 第二部分 -->
  					<div class="panel panel-warning">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" 
					          href="#collapseTwo">
					          活动反馈信息
					        </a>
					      </h4>
					    </div>
					    <div id="collapseTwo" class="panel-collapse collapse in">
					      <div class="panel-body">
					        <table class="table">
							<tbody>
								<tr class="trClass">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											活动实际开始时间：<label id="actStartCtime"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											活动实际结束时间：<label id="actEndCtime"></label>
										</div>
											</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											反馈时间：<label id=feedbackTime></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="panel panel-info col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        活动效果图片
										      </h3>
										   </div>
										   <div id="hcpTitleFile"  class="panel-body">
										   </div>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="panel panel-success col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        反馈评价
										      </h3>
										   </div>
										   <div class="panel-body">
										       <div class="col-md-3 col-sm-3 col-xs-3">
													专业度：<label id="feedbackPro"></label>
												</div>
												 <div class="col-md-3 col-sm-3 col-xs-3">
													丰富度：<label id="feedbackFull"></label>
												</div>
												 <div class="col-md-3 col-sm-3 col-xs-3">
													清晰度：<label id="feedbackClear"></label>
												</div>
												 <div class="col-md-3 col-sm-3 col-xs-3">
													互动：<label id="feedbackInteraction"></label>
												</div>
										   </div>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											其他评价：<label id="feedbackOther"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											审核备注：<label id="feedbackRemark"></label>
										</div>
									</td>
								</tr>
							</tbody>
						</table><hr>
					      </div>
					    </div>
					  </div><br>
					  <!-- 第三部分 -->
					    <div class="panel panel-primary">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" 
						          href="#collapseThree">
						          活动反馈审核
						        </a>
						      </h4>
						    </div>
						    <div id="collapseThree" class="panel-collapse collapse in">
						      <div class="panel-body">
						      	<div class="col-md-12 col-sm-12 col-xs-12">
									是否通过审核：<label class="checkbox-inline">
										      <input type="radio" name="receive" 
										         value="1" onclick="is_access()">是
										   </label>
										   <label class="checkbox-inline">
										      <input type="radio" name="receive" 
										         value="0"  onclick="not_access()">否
										   </label>
								</div>
						       <table id="table_access" class="table">
									<tbody>
							      <%--  <form action="<%=path%>/activity/admin/update" method="post" onsubmit="return check()"> --%>
									<tr>
										<td>
										 <div class="input-group col-md-3 col-sm-3 col-xs-3">
									         <span class="input-group-addon">实际支付金额</span>
									         <input id="feedPaid" type="number" name="feedPaid" value="1"  min="0.0" step="0.1" class="form-control">
									         <span class="input-group-addon">元</span>
									         <input name="id" id="activityId" type="hidden">
									      </div>
										</td>
									</tr>
									<tr >
										<td>
											<div class="input-group col-md-6 col-sm-6 col-xs-6">
										         <span class="input-group-addon">支付<br>金额<br>备注</span>
										         <textarea  id="feedRemark1" name="feedRemark" maxlength="255" style="height:70px;" class="form-control" ></textarea>
										      </div>
										</td>
									</tr>
									<tr>
									  <td>
									  	 <div class="col-md-2 col-sm-2 col-xs-2">
								       		 <button onclick="check(1)" class="btn btn-primary">提交</button>
								    	 </div>
									  </td>
									</tr>
									</tbody>
								</table>
								 <table id="not_access_table" class="table">
									<tbody>
										<tr>
											<td>
												 <div class="input-group col-md-12 col-sm-12 col-xs-12">
											         <span class="input-group-addon">审核<br>备注</span>
											         <textarea id="feedbackRemark1" name="feedbackRemark" maxlength="255" style="height:70px;" class="form-control" ></textarea>
											      </div>
											</td>
										</tr>
							      		<tr>
											<td>
									     	  <div class="col-md-2 col-sm-2 col-xs-2">
									       		 <button onclick="check(2)" class="btn btn-primary">提交</button>
									    	  </div>
									    	 </td>
									    </tr>
									  </tbody>
									 </table>  
							<!-- 	</form> -->
						      </div>
						    </div>
						  </div>
				</div>
			</div>
			<!--inner block end here-->
			<!--copy rights start here-->
			<div class="copyrights">
				<jsp:include page="/menu/foot.jsp"></jsp:include>
			</div>
			<!--COPY rights end here-->
	</div>
	<!--slider menu-->
	<div class="sidebar-menu">
		<div class="logo">
			<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span>
			</a> <a href="#"> <span id="logo"></span> <!--<img id="logo" src="" alt="Logo"/>-->
			</a>
		</div>
		<div class="menu">
			<ul id="menu">
				<li id="menu-home"><a href="<%=path%>/admin"> <i
						class="fa fa-home"></i> <span>审核员</span></a></li>
				<li ><a><i class="fa fa-user-md"></i><span>HCP管理</span><span
							class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hcp/admin/verify">HCP审核</a></li>
						<li><a href="<%=path%>/hcp/admin/view">HCP列表</a></li>
					</ul></li>
				<li class="active"><a href="<%=path%>/activity/admin/view">
					<i class="fa fa-leaf"></i><span>活动管理</span></a></li>
				<li><a><i class="fa fa-hospital-o"></i><span>医院管理</span> <span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hospital/view">查看列表</a></li>
						<li><a href="<%=path%>/hospital/add">添加医院</a></li>
					</ul></li>
				<li ><a href="<%=path%>/contract/admin/view"><i class="fa  fa-file-text"></i><span>合同管理</span> </a></li>
				<li><a><i class="fa fa-jpy"></i><span>支付管理</span> <span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<%-- <li><a href="<%=path%>/payment/admin/view/5">付款管理</a></li> --%>
						<li><a href="<%=path%>/payment/admin/view/0">待支付</a></li>
						<li><a href="<%=path%>/payment/admin/view/1">已支付</a></li>
						<li><a href="<%=path%>/payment/admin/view/2">支付失败</a></li>
					</ul></li>
				<li ><a href="<%=path%>/group/admin/view"><i class="fa  fa-wechat"></i><span>微信群管理</span> </a></li>
			</ul>
		</div>
	</div>
	<div class="clearfix"></div>
	</div>
	<!--slide bar menu end here-->
<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="height: 570px; overflow:auto;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">合同信息详情</h4>
				</div>
				<div class="modal-body">
					<div class="panel-body">
						<table class="table">
							<tbody>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											医生：<label id="hcpName1"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											业务员：<label id="staffName1"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											合同ID：<label id="contractId1" ></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											提交时间：<label id="ctime"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											生效时间：<label id="contractStartTime"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											失效时间：<label id="contractEndTime"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="panel panel-default col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        合同图片
										      </h3>
										   </div>
										   <div id="fileId"  class="panel-body">
										   </div>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											合同运送方式：<label id="type"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											份数：<label id="contractNumber"></label>
										</div>
									</td>
								</tr>
								<tr id="trClass">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											快递公司：<label id="expressName"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											快递单号：<label id="expressId"></label>
										</div>
									</td>
								</tr>
								<form id="myform" action="<%=path%>/activity/admin/save" method="post" >
								<tr >
									<td>
										<div id="judge" class="col-md-12 col-sm-12 col-xs-12">
											是否收到：<label class="checkbox-inline">
												      <input type="radio" name="receive" 
												         value="1" onclick="is_receive()">是
												   </label>
												   <label class="checkbox-inline">
												      <input type="radio" name="receive" 
												         value="0" checked="checked" onclick="not_receive()">否
												   </label>
										</div>
									</td>
								</tr>
								<tr class="is_receive">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
										 <input id="contractId2" name="id" type="hidden">
										 <input id="input_receive" name="receive" type="hidden">
											是否通过：<label class="checkbox-inline">
												      <input type="radio" name="state" 
												         value="1" >是
												   </label>
												   <label class="checkbox-inline">
												      <input type="radio" name="state" 
												         value="2" checked="checked">否
												   </label>
										</div>
									</td>
								</tr>
								<tr class="is_receive">
									<td>
									 <div class="input-group col-md-12 col-sm-12 col-xs-12">
								         <span class="input-group-addon">审核<br>备注</span>
								         <textarea id="feedRemark" name="feedRemark" maxlength="255" style="height:70px;" class="form-control" ></textarea>
								      </div>
								     </td>
								</tr>
								<tr class="is_receive">
									<td>
									 <div class="col-md-12 col-sm-12 col-xs-12">
									 	 <button type="submit" class="btn btn-primary" >提交合同审核</button>
									 </div>
									</td>
								</tr>
								</form>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>

	

	<script>
		var toggle = true;

		$(".sidebar-icon").click(
				function() {
					if (toggle) {
						$(".page-container").addClass("sidebar-collapsed")
								.removeClass("sidebar-collapsed-back");
						$("#menu span").css({
							"position" : "absolute"
						});
					} else {
						$(".page-container").removeClass("sidebar-collapsed")
								.addClass("sidebar-collapsed-back");
						setTimeout(function() {
							$("#menu span").css({
								"position" : "relative"
							});
						}, 400);
					}
					toggle = !toggle;
				});
	</script>
	<!--scrolling js-->
	<script src="<%=path%>/js/jquery.nicescroll.js"></script>
	<script src="<%=path%>/js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="<%=path%>/js/bootstrap.js"></script>
	<script src="<%=path%>/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="<%=path%>/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function() {
			  $("#table_access").hide();
			  $("#not_access_table").hide();
			  var aid = '${id}';
			  var hcpId = '${hcpId}';
			  $("#myform").attr("action","<%=path%>/activity/admin/save/"+aid+"/"+hcpId);
		      $.ajax({
				   type: "POST",
				   url: "<%=path%>/activity/admin/verify/data/"+aid+"/"+hcpId,
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				    $("#id").html(data.activity.id);
				     if(data.activity.state==0){
				     	$("#state").html("状态：<label>未反馈</label>");
					  }else if(data.activity.state==1){
					      $("#state").html("状态：<label >待审核</label>");
				     }else if(data.activity.state==2){
					      $("#state").html("状态：<label >已通过</label>");
				     }else if(data.activity.state==3){
					      $("#state").html("状态：<label >未通过</label>");
				     }else{
					       $("#state").html("状态：<label>活动取消</label>");
				     }
				     $("#actSubject1").html(data.activity.actSubject);
				     $("#groupId").html(data.activity.groupId);
				     $("#hcpName").html(data.activity.hcpName);
				     $("#hcpClass").html(data.activity.hcpClass);
				     $("#staffId").html(data.activity.staffId);
				     $("#actStartTime").html(data.startTime);
				     $("#actPeriod").html(data.activity.actPeriod);
				     $("#actCtime").html(data.actCtime);
				     $("#actEndTime").html(data.endTime);
				     $("#actReview").html(data.activity.actReview);
				     $("#actPaid").html(data.activity.actPaid);
				     $("#actCourseware").html(data.activity.actCourseware);
				     $("#hcpWxName").html(data.activity.hcpWxName);
				     $("#hcpWxNick").html(data.activity.hcpWxNick);
				     $("#remark").html(data.activity.remark);
				     $("#actRealStrat").html(data.activity.actRealStrat);
				     $("#actRealEnd").html(data.activity.actRealEnd);
				     $("#feedbackTime").html(data.feedbackTime);
				     $("#contractId").html(data.activity.contractId);
				     $("#oaName").html(data.activity.oaName);
				     $("#oaPhone").html(data.activity.oaPhone);
				     $("#feedPaid").val(data.activity.actPaid);
				     $("#activityId").val(data.activity.id);
				      $("#hcpTitleFile").html("");
				     $(data.file).each(function(index,item){
				         var row = "";
				         if(index!=0)
				            row = "<hr>";
				         row += "<a href=\"<%=path%>/admin/view/photo"+item.path+"/\" target='_blank'>"+spitString(item.fileName)+"</a>";
				       $("#hcpTitleFile").append(row);
				     });
				     if($("#hcpTitleFile").html()==""){
				        $("#hcpTitleFile").html("未上传活动照片");
				     }
				     $("#feedbackPro").html(data.activity.feedbackPro+"分");
				     $("#feedbackFull").html(data.activity.feedbackFull+"分");
				     $("#feedbackClear").html(data.activity.feedbackClear+"分");
				     $("#feedbackInteraction").html(data.activity.feedbackInteraction+"分");
				     $("#feedbackOther").html(data.activity.feedbackOther);
				     $("#feedRemark").html(data.activity.feedRemark);
				     $("#feedbackRemark").html(data.activity.feedbackRemark);
				     $("#actStartCtime").html(data.actRealStart);
				     $("#actEndCtime").html(data.actRealEnd);
				     $("#button_add").hide();
					     if(data.contract.state==0){
					       $("#contractState").html("待审核");
					       $("#button_add").show();
				     	   $("#button_add").attr("onclick","verify('"+data.activity.contractId+"','"+data.activity.hcpName+"','"+data.activity.staffId+"')");
				     	} else if(data.contract.state==1){
					       $("#contractState").html("待签章");
					     }else  if(data.contract.state==2||data.contract.state==6){
					       $("#contractState").html("未通过");
					     }else  if(data.contract.state==3){
					       $("#contractState").html("待上传");
					     }else  if(data.contract.state==4){
					       $("#contractState").html("待终审");
					     }else  if(data.contract.state==5){
					       $("#contractState").html("通过");
					       if(data.contract.contractEndTime<=new Date()){
					      	 $("#contractState").html("已失效");
					     	}
					     }
				     }
				 });
				 });
		function verify(id,staffId,hcpName){
		$("#staffName1").html(staffId);
		$("#hcpName1").html(hcpName);
			   $.ajax({
				   type: "POST",
				   url: "<%=path%>/activity/admin/verify/contract/"+id,
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				    	$("#ctime").html(data.ctime);
						$("#contractId1").html(id);
						$("#contractId2").val(id);
						$("#contractStartTime").html(data.contractStartTime);
						$("#contractEndTime").html(data.contractEndTime);
						$("#contractNumber").html(data.contract.contractNumber);
						$("#expressName").html(data.contract.expressName);
						$("#expressId").html(data.contract.expressId);
						if(data.contract.type==1){
						  $("#trClass").hide();
						  	$("#type").html(data.contract.expressName);
						}else{
							$("#type").html("快递｜物流");
						}
						$(data.file).each(function(index,item){
				         var row = "";
				         if(index!=0)
				            row = "<hr>";
				        row += "<a href=\"<%=path%>/admin/view/photo"+item.path+"/\" target='_blank'>"+spitString(item.fileName)+"</a>";
				        $("#fileId").append(row);
				     });
				      $("#myModal").modal('show');
				       $(".is_receive").hide();
				   }
				 });
				   
		}
		function check(flag){
			var data = "";
			var activityId = $("#activityId").val();
			if(flag==1){
				var feedPaid = $("#feedPaid").val();
				var feedRemark = $("#feedRemark1").val();
				
				if(feedPaid==null){
					alert("实付金额不能为空！");
					return false;
				}else{
					data = "id="+activityId+"&feedPaid="+feedPaid+"&feedRemark="+feedRemark+"&state=2";
				}
			}else{
				data = "id="+activityId+"&feedbackRemark="+$("#feedbackRemark1").val()+"&state=3";
			}
			 $.ajax({  //绑定数据
				   type: "POST",
				   url: "<%=path%>/activity/admin/update",
				   data:data,
				   dataType:"json",  
				   success: function(data){
				     if(data.count==1){
				     	alert("提交成功！");
				     	window.close();
				     }else{
				    	 alert("提交失败！");
				     }
				   }
				});
			
		};
		function is_receive(){
		 $("#judge").html("已经收到合同文件，请填写审核结果：");
		 $(".is_receive").show();
		 $("#input_receive").val("1");
		};
		function not_receive(){
		
		};
		function is_access(){
			$("#table_access").show();
			$("#not_access_table").hide();
		};
		function not_access(){
			$("#table_access").hide();
			$("#not_access_table").show();
		};
	</script>
	<!-- mother grid end here-->
</body>
</html>
