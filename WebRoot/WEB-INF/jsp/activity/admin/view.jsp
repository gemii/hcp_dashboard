<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>
<html>
<head>
<link rel="SHORTCUT ICON" href="<%=path%>/image/index/logo.ico" type="image/x-icon" >
<title>HCP-审核员</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all">
<link href="<%=path%>/css/dataTables.bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link type="text/css" rel="stylesheet" href="<%=path %>/js/jedate/skin/jedate.css">
<!-- Custom Theme files -->
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css"
	media="all" />
<!--js-->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="<%=path%>/css/font-awesome.css" rel="stylesheet">

<!--skycons-icons-->
<script src="<%=path%>/js/skycons.js"></script>
<!--//skycons-icons-->
</head>
<body>
	<div class="page-container" >
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main" style="z-index:999;">
					<div class="header-left">
						<div class="logo-name">
							<a href="<%=path%>"> <!-- <h1>KPI Of College</h1> --> <img
								id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" />
							</a>
						</div>

					</div>
					<div class="header-right">
						<div class="profile_details_left">
							<div class="clearfix"></div>
						</div>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img">
											</span>
											<div class="user-name">
												<p>${sessionScope.user.name }</p>
												<span>${sessionScope.user.type }</span>
											</div>
											<i class="fa fa-angle-down lnr"></i> <i
												class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
								</a>
									<ul class="dropdown-menu drp-mnu">
										<li><a href="<%=path%>/user/quit"><i
												class="fa fa-sign-out"></i> 退出</a></li>
									</ul></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script>
					$(document).ready(function() {
						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});

					});
				</script>
				<!-- /script-for sticky-nav -->
				<!--inner block start here-->
				<div class="inner-block" style="min-height:570px;overflow-y :auto;">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">活动列表</h3>
						</div>
						<div class="panel-body">
							  	<div class="col-md-3 col-sm-3 col-xs-3"  >
						         <input type="text" id="actSubject" class="form-control"  placeholder="请输入活动主题">
						      </div>
						      <div class=" col-md-2 col-sm-2 col-xs-2">
									<input class="form-control datainp" id="dateinfo1" type="text" 
									placeholder="活动开始时间" readonly name="startTime"  >
								</div>
								 <div class=" col-md-2 col-sm-2 col-xs-2">
									<input class=" form-control datainp" id="dateinfo2" type="text" 
									placeholder="活动结束时间" readonly name="endTime"  >
								</div>
						  	 <button type="button" class="btn btn-primary" onclick="chaxun()">查询</button>
							    <hr>
							<div id="loading" align="center"></div>
							<div id="contentDIV">
								<div id="tishi" class="alert alert-danger" hidden="hidden"
									style="clear: both;">空空如也...</div>
								<div id="table">
									<table id="example"
										class="table table-striped table-bordered table-hover"
										cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>活动编号</th>
												<th>微信群</th>
												<th>医生</th>
												<th>业务员</th>
												<th>活动主题</th>
												<!-- <th>活动薪酬</th> -->
												<th>状态</th>
												<th class="oper">操作</th>
											</tr>
										</thead>
										<tbody id="tbody">

										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--inner block end here-->
			<!--copy rights start here-->
			<div class="copyrights">
				<jsp:include page="/menu/foot.jsp"></jsp:include>
			</div>
			<!--COPY rights end here-->
	</div>
	<!--slider menu-->
	<div class="sidebar-menu">
		<div class="logo">
			<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span>
			</a> <a href="#"> <span id="logo"></span> <!--<img id="logo" src="" alt="Logo"/>-->
			</a>
		</div>
		<div class="menu">
			<ul id="menu">
				<li id="menu-home"><a href="<%=path%>/admin"> <i
						class="fa fa-home"></i> <span>审核员</span></a></li>
				<li ><a><i class="fa fa-user-md"></i><span>HCP管理</span><span
							class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hcp/admin/verify">HCP审核</a></li>
						<li><a href="<%=path%>/hcp/admin/view">HCP列表</a></li>
					</ul></li>
				<li class="active"><a href="<%=path%>/activity/admin/view">
					<i class="fa fa-leaf"></i><span>活动管理</span></a></li>
				<li><a><i class="fa fa-hospital-o"></i><span>医院管理</span> <span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hospital/view">查看列表</a></li>
						<li><a href="<%=path%>/hospital/add">添加医院</a></li>
					</ul></li>
				<li ><a href="<%=path%>/contract/admin/view"><i class="fa  fa-file-text"></i><span>合同管理</span> </a></li>
				<li><a><i class="fa fa-jpy"></i><span>支付管理</span> <span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/payment/admin/view/5">付款管理</a></li>
						<li><a href="<%=path%>/payment/admin/view/0">待支付</a></li>
						<li><a href="<%=path%>/payment/admin/view/1">已支付</a></li>
						<li><a href="<%=path%>/payment/admin/view/2">支付失败</a></li>
					</ul></li>
				<li ><a href="<%=path%>/group/admin/view"><i class="fa  fa-wechat"></i><span>微信群管理</span> </a></li>
			</ul>
		</div>
	</div>
	<div class="clearfix"></div>
	</div>
	<!--slide bar menu end here-->

	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="height: 570px; overflow:auto;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">活动详细信息</h4>
				</div>
				<div class="modal-body">
					<div class="panel-body">
						<table class="table">
							<tbody>
								<tr>
									<td>
										<div class="col-md-8 col-sm-8 col-xs-8">
											活动编号：<label id="id"></label>
										</div>
										<div id="state" class="col-md-4 col-sm-4 col-xs-4">
											
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											活动主题：<label id="actSubject1"></label>
										</div>
										</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-7 col-sm-7 col-xs-7">
											微信群：<label id="groupName"></label>
										</div>
										<div class="col-md-5 col-sm-5 col-xs-5">
											群编号：<label id="groupId"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-7 col-sm-7 col-xs-7">
											业务员：<label id="staffName"></label>
										</div>
										<div class="col-md-5 col-sm-5 col-xs-5">
											大区：<label id="areaName"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-4 col-sm-4 col-xs-4">
											医生编号：<label id="hcpId"></label>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-4">
											姓名：<label id="hcpName"></label>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-4">
											级别：<label id="hcpClass"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-7 col-sm-7 col-xs-7">
											申请时间：<label id="actCtime"></label>
										</div>
										<div class="col-md-5 col-sm-5 col-xs-5">
											活动时长：<label id="actPeriod"></label>小时
										</div>
									</td>
								</tr>
								<tr>
								   <td>	
								   		<div class="col-md-12 col-sm-12 col-xs-12">
											活动开始时间：<label id="actStartTime"></label>
										</div>
									</td>
								</tr>
								<tr>
								   <td>	
										<div class="col-md-12 col-sm-12 col-xs-12">
											活动结束时间：<label id="actEndTime"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											审核意见：<label id="actReview"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											活动薪酬：<label id="actPaid"></label>元
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											活动课件名：<label id="actCourseware"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											微信名：<label id="hcpWxName"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											微信昵称：<label id="hcpWxNick"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											OA姓名：<label id="oaName"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											OA联系方式：<label id="oaPhone"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											备注：<label id="remark"></label>
										</div>
									</td>
								</tr>
								
								<tr class="trClass">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											活动实际开始时间：<label id="actStartCtime"></label>
										</div>
									</td>
								</tr>
								
								<tr class="trClass">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											活动实际结束时间：<label id="actEndCtime"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											反馈时间：<label id=feedbackTime></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											合同ID：<label id="contractId"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="panel panel-info col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        活动效果图片
										      </h3>
										   </div>
										   <div id="hcpTitleFile"  class="panel-body">
										   </div>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="panel panel-success col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        反馈评价
										      </h3>
										   </div>
										   <div class="panel-body">
										       <div class="col-md-6 col-sm-6 col-xs-6">
													专业度：<label id="feedbackPro"></label>
												</div>
												 <div class="col-md-6 col-sm-6 col-xs-6">
													丰富度：<label id="feedbackFull"></label>
												</div>
												 <div class="col-md-6 col-sm-6 col-xs-6">
													清晰度：<label id="feedbackClear"></label>
												</div>
												 <div class="col-md-6 col-sm-6 col-xs-6">
													互动：<label id="feedbackInteraction"></label>
												</div>
										   </div>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											其他评价：<label id="feedbackOther"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass verfiryClass">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											反馈审核备注：<label id="feedbackRemark"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass verfiryClass">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											反馈审核时间：<label id="verfiryTime"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass pay_class">
									<td>
										<div class="col-md-7 col-sm-7 col-xs-7">
											支付ID：<label id="paymentId"></label>
										</div>
										<div class="col-md-5 col-sm-5 col-xs-5">
											实际支付金额：<label id="feedPaid"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass pay_class">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											支付金额备注：<label id="feedRemark"></label>
										</div>
									</td>
								</tr>
								<tr id="tr_payFile" class="pay_class" >
									<td>
										<div class="panel panel-default col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        支付凭证照片
										      </h3>
										   </div>
										   <div id="fileId2"  class="panel-body">
										   </div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>


<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="height: 480px; width:680px;overflow:auto;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">合同信息</h4>
				</div>
				<div class="modal-body">
					<div class="panel-body">
						<table class="table">
							<tbody>
								<tr>
									<td>
										<div class="col-md-7 col-sm-7 col-xs-7">
											活动ID：<label id="aid"></label>
										</div>
										<div  class="col-md-5 col-sm-5 col-xs-5">
											医生姓名：<label id="hcpName1"></label>
										</div>
									</td>
								</tr>
								<!-- <tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											活动主题：<label id="actSubject2"></label>
										</div>
									</td>
								</tr> -->
								<tr>
									<td>
										<div class="panel panel-info col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        所签订的合同
										      </h3>
										   </div>
										   <table class="table mytable"> 
											    <thead> 
											        <tr> 
											            <th>合同ID</th> 
											            <th>签订公司</th> 
											            <th>生效日期</th>
											            <th>失效日期</th>
											            <th>状态</th> 
											            <th>操作</th>
											        </tr> 
											    </thead> 
											    <tbody id="contract"> 
											    </tbody> 
											</table>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>

	<script>
		var toggle = true;

		$(".sidebar-icon").click(
				function() {
					if (toggle) {
						$(".page-container").addClass("sidebar-collapsed")
								.removeClass("sidebar-collapsed-back");
						$("#menu span").css({
							"position" : "absolute"
						});
					} else {
						$(".page-container").removeClass("sidebar-collapsed")
								.addClass("sidebar-collapsed-back");
						setTimeout(function() {
							$("#menu span").css({
								"position" : "relative"
							});
						}, 400);
					}
					toggle = !toggle;
				});
	</script>
	<!--scrolling js-->
	<script src="<%=path%>/js/jquery.nicescroll.js"></script>
	<script src="<%=path%>/js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="<%=path%>/js/bootstrap.js"></script>
	<script src="<%=path%>/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="<%=path%>/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script type="text/javascript" src="<%=path%>/js/jedate/jedate.js"></script>
	<script type="text/javascript">
	 	jeDate({
			dateCell:"#dateinfo1",
			format:"YYYY-MM-DD",
			isinitVal:false,
			isTime:false
		});
	  jeDate({
			dateCell:"#dateinfo2",
			format:"YYYY-MM-DD",
			isinitVal:false,
			isTime:false
		});
	var table = null;
	$(document).ready(function() {
		table = $('#example').DataTable();
        $('#loading').hide();
		$('#contentDIV').hide();
    });
    function chaxun(){
   		 var date1 = $('#dateinfo1').val();
		  var date2 = $('#dateinfo2').val();
		  if(date1!=""&&date2==""){
		  	alert("活动结束日期必须选择！");
		  	return false;
		  }else if(date2!=""&&date1==""){
		  	alert("活动开始日期必须选择！");
		  	return false;
		  }else if(date1!=""&&date2!=""&&date1>=date2){
			  alert("活动开始日期必须晚于结束日期！");
			  return false;
		  }
    	    $.ajax({  //绑定数据
			   type: "POST",
			   url: "<%=path%>/activity/admin/list",
			   data:"actSubject="+$("#actSubject").val()+"&actStartTime="+date1+"&actEndTime="+date2,
			   dataType:"json",  
			   beforeSend:function(XMLHttpRequest){ 
				   $('#loading').show();
				   $('#contentDIV').hide();
		           $("#loading").html("<img src='<%=path%>/image/loading.gif' />");
										},
										error : function(e) {
											alert("数据加载失败，请刷新重试！");
											$('#loading').hide();
										},
										success : function(data) {
											$('#contentDIV').show();
											$('#loading').hide();
											if (data == "") {
												$('#tishi').show();
												$('#table').hide();
											} else {
												$('#table').show();
												$('#tishi').hide();
											}
											table.destroy(false);
											$('#tbody').html("");
											$(data).each(
															function(index,item) {
																var row = "<tr>";
																row += "<td>" + item.id + "</td>";
																row +="<td><a title='"+item.groupId+"' style='color:#333;'>"; 
															   if(item.groupId.length>10){
																   row +=item.groupId.substring(0,10) + "...";
															   }else{
																   row += item.groupId;
															   } 
																row += "<td>" + item.hcpId + "</td>";
																row += "<td>" + item.staffId + "</td>";
																row +="<td><a title='"+item.actSubject+"' style='color:#333;'>"; 
															   if(item.actSubject.length>10){
																   row +=item.actSubject.substring(0,10) + "...";
															   }else{
																   row += item.actSubject;
															   } 
															   row +="</a></td>";
																/* row += "<td>" + item.actPaid + "</td>"; */
																if(item.state==0){
																	row += "<td>待提交</td>";
																}else if(item.state==1){
																	row += "<td>审核中</td>";
																}else if(item.state==2){
																	row += "<td>已审核</td>";
																}else if(item.state==3){
																	row += "<td>未通过</td>";
																}else if(item.state==4){
																	row += "<td>已取消</td>";
																}
																row += "<td><button class=\"btn btn-primary btn-sm\" onclick=\"view('" + item.id + "')\">详情</button>";
																if(item.state==1){
																	row += "&nbsp;<button class=\"btn btn-success btn-sm\" onclick=\"feed('" + item.remark + "','"+item.id+"')\">审核</button>";
																}else if(item.state==2&&item.contractId==null){
																	/* $("#actSubject2").html(item.actSubject); */
																	row += "&nbsp;<button class=\"btn btn-info btn-sm\" onclick=\"binding('" + item.hcpId + "','"+item.id+"','"+item.actReview+"','')\">绑定合同</button>";
																}else if(item.state==2&&item.contractId!=null){
																	/* $("#actSubject2").html(item.actSubject); */
																	row += "&nbsp;<button class=\"btn btn-info btn-sm\" onclick=\"binding('" + item.hcpId + "','"+item.id+"','"+item.actReview+"','"+item.contractId+"')\">查看合同</button>";
																}
																row += "</td></tr>";

																$('#tbody').append(row);
															});
											table=$('#example').DataTable();
										}
									});
						};
		function feed(hcpId,id){
		  window.open("<%=path%>/activity/admin/verify/"+id+"/"+hcpId);
		};
		function view(id) {
		   $.ajax({
				   type: "POST",
				   url: "<%=path%>/activity/admin/show/"+id,
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				   $(".trClass").show();
				   $(".pay_class").show();
				     $("#id").html(data.activity.id);
				      if(data.activity.state==0){
				     	$("#state").html("状态：<label>未反馈</label>");
				     	$(".trClass").hide();
					  }else if(data.activity.state==1){
					      $("#state").html("状态：<label >审核中</label>");
				     }else if(data.activity.state==2){
					      $("#state").html("状态：<label >通过</label>");
				     }else if(data.activity.state==3){
					      $("#state").html("状态：<label >未通过</label>");
					      $(".pay_class").hide();
				     }else{
					       $("#state").html("状态：<label>已取消</label>");
					       $(".trClass").hide();
				     }
				     $("#actSubject1").html(data.activity.actSubject);
				     $("#groupId").html(data.activity.groupId);
				     $("#groupName").html(data.activity.groupName);
				     $("#groupId").html(data.activity.groupId);
				     $("#hcpId").html(data.activity.hcpId);
				     $("#hcpName").html(data.activity.hcpName);
				     $("#staffName").html(data.activity.staffId);
				     $("#areaName").html(data.activity.areaName);
				     $("#actStartTime").html(data.startTime);
				     $("#actPeriod").html(data.activity.actPeriod);
				     $("#actCtime").html(data.actCtime);
				     $("#actEndTime").html(data.endTime);
				     $("#actReview").html(data.activity.actReview);
				     $("#actPaid").html(data.activity.actPaid);
				     $("#actCourseware").html(data.activity.actCourseware);
				     $("#hcpWxName").html(data.activity.hcpWxName);
				     $("#hcpWxNick").html(data.activity.hcpWxNick);
				     $("#remark").html(data.activity.remark);
				     $("#actRealStrat").html(data.activity.actRealStrat);
				     $("#actRealEnd").html(data.activity.actRealEnd);
				     $("#feedbackTime").html(data.feedbackTime);
				     $("#contractId").html(data.activity.contractId);
				     $("#oaName").html(data.activity.oaName);
				     $("#oaPhone").html(data.activity.oaPhone);
				     $("#hcpClass").html(data.activity.hcpClass);
				      $("#hcpTitleFile").html("");
				     $(data.file).each(function(index,item){
				         var row = "";
				         if(index!=0)
				            row = "<hr>";
				         row += "<a href=\"<%=path%>/admin/view/photo"+item.path+"/\" target='_blank' title='"+spitStringTitle(item.fileName)+"'>"+spitString(item.fileName)+"</a>";
				        $("#hcpTitleFile").append(row);
				     });
				     if($("#hcpTitleFile").html()==""){
				        $("#hcpTitleFile").html("未上传活动照片");
				     }
				     $("#feedbackPro").html(data.activity.feedbackPro+"分");
				     $("#feedbackFull").html(data.activity.feedbackFull+"分");
				     $("#feedbackClear").html(data.activity.feedbackClear+"分");
				     $("#feedbackInteraction").html(data.activity.feedbackInteraction+"分");
				     $("#feedbackOther").html(data.activity.feedbackOther);
				     $(".verfiryClass").hide();
				     if(data.activity.state==2||data.activity.state==3){
				     	$("#feedbackRemark").html(data.activity.feedbackRemark);
				    	$("#verfiryTime").html(data.verfiryTime);
				    	$(".verfiryClass").show();
				     }
				     
				     if(data.activity.payState==0){
				       $(".pay_class").hide();
				     }else{
				    	 $(".pay_class").show();
				    	  $("#feedPaid").html(data.activity.feedPaid);
				    	  $("#feedRemark").html(data.activity.feedRemark);
				    	  
				    	 if(data.activity.payState==1){
				    	  $("#tr_payFile").show();
				    	  	$("#paymentId").html(data.activity.paymentId);
		    	  			$("#fileId2").html("");
								$(data.file1).each(function(index,item){
							         var row = "";
							         if(index!=0)
							            row += "<hr>";
							        row += "<a href=\"<%=path%>/view/photo"+item.path+"/\" target='_blank' title='"+spitStringTitle(item.fileName)+"'>"+spitString(item.fileName)+"</a>";
							        $("#fileId2").append(row);
							     });
							     if($("#fileId2").html()==""){
							     	$("#fileId2").html("未上传付款凭证");
							     }
				    	  	$("#paymentId").html(data.activity.paymentId);
				    	  }else{
				    	  
				    	  }
				     }
				     $("#actStartCtime").html(data.actRealStart);
				     $("#actEndCtime").html(data.actRealEnd);
				     
				     $("#myModal").modal('show');
				   }
				 }); 
		};
		function binding(hcpName,activityId,hcpId,contractId){
			$("#aid").html(activityId);
			$("#hcpName1").html(hcpName);
			  $.ajax({
				   type: "POST",
				   url: "<%=path%>/activity/admin/contract/"+hcpId,
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				   $("#contract").html("");
				     if(data==null||data==""){
				     	$(".mytable").html("暂未提交合同");
				     }else{
				     var html = "    <thead>  <tr>    <th>合同ID</th>  <th>签订公司</th>  <th>生效日期</th> <th>失效日期</th> <th>状态</th>  <th>操作</th>  </tr>  </thead>  <tbody id=\"contract\">    </tbody> ";
				     	$(".mytable").html(html);
				     	$(data).each(function(index,item){
					     	if(contractId==null||contractId==""){
					     		   var row = "<tr>";
					            row += "<td><a href=\"<%=path%>/contract/admin/detail/"+item.id+"\" target='_blank' title='"+item.id+"'>"+item.id+"</a></td>";
					            row +="<td>"+item.tp_name+"</td>";
					            row +="<td>"+FormatDate(item.contractStartTime,2)+"</td>";
					            row +="<td>"+FormatDate(item.contractEndTime,2)+"</td>";
					            row +="<td>"+item.state+"</td>";
					            if(item.state=="通过")
					              row +="<td><button class=\"btn btn-info btn-sm\" onclick=\"binding_contract('" + item.id + "','"+activityId+"')\">绑定</button></td>";
					            else
					              row +="<td></td>";
					            row += "</tr>";
					      	 $("#contract").append(row);
					     	}else{
					     		if(item.id==contractId){
					     			var row = "<tr>";
						            row += "<td><a href=\"<%=path%>/contract/admin/detail/"+item.id+"\" target='_blank' title='"+item.id+"'>"+item.id+"</a></td>";
						            row +="<td>"+item.tp_name+"</td>";
						            row +="<td>"+FormatDate(item.contractStartTime,2)+"</td>";
						            row +="<td>"+FormatDate(item.contractEndTime,2)+"</td>";
						            row +="<td>"+item.state+"</td>";
						            row +="<td>已绑定</td>";
						            row += "</tr>";
						      	 $("#contract").append(row);
					     		}
					     	}
				      
				   		});
				     }
				   	$("#myModal1").modal('show');
				   }
			 });
			
		};
		function binding_contract(contractId,activityId){
				 $.ajax({  //绑定数据
				   type: "POST",
				   url: "<%=path%>/activity/admin/update",
				   data:"id="+activityId+"&contractId="+contractId,
				   dataType:"json",  
				   success: function(data){
				     if(data.count==1){
				     	alert("提交成功！");
				     	$("#myModal1").modal('hide');
				     	chaxun();
				     }else{
				    	 alert("提交失败！");
				     }
				   }
				});
		}
		
	</script>
	<!-- mother grid end here-->
</body>
</html>
