<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>
<html>
<head>
<link rel="SHORTCUT ICON" href="<%=path%>/image/index/logo.ico" type="image/x-icon" >
<title>HCP-业务员</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet"	type="text/css" media="all">
<link href="<%=path%>/css/dataTables.bootstrap.min.css" type="text/css"	rel="stylesheet">
<link rel="stylesheet" href="<%=path %>/js/zyupload/skins/zyupload-1.0.0.min.css" 	type="text/css">
<link type="text/css" rel="stylesheet" href="<%=path %>/js/jedate/skin/jedate.css">
<!-- Custom Theme files -->
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css"	media="all" />
<!--js-->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="<%=path%>/css/font-awesome.css" rel="stylesheet">

<!--skycons-icons-->
<script src="<%=path%>/js/skycons.js"></script>
<!--//skycons-icons-->
</head>
<body >
	<div class="page-container">
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main" style="z-index:999;">
					<div class="header-left">
						<div class="logo-name">
							<a href="<%=path%>"> <!-- <h1>KPI Of College</h1> --> <img
								id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" />
							</a>
						</div>

					</div>
					<div class="header-right">
						<div class="profile_details_left">
							<div class="clearfix"></div>
						</div>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img"> 
											</span>
											<div class="user-name">
												<p>${sessionScope.user.name }</p>
												<span>${sessionScope.user.type }</span>
											</div>
											<i class="fa fa-angle-down lnr"></i> <i
												class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
								</a>
									<ul class="dropdown-menu drp-mnu">
										<li><a href="<%=path%>/user/quit"><i
												class="fa fa-sign-out"></i> 退出</a></li>
									</ul></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script>
					$(document).ready(function() {
						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});

					});
				</script>
				<!-- /script-for sticky-nav -->
				<!--inner block start here-->
				<div class="inner-block" style="overflow-y :auto;">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">活动信息</h3>
						</div>
						<div class="panel-body">
							<table id="table1" class="table">
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											活动编号：<label id="id"></label>
										</div>
										<div id="state" class="col-md-6 col-sm-6 col-xs-6">
											
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											活动主题：<label id="actSubject1"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											微信群编号：<label id="groupId"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											微信群名：<label id="groupName"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											医生编号：<label id="hcpId"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											医生姓名：<label id="hcpName"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											微信号：<label id="hcpWxName"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											昵称：<label id="hcpWxNick"></label>
										</div>
									</td>
								</tr>
						</table>
						</div>
					</div><br>
					<div class="panel panel-warning">
						<div class="panel-heading">
							<h3 class="panel-title">活动反馈</h3>
						</div>
						<div class="panel-body">
						<form action="<%=path%>/activity/staff/save" method="post" onsubmit="return check()">
							<table class="table">
							<tbody>
								<tr class="trClass">
									<td>  <input id="input_id" name="id" type="hidden"/>
									<div class="input-group col-md-6 col-sm-6 col-xs-6">
										<span class=" input-group-addon">活动实际开始时间：</span>
										<input class="form-control datainp" id="dateinfo1" type="text" 
										placeholder="点击选择时间" readonly name="startTime"   >
									</div><br>
									<div class="input-group col-md-6 col-sm-6 col-xs-6">
										<span class=" input-group-addon">活动实际结束时间：</span>
										<input class="form-control datainp" id="dateinfo2" type="text" 
										placeholder="点击选择时间" readonly name="endTime"  >
									</div>
									</td>
								</tr>
								<tr class="trClass">
									   <td>
									   		 <div class="input-group col-md-12 col-sm-12 col-xs-12">
										         <button type="button" class="btn btn-success"
														data-toggle="modal" data-target=".bs-example-modal-lg">
														上传活动照片</button><br>
												   <label id="label_filepath"></label>
												   <input id="fileId" name="actFile" type="hidden"/>
										      </div>
									   </td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="panel panel-success col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        反馈评价
										      </h3>
										   </div>
										   <div class="panel-body">
												 <div class="col-md-3 col-sm-3 col-xs-3">
											       <div class="input-group ">
											         <span class="input-group-addon">专业度</span>
											           <select ID="feedbackPro" name="feedbackPro" class="form-control" > 
											          		<option>请选择</option>
											          		<option value="5">5分</option>
											          		<option value="4">4分</option>
											          		<option value="3">3分</option>
											          		<option value="2">2分</option>
											          		<option value="1">1分</option>
													   </select>
											      	</div>
											      </div>
											      <div class="col-md-3 col-sm-3 col-xs-3">
											       <div class="input-group ">
											         <span class="input-group-addon">丰富度</span>
											           <select ID="feedbackFull" name="feedbackFull" class="form-control" > 
											          		<option>请选择</option>
											          		<option value="6">6分</option>
											          		<option value="5">5分</option>
											          		<option value="4">4分</option>
											          		<option value="3">3分</option>
											          		<option value="2">2分</option>
											          		<option value="1">1分</option>
													   </select>
											      	</div>
											      </div>
											      <div class="col-md-3 col-sm-3 col-xs-3">
											       <div class="input-group ">
											         <span class="input-group-addon">清晰度</span>
											           <select ID="feedbackClear" name="feedbackClear" class="form-control" > 
											          		<option>请选择</option>
											          		<option value="7">7分</option>
											          		<option value="6">6分</option>
											          		<option value="5">5分</option>
											          		<option value="4">4分</option>
											          		<option value="3">3分</option>
											          		<option value="2">2分</option>
											          		<option value="1">1分</option>
													   </select>
											      	</div>
											      </div>
											      <div class="col-md-3 col-sm-3 col-xs-3">
											       <div class="input-group ">
											         <span class="input-group-addon">互动</span>
											           <select ID="feedbackInteraction" name="feedbackInteraction" class="form-control" > 
											          		<option>请选择</option>
											          		<option value="8">8分</option>
											          		<option value="7">7分</option>
											          		<option value="6">6分</option>
											          		<option value="5">5分</option>
											          		<option value="4">4分</option>
											          		<option value="3">3分</option>
											          		<option value="2">2分</option>
											          		<option value="1">1分</option>
													   </select>
											      	</div>
											      </div>
										   </div>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
									 <div class="input-group col-md-12 col-sm-12 col-xs-12">
								         <span class="input-group-addon">文字<br>评价</span>
								         <textarea id="feedbackOther" name="feedbackOther" maxlength="255" style="height:70px;" class="form-control" ></textarea>
								      </div>
								    </td>
								</tr>
								<tr class="trClass">
									<td>
									 <div class="input-group col-md-12 col-sm-12 col-xs-12">
								         <span class="input-group-addon">审核<br>备注</span>
								         <textarea id="feedbackRemark" name="feedbackRemark" maxlength="255" style="height:70px;" class="form-control" ></textarea>
								      </div>
								    </td>
								</tr>
								<tr class="trClass">
									<td>
									 <div class="trClass col-md-2 col-sm-2 col-xs-2">
									 	 <button type="submit" class="btn btn-primary" >提交反馈</button>
									 </div>
									  <div class="col-md-2 col-sm-2 col-xs-2">
									 	 <button class="btn btn-danger" type="button" onclick="a_end()">活动取消</button>
									 </div>
									   <div class="col-md-2 col-sm-2 col-xs-2">
									 	 <button class="btn btn-warning" type="button" onclick="h_update()">修改医生</button>
									 </div>
								</tr>
								<tr class="a_end">
									<td>
										<div class="input-group col-md-12 col-sm-12 col-xs-12">
								         <span class="input-group-addon">取消<br>说明</span>
								         <textarea id="feedRemark"  maxlength="255" style="height:70px;" class="form-control" ></textarea>
								      </div>
									</td>
								</tr>
								<tr class="a_end">
									<td>
										<div class="input-group col-md-12 col-sm-12 col-xs-12">
										 <button  type="button" class="btn btn-primary" onclick="a_end_submit()">提交</button>
								      </div>
									</td>
								</tr>
								<tr class="h_update">
									<td>
										<div class="input-group col-md-4 col-sm-4 col-xs-4">
								         <span class="input-group-addon">医生编号</span>
								          <input type="text" id="hcpId1"  placeholder="必填项" class="form-control" onblur="verify()">
								      </div><label id="label_hcp" style="color:red;"></label>
									</td>
								</tr>
								
								<tr class="h_update">
									<td>
										<div class="input-group col-md-4 col-sm-4 col-xs-4">
								         <span class="input-group-addon">微信名</span>
								          <input type="text" id="hcpWxName1"  placeholder="必填项" class="form-control">
								      </div>
									</td>
								</tr>
								<tr class="h_update">
									<td>
										<div class="input-group col-md-4 col-sm-4 col-xs-4">
								         <span class="input-group-addon">微信昵称</span>
								          <input type="text" id="hcpWxNick1"  placeholder="必填项" class="form-control">
								      </div>
									</td>
								</tr>
								<tr class="h_update">
									<td>
										<div class="input-group col-md-12 col-sm-12 col-xs-12">
										 <button  type="button" class="btn btn-primary" onclick="h_update_submit()">提交</button>
								      </div>
									</td>
								</tr>
							</tbody>
						</table><hr>
						</form>
						</div>
					</div>
				</div>
			</div>
			<!--inner block end here-->
			<!--copy rights start here-->
			<div class="copyrights">
				<jsp:include page="/menu/foot.jsp"></jsp:include>
			</div>
			<!--COPY rights end here-->
	</div>
	<!--slider menu-->
	<div class="sidebar-menu">
		<div class="logo">
			<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span>
			</a> <a href="#"> <span id="logo"></span> <!--<img id="logo" src="" alt="Logo"/>-->
			</a>
		</div>
		<div class="menu">
						<ul id="menu">
				<li id="menu-home"><a href="<%=path%>/staff"> <i
						class="fa fa-home"></i> <span>业务员</span></a></li>
				<li ><a><i class="fa fa-user-md"></i><span>HCP管理</span><span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hcp/staff/add">HCP建档申请</a></li>
						<li><a href="<%=path%>/hcp/staff/view">HCP列表</a></li>
					</ul></li>
				<li class="active"><a href="<%=path%>/activity/staff/view">
					<i class="fa fa-leaf"></i><span>活动管理</span></a></li>
				<li><a href="<%=path%>/hospital/view"><i class="fa fa-hospital-o"></i><span>查看医院</span> 
					<%-- <span class="fa fa-angle-right" style="float: right"></span></a>
						<ul>
							<li><a href="<%=path%>/hospital/view">查看列表</a></li>
							<li><a href="<%=path%>/hospital/add">添加医院</a></li>
						</ul> 
					--%>
				</li>
				<li ><a href="<%=path%>/contract/staff/view"><i class="fa  fa-file-text"></i><span>合同管理</span> </a></li>
			</ul>
		</div>
	</div>
	<div class="clearfix"></div>
	</div>
	<!--slide bar menu end here-->

	<!-- 添加附件模态框 -->
	<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
		aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">添加附件
						<font size="2.5" color="blue">&nbsp;&nbsp;附件仅支持jpg,png,jpeg,gif类型的图片</font>
					</h4>
					
				</div>
				<div class="modal-body">
					<div id="zyupload" class="zyupload">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"
						onclick="save()">确定</button>
				</div>
			</div>
		</div>
	</div>


	<script>
		var toggle = true;

		$(".sidebar-icon").click(
				function() {
					if (toggle) {
						$(".page-container").addClass("sidebar-collapsed")
								.removeClass("sidebar-collapsed-back");
						$("#menu span").css({
							"position" : "absolute"
						});
					} else {
						$(".page-container").removeClass("sidebar-collapsed")
								.addClass("sidebar-collapsed-back");
						setTimeout(function() {
							$("#menu span").css({
								"position" : "relative"
							});
						}, 400);
					}
					toggle = !toggle;
				});
	</script>
	<!--scrolling js-->
	<script src="<%=path%>/js/jquery.nicescroll.js"></script>
	<script src="<%=path%>/js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="<%=path%>/js/bootstrap.js"></script>
	<script src="<%=path%>/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="<%=path%>/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=path%>/js/jedate/jedate.js"></script>
	<script type="text/javascript" 	src="<%=path %>/js/zyupload/zyupload-1.0.0.min.js"></script>
	<script type="text/javascript">
  jeDate({
		dateCell:"#dateinfo1",
		format:"YYYY-MM-DD hh:mm:ss",
		isinitVal:false,
		isTime:true //isClear:false,
	});
  jeDate({
		dateCell:"#dateinfo2",
		format:"YYYY-MM-DD hh:mm:ss",
		isinitVal:false,
		isTime:true //isClear:false,
	});
	var filePath = "",allPath="";
	$(document).ready(function() {
	  var id = '${id}';
			$("#input_id").val(id);
		     $("#trClass").hide();
		      $.ajax({
				   type: "POST",
				   url: "<%=path%>/activity/staff/show/"+id,
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				    $("#id").html(data.activity.id);
				      if(data.activity.state==0){
				     	$("#state").html("状态：<label>未反馈</label>");
					  }else if(data.activity.state==1){
					      $("#state").html("状态：<label >待审核</label>");
				     }else if(data.activity.state==2){
					      $("#state").html("状态：<label >已通过</label>");
				     }else if(data.activity.state==3){
					      $("#state").html("状态：<label >未通过</label>");
					      $("#table1").append("<tr><td><div class=\"col-md-12 col-sm-12 col-xs-12\">审核备注：<label>"+data.activity.feedbackRemark+"</lavel></div></td></tr>")
				     }else{
					       $("#state").html("状态：<label>活动取消</label>");
				     }
				     $("#actSubject1").html(data.activity.actSubject);
				     $("#groupId").html(data.activity.groupId);
				     $("#groupName").html(data.activity.groupName);
				     $("#hcpId").html(data.activity.hcpId);
				     $("#hcpName").html(data.activity.hcpName);
				     $("#hcpWxNick").html(data.activity.hcpWxNick);
				     $("#hcpWxName").html(data.activity.hcpWxName);
				   }
				 });
				  // 初始化插件
			$("#zyupload").zyUpload({
				width            : "650px",           // 宽度
				height           : "160px",           // 宽度
				itemWidth        : "80px",            // 文件项的宽度
				itemHeight       : "65px",            // 文件项的高度
				url              : "<%=path%>/uploadFile", // 上传文件的路径
				fileType         : [ "jpg", "png","jpeg","gif","JPG","PNG"], // 上传文件的类型
				fileSize         : 51200000,          // 上传文件的大小
				multiple         : true,            // 是否可以多个文件上传
				dragDrop         : false,            // 是否可以拖动上传文件
				tailor           : false,            // 是否可以裁剪图片
				del              : false,             // 是否可以删除文件
				finishDel        : false,            // 是否在上传文件完成后删除预览
				/* 外部获得的回调接口 */
				onSelect : function(selectFiles, allFiles) { // 选择文件的回调方法  selectFile:当前选中的文件  allFiles:还没上传的全部文件
				},
				onDelete : function(file, files) { // 删除一个文件的回调方法 file:当前删除的文件  files:删除之后的文件
				},
				onSuccess : function(file, response) {
					 fileName = file.name;// 文件上传成功的回调方法
					if (file.name.indexOf("+") >= 0) {
						fileName = file.name.replace("+", "_");
					}
					if (filePath == "") {
					    filePath = fileName;
					    allPath = response;
						$('#label_filepath').html(filePath);
					} else {
						filePath += "<br>" + fileName;
						allPath += "||" + response;
						$('#label_filepath').html(filePath);
					}
					
				},
				onFailure : function(file, response) { // 文件上传失败的回调方法
				},
				onComplete : function(response) { // 上传完成的回调方法
				}
			});
		});
		function add(hcpId){
			 window.open("<%=path%>/contract/staff/add/"+hcpId);
		}
		function save(){
		      $.ajax({  //绑定数据
			   type: "POST",
			   url: "<%=path%>/upload/save",
			   data:"allPath="+allPath,
			   dataType:"text",  
		       error:function(e) { 
					alert("保存文件出错，请重试！"); 
				},
			   success: function(data){
			      $("#fileId").val(data);
			   }
			});
		};
		function check(){
		/* 	if($("#contractId").html()=="尚未签订合同"){
				alert("请先添加合同信息");
				return false;
			} */
		    var date1 = $('#dateinfo1').val();
			  var date2 = $('#dateinfo2').val();
			  if(date1==null||date1==""){
			  	alert("活动实际开始时间不能为空！");
			  	return false;
			  }
			  if(date2==null||date2==""){
			  	alert("活动实际结束时间不能为空！");
			  	return false;
			  }
			  if(date1>=date2){
				  alert("活动开始时间必须晚于结束时间！");
				  return false;
			  }
			  var fileId = $("#fileId").val();
			  if(fileId==null||fileId==""){
			    alert("必须上传活动照片");
			    return false;
			  } 
			  if($("#feedbackPro").val()=="请选择"){
			    alert("反馈评价中的专业度尚未选择");
			    return false;
			  }
			   if($("#feedbackFull").val()=="请选择"){
			    alert("反馈评价中的丰富度尚未选择");
			    return false;
			  }
			   if($("#feedbackClear").val()=="请选择"){
			    alert("反馈评价中的清晰度尚未选择");
			    return false;
			  }
			   if($("#feedbackInteraction").val()=="请选择"){
			    alert("反馈评价中的互动尚未选择");
			    return false;
			  }else{
			  	//  $(".h_update input").removeAttr("name");
				  return true;
			  }
		};
		$(".a_end").hide();
		$(".h_update").hide();
		function a_end(){
			$(".a_end").show();
			$(".trClass").hide();
		};
		function a_end_submit(){
			var id = $("#input_id").val();
			var feedRemark = $("#feedRemark").val();
			if(feedRemark==null){
				alert("活动取消原因必须填写！")
			}else{
				 $.ajax({  //绑定数据
				   type: "POST",
				   url: "<%=path%>/activity/staff/update",
				   data:"id="+id+"&feedRemark="+feedRemark+"&state=4",
				   dataType:"json",  
				   success: function(data){
				     if(data.count==1){
				     	alert("提交成功！");
				     	window.close();
				     }else{
				    	 alert("提交失败！");
				     }
				   }
				});
			}
		}
		function h_update(){
			$(".h_update").show();
			$(".trClass").hide();
		};
		function h_update_submit(){
			var id = $("#input_id").val();
			var hcpId = $("#hcpId1").val();
			var hcpWxName = $("#hcpWxName1").val();
			var hcpWxNick = $("#hcpWxNick1").val();
			if(hcpId==null){
				alert("医生ID不能为空！");
				return false;
			}else if($("#label_hcp").val()=="该医生不存在"){
				alert("医生编号有误！");
				return false;
			}else if(hcpWxName==null){
				alert("微信名不能为空！");
				return false;
			}else if(hcpWxNick==null){
				alert("微信昵称不能为空！");
				return false;
			} else{
				 $.ajax({  //绑定数据
				   type: "POST",
				   url: "<%=path%>/activity/staff/update",
				   data:"id="+id+"&hcpId="+hcpId+"&hcpWxName="+hcpWxName+"&hcpWxNick="+hcpWxNick,
				   dataType:"json",  
				   success: function(data){
				     if(data.count==1){
				     	alert("提交成功！");
				     	window.close();
				     }else{
				    	 alert("提交失败！");
				     }
				   }
				});
			}
		};
		function verify(){
		var hcpId = $('#hcpId1').val();
		if(hcpId!="")
		  $.ajax({  //获取HCP级别
			   type: "POST",
			   url: "<%=path%>/hcp/staff/verify/hcpId",
			   data:"hcpId="+hcpId,
			   dataType:"json",
			   success: function(data){
				   if(data.flag==true){
				      $('#label_hcp').html("");
				   }else{
				    $('#label_hcp').html("该医生不存在");
				   }
			   }
			});
	};
	</script>
	<!-- mother grid end here-->
</body>
</html>
