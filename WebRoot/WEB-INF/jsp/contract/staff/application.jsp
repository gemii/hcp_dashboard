<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>
<html>
<head>
<link rel="SHORTCUT ICON" href="<%=path%>/image/index/logo.ico" type="image/x-icon" >
<title>HCP-业务员</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all">
<link href="<%=path%>/css/dataTables.bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link type="text/css" rel="stylesheet" href="<%=path %>/js/jedate/skin/jedate.css">
<!-- Custom Theme files -->
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css"
	media="all" />
<!--js-->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="<%=path%>/css/font-awesome.css" rel="stylesheet">

<!--skycons-icons-->
<script src="<%=path%>/js/skycons.js"></script>
<!--//skycons-icons-->
</head>
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main" style="z-index:999;">
					<div class="header-left">
						<div class="logo-name">
							<a href="<%=path%>"> <!-- <h1>KPI Of College</h1> --> <img
								id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" />
							</a>
						</div>

					</div>
					<div class="header-right">
						<div class="profile_details_left">
							<div class="clearfix"></div>
						</div>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img"> 
											</span>
											<div class="user-name">
												<p>${sessionScope.user.name }</p>
												<span>${sessionScope.user.type }</span>
											</div>
											<i class="fa fa-angle-down lnr"></i> <i
												class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
								</a>
									<ul class="dropdown-menu drp-mnu">
										<li><a href="<%=path%>/user/quit"><i
												class="fa fa-sign-out"></i> 退出</a></li>
									</ul></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script>
					$(document).ready(function() {
						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});

					});
				</script>
				<!-- /script-for sticky-nav -->
				<!--inner block start here-->
				<div class="inner-block" style="min-height:570px;overflow-y :auto;">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">添加合同</h3>
						</div>
						<div class="panel-body">
						<form action="<%=path%>/contract/staff/save" method="post" onsubmit="return check()">
							<table class="table">
								<tbody>
									<tr>
										<td>
											<div class="col-md-6 col-sm-6 col-xs-6">
												医生：<label id="hcpName"></label>
												<input id="hcpId" name="hcpId" type="hidden"/>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-6">
												手机号码：<label id="hcpPhone"></label>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="col-md-12 col-sm-12 col-xs-12">
												所在医院：<label id="hsptName"></label>
											</div>
										</td>
									</tr>
									<tr>
									    <td>
									    	 <div  class="input-group col-md-6 col-sm-6 col-xs-68" style="height:30px;">
									             <span class="hcpClass input-group-addon">签订公司</span>
									                <select id="thirdParty" name="tpId" class="form-control" > 
									               
												      </select>
											</div>
									    </td>
									</tr>
									<tr>
										<td>
											  <div id="div_11" class="input-group col-md-6 col-sm-6 col-xs-6">
										         <span id="span_title" class="input-group-addon">快递公司名称</span>
										           <select ID="expressNameS" name="expressName" class="form-control" onchange="express_change()" > 
										           		<option>邮政EMS</option>
										           		<option>圆通快递</option>
										           		<option>申通快递</option>
										           		<option>中通快递</option>
										           		<option>百世快递</option>
										           		<option>顺丰快递</option>
										           		<option>韵达快递</option>
										           		<option>天天快递</option>
										           		<option>德邦物流</option>
										           		<option>全峰快递</option>
										           		<option>宅急送</option>
										           		<option>优速物流</option>
										           		<option>嘉里大通物流</option>
										           		<option>全一快递</option>
										           		<option>民航快递</option>
										           		<option >其他</option>
							   				       </select>
										      </div> 
										      <br>
											  <div id="div_express" class="input-group col-md-6 col-sm-6 col-xs-6">
										         <span class="input-group-addon">快递单号</span>
										         <input id="expressId" type="text" name="expressId" class="form-control" placeholder="必填项" required="required">
										      </div><br>
										      <div class="input-group col-md-6 col-sm-6 col-xs-6">
										         <span class="input-group-addon">合同份数</span>
										         <input type="number" name="contractNumber" min="1" class="form-control" placeholder="必填项" required="required">
										      </div><br>
										      	<div class="input-group col-md-6 col-sm-6 col-xs-6">
													<span class=" input-group-addon">合同生效时间：</span>
													<input class="form-control datainp" id="dateinfo1" type="text" 
													placeholder="点击选择时间" readonly name="startTime" required="required"  >
												</div><br>
												<div class="input-group col-md-6 col-sm-6 col-xs-6">
													<span class=" input-group-addon">合同失效时间：</span>
													<input class=" form-control datainp" id="dateinfo2" type="text" 
													placeholder="点击选择时间" readonly name="endTime"  required="required">
												</div></br>
												  <div id="remark" class="input-group col-md-6 col-sm-6 col-xs-6">
											         <span class="input-group-addon">备注</span>
											         <textarea  name="remark" maxlength="255" style="height:70px;" class="form-control" ></textarea>
											      </div><br>
												<div class="col-md-3 col-sm-3 col-xs-3">
											        <button type="submit" class="btn btn-primary" >提交</button>
											     </div>
										</td>
									</tr>
								</tbody>
							</table>
							</form>
					   </div>
					   </div>
				</div>
			</div>
			<!--inner block end here-->
			<!--copy rights start here-->
			<div class="copyrights">
				<jsp:include page="/menu/foot.jsp"></jsp:include>
			</div>
			<!--COPY rights end here-->
	</div>
	<!--slider menu-->
	<div class="sidebar-menu">
		<div class="logo">
			<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span>
			</a> <a href="#"> <span id="logo"></span> <!--<img id="logo" src="" alt="Logo"/>-->
			</a>
		</div>
		<div class="menu">
						<ul id="menu">
				<li id="menu-home"><a href="<%=path%>/staff"> <i
						class="fa fa-home"></i> <span>业务员</span></a></li>
				<li ><a><i class="fa fa-user-md"></i><span>HCP管理</span><span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hcp/staff/add">HCP建档申请</a></li>
						<li><a href="<%=path%>/hcp/staff/view">HCP列表</a></li>
					</ul></li>
				<li ><a href="<%=path%>/activity/staff/view">
					<i class="fa fa-leaf"></i><span>活动管理</span></a></li>
				<li><a href="<%=path%>/hospital/view"><i class="fa fa-hospital-o"></i><span>查看医院</span> 
					<%-- <span class="fa fa-angle-right" style="float: right"></span></a>
						<ul>
							<li><a href="<%=path%>/hospital/view">查看列表</a></li>
							<li><a href="<%=path%>/hospital/add">添加医院</a></li>
						</ul> 
					--%>
				</li>
				<li class="active"><a href="<%=path%>/contract/staff/view"><i class="fa  fa-file-text"></i><span>合同管理</span> </a></li>
			</ul>
		</div>
	</div>
	<div class="clearfix"></div>
	</div>
	<!--slide bar menu end here-->

	<script>
		var toggle = true;

		$(".sidebar-icon").click(
				function() {
					if (toggle) {
						$(".page-container").addClass("sidebar-collapsed")
								.removeClass("sidebar-collapsed-back");
						$("#menu span").css({
							"position" : "absolute"
						});
					} else {
						$(".page-container").removeClass("sidebar-collapsed")
								.addClass("sidebar-collapsed-back");
						setTimeout(function() {
							$("#menu span").css({
								"position" : "relative"
							});
						}, 400);
					}
					toggle = !toggle;
				});
	</script>
	<!--scrolling js-->
	<script src="<%=path%>/js/jquery.nicescroll.js"></script>
	<script src="<%=path%>/js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="<%=path%>/js/bootstrap.js"></script>
	<script src="<%=path%>/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="<%=path%>/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script type="text/javascript" src="<%=path%>/js/jedate/jedate.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$("#hcpId").val('${hcpId}');
		$("#hcpName").html('${hcpName}');
		$("#hcpPhone").html('${hcpPhone}');
		$("#hsptName").html('${hsptName}');
		 $.ajax({
		   type: "POST",
		   url: "<%=path%>/user/getUser/",
		   dataType:"json",
		   success: function(data){
		   	 $(data).each(function(index,item){
		   	 	$("#thirdParty").append("<option value=\""+item.id+"\">"+item.name+"</option>");
		   	 });
		   }
		 });
	 });
	 var dd = '${flag}';
	 if(dd.length>5){
	 		jeDate({
				dateCell:"#dateinfo1",
				format:"YYYY-MM-DD",
				isinitVal:false,
				isTime:false, //isClear:false,
				maxDate:dd
			});
	 }else{
	 	jeDate({
				dateCell:"#dateinfo1",
				format:"YYYY-MM-DD",
				isinitVal:false,
				isTime:false
			});
	 }

		  jeDate({
				dateCell:"#dateinfo2",
				format:"YYYY-MM-DD",
				isinitVal:false,
				isTime:false, //isClear:false,
				okfun:function(val){}
			});
		function check(){
			  var date1 = $('#dateinfo1').val();
			  var date2 = $('#dateinfo2').val();
			  if(date1==null||date1==""){
			  	alert("合同生效日期不能为空！");
			  	return false;
			  }
			  if(date2==null||date2==""){
			  	alert("合同失效日期不能为空！");
			  	return false;
			  }
			  if(date1>=date2){
				  alert("生效时间必须晚于失效时间！");
				  return false;
			  }else{
				  return true;
			  }
		};
		function express_change(){
		   if($("#expressNameS").val()=="其他"){
		 	$("#div_11").html("  <span id=\"span_title\" class=\"input-group-addon\">快递公司名称</span> <input id=\"expressNameI\" type=\"text\" name=\"expressName\"  class=\"form-control\" placeholder=\"必填项\" required=\"required\">");
		   }
		};
	</script>
	<!-- mother grid end here-->
</body>
</html>
