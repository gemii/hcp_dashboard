<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML>
<html>
<head>
<link rel="shortcut icon" href="<%=path%>/image/index/logo.ico">
<title>HCP-审核员</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all">
<!-- Custom Theme files -->
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css"
	media="all" />
<!--js-->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="<%=path%>/css/font-awesome.css" rel="stylesheet">

<!--skycons-icons-->
<script src="<%=path%>/js/skycons.js"></script>
<!--//skycons-icons-->

</head>
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main">
					<div class="header-left">
						<div class="logo-name">
							<a href="<%=path%>"> <!-- <h1>KPI Of College</h1> -->
								<img id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" />
							</a>
						</div>

					</div>
					<div class="header-right">
						<div class="profile_details_left">
							<!--notifications of menu start -->
							<div class="clearfix"></div>
						</div>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img">
												</span>
												<div class="user-name">
												  <p>${sessionScope.user.name }</p>
												  <span>${sessionScope.user.type }</span>
												</div>
											<i class="fa fa-angle-down lnr"></i> <i
												class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
								</a>
									<ul class="dropdown-menu drp-mnu">
										<li><a href="<%=path%>/user/quit"><i class="fa fa-sign-out"></i> 退出</a></li>
									</ul></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script>
					$(document).ready(function() {
						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});

					});
				</script>
				<!-- /script-for sticky-nav -->
				<!--inner block start here-->
				<div class="inner-block" style="min-height:560px">
						<!-- 合同基础信息 -->
					 <div class="panel panel-info">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" 
					          href="#collapseOne">
					          合同基础信息
					        </a>
					      </h4>
					    </div>
					    <div id="collapseOne" class="panel-collapse collapse in">
					      <div class="panel-body">
							<table class="table">
							<tbody>
								<tr class="trClass">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											合同ID：<label id="id"></label>
										</div>
										<div id="div_state" class="col-md-6 col-sm-6 col-xs-6">
												状态：<label id="contractState"></label>&nbsp;
											</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											医生姓名：<label id="hcpName"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											业务员：<label id="staffName"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											提交时间：<label id="ctime"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											签订公司：<label id="tp_name"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											生效日期：<label id="contractStartTime"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											失效日期：<label id="contractEndTime"></label>
										</div>
									</td>
								</tr>
						<!-- 		<tr>
									<td>
										<div class="panel panel-default col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        合同图片
										      </h3>
										   </div>
										   <div id="fileId"  class="panel-body">
										   </div>
										</div>
									</td>
								</tr> -->
						
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											合同运送方式：<label id="type"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											份数：<label id="contractNumber"></label>
										</div>
									</td>
								</tr>
								<tr id="trClass">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											快递公司：<label id="expressName"></label>
										</div>
										<div id="div_express" class="col-md-6 col-sm-6 col-xs-6">
											快递单号：<label id="expressId"></label>
										</div>
									</td>
								</tr>
								<tr id="tr_receive">
									<td>
										<div id="div_receive" class="col-md-6 col-sm-6 col-xs-6">
											
										</div>
										<div id="div_access" class="col-md-6 col-sm-6 col-xs-6">
											
										</div>
									</td>
								</tr>
								<tr id="tr_feedMark">
									<td>
										<div id="div_feedMark" class="col-md-12 col-sm-12 col-xs-12">
											审核备注：<label id="feedMark"></label>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						</div>
					    </div>
					  </div><br>
					  <!-- 合同基础信息 -->
					  
					  <!-- 合同签章信息 -->
					  	<div id="third_part" class="panel panel-warning">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" 
					          href="#collapseTwo">
					          合同签章信息
					        </a>
					      </h4>
					    </div>
					    <div id="collapseTwo" class="panel-collapse collapse in">
					      <div class="panel-body">
					       <form action="<%=path %>/contract/admin/last" method="post" >
					      	<table class="table">
							<tbody>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											提交时间：<label id="ctime1"></label>
										</div>
									</td>
								</tr>
						
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											合同运送方式：<label id="type1"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											份数：<label id="contractNumber1"></label>
										</div>
									</td>
								</tr>
								<tr id="trClass1">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											快递公司：<label id="expressName1"></label>
										</div>
										<div id="div_express" class="col-md-6 col-sm-6 col-xs-6">
											快递单号：<label id="expressId1"></label>
										</div>
									</td>
								</tr>
								<tr id="tr_receive1">
									<td>
										<div id="div_receive1" class="col-md-6 col-sm-6 col-xs-6">
											
										</div>
										<div id="div_access1" class="col-md-6 col-sm-6 col-xs-6">
											
										</div>
									</td>
								</tr>
								<tr id="tr_feedMark1">
									<td>
										<div id="div_feedMark1" class="col-md-12 col-sm-12 col-xs-12">
											备注：<label id="feedMark1"></label>
										</div>
									</td>
								</tr>
								<tr class="tr_receive">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											上传签章时间：<label id="ctime2"></label>
										</div>
									</td>
								</tr>
								<tr class="tr_receive">
									<td>
										<div class="panel panel-default col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        合同签章图片
										      </h3>
										   </div>
										   <div id="fileId2"  class="panel-body">
										   </div>
										</div>
									</td>
								</tr>
								<tr class="tr_receive">
									<td>
										签章备注：<label id="qz"></label>
									</td>
								</tr>
								<tr class="tr_last">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											是否通过：<label class="checkbox-inline">
												      <input type="radio" name="state" 
												         value="2" >是
												   </label>
												   <label class="checkbox-inline">
												      <input type="radio" name="state" 
												         value="3" checked="checked">否
												   </label>
												   <input id="thirdId" name="contractId" type="hidden" />
													<input id="contractId" name="ctId" type="hidden">
										</div>
									</td>
								</tr>
								<tr class="tr_last">
									<td>
										<div class="input-group col-md-6 col-sm-6 col-xs-6">
								         <span class="input-group-addon">终审<br>备注</span>
								         <textarea name="remark3" maxlength="255" style="height:70px;" class="form-control" ></textarea>
								      </div>
									</td>
								</tr>
								<tr class="tr_last">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
									 	 <button type="submit" class="btn btn-primary" >提交</button>
										</div>
									</td>
								</tr>
								<tr class="tr_last_show">
									<td>
										最终审核情况：<label id="lastFeed"></label>
									</td>
								</tr>
								<tr class="tr_last_show">
									<td>
										最终审核备注：<label id="lastRemark"></label>
									</td>
								</tr>
							</tbody>
						</table>
						</form>
					      </div>
					    </div>
					  </div><br>				  
				</div>
				</div>
				<!--inner block end here-->
				<!--copy rights start here-->
				<div class="copyrights">
					<jsp:include page="/menu/foot.jsp"></jsp:include>
				</div>
				<!--COPY rights end here-->
		
		</div>
		<!--slider menu-->
		<div class="sidebar-menu">
			<div class="logo">
				<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span>
				</a> <a href="#"> <span id="logo"></span> <!--<img id="logo" src="" alt="Logo"/>-->
				</a>
			</div>
			<div class="menu">
				<ul id="menu">
				<li id="menu-home"><a href="<%=path%>/admin"> <i
						class="fa fa-home"></i> <span>审核员</span></a></li>
				<li ><a><i class="fa fa-user-md"></i><span>HCP管理</span><span
							class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hcp/admin/verify">HCP审核</a></li>
						<li><a href="<%=path%>/hcp/admin/view">HCP列表</a></li>
					</ul></li>
				<li ><a href="<%=path%>/activity/admin/view">
					<i class="fa fa-leaf"></i><span>活动管理</span></a></li>
				<li><a><i class="fa fa-hospital-o"></i><span>医院管理</span> <span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hospital/view">查看列表</a></li>
						<li><a href="<%=path%>/hospital/add">添加医院</a></li>
					</ul></li>
				<li class="active"><a href="<%=path%>/contract/admin/view"><i class="fa  fa-file-text"></i><span>合同管理</span> </a></li>
				<li><a><i class="fa fa-jpy"></i><span>支付管理</span> <span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<%-- <li><a href="<%=path%>/payment/admin/view/5">付款管理</a></li> --%>
						<li><a href="<%=path%>/payment/admin/view/0">待支付</a></li>
						<li><a href="<%=path%>/payment/admin/view/1">已支付</a></li>
						<li><a href="<%=path%>/payment/admin/view/2">支付失败</a></li>
					</ul></li>
				<li ><a href="<%=path%>/group/admin/view"><i class="fa  fa-wechat"></i><span>微信群管理</span> </a></li>
			</ul>
			</div>
		</div>
		<div class="clearfix"></div>
		</div>
	</div>
	<!--slide bar menu end here-->
	<script>
		var toggle = true;

		$(".sidebar-icon").click(
				function() {
					if (toggle) {
						$(".page-container").addClass("sidebar-collapsed")
								.removeClass("sidebar-collapsed-back");
						$("#menu span").css({
							"position" : "absolute"
						});
					} else {
						$(".page-container").removeClass("sidebar-collapsed")
								.addClass("sidebar-collapsed-back");
						setTimeout(function() {
							$("#menu span").css({
								"position" : "relative"
							});
						}, 400);
					}
					toggle = !toggle;
				});
	</script>
	<!--scrolling js-->
	<script src="<%=path%>/js/jquery.nicescroll.js"></script>
	<script src="<%=path%>/js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="<%=path%>/js/bootstrap.js"></script>
    <script type="text/javascript">
	   $(document).ready(function(){
	   var id = '${id}';
	   		$("#button_add").hide();
			 $.ajax({
				   type: "POST",
				   url: "<%=path%>/contract/admin/show/"+id,
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				   
				    	$("#ctime").html(FormatDate(data.contract.ctime,1) );
						$("#id").html(id);
						$("#hcpName").html(data.contract.hcpName+"|"+data.contract.hcpId);
						$("#staffName").html(data.contract.staffName);
						$("#contractStartTime").html(FormatDate(data.contract.contractStartTime));
						$("#contractEndTime").html(FormatDate(data.contract.contractEndTime));
						$("#contractNumber").html(data.contract.contractNumber);
						$("#expressName").html(data.contract.expressName);
						$("#expressId").html(data.contract.expressId);
						$("#tp_name").html(data.tp_name);
						if(data.contract.type==1){
						    $("#trClass").hide();
						  	$("#type").html(data.contract.expressName);
						}else{
						     $("#type").html("快递｜物流");
							 $("#trClass").show();
						}
				     if(data.contract.state==0){
				       $("#contractState").html("待审核");
				     } else if(data.contract.state==1){
				       $("#contractState").html("待签章");
				     }else  if(data.contract.state==2||data.contract.state==6){
				       $("#contractState").html("未通过");
				     }else  if(data.contract.state==3){
				       $("#contractState").html("待上传");
				     }else  if(data.contract.state==4){
				       $("#contractState").html("待终审");
				     }else  if(data.contract.state==5){
				       $("#contractState").html("通过");
				       if(data.contract.contractEndTime<=new Date()){
				      	 $("#contractState").html("已失效");
				     	}
				     }
				     $("#tr_feedMark").hide();
				     if(data.contract.receive==1){
				        $("#div_receive").html("是否已收到合同：<label>是</label>");
				        $("#tr_feedMark").show();
				        $("#feedMark").html(data.contract.feedRemark);
				     } else{
				        $("#div_receive").html("是否已收到合同：<label>否</label>");
				     }
				     
				     // 合同签章信息
				     if(data.third.state==null){
				  		$("#third_part").hide();
				     }else{
				     $("#third_part").show();
				        $("#ctime1").html(FormatDate(data.third.ctime,1) );
				        $("#contractNumber1").html(data.third.contractCount);
				     	$("#expressName1").html(data.third.expressName);
						$("#expressId1").html(data.third.expressNumber);
						if(data.third.type==1){
						    $("#trClass1").hide();
						  	$("#type1").html(data.third.expressName);
						}else{
						     $("#type1").html("快递｜物流");
							 $("#trClass1").show();
						}
						   $(".tr_receive").hide();
						   $("#feedMark1").html(data.third.remark1);
					     if(data.third.receive==1){
					        $("#div_receive1").html("第三方公司是否已收到合同：<label>是</label>");
					          $(".tr_receive").show();
					        $("#feedMark1").html(data.third.remark1);
					        
					        $("#qz").html(data.third.remark2);
					        $("#ctime2").html(FormatDate(data.third.ftime,1) );
					        $("#fileId2").html("");
							$(data.file1).each(function(index,item){
					         var row = "";
					         if(index!=0)
					            row += "<hr>";
					        row += "<a href=\"<%=path%>/view/photo"+item.path+"/\" target='_blank' title='"+spitStringTitle(item.fileName)+"'>"+spitString(item.fileName)+"</a>";
					        $("#fileId2").append(row);
					     });
					         if(data.third.state==1){
					           $(".tr_last").show();
					           $(".tr_last_show").hide();
					           $("#thirdId").val(data.third.contractId);
					           $("#contractId").val(data.contract.id);
					        }else{
					       	   $(".tr_last").hide();
					       	   $(".tr_last_show").show();
					       	   if(data.third.state==2){
					       	   	 $("#lastFeed").html("通过");
					       	   }else if(data.third.state==3){
					       	   	 $("#lastFeed").html("未通过");
					       	   }
					       	    $("#lastRemark").html(data.third.remark3);
					        }
					     } else{
					        $(".tr_last").hide();
					        $(".tr_last_show").hide();
					        $("#div_receive1").html("第三方公司是否已收到合同：<label>否&nbsp;</label>");
					     }
				     }
				     
				   }
				 });
	   	});
    </script>
	<!-- mother grid end here-->
</body>
</html>
