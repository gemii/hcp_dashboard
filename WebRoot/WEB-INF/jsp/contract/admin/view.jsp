<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>
<html>
<head>
<link rel="SHORTCUT ICON" href="<%=path%>/image/index/logo.ico" type="image/x-icon" >
<title>HCP-审核员</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all">
<link href="<%=path%>/css/dataTables.bootstrap.min.css" type="text/css"	rel="stylesheet">
<!-- Custom Theme files -->
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css"	media="all" />
<!--js-->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="<%=path%>/css/font-awesome.css" rel="stylesheet">

<!--skycons-icons-->
<script src="<%=path%>/js/skycons.js"></script>
<!--//skycons-icons-->
</head>
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main" style="z-index:999;">
					<div class="header-left">
						<div class="logo-name">
							<a href="<%=path%>"> <!-- <h1>KPI Of College</h1> --> <img
								id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" />
							</a>
						</div>

					</div>
					<div class="header-right">
						<div class="profile_details_left">
							<div class="clearfix"></div>
						</div>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img"> 
											</span>
											<div class="user-name">
												<p>${sessionScope.user.name }</p>
												<span>${sessionScope.user.type }</span>
											</div>
											<i class="fa fa-angle-down lnr"></i> <i
												class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
								</a>
									<ul class="dropdown-menu drp-mnu">
										<li><a href="<%=path%>/user/quit"><i
												class="fa fa-sign-out"></i> 退出</a></li>
									</ul></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script>
					$(document).ready(function() {
						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});

					});
				</script>
				<!-- /script-for sticky-nav -->
				<!--inner block start here-->
				<div class="inner-block" style="min-height:570px;overflow-y :auto;">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">合同列表</h3>
						</div>
						<div class="panel-body">
							<div class="row clearfix ">
							 <div  class="input-group col-md-4 col-sm-4 col-xs-4" style="height:30px;">
					              <span class="hcpClass input-group-addon">选择公司名查询</span>
					                <select  id="tp_name1" class="hcpClass form-control" onChange="serach()" > 
								    </select>
							   </div>
							</div>
							<hr>
							<table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
								<thead>
									<tr>
										<!-- <th><input  type="checkbox" onclick="AllCheck_click(this)"/></th> -->
										<th>编号</th>
										<th>医生</th>
										<th>业务员</th>
										<th>提交时间</th>
										<!-- <th>生效日期</th> -->
										<th>失效日期</th>
										<th>状态</th>
										<th class="oper">操作</th>
									</tr>
								</thead>
								<tbody id="tbody">

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!--inner block end here-->
			<!--copy rights start here-->
			<div class="copyrights">
				<jsp:include page="/menu/foot.jsp"></jsp:include>
			</div>
			<!--COPY rights end here-->
	</div>
	<!--slider menu-->
	<div class="sidebar-menu">
		<div class="logo">
			<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span>
			</a> <a href="#"> <span id="logo"></span> <!--<img id="logo" src="" alt="Logo"/>-->
			</a>
		</div>
		<div class="menu">
			<ul id="menu">
				<li id="menu-home"><a href="<%=path%>/admin"> <i
						class="fa fa-home"></i> <span>审核员</span></a></li>
				<li ><a><i class="fa fa-user-md"></i><span>HCP管理</span><span
							class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hcp/admin/verify">HCP审核</a></li>
						<li><a href="<%=path%>/hcp/admin/view">HCP列表</a></li>
					</ul></li>
				<li ><a href="<%=path%>/activity/admin/view">
					<i class="fa fa-leaf"></i><span>活动管理</span></a></li>
				<li><a><i class="fa fa-hospital-o"></i><span>医院管理</span> <span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hospital/view">查看列表</a></li>
						<li><a href="<%=path%>/hospital/add">添加医院</a></li>
					</ul></li>
				<li class="active"><a href="<%=path%>/contract/admin/view"><i class="fa  fa-file-text"></i><span>合同管理</span> </a></li>
				<li><a><i class="fa fa-jpy"></i><span>支付管理</span> <span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<%-- <li><a href="<%=path%>/payment/admin/view/5">付款管理</a></li> --%>
						<li><a href="<%=path%>/payment/admin/view/0">待支付</a></li>
						<li><a href="<%=path%>/payment/admin/view/1">已支付</a></li>
						<li><a href="<%=path%>/payment/admin/view/2">支付失败</a></li>
					</ul></li>
				<li ><a href="<%=path%>/group/admin/view"><i class="fa  fa-wechat"></i><span>微信群管理</span> </a></li>
			</ul>
		</div>
	</div>
	<div class="clearfix"></div>
	</div>
	<!--slide bar menu end here-->
<!-- 合同审核 -->
<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="height: 570px; overflow:auto;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">合同信息详情</h4>
				</div>
				<div class="modal-body">
					<div class="panel-body">
					<form id="myform" action="<%=path%>/contract/admin/save" method="post" >
						<table class="table">
							<tbody>
								<tr>
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											医生：<label id="hcpName1"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											业务员：<label id="staffName1"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											合同编号：<label id="contractId1" ></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											签订公司：<label id="tp_name"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											提交时间：<label id="ctime1"></label>
										</div>
									</td>
								</tr>
								<tr class="trClass1">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											生效时间：<label id="contractStartTime1"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											失效时间：<label id="contractEndTime1"></label>
										</div>
									</td>
								</tr>
				<!-- 				<tr class="trClass1">
									<td>
										<div class="panel panel-default col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        合同图片
										      </h3>
										   </div>
										   <div id="fileId1"  class="panel-body">
										   </div>
										</div>
									</td>
								</tr> -->
								<tr class="trClass1">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											合同运送方式：<label id="type1"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											份数：<label id="contractNumber1"></label>
										</div>
									</td>
								</tr>
								<tr id="trClass1">
									<td>
										<div class="col-md-6 col-sm-6 col-xs-6">
											快递公司：<label id="expressName1"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											快递单号：<label id="expressId1"></label>
										</div>
									</td>
								</tr>
									<tr >
									<td>
										<div id="judge" class="col-md-12 col-sm-12 col-xs-12">
											是否收到：<label class="checkbox-inline">
												      <input type="radio" name="receive" 
												         value="1" onclick="is_receive()">是
												   </label>
												   <label class="checkbox-inline">
												      <input type="radio" name="receive" 
												         value="0" checked="checked" onclick="not_receive()">否
												   </label>
										</div>
									</td>
								</tr>
								<tr class="is_receive1">
									<td>
								    	<input id="contractId2" name="id" type="hidden">
										 <input id="input_receive" name="receive" type="hidden">
										<div class="col-md-6 col-sm-6 col-xs-6">
											是否通过：<label class="checkbox-inline">
												      <input type="radio" name="state" 
												         value="1" >是
												   </label>
												   <label class="checkbox-inline">
												      <input type="radio" name="state" 
												         value="2" checked="checked">否
												   </label>
										</div>
									</td>
								</tr>
								<tr class="is_receive1">
									<td>
									 <div class="input-group col-md-12 col-sm-12 col-xs-12">
								         <span class="input-group-addon">审核<br>备注</span>
								         <textarea id="feedRemark1" name="feedRemark" maxlength="255" style="height:70px;" class="form-control" ></textarea>
								      </div>
								     </td>
								</tr>
								<tr class="is_receive1">
									<td>
									 <div class="col-md-12 col-sm-12 col-xs-12">
									 	 <button type="submit" class="btn btn-primary" >提交合同审核</button>
									 </div>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="height: 550px; overflow:auto;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">发送第三方公司</h4>
				</div>
				<div class="modal-body">
					<div class="panel-body">
					<form action="<%=path%>/contract/admin/third" method="post">
					<table class="table">
							<tbody>
									<tr>
										<td>
											<div class="col-md-6 col-sm-6 col-xs-6">
												合同ID：<label id="contractIdL"></label>
												<input id="contractIdI" name="contractId" type="hidden"/>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-6">
												签订公司：<label id="tp_nameL"></label>
											</div>
										</td>
									</tr>
							
									<tr>
										<td>
										  <div id="div_11" class="input-group col-md-6 col-sm-6 col-xs-6">
									         <span id="span_title1" class="input-group-addon">快递公司名称</span>
									         <select ID="expressNameS" name="expressName" class="form-control" onchange="express_change()" > 
										           		<option>邮政EMS</option>
										           		<option>圆通快递</option>
										           		<option>申通快递</option>
										           		<option>中通快递</option>
										           		<option>百世快递</option>
										           		<option>顺丰快递</option>
										           		<option>韵达快递</option>
										           		<option>天天快递</option>
										           		<option>德邦物流</option>
										           		<option>全峰快递</option>
										           		<option>宅急送</option>
										           		<option>优速物流</option>
										           		<option>嘉里大通物流</option>
										           		<option>全一快递</option>
										           		<option>民航快递</option>
										           		<option >其他</option>
							   				       </select>
										      </div> 
									  	 </td>
									</tr>
									<tr id="div_express1" >
										<td>
										  <div class="input-group col-md-6 col-sm-6 col-xs-6">
									         <span class="input-group-addon">快递单号</span>
									         <input id="expressId2" type="text" name="expressNumber" class="form-control" required="required">
									      </div>
										   </td>
									</tr>
									<tr>
										<td>
									      <div class="input-group col-md-6 col-sm-6 col-xs-6">
									         <span class="input-group-addon">合同份数</span>
									         <input type="number" name="contractCount" min="1" class="form-control" required="required">
									      </div>
	   									</td>
									</tr>
									<tr>
										<td>
											  <div id="remark1" class="input-group col-md-6 col-sm-6 col-xs-6">
										         <span class="input-group-addon">备注</span>
										         <textarea  name="remark1" maxlength="255" style="height:70px;" class="form-control" ></textarea>
										      </div>
										   </td>
									</tr>
									<tr>
										<td>
											<div class="col-md-3 col-sm-3 col-xs-3">
										        <button type="submit" class="btn btn-primary" >提交</button>
										     </div>
										  </td>
										</tr>
								</tbody>
						 </table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	</div>
	<script>
		var toggle = true;

		$(".sidebar-icon").click(
				function() {
					if (toggle) {
						$(".page-container").addClass("sidebar-collapsed")
								.removeClass("sidebar-collapsed-back");
						$("#menu span").css({
							"position" : "absolute"
						});
					} else {
						$(".page-container").removeClass("sidebar-collapsed")
								.addClass("sidebar-collapsed-back");
						setTimeout(function() {
							$("#menu span").css({
								"position" : "relative"
							});
						}, 400);
					}
					toggle = !toggle;
				});
	</script>
	<!--scrolling js-->
	<script src="<%=path%>/js/jquery.nicescroll.js"></script>
	<script src="<%=path%>/js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="<%=path%>/js/bootstrap.js"></script>
	<script src="<%=path%>/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="<%=path%>/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script type="text/javascript">
	var table = null;
	$(document).ready(function() {
	table = $('#example').DataTable();
		$.ajax({
			   type: "POST",
			   url: "<%=path%>/user/getUser/",
			   dataType:"json",
			   error : function(e) {
						alert("数据加载失败，请刷新重试！");
					},
			   success: function(data){
			     	$(data).each(
							function(index,item) {
							  $("#tp_name1").append("<option value='" + item.id + "'>"+item.name+"</option>");
							}
						);
						 serach();
				   	}
			 }); 
		});
		function serach(){
			 $.ajax({
				   type: "POST",
				   url: "<%=path%>/contract/admin/list/"+$("#tp_name1").val(),
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				 	   table.destroy(false);
				   		$('#tbody').html("");
				 		 $(data.list).each(
								function(index,item) {
									var row = "<tr>";
									var flag=0;
									var str = "";
									if(item.state==0){
										str = "<td>待审核</td>";
									}else if(item.state==1){
									  	str = "<td>待签章</td>";
									  	flag = 1;
									 }else if(item.state==2||item.state==6){
										str = "<td>未通过</td>";
									}else if(item.state==3){
										str = "<td>待上传</td>";
									}else if(item.state==4){
										str = "<td>待终审</td>";
									}else if(item.state==5){
									  if(item.contractEndTime<=data.time){
									  	str = "<td>已失效</td>";
									  }
									  else{
									  	str = "<td>通过</td>";
									  }
									}
							/* 		if(flag==1){
										row += "<td><input class=\"Check\" type=\"checkbox\" abc=\""+item.id+"\"/></td>";
									}else{
									   row +="<td></td>";
									} */
									
									row += "<td>" + item.id + "</td>";
									row += "<td>" + item.hcpName + "</td>";
									row += "<td>" + item.staffName + "</td>";
									row += "<td>" + FormatDate(item.ctime,1) + "</td>";
									//row += "<td>" + FormatDate(item.contractStartTime,2) + "</td>";
									row += "<td>" + FormatDate(item.contractEndTime,2) + "</td>";
									row += str;
										
									row += "<td><button class=\"btn btn-primary btn-sm\" onclick=\"view('" + item.id + "')\">详情</button>&nbsp;";
									if(item.state==0){
										row += "<button class=\"btn btn-info btn-sm\" onclick=\"verify('" + item.id + "','"+item.staffName+"','"+item.hcpName+"')\">审核</button>";
									} if(flag==1){
										row += "<button class=\"btn btn-info btn-sm\" onclick=\"but_check('" + item.id + "')\">第三方</button>";
									} 
									row += "</td></tr>";

									$('#tbody').append(row);
								});
							table=$('#example').DataTable();
						}
				 }); 
		}
		function view(id){
		 window.open("<%=path%>/contract/admin/detail/"+id);
		};
		function verify(id,staffName,hcpName){
		$("#staffName1").html(staffName);
		$("#hcpName1").html(hcpName);
			   $.ajax({
				   type: "POST",
				   url: "<%=path%>/activity/admin/verify/contract/"+id,
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				    	$("#ctime1").html(data.ctime);
						$("#contractId1").html(id);
						$("#contractId2").val(id);
						$("#contractStartTime1").html(data.contractStartTime);
						$("#contractEndTime1").html(data.contractEndTime);
						$("#contractNumber1").html(data.contract.contractNumber);
						$("#expressName1").html(data.contract.expressName);
						$("#expressId1").html(data.contract.expressId);
						$("#tp_name").html(data.tp_name);
						if(data.contract.type==1){
						  $("#trClass1").hide();
						  	$("#type1").html(data.contract.expressName);
						}else{
							$("#type1").html("快递｜物流");
						}
				      $("#myModal1").modal('show');
				       $(".is_receive1").hide();
				   }
				 });
		};
		function is_receive(){
		 //$("#judge").html("确认已经收到合同文件，请填写审核结果：");
		 $(".is_receive1").show();
		 $("#input_receive").val("1");
		};
	/*	function not_receive(){
		};
		function AllCheck_click(obj){
			$(".Check").prop('checked', $(obj).prop('checked'));
		};
 	function button_click(){
		 var ids = $(".Check:checked").map(function(){return $(this).attr("abc");}).get().join(',');
		 if(ids==null||ids==""){
		   alert("请选择要操作的合同！");
		 }else{
		 	 but_check(ids);
		 }
		}; */
		function but_check(id){
			$("#contractIdL").html(id);
			$("#contractIdI").val(id);
			   $.ajax({
				   type: "POST",
				   url: "<%=path%>/contract/admin/getTPN/"+id,
				   dataType:"json",
				   success: function(data){
				     $("#tp_nameL").html(data.tp_name);
				   }
				});
			$("#myModal2").modal('show');
		};
		function express_change(){
		   if($("#expressNameS").val()=="其他"){
		 	  $("#div_11").html("  <span id=\"span_title\" class=\"input-group-addon\">快递公司名称</span> <input id=\"expressNameI\" type=\"text\" name=\"expressName\"  class=\"form-control\" placeholder=\"必填项\" required=\"required\">");
		   }
		};
	</script>
	<!-- mother grid end here-->
</body>
</html>
