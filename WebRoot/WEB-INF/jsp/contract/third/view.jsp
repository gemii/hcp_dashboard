<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>
<html>
<head>
<link rel="SHORTCUT ICON" href="<%=path%>/image/index/logo.ico" type="image/x-icon" >
<title>HCP-第三方</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all">
<link href="<%=path%>/css/dataTables.bootstrap.min.css" type="text/css"
	rel="stylesheet">
<!-- Custom Theme files -->
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css"
	media="all" />
<!--js-->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="<%=path%>/css/font-awesome.css" rel="stylesheet">
<!--skycons-icons-->
<script src="<%=path%>/js/skycons.js"></script>
<!--//skycons-icons-->
</head>
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main" style="z-index:999;">
					<div class="header-left">
						<div class="logo-name">
							<a href="<%=path%>"> <!-- <h1>KPI Of College</h1> --> <img
								id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" />
							</a>
						</div>

					</div>
					<div class="header-right">
						<div class="profile_details_left">
							<div class="clearfix"></div>
						</div>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img"> 
											</span>
											<div class="user-name">
												<p>${sessionScope.user.name }</p>
												<span>${sessionScope.user.type }</span>
											</div>
											<i class="fa fa-angle-down lnr"></i> <i
												class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
								</a>
									<ul class="dropdown-menu drp-mnu">
										<li><a href="<%=path%>/user/quit"><i
												class="fa fa-sign-out"></i> 退出</a></li>
									</ul></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script>
					$(document).ready(function() {
						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});

					});
				</script>
				<!-- /script-for sticky-nav -->
				<!--inner block start here-->
				<div class="inner-block" style="min-height:570px;overflow-y :auto;">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">合同列表</h3>
						</div>
						<div class="panel-body">
							<table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" >
								<thead>
									<tr>
										<th>ID</th>
										<th>医生</th>
										<th>业务员</th>
										<th>提交时间</th>
										<!-- <th>生效日期</th> -->
										<th>失效日期</th>
										<th>状态</th>
										<th class="oper">操作</th>
									</tr>
								</thead>
								<tbody id="tbody">

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!--inner block end here-->
			<!--copy rights start here-->
			<div class="copyrights">
				<jsp:include page="/menu/foot.jsp"></jsp:include>
			</div>
			<!--COPY rights end here-->
	</div>
	<!--slider menu-->
	<div class="sidebar-menu">
		<div class="logo">
			<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span>
			</a> <a href="#"> <span id="logo"></span> <!--<img id="logo" src="" alt="Logo"/>-->
			</a>
		</div>
		<div class="menu">
			<ul id="menu">
				<li id="menu-home"><a href="<%=path%>/third"> <i
						class="fa fa-home"></i> <span>第三方</span></a></li>
				<li class="active"><a href="<%=path%>/contract/third/view"><i class="fa  fa-file-text"></i><span>合同管理</span> </a></li>
				<li ><a><i class="fa fa-jpy"></i><span>支付管理</span> 
					<span class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/payment/third/view/0">待支付</a></li>
						<li><a href="<%=path%>/payment/third/view/1">支付成功</a></li>
						<li><a href="<%=path%>/payment/third/view/2">支付失败</a></li>
					</ul></li>
			</ul>
		</div>
	</div>
	<div class="clearfix"></div>
	</div>
	<!--slide bar menu end here-->
	<script>
		var toggle = true;

		$(".sidebar-icon").click(
				function() {
					if (toggle) {
						$(".page-container").addClass("sidebar-collapsed")
								.removeClass("sidebar-collapsed-back");
						$("#menu span").css({
							"position" : "absolute"
						});
					} else {
						$(".page-container").removeClass("sidebar-collapsed")
								.addClass("sidebar-collapsed-back");
						setTimeout(function() {
							$("#menu span").css({
								"position" : "relative"
							});
						}, 400);
					}
					toggle = !toggle;
				});
	</script>
	<!--scrolling js-->
	<script src="<%=path%>/js/jquery.nicescroll.js"></script>
	<script src="<%=path%>/js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="<%=path%>/js/bootstrap.js"></script>
	<script src="<%=path%>/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="<%=path%>/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script type="text/javascript">
	
	$(document).ready(function() {
		   $.ajax({
				   type: "POST",
				   url: "<%=path%>/contract/third/list/",
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				 		 $(data.list).each(
								function(index,item) {
									var row = "<tr>";
									var str = "";
									if(item.state==0){
										str = "<td>待审核</td>";
									}else if(item.state==1){
									  	str = "<td>待签章</td>";
									 }else if(item.state==2||item.state==6){
										str = "<td>未通过</td>";
									}else if(item.state==3){
										str = "<td>待上传</td>";
									}else if(item.state==4){
										str = "<td>待终审</td>";
									}else if(item.state==5){
									  if(item.contractEndTime<=data.time){
									  	str = "<td>已失效</td>";
									  }
									  else{
									  	str = "<td>通过</td>";
									  }
									}
									
									row += "<td>" + item.id + "</td>";
									row += "<td>" + item.hcpName + "</td>";
									row += "<td>" + item.staffName + "</td>";
									row += "<td>" + FormatDate(item.ctime,1) + "</td>";
									//row += "<td>" + FormatDate(item.contractStartTime,2) + "</td>";
									row += "<td>" + FormatDate(item.contractEndTime,2) + "</td>";
									row += str;
									row += "<td><button class=\"btn btn-primary btn-sm\" onclick=\"view('" + item.id + "')\">详情</button>&nbsp;";
									row += "</td></tr>";

									$('#tbody').append(row);
								});
							$('#example').dataTable({
				                "order": [[ 0, "desc" ]]
				              });
						}
				 }); 
		});
		function view(id){
		 window.open("<%=path%>/contract/third/detail/"+id);
		};
		
	</script>
	<!-- mother grid end here-->
</body>
</html>
