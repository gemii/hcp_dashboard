<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML>
<html>
<head>
<link rel="SHORTCUT ICON" href="<%=path%>/image/index/logo.ico" type="image/x-icon" >
<title id="title">HCP-业务员</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet"	type="text/css" media="all">
<link href="<%=path%>/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
<!-- Custom Theme files -->
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--js-->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="<%=path%>/css/font-awesome.css" rel="stylesheet">

<!--skycons-icons-->
<script src="<%=path%>/js/skycons.js"></script>
<!--//skycons-icons-->
</head>
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main" style="z-index:999;">
					<div class="header-left">
						<div class="logo-name">
							<a href="<%=path%>"> <!-- <h1>KPI Of College</h1> -->
								<img id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" />
							</a>
						</div>

					</div>
					<div class="header-right" >
						<div class="profile_details_left">
							<div class="clearfix"></div>
						</div>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img">
												</span>
												<div class="user-name">
												  <p>${sessionScope.user.name }</p>
												  <span>${sessionScope.user.type }</span>
												</div>
											<i class="fa fa-angle-down lnr"></i> <i
												class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
								</a>
									<ul class="dropdown-menu drp-mnu">
										<li><a href="<%=path%>/user/quit"><i class="fa fa-sign-out"></i> 退出</a></li>
									</ul></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script>
					$(document).ready(function() {
						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});

					});
				</script>
				<!-- /script-for sticky-nav -->
				<!--inner block start here-->
				<div class="inner-block" style="min-height: 570px; overflow:auto;">
				     	<div class="panel panel-info" >
							<div class="panel-heading">
								<h3 class="panel-title">医院列表</h3>
							</div>
							<div class="panel-body">
						   	  <div class="col-md-4 col-sm-4 col-xs-4"  >
						         <input type="text" id="hsptName" class="form-control" required="required" placeholder="请输入医院名">
						      </div>
						  	 <button type="button" class="btn btn-primary" onclick="chaxun()">查询</button>
						  	 <label id="label_tishi" hidden="hidden" style="color:red;"> 查询条件过于模糊，结果较多，请尝试精确查询条件。</label>
							    <hr><div id="loading" align="center" ></div>
								<div id="contentDIV">
				                   <div id="tishi" class="alert alert-danger" hidden="hidden" style="clear: both;">空空如也...</div>
									 <div id="table">
										 <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
										        <thead>
										            <tr>
										                <th >医院名称</th>
										                <th>医院代码</th>
										                <th>所在城市</th>
										                 <!-- <th>月新生数</th>  -->
										                <th>类型</th>
										                <!-- <th>等级</th> -->
										                <!-- <th>电话</th> -->
										                <!-- <th>状态</th> -->
										                <th class="oper">操作</th>
										            </tr>
										        </thead>
										        <tbody id="tbody">
													
										        </tbody>
										    </table>
									 </div> 
								 </div>
							</div>
						</div>
						</div>
				</div>
				<!--inner block end here-->
				<!--copy rights start here-->
				<div class="copyrights">
					<jsp:include page="/menu/foot.jsp"></jsp:include>
				</div>
				<!--COPY rights end here-->
		</div>
		<!--slider menu-->
		<div class="sidebar-menu">
			<div class="logo">
				<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span>
				</a> <a href="#"> <span id="logo"></span> <!--<img id="logo" src="" alt="Logo"/>-->
				</a>
			</div>
			<div class="menu">
				<ul id="menu">
				<li id="menu-home"><a href="<%=path%>/staff"> <i
						class="fa fa-home"></i> <span>业务员</span></a></li>
				<li ><a><i class="fa fa-user-md"></i><span>HCP管理</span><span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hcp/staff/add">HCP建档申请</a></li>
						<li><a href="<%=path%>/hcp/staff/view">HCP列表</a></li>
					</ul></li>
				<li ><a href="<%=path%>/activity/staff/view">
					<i class="fa fa-leaf"></i><span>活动管理</span></a></li>
				<li class="active"><a href="<%=path%>/hospital/view"><i class="fa fa-hospital-o"></i><span>查看医院</span> 
					<%-- <span class="fa fa-angle-right" style="float: right"></span></a>
						<ul>
							<li><a href="<%=path%>/hospital/view">查看列表</a></li>
							<li><a href="<%=path%>/hospital/add">添加医院</a></li>
						</ul> 
					--%>
				</li>
				<li ><a href="<%=path%>/contract/staff/view"><i class="fa  fa-file-text"></i><span>合同管理</span> </a></li>
			</ul>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<!--slide bar menu end here-->
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="height: 530px; overflow:auto;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">医院详细信息</h4>
				</div>
				<div class="modal-body">
					<div class="panel-body">
						<table class="table">
							<tbody>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											医院名称：<label id="hsptName1"></label>
										</div>
										<!-- <div id="state" class="col-md-4 col-sm-4 col-xs-4">
											
										</div> -->
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-5 col-sm-5 col-xs-5">
											医院代码：<label id="hsptCode"></label>
										</div>
										<!-- <div class="col-md-7 col-sm-7 col-xs-7">
											医院月新生数：<label id="hsptNbm"></label>
										</div> -->
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-5 col-sm-5 col-xs-5">
											所属大区：<label id="areaName"></label>
										</div>
										<div class="col-md-7 col-sm-7 col-xs-7">
											所在城市：<label id="cityName"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-5 col-sm-5 col-xs-5">
											医院类型：<label id="hsptType"></label>
										</div>
										<div class="col-md-7 col-sm-7 col-xs-7">
											医院等级：<label id="hsptLevel"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											医院官网：<label id="hsptUrl"></label>
										</div>
									</td>
								</tr>
								<tr>
								   <td>	
								   		<div class="col-md-12 col-sm-12 col-xs-12">
											医院地址：<label id="hsptAddress"></label>
										</div>	
										</td>
								</tr>
								<tr>
								   <td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											医院电话：<label id="hsptPhone"></label>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	
	<script>
		var toggle = true;

		$(".sidebar-icon").click(
				function() {
					if (toggle) {
						$(".page-container").addClass("sidebar-collapsed")
								.removeClass("sidebar-collapsed-back");
						$("#menu span").css({
							"position" : "absolute"
						});
					} else {
						$(".page-container").removeClass("sidebar-collapsed")
								.addClass("sidebar-collapsed-back");
						setTimeout(function() {
							$("#menu span").css({
								"position" : "relative"
							});
						}, 400);
					}
					toggle = !toggle;
				});
	</script>
	<!--scrolling js-->
	<script src="<%=path%>/js/jquery.nicescroll.js"></script>
	<script src="<%=path%>/js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="<%=path%>/js/bootstrap.js"></script>
	<script src="<%=path%>/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<%=path%>/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    var table = null;
	$(document).ready(function() {
		table = $('#example').DataTable();
    	$('#loading').hide();
		$('#contentDIV').hide();
		$("#title").html('${title}');
    });
    $.ajaxSetup({
 contentType: "application/x-www-form-urlencoded; charset=utf-8"
});
       function chaxun(){
         var hsptName = $("#hsptName").val();
         if(hsptName==null||$.trim(hsptName)==""){
           alert("请输入医院名进行查询！");
           return false;
         }
		   $.ajax({  //绑定数据
			   type: "POST",
			   url: "<%=path%>/hospital/list/"+encodeURI(hsptName),
			   dataType:"json",  
			   beforeSend:function(XMLHttpRequest){ 
				   $('#loading').show();
				   $('#contentDIV').hide();
		           $("#loading").html("<img src='<%=path%>/image/loading.gif' />"); 
		       },
		       error:function(e) { 
					alert("查询出错，请重试！"); 
					$('#loading').hide();
				},
			   success: function(data){
				   $('#contentDIV').show();
				   $('#loading').hide();
				   if(data==""){
					   $('#tishi').show();
					   $('#table').hide();
				   }else{
					   $('#table').show();
					   $('#tishi').hide();
				   }
				   table.destroy(false);
				   $('#tbody').html("");
				   $(data).each(function(index,item){
					   var row ="<tr>";
					   if(item.hsptUrl==""||item.hsptUrl==null){
					   	row +="<td><a title='"+item.hsptName+"'>";
					    if(item.hsptName.length>25){
							   row +=item.hsptName.substring(0,25) + "...";
						   }else{
							   row += item.hsptName;
						   } 
						row +="</a></td>";
					   }else{
					    row +="<td><a href='"+item.hsptUrl+"' target='_blank' title='"+item.hsptName+"'>";
					    if(item.hsptName.length>25){
							   row +=item.hsptName.substring(0,25) + "...";
						   }else{
							   row += item.hsptName;
						   } 
						row +="</a></td>";
					   }
					  
					   row +="<td>"+item.hsptCode +"</td>";
						row +="<td><a title='"+item.cityName+"' style='color:#333;'>"; 
						   if(item.cityName.length>20){
							   row +=item.cityName.substring(0,20) + "...";
						   }else{
							   row += item.cityName;
						   } 
						row +="</a></td>";
					 /*  row +="<td>"+item.hsptNbm +"</td>"; */
					   row +="<td>"+item.hsptType +"</td>";
					 /*   row +="<td>"+item.hsptLevel +"</td>";
					   row +="<td>"+item.hsptPhone +"</td>"; 
					   if(item.hsptVerify==1){
					      row +="<td>已验证</td>";
					   }else{
					      row +="<td>未验证</td>";
					   } */
					   row += "<td><button class=\"btn btn-primary btn-sm\" onclick=\"view('" + item.id + "')\">详情</button></td>";
						row +="</tr>";										
						 $('#tbody').append(row);
						 
						 if(index==9){
						     $('#label_tishi').show();
						   }
					});
					 table=$('#example').DataTable();
			   }
			});
	   };
	function view(id) {
		   $.ajax({
				   type: "POST",
				   url: "<%=path%>/hospital/show/"+id,
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				     $("#hsptName1").html(data.hsptName);
				      if(data.hsptVerify==0||data.hsptVerify==null){
				     	$("#state").html("状态:<label>未验证</label>");
					  }else if(data.hsptVerify==1){
					      $("#state").html("状态:<label >已验证</label>");
				     }
				     $("#hsptName").html(data.hsptName);
				     $("#hsptCode").html(data.hsptCode);
				     $("#cityName").html(data.cityName);
				     $("#areaName").html(data.areaName);
				     $("#staffId").html(data.staffId);
				     $("#hsptNbm").html(data.hsptNbm);
				     $("#hsptType").html(data.hsptType);
				     $("#hsptLevel").html(data.hsptLevel);
				     if(data.hsptUrl!=null&&data.hsptUrl!=""){
				     	$("#hsptUrl").html("<a href=\""+data.hsptUrl+"\" target='_blank'>"+data.hsptUrl+"</a>");
				     }
				     $("#hsptAddress").html(data.hsptAddress);
				     $("#hsptPhone").html(data.hsptPhone);
				     $("#myModal").modal('show');
				   }
				 }); 
		};
    </script>
	<!-- mother grid end here-->
</body>
</html>
