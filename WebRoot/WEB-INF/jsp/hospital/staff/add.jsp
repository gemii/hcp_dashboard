<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML>
<html>
<head>
<link rel="SHORTCUT ICON" href="<%=path%>/image/index/logo.ico" type="image/x-icon" >
<title id="title">HCP-业务员</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet"	type="text/css" media="all">
<link href="<%=path%>/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="<%=path %>/js/zyupload/skins/zyupload-1.0.0.min.css " type="text/css">
<!-- Custom Theme files -->
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--js-->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="<%=path%>/css/font-awesome.css" rel="stylesheet">

<!--skycons-icons-->
<script src="<%=path%>/js/skycons.js"></script>
<!--//skycons-icons-->
</head>
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main" style="z-index:999;">
					<div class="header-left">
						<div class="logo-name">
							<a href="<%=path%>"> <!-- <h1>KPI Of College</h1> -->
								<img id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" />
							</a>
						</div>

					</div>
					<div class="header-right" >
						<div class="profile_details_left">
							<div class="clearfix"></div>
						</div>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img">
												</span>
												<div class="user-name">
												  <p>${sessionScope.user.name }</p>
												  <span>${sessionScope.user.type }</span>
												</div>
											<i class="fa fa-angle-down lnr"></i> <i
												class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
								</a>
									<ul class="dropdown-menu drp-mnu">
										<li><a href="<%=path%>/user/quit"><i class="fa fa-sign-out"></i> 退出</a></li>
									</ul></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script>
					$(document).ready(function() {
						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});

					});
				</script>
				<!-- /script-for sticky-nav -->
				<!--inner block start here-->
				<div class="inner-block" style="min-height:700px">
				     	<form action="<%=path%>/hospital/save" method="post" onsubmit="return toVaild()">
				     	<div class="panel panel-info">
							<div class="panel-heading">
								<h3 class="panel-title">添加医院信息</h3>
							</div>
							<div class="panel-body">
							     <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">医院名称</span>
							         <input type="text" name="hsptName" class="form-control" placeholder="必填项" required="required">
							      </div></br>
							       <label id="label_hcp" style="color:red;"></label>
							       <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">医院代码</span>
							         <input type="text" id="hsptCode" name="hsptCode" class="form-control" placeholder="必填项" required="required" onblur="verify()">
							      </div></br>
							      <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">所属大区</span>
							         <select ID="s_areasId" name="areaId" class="form-control" > 
									 </select>
							      </div></br>
							       <div class="form-group" >
									<div class=" col-md-2 col-sm-2 col-xs-2">
								      请选择城市信息
								    </div></br> 
								      <div class=" col-md-3 col-sm-3 col-xs-3">
									     <select ID="s_province" name="province" class="form-control" onChange="province_change()" > 
									         <option> －省份－</option>
									      </select>
									  </div>
									  <div class=" col-md-3 col-sm-3 col-xs-3">
									      <select ID="s_city" name="city" class="form-control" onChange="city_change()"> 
									         <option>－城市－</option>
									      </select>
									  </div>
									  <div class=" col-md-3 col-sm-3 col-xs-3">
									      <select ID="s_area" name="cityId" class="form-control" > 
									         <option>－区域－</option>
									      </select>
								     </div>
								    </div> 
								   </br> </br> 
							       <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">医院月新生数</span>
							         <input type="number" name="hsptNbm"  min="0" step="1" class="form-control"  placeholder="必填项" required="required">
							      </div></br>
							      <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">医院类型</span>
							         <select ID="s_hsptType" name="hsptType" class="form-control" > 
									 </select>
							      </div></br>
							      <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">医院等级</span>
							           <select ID="s_hsptLevel" name="hsptLevel" class="form-control" > 
									      </select>
							      </div></br>
							      <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">医院官网</span>
							         <input type="text" name="hsptUrl" maxlength="50" class="form-control" >
							      </div></br>
							      <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">医院地址</span>
							         <input type="text" name="hsptAddress" maxlength="50" class="form-control">
							      </div></br>
							      <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">医院电话</span>
							         <input type="text" name="hsptPhone" maxlength="13" class="form-control">
							      </div></br>
							     <div class="col-md-2 col-sm-2 col-xs-2">
							        <button type="submit" class="btn btn-primary">提交</button>
							     </div>
							</div>
						</div>
						</form>
				</div>
				<!--inner block end here-->
				<!--copy rights start here-->
				<div class="copyrights">
					<jsp:include page="/menu/foot.jsp"></jsp:include>
				</div>
				<!--COPY rights end here-->
			</div>
			</div>
		<!--slider menu-->
		<div class="sidebar-menu">
			<div class="logo">
				<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span>
				</a> <a href="#"> <span id="logo"></span> <!--<img id="logo" src="" alt="Logo"/>-->
				</a>
			</div>
			<div class="menu">
				<ul id="menu">
				<li id="menu-home"><a href="#"> <i
						class="fa fa-home"></i> <span>业务员</span></a></li>
				<li ><a><i class="fa fa-user-md"></i><span>HCP管理</span><span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hcp/staff/add">HCP建档申请</a></li>
						<li><a href="<%=path%>/hcp/staff/view">HCP列表</a></li>
					</ul></li>
				<li ><a href="<%=path%>/activity/staff/view">
					<i class="fa fa-leaf"></i><span>活动管理</span></a></li>
				<li class="active"><a><i class="fa fa-hospital-o"></i><span>医院管理</span> <span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hospital/view">查看列表</a></li>
						<li><a href="<%=path%>/hospital/add">添加医院</a></li>
					</ul></li>
				<li ><a href="<%=path%>/contract/staff/view"><i class="fa  fa-file-text"></i><span>合同管理</span> </a></li>
			</ul>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<!--slide bar menu end here-->
	
	
	<script>
		var toggle = true;

		$(".sidebar-icon").click(
				function() {
					if (toggle) {
						$(".page-container").addClass("sidebar-collapsed")
								.removeClass("sidebar-collapsed-back");
						$("#menu span").css({
							"position" : "absolute"
						});
					} else {
						$(".page-container").removeClass("sidebar-collapsed")
								.addClass("sidebar-collapsed-back");
						setTimeout(function() {
							$("#menu span").css({
								"position" : "relative"
							});
						}, 400);
					}
					toggle = !toggle;
				});
	</script>
	<!--scrolling js-->
	<script src="<%=path%>/js/jquery.nicescroll.js"></script>
	<script src="<%=path%>/js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="<%=path%>/js/bootstrap.js"></script>
	<script type="text/javascript" 	src="<%=path %>/js/zyupload/zyupload-1.0.0.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
		$("#title").html('${title}');
		  $.ajax({  //绑定大区信息
		   type: "GET",
		   url: "<%=path%>/areas/getALl",
		   dataType:"json",
		   success: function(data){
			   $(data).each(function(index,el){
					 $('#s_areasId').append("<option value='"+el.areaId+"'>"+el.areaName+"</option>");
				});
		   }
		}); 
		
        $.ajax({  //绑定省份信息
		   type: "GET",
		   url: "<%=path%>/Info/city/1/0",
		   dataType:"json",
		   success: function(data){
			   $(data).each(function(index,el){
					 $('#s_province').append("<option value='"+el.id+"'>"+el.content+"</option>");
					 $('#s_city').html("<option>－城市－</option>");
				});
		   }
		}); 
		 $.ajax({  //绑定医院类型
		   type: "GET",
		   url: "<%=path%>/Info/city/2/0",
		   dataType:"json",
		   success: function(data){
			   $(data).each(function(index,el){
					 $('#s_hsptType').append("<option value='"+el.id+"'>"+el.content+"</option>");
				});
		   }
		});
		 $.ajax({  //绑定医院等级
		   type: "GET",
		   url: "<%=path%>/Info/city/3/0",
		   dataType:"json",
		   success: function(data){
			   $(data).each(function(index,el){
					 $('#s_hsptLevel').append("<option value='"+el.id+"'>"+el.content+"</option>");
				});
		   }
		});
    });
    function province_change(){
       var province = $('#s_province').val();
        $.ajax({  //绑定城市信息
		   type: "GET",
		   url: "<%=path%>/Info/city/1/"+province,
		   dataType:"json",
		   success: function(data){
		       $('#s_city').html('<option>－城市－</option>');
			   $(data).each(function(index,el){
					 $('#s_city').append("<option value='"+el.id+"'>"+el.content+"</option>");
					 $('#s_area').html("<option value='-1'>－区域－</option>");
				});
		   }
		});
    };
      function city_change(){
       var city = $('#s_city').val();
        $.ajax({  //绑定区域信息
		   type: "GET",
		   url: "<%=path%>/Info/city/1/"+city,
		   dataType:"json",
		   success: function(data){
		       $('#s_area').html('');
			   $(data).each(function(index,el){
					 $('#s_area').append("<option value='"+el.id+"'>"+el.content+"</option>");
				});
		   }
		});
    };
    function toVaild(){
	    var s_area =  $("#s_area").val();
	    if(s_area=="-1"){
	        alert("请选择城市信息！")
	        return false;
	    }if($('#label_hcp').html()=="该医院代码已经存在"){
			alert("该医院代码已经存在,无法提交！");
			return false;		
		}
	    return true;
    };
    function verify(){
		var hsptCode = $('#hsptCode').val();
		if(hsptCode!="")
		  $.ajax({  //获取HCP级别
			   type: "POST",
			   url: "<%=path%>/hospital/verify",
			   data:"hsptCode="+hsptCode,
			   dataType:"json",
			   success: function(data){
				   if(data.count>0){
				     $('#label_hcp').html("该医院代码已经存在");
				   }else{
				    $('#label_hcp').html("");
				   }
			   }
			});
	};
      
    </script>
	<!-- mother grid end here-->
</body>
</html>
