<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>
<html>
<head>
<link rel="SHORTCUT ICON" href="<%=path%>/image/index/logo.ico" type="image/x-icon" >
<title>HCP-业务员</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all">
<link href="<%=path%>/css/dataTables.bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link rel="stylesheet"
	href="<%=path %>/js/zyupload/skins/zyupload-1.0.0.min.css "
	type="text/css">
<!-- Custom Theme files -->
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css"
	media="all" />
<!--js-->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="<%=path%>/css/font-awesome.css" rel="stylesheet">

<!--skycons-icons-->
<script src="<%=path%>/js/skycons.js"></script>
<!--//skycons-icons-->
</head>
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main" style="z-index:999;">
					<div class="header-left">
						<div class="logo-name">
							<a href="<%=path%>"> <!-- <h1>KPI Of College</h1> --> <img
								id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" />
							</a>
						</div>

					</div>
					<div class="header-right">
						<div class="profile_details_left">
							<div class="clearfix"></div>
						</div>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img"> 
											</span>
											<div class="user-name">
												<p>${sessionScope.user.name }</p>
												<span>${sessionScope.user.type }</span>
											</div>
											<i class="fa fa-angle-down lnr"></i> <i
												class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
								</a>
									<ul class="dropdown-menu drp-mnu">
										<li><a href="<%=path%>/user/quit"><i
												class="fa fa-sign-out"></i> 退出</a></li>
									</ul></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script>
					$(document).ready(function() {
						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});

					});
				</script>
				<!-- /script-for sticky-nav -->
				<!--inner block start here-->
				<div class="inner-block" style="min-height:560px;overflow-y :auto;">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">HCP列表</h3>
						</div>
						<div class="panel-body">
						  	<div class="col-md-4 col-sm-4 col-xs-4"  >
						         <input type="text" id="hsptName1" class="form-control"  placeholder="请输入医院名">
						      </div>
						      <div class="col-md-4 col-sm-4 col-xs-4"  >
						         <input type="text" id="hcpName1" class="form-control"  placeholder="请输入医生名">
						      </div>
						  	 <button type="button" class="btn btn-primary" onclick="chaxun()">查询</button>
							 <hr>
							<div id="loading" align="center"></div>
							<div id="contentDIV">
								<div id="tishi" class="alert alert-danger" hidden="hidden"
									style="clear: both;">空空如也...</div>
								<div id="table">
									<table id="example"
										class="table table-striped table-bordered table-hover"
										cellspacing="0" width="100%">
										<thead>
											<tr>
											 	<th style="width:10%">编号</th> 
												<th style="width:15%">姓名</th>
												<th>手机</th>
												<th>所属医院</th>
												<th>活动模块</th>
											<!-- 	<th>OA联系人</th>
												<th>OA联系方式</th> -->
												<th>状态</th>
												<th class="oper">操作</th>
											</tr>
										</thead>
										<tbody id="tbody">

										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			<!--inner block end here-->
			<!--copy rights start here-->
			<div class="copyrights">
				<jsp:include page="/menu/foot.jsp"></jsp:include>
			</div>
			<!--COPY rights end here-->
		</div>
		</div>
	<!--slider menu-->
	<div class="sidebar-menu">
		<div class="logo">
			<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span>
			</a> <a href="#"> <span id="logo"></span> <!--<img id="logo" src="" alt="Logo"/>-->
			</a>
		</div>
		<div class="menu">
			<ul id="menu">
				<li id="menu-home"><a href="<%=path%>/staff"> <i
						class="fa fa-home"></i> <span>业务员</span></a></li>
				<li class="active"><a><i class="fa fa-user-md"></i><span>HCP管理</span><span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hcp/staff/add">HCP建档申请</a></li>
						<li><a href="<%=path%>/hcp/staff/view">HCP列表</a></li>
					</ul></li>
				<li ><a href="<%=path%>/activity/staff/view">
					<i class="fa fa-leaf"></i><span>活动管理</span></a></li>
				<li><a href="<%=path%>/hospital/view"><i class="fa fa-hospital-o"></i><span>查看医院</span> 
					<%-- <span class="fa fa-angle-right" style="float: right"></span></a>
						<ul>
							<li><a href="<%=path%>/hospital/view">查看列表</a></li>
							<li><a href="<%=path%>/hospital/add">添加医院</a></li>
						</ul> 
					--%>
				</li>
				<li ><a href="<%=path%>/contract/staff/view"><i class="fa  fa-file-text"></i><span>合同管理</span> </a></li>
			</ul>
		</div>
	</div>
	<div class="clearfix"></div>
	</div>
	<!--slide bar menu end here-->

	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="height: 570px; overflow:auto;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">HCP详细信息</h4>
				</div>
				<div class="modal-body">
					<div class="panel-body">
						<table class="table">
							<tbody>
								<tr>
									<td>
										<div class="col-md-5 col-sm-5 col-xs-5">
											编号：<label id="hcpId"></label>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-4">
											姓名：<label id="hcpName"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div id="hcpClass" class="col-md-5 col-sm-5 col-xs-5">
											
										</div>
										<div id="hcp_state" class="col-md-6 col-sm-6 col-xs-6">
											
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-5 col-sm-5 col-xs-5">
											电话：<label id="hcpPhone"></label>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-4">
											工作年限：<label id="hcpYears"></label>年
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											身份证号码：<label id="idCard"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											简介：<label id="hcpInfo"></label>
										</div>
									</td>
								</tr>
								<tr>
								   <td>	
								   		<div class="col-md-12 col-sm-12 col-xs-12">
											医院：<label id="hsptName"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-4 col-sm-4 col-xs-4">
											科室：<label id="hcpDepart"></label>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-4">
											职务：<label id="hcpPost"></label>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-4">
											职称：<label id="hcpTitle"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="panel panel-default col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        职称证明文件
										      </h3>
										   </div>
										   <div id="hcpTitleFile"  class="panel-body">
										   </div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-9 col-sm-9 col-xs-9">
											社会职务：<label id="hcpSocial"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											社会职务描述：<label id="hcpSocialDesp"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											擅长领域：<label id="hcpPrefer"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											建议的活动模块：<label id="hcpActivity"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-5 col-sm-5 col-xs-5">
											是否为孕妇学校老师：<label id="hcpMomTeacher"></label>
										</div>
										<div class="col-md-5 col-sm-5 col-xs-5">
											是否母乳喂养咨询师：<label id="hcpMomConsultsant"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-8 col-sm-8 col-xs-8">
											开户行：<label id="bank"></label>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-4">
											户名：<label id="bankCardName"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											银行卡账号：<label id="bankCardNumber"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-5 col-sm-5 col-xs-5">
											OA联系人：<label id="oaName"></label>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											OA联系方式：<label id="oaPhone"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-5 col-sm-5 col-xs-5">
											资料是否齐全：<label id="hcpProfileFull"></label>
										</div>
										<div class="col-md-7 col-sm-7 col-xs-7">
											提交时间：<label id="hcpCtime"></label>
										</div>
										</td>
								</tr>
								<tr id="div_hcpAddition" >
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											资料不全的说明：<label id="hcpAddition"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="panel panel-info col-md-12 col-sm-12 col-xs-12" >
										   <div class="panel-heading">
										      <h3 class="panel-title">
										        所签订的合同
										      </h3>
										   </div>
										   <table class="table mytable"> 
											    <thead> 
											        <tr> 
											            <th>合同ID</th> 
											            <th>状态</th> 
											            <th>签订公司</th> 
											            <th>剩余日期</th>
											        </tr> 
											    </thead> 
											    <tbody id="contract"> 
											    </tbody> 
											</table>
											<div>
											  <button id="add_contract" class="btn btn-success" >添加合同</button>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											备注：<label id="remark"></label>
										</div>
									</td>
								</tr>
								<tr id="update">
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											申请修改原因：<label id="updateRemark"></label>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="height: 550px; overflow:auto;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">HCP详细信息</h4>
				</div>
				<div class="modal-body">
					<div class="panel-body">
					<form action="<%=path %>/hcp/staff/update" method="post" onSubmit="return update_check()">
						<table class="table">
							<tbody>
								<tr>
									<td>
										<div class="col-md-5 col-sm-5 col-xs-5">
											编号：<label id="hcpId2"></label>
											<input type="hidden" id="hcpId3" name="id">
										</div>
										<div class="col-md-4 col-sm-4 col-xs-4">
											姓名：<label id="hcpName2"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="col-md-12 col-sm-12 col-xs-12">
											所在医院：<label id="hsptName2"></label>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class=" col-md-6 col-sm-6 col-xs-6">
									        <div class="input-group">
									         <span class="input-group-addon">HCP级别</span>
									         <input id="hcpClass3" type="hidden" name="hcpClass">
									         <select ID="hcpClass1" name="hcpClass1" class="form-control" <!-- onchange="hcpClass1_change()" --> > 
									         </select>
									         </div>
									      </div>
									       <!-- <div class=" col-md-6 col-sm-6 col-xs-6">
									        <div class="input-group">
									         <span class="input-group-addon">价位</span>
									         <select ID="hcpClass2" name="hcpClass2" class="form-control" > </select>
									         </div>
								      </div> -->
									</td>
								</tr>
								<tr>
									<td>
										 <div class="input-group col-md-8 col-sm-8 col-xs-8">
									         <span class="input-group-addon">开户行</span>
									         <input type="text" id="bank1" name="bank" maxlength="100"  required="required" placeholder="必须包括银行名和开户支行" class="form-control">
									      </div>
							      </td>
								</tr>
								<tr>
									<td>
								        <div class="input-group col-md-8 col-sm-8 col-xs-8">
								        	 <span class="input-group-addon">银行卡户名</span>
								        	 <input type="text" id="bankCardName1" name="bankCardName" required="required" placeholder="必填项" maxlength="50" class="form-control">
								      	</div>
									</td>
								</tr>
								<tr>
									<td>
							      		 <div class="input-group col-md-8 col-sm-8 col-xs-8">
							      		   <span class="input-group-addon">银行卡号</span>
							       			 <input type="text" id="bankCardNumber1" name="bankCardNumber" required="required" maxlength="42" placeholder="必填项" class="form-control">
							      		</div>
									</td>
								</tr>	
								<tr>
									<td>
										 <div class=" col-md-6 col-sm-6 col-xs-6">
									        <div class="input-group">
										         <span class="input-group-addon">修改原因</span>
										         <select ID="update_remark" name="updateRemark" class="form-control" > 
										         	<option>市级->省级</option>
										         	<option>省级->全国</option>
										         	<option>市级->全国</option>
										         	<option>修改银行卡信息</option>
										         	<option>申请升级和修改银行卡信息</option>
										         </select>
									        </div>
								      </div>
									</td>
								</tr>
								<tr>
									<td>
										<button type="submit" class="btn btn-success" >确认修改</button>
									</td>
								</tr>									
							</tbody>
						</table>
					</form>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>

	<script>
		var toggle = true;

		$(".sidebar-icon").click(
				function() {
					if (toggle) {
						$(".page-container").addClass("sidebar-collapsed")
								.removeClass("sidebar-collapsed-back");
						$("#menu span").css({
							"position" : "absolute"
						});
					} else {
						$(".page-container").removeClass("sidebar-collapsed")
								.addClass("sidebar-collapsed-back");
						setTimeout(function() {
							$("#menu span").css({
								"position" : "relative"
							});
						}, 400);
					}
					toggle = !toggle;
				});
	</script>
	<!--scrolling js-->
	<script src="<%=path%>/js/jquery.nicescroll.js"></script>
	<script src="<%=path%>/js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="<%=path%>/js/bootstrap.js"></script>
	<script src="<%=path%>/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="<%=path%>/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script type="text/javascript">
    var table = null;
	$(document).ready(function() {
		table = $('#example').DataTable();
    	$('#loading').hide();
		$('#contentDIV').hide();
	});
						
		function chaxun(){
			$.ajax({  //绑定数据
			   type: "POST",
			   url: "<%=path%>/hcp/staff/list",
			   data:"hsptName="+$("#hsptName1").val()+"&hcpName="+$("#hcpName1").val(),
			   dataType:"json",  
			   beforeSend:function(XMLHttpRequest){ 
				   $('#loading').show();
				   $('#contentDIV').hide();
		           $("#loading").html("<img src='<%=path%>/image/loading.gif' />");
										},
										error : function(e) {
											alert("数据加载失败，请刷新重试！");
											$('#loading').hide();
										},
										success : function(data) {
											$('#contentDIV').show();
											$('#loading').hide();
											if (data == "") {
												$('#tishi').show();
												$('#table').hide();
											} else {
												$('#table').show();
												$('#tishi').hide();
											}
											table.destroy(false);
											$('#tbody').html("");
											$(data).each(
															function(index,item) {
																var row = "<tr>";
																row += "<td>" + item.id + "</td>";
																row += "<td>" + item.hcpName + "</td>";
																row += "<td>" + item.hcpPhone + "</td>";
																row += "<td>" + item.remark + "</td>";
																row += "<td>" + item.hcpActivity + "</td>";
																/* row += "<td>" + item.oaName + "</td>";
																row += "<td>" + item.oaPhone + "</td>"; */
																if(item.hcpAddition==0||item.hcpAddition==3){
																	row += "<td>审核中</td>";
																}else if(item.hcpAddition==1){
																	row += "<td>通过</td>";
																}else if(item.hcpAddition==2){
																	row += "<td>未通过</td>";
																}
																row += "<td><button class=\"btn btn-primary btn-sm\" onclick=\"view('" + item.id + "','"+item.hcpAddition+"','"+item.hcpInfo+"')\">详情</button>&nbsp;";
																row += "<button class=\"btn btn-info btn-sm\" onclick=\"update('" + item.id + "')\">修改</button>&nbsp;";
																if(item.hcpAddition==0){
																	row += "<button class=\"btn btn-primary btn-sm\" onclick=\"goback('" + item.id + "')\">撤回</button>&nbsp;";
																}
																row +="</td></tr>";
																$('#tbody').append(row);

																if (index == 9) {
																	$('#label_tishi').show();
																}
															});
											table=$('#example').DataTable();
										}
									});
		}
		function view(id,state,remark) {
		
		   $.ajax({
				   type: "POST",
				   url: "<%=path%>/hcp/staff/show/"+id,
				   dataType:"json",
				   error : function(e) {
							alert("数据加载失败，请刷新重试！");
						},
				   success: function(data){
				     $("#hcpId").html(data.doctor.id);
				     $("#hcpName").html(data.doctor.hcpName);
				     $("#hcpPhone").html(data.doctor.hcpPhone);
				     $("#hcpInfo").html(data.doctor.hcpInfo);
				     $("#hsptName").html(data.doctor.hsptId);
				     $("#idCard").html(data.doctor.idCard);
				     $("#bankCardName").html(data.doctor.bankCardName);
				     $("#bank").html(data.doctor.bank);
				     $("#bankCardNumber").html(data.doctor.bankCardNumber);
				     $("#hcpActivity").html(data.doctor.hcpActivity);
 					 $("#hcp_state").html("HCP级别：<label >"+data.doctor.hcpClass+"</label>");
				     if(state=="0"||state=="3"){
				     	$("#hcpClass").html("审核情况：<label>审核中</label>");
					  }else if(state=="1"){
					     $("#hcpClass").html("审核情况：<label>通过</label>");
				     }else{
					       $("#hcpClass").html("审核情况：<label>未通过</label>");
				     }
				     
				     $("#hcpCtime").html(data.doctor.remark);
				     $("#hcpDepart").html(data.doctor.hcpDepart);
				     if(data.doctor.hcpMomConsultsant==1)
				        $("#hcpMomConsultsant").html("是");
				     else
				     	$("#hcpMomConsultsant").html("否");
				     if(data.doctor.hcpMomTeacher==1)
				        $("#hcpMomTeacher").html("是");
				     else
				     	$("#hcpMomTeacher").html("否");
				     if(data.doctor.hcpProfileFull==1){
				        $("#hcpProfileFull").html("是");
				        $("#div_hcpAddition").hide();
				     }
				     else{
				    	 $("#hcpProfileFull").html("否");
				    	  $("#div_hcpAddition").show();
				    	  $("#hcpAddition").html(data.doctor.hcpAddition);
				     }
				     
				     $("#hcpPost").html(data.doctor.hcpPost);
				     $("#hcpTitle").html(data.doctor.hcpTitle);
				     $("#hcpPrefer").html(data.doctor.hcpPrefer);
				     $("#hcpSocial").html(data.doctor.hcpSocial);
				     $("#hcpSocialDesp").html(data.doctor.hcpSocialDesp);
				     $("#hcpYears").html(data.doctor.hcpYears);
				     $("#oaName").html(data.doctor.oaName);
				     $("#oaPhone").html(data.doctor.oaPhone);
				     $("#remark").html(data.doctor.verifyRemark);
				     $("#hcpTitleFile").html("");
				     $("#contract").html("");
				     $("#update").hide();
				     var updateRemark = data.doctor.updateRemark;
				     if(updateRemark!=null&&updateRemark!=""){
				     	$("#update").show();
				     	$("#updateRemark").html();
				     }
				     $(data.file).each(function(index,item){
				         var row = "";
				         if(index!=0)
				            row = "<hr>";
				        row += "<a href=\"<%=path%>/staff/view/photo"+item.path+"/\" target='_blank' title='"+spitStringTitle(item.fileName)+"'>"+spitString(item.fileName)+"</a>";
				       $("#hcpTitleFile").append(row);
				     });
				     if($("#hcpTitleFile").html()==""){
					     	$("#hcpTitleFile").html("未上传职称证明文件");
					   }
				     if(data.contract==null||data.contract==""){
				     	$(".mytable").html("暂未提交合同");
				     }else{
				     	var html = "    <thead>  <tr> <th>合同ID</th> <th>状态</th>  <th>签订公司</th> <th>剩余日期</th>  </tr>  </thead>  <tbody id=\"contract\">    </tbody> ";
				     	$(".mytable").html(html);
				     }
				       $(data.contract).each(function(index,item){
				         var row = "<tr>";
				            row += "<td><a href=\"<%=path%>/contract/staff/detail/"+item.id+"\" target='_blank' title='"+item.id+"'>"+item.id+"</a></td>";
				            row +="<td>"+item.state+"</td>";
				            row +="<td>"+item.tp_name+"</td>";
				            row +="<td>"+item.date+"</td>";
				            row += "</tr>";
				       $("#contract").append(row);
				     }); 
				     if(data.flag!="false"){
				     	$("#add_contract").show();
				     	$("#add_contract").attr("onclick","add_click('"+data.doctor.id+"')");
				     }else{
				        $("#add_contract").hide();
				     }
				     $("#myModal").modal('show');
				   }
				 }); 
		};
		function add_click(id){
			window.open("<%=path%>/contract/staff/add/"+id);
		};
		function spitString(fileName){
		  var index = fileName.indexOf("_");
		  return fileName.substring(index+1);
		};
		function update(hcpId){
		 $.ajax({  //获取HCP级别
			   type: "GET",
			   url: "<%=path%>/Info/city/7/0",
			   dataType:"json",
			   success: function(data){
			       $('#hcpClass1').html("");
				   $(data).each(function(index,el){
						 $('#hcpClass1').append("<option value='"+el.id+"'>"+el.content+"</option>");
					});
					 $.ajax({
						   type: "POST",
						   url: "<%=path%>/hcp/staff/show/"+hcpId,
						   dataType:"json",
						   error : function(e) {
									alert("数据加载失败，请刷新重试！");
								},
						   success: function(data){
						   		 $("#hcpId3").val(data.doctor.id);
						   		 $("#hcpId2").html(data.doctor.id);
						    	 $("#hcpName2").html(data.doctor.hcpName);
						    	 $("#hsptName2").html(data.doctor.hsptId);
						    	 $("#bank1").val(data.doctor.bank);
						    	 $("#bankCardName1").val(data.doctor.bankCardName);
						    	 $("#bankCardNumber1").val(data.doctor.bankCardNumber);
						    	 var arr = data.doctor.hcpClass.split(":");
						    	 var x = "";
						    	 $("#hcpClass1 option").each(function() {
								    if($(this).text() == arr[0]) {
								        x = $(this).val();
								    }
								}); 
								$("#hcpClass1").val(x);
							<%-- 	$('#hcpClass2').html("");
								  $.ajax({  //获取HCP级别
										   type: "POST",
										   url: "<%=path%>/Info/city/7/"+$('#hcpClass1').val(),
										   dataType:"json",
										   success: function(data){
											   $(data).each(function(index,el){
													 $('#hcpClass2').append("<option value='"+el.id+"'>"+el.content+"</option>");
												});
													$("#hcpClass2 option").each(function() {
													    if($(this).text() == arr[1]) {
													        x = $(this).val();
													    }
													}); 
													$("#hcpClass2").val(x);
										   }
										});  --%>
						   }
					 });
			   }
			}); 
		     
			 $("#myModal1").modal('show');
		};
		function hcpClass1_change(){
			$('#hcpClass2').html("");
			  $.ajax({  //获取HCP级别
					   type: "POST",
					   url: "<%=path%>/Info/city/7/"+$('#hcpClass1').val(),
					   dataType:"json",
					   success: function(data){
						   $(data).each(function(index,el){
								 $('#hcpClass2').append("<option value='"+el.id+"'>"+el.content+"</option>");
							});
					   }
					}); 
	};
	function update_check(){
		$('#hcpClass3').val($('#hcpClass1 option:selected').text());/* +":"+$('#hcpClass2 option:selected').text()) */
		if($('#bank1').val().indexOf('支行')==-1){
				alert("必须输入开户银行和支行名！");
				return false;
			}
		return true;
	};
	function goback(hcpId){
		if(confirm("确定要撤回申请吗？")){
			$.ajax({  
			   type: "POST",
			   url: "<%=path%>/hcp/staff/goback",
			   data:"hcpId="+hcpId,
			   dataType:"json",
			   success: function(data){
			   		if(data.data=="1"){
			   			alert("撤回成功，请刷新页面！");
			   		}
					else{
						alert("撤回失败，请重试！原因："+data.data);
					}
			   }
		 });
		}
	}
	
	</script>
	<!-- mother grid end here-->
</body>
</html>
