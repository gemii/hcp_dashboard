<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML>
<html>
<head>
<link rel="SHORTCUT ICON" href="<%=path%>/image/index/logo.ico" type="image/x-icon" >
<title>HCP-业务员</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="<%=path%>/css/bootstrap.css" rel="stylesheet"	type="text/css" media="all">
<link href="<%=path%>/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="<%=path %>/js/zyupload/skins/zyupload-1.0.0.min.css" type="text/css">
<!-- Custom Theme files -->
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--js-->
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="<%=path%>/css/font-awesome.css" rel="stylesheet">

<!--skycons-icons-->
<script src="<%=path%>/js/skycons.js"></script>
<!--//skycons-icons-->
</head>
<body>
	<div class="page-container">
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main" style="z-index:999;">
					<div class="header-left">
						<div class="logo-name">
							<a href="<%=path%>"> <!-- <h1>KPI Of College</h1> -->
								<img id="logo" src="<%=path%>/image/index/logo.gif" alt="Logo" />
							</a>
						</div>

					</div>
					<div class="header-right" >
						<div class="profile_details_left">
							<!--notifications of menu start -->
							<div class="clearfix"></div>
						</div>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>
								<li class="dropdown profile_details_drop"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img">
												</span>
												<div class="user-name">
												  <p>${sessionScope.user.name }</p>
												  <span>${sessionScope.user.type }</span>
												</div>
											<i class="fa fa-angle-down lnr"></i> <i
												class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
								</a>
									<ul class="dropdown-menu drp-mnu">
										<li><a href="<%=path%>/user/quit"><i class="fa fa-sign-out"></i> 退出</a></li>
									</ul></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script>
					$(document).ready(function() {
						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});

					});
				</script>
				<!-- /script-for sticky-nav -->
				<!--inner block start here-->
				<div class="inner-block" style="min-height:560px;overflow-y :auto;">
				     	<form action="<%=path %>/hcp/staff/save" method="post" onSubmit="return check1()">
				     	<div class="panel panel-info">
							<div class="panel-heading">
								<h3 class="panel-title">HCP建档申请</h3>
							</div>
							<div class="panel-body">
								     <div class="input-group col-md-3 col-sm-3 col-xs-3">
								         <span class="input-group-addon">医生姓名</span>
								         <input type="text" id="hcpName" name="hcpName" class="form-control"  placeholder="必填项" required="required" onblur="getName()">
								      </div>
							      <label id="label_hcp" style="color:red;"></label>
							       <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">联系方式</span>
							         <input type="text" name="hcpPhone" id="hcpPhone" class="form-control" placeholder="必填项" required="required">
							      </div></br>
							        <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">身份证</span>
							         <input type="text" name="idCard" id="idCard" class="form-control" placeholder="必填项" required="required" onblur="verify()">
							      </div></br>
							      <div class=" col-md-3 col-sm-3 col-xs-3">
							        <div class="input-group">
							         <span class="input-group-addon">HCP级别</span>
							         <input id="hcpClass" type="hidden" name="hcpClass">
							         <select ID="hcpClass1" name="hcpClass1" class="form-control"  > <!-- onchange="hcpClass1_change()" -->
							         </select>
							         </div>
							      </div>
							      <!--  <div class=" col-md-3 col-sm-3 col-xs-3">
							        <div class="input-group">
							         <span class="input-group-addon">价位</span>
							         <select ID="hcpClass2" name="hcpClass2" class="form-control" > </select>
							         </div>
							      </div> -->
							      <br><br><br>
							       <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">医生<br>简介</span>
							         <textarea  name="hcpInfo" style="width:180px;height:70px;" class="form-control" placeholder="必填项" required="required"></textarea>
							      </div></br>
							      <div class="input-group col-md-5 col-sm-5 col-xs-5">
							        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">选择医院</button>&nbsp;
							        <label id="hspt_label" >尚未选择</label>
							        <input id="hspt_input" name="hsptId" type="hidden"/>
							      </div></br>
							      <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">科室</span>
							         <select ID="s_depart" name="hcpDepart" class="form-control" > </select>
							      </div></br>
							      <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">职务</span>
							         <select ID="s_post" name="hcpPost" class="form-control" > </select>
							      </div></br>
							      <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">职称</span>
							         <select ID="s_title" name="hcpTitle" class="form-control" > </select>
							      </div></br>
							      <div class="input-group col-md-6 col-sm-6 col-xs-6">
							         <button type="button" class="btn btn-success"
											data-toggle="modal" data-target=".bs-example-modal-lg">
											上传职称文件</button><br>
									   <label id="label_filepath"></label>
									   <input id="hcp_title_file" name="hcpTitleFile" type="hidden"/>
							      </div></br>
							      <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">工作年限</span>
							         <input type="number" name="hcpYears" placeholder="必填项" required="required" min="0" step="1" class="form-control">
							         <span class="input-group-addon">年</span>
							      </div></br>
							      <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">社会职务</span>
							          <select  name="hcpSocial" class="form-control" > 
							          	<option>有全国职务</option>
							          	<option>有省级职务</option>
							          	<option>无全国/省级职务</option>
							          </select>
							      </div></br>
							      <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">社会<br>职务<br>描述</span>
							         <textarea  name="hcpSocialDesp" maxlength="255" style="width:180px;height:70px;" class="form-control" ></textarea>
							      </div></br>
							      <div class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">擅长<br>领域</span>
							         <textarea  name="hcpPrefer" maxlength="255" style="width:180px;height:70px;" class="form-control" ></textarea>
							      </div></br>
							      <div class="input-group col-md-4 col-sm-4 col-xs-4">
							      是否孕妇学校老师 <label class="checkbox-inline">
								      <input type="radio" name="hcpMomTeacher" 
								         value="1" >&nbsp;是
								   </label>
								   <label class="checkbox-inline">
								      <input type="radio" name="hcpMomTeacher" 
								         value="0" checked="checked">&nbsp;否
								   </label>
								</div></br>
								<div class="input-group col-md-4 col-sm-4 col-xs-4">
							      是否母乳喂养咨询师 <label class="checkbox-inline">
								      <input type="radio" name="hcpMomConsultsant" 
								         value="1" >&nbsp;是
								   </label>
								   <label class="checkbox-inline">
								      <input type="radio" name="hcpMomConsultsant" 
								         value="0" checked="checked">&nbsp;否
								   </label>
								</div></br>
								<div class="input-group col-md-4 col-sm-4 col-xs-4" >
								    <span class="input-group-addon">建议的活动模块</span>  
									      <select ID="s_area" name="hcpActivity" class="form-control" > 
									         <option>线上咨询-答疑类</option>
									         <option>线上咨询-专业支持类</option>
									         <option>线上咨询-专业知识类</option>
									      </select>
								  </div></br>
								   <div class="input-group col-md-4 col-sm-4 col-xs-4">
							         <span class="input-group-addon">开户行</span>
							         <input id="bankName" type="text" name="bank" maxlength="100" required="required" placeholder="必须包括银行名和开户支行" class="form-control">
							      </div></br>
							        <div class="input-group col-md-4 col-sm-4 col-xs-4">
							         <span class="input-group-addon">银行卡户名</span>
							         <input type="text" id="bankCardName" name="bankCardName" required="required" placeholder="必填项" maxlength="50" class="form-control">
							      </div></br>
							       <div class="input-group col-md-4 col-sm-4 col-xs-4">
							         <span class="input-group-addon">银行卡号</span>
							         <input type="text" name="bankCardNumber" maxlength="42" required="required" placeholder="必填项" class="form-control">
							      </div></br>
							       <div class="input-group col-md-4 col-sm-4 col-xs-4">
							         <span class="input-group-addon">OA联系人</span>
							         <input type="text" name="oaName" maxlength="20" class="form-control">
							      </div></br>
							      <div class="input-group col-md-4 col-sm-4 col-xs-4">
							         <span class="input-group-addon">OA联系方式</span>
							         <input type="text" name="oaPhone" maxlength="20" class="form-control">
							      </div></br>
							      <div class="input-group col-md-4 col-sm-4 col-xs-4">
							      资料是否齐全 <label class="checkbox-inline">
								      <input type="radio" name="hcpProfileFull" 
								         value="1" checked="checked" onClick="hide()">&nbsp;是
								   </label>
								   <label class="checkbox-inline">
								      <input type="radio" name="hcpProfileFull" 
								         value="0" id＝"is_complete" onClick="show()">&nbsp;否
								   </label>
								</div></br>
								  <div id="div_hcp_addition" class="input-group col-md-3 col-sm-3 col-xs-3">
							         <span class="input-group-addon">资料<br>不全<br>原因</span>
							         <textarea id="hcp_addition" name="hcpAddition" maxlength="255" style="width:180px;height:70px;" class="form-control" ></textarea>
							      </div></br>
								
							     <div class="col-md-2 col-sm-2 col-xs-2">
							        <button type="submit" class="btn btn-primary">提交</button>
							     </div>
							</div>
						</div>
						</form>
				</div>
				<!--inner block end here-->
			</div>
			<!--copy rights start here-->
			<div class="copyrights">
				<jsp:include page="/menu/foot.jsp"></jsp:include>
			</div>
			<!--COPY rights end here-->
			</div>
		<!--slider menu-->
		<div class="sidebar-menu">
			<div class="logo">
				<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span>
				</a> <a href="#"> <span id="logo"></span> <!--<img id="logo" src="" alt="Logo"/>-->
				</a>
			</div>
			<div class="menu">
							<ul id="menu">
				<li id="menu-home"><a href="<%=path%>/staff"> <i class="fa fa-home"></i> <span>业务员</span></a></li>
				<li class="active"><a><i class="fa fa-user-md"></i><span>HCP管理</span><span
						class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hcp/staff/add">HCP建档申请</a></li>
						<li><a href="<%=path%>/hcp/staff/view">HCP列表</a></li>
					</ul></li>
				<li ><a href="<%=path%>/activity/staff/view">
					<i class="fa fa-leaf"></i><span>活动管理</span></a></li>
				<li><a href="<%=path%>/hospital/view"><i class="fa fa-hospital-o"></i><span>查看医院</span> 
				<%-- <span class="fa fa-angle-right" style="float: right"></span></a>
					<ul>
						<li><a href="<%=path%>/hospital/view">查看列表</a></li>
						<li><a href="<%=path%>/hospital/add">添加医院</a></li>
					</ul> 
				--%>
				</li>
				<li ><a href="<%=path%>/contract/staff/view"><i class="fa  fa-file-text"></i><span>合同管理</span> </a></li>
			</ul>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<!--slide bar menu end here-->
	
		<!-- 添加附件模态框 -->
	<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
		aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">添加附件
						<font size="2.5" color="blue">&nbsp;&nbsp;附件仅支持jpg,png,jpeg,PNG类型的图片</font>
					</h4>
					
				</div>
				<div class="modal-body">
					<div id="zyupload" class="zyupload">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"
						onclick="save()">确定</button>
				</div>
			</div>
		</div>
	</div>
	<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" 
   aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" 
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <h4 class="modal-title" id="myModalLabel">
               请选择所在医院
            </h4>
         </div>
         <div class="modal-body">
            <div class="panel-body">
			   	  <div class="col-md-6 col-sm-6 col-xs-6"  >
			         <input type="text" id="hsptName" class="form-control" required="required" placeholder="请输入医院名">
			      </div>
			  	 <button type="button" class="btn btn-primary" onclick="chaxun()">查询</button>
			  	  <label id="label_tishi"  style="color:red;"> </label>
		  	  </div>
		  	  <div id="hspt_div" class="panel-body">
			     
			   </div>
         </div>
         <div class="modal-footer">
          <!-- <button id="add_hospital" type="button" class="btn btn-info" onclick="add()" >
              添加医院
            </button> -->
            <button type="button" class="btn btn-primary" onclick="return check()" >
                确认选择
            </button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
</div>
	
	
	<script>
		var toggle = true;

		$(".sidebar-icon").click(
				function() {
					if (toggle) {
						$(".page-container").addClass("sidebar-collapsed")
								.removeClass("sidebar-collapsed-back");
						$("#menu span").css({
							"position" : "absolute"
						});
					} else {
						$(".page-container").removeClass("sidebar-collapsed")
								.addClass("sidebar-collapsed-back");
						setTimeout(function() {
							$("#menu span").css({
								"position" : "relative"
							});
						}, 400);
					}
					toggle = !toggle;
				});
	</script>
	<!--scrolling js-->
	<script src="<%=path%>/js/jquery.nicescroll.js"></script>
	<script src="<%=path%>/js/scripts.js"></script>
	<!--//scrolling js-->
	<script src="<%=path%>/js/bootstrap.js"></script>
	<script type="text/javascript" 	src="<%=path %>/js/zyupload/zyupload-1.0.0.min.js"></script>
    <script type="text/javascript">
    var filePath = "",allPath="";
    $(document).ready(function() {
      $("#div_hcp_addition").hide();
      $("#add_hospital").hide();
    });
       function hide(){
        $("#hcp_addition").val("");
          $("#div_hcp_addition").hide();
       };
       function show(){
          $("#div_hcp_addition").show();
       };
       $(function() {
          $.ajax({  //获取医院科室信息
		   type: "GET",
		   url: "<%=path%>/Info/city/4/0",
		   dataType:"json",
		   success: function(data){
			   $(data).each(function(index,el){
					 $('#s_depart').append("<option value='"+el.id+"'>"+el.content+"</option>");
				});
		   }
		}); 
		  $.ajax({  //获取职务信息
		   type: "GET",
		   url: "<%=path%>/Info/city/5/0",
		   dataType:"json",
		   success: function(data){
			   $(data).each(function(index,el){
					 $('#s_post').append("<option value='"+el.id+"'>"+el.content+"</option>");
				});
		   }
		}); 
		$.ajax({  //获取职称信息
		   type: "GET",
		   url: "<%=path%>/Info/city/6/0",
		   dataType:"json",
		   success: function(data){
			   $(data).each(function(index,el){
					 $('#s_title').append("<option value='"+el.id+"'>"+el.content+"</option>");
				});
		   }
		}); 
		  $.ajax({  //获取HCP级别
		   type: "GET",
		   url: "<%=path%>/Info/city/7/0",
		   dataType:"json",
		   success: function(data){
			   $(data).each(function(index,el){
					 $('#hcpClass1').append("<option value='"+el.id+"'>"+el.content+"</option>");
				});
			hcpClass1_change();
		   }
		}); 
			// 初始化插件
			$("#zyupload").zyUpload({
				width            : "650px",           // 宽度
				height           : "160px",           // 宽度
				itemWidth        : "80px",            // 文件项的宽度
				itemHeight       : "65px",            // 文件项的高度
				url              : "<%=path%>/uploadFile", // 上传文件的路径
				fileType         : [ "jpg", "png","jpeg","gif","JPG","PNG"], // 上传文件的类型
				fileSize         : 51200000,          // 上传文件的大小
				multiple         : true,            // 是否可以多个文件上传
				dragDrop         : false,            // 是否可以拖动上传文件
				tailor           : false,            // 是否可以裁剪图片
				del              : false,             // 是否可以删除文件
				finishDel        : false,            // 是否在上传文件完成后删除预览
				/* 外部获得的回调接口 */
				onSelect : function(selectFiles, allFiles) { // 选择文件的回调方法  selectFile:当前选中的文件  allFiles:还没上传的全部文件
				},
				onDelete : function(file, files) { // 删除一个文件的回调方法 file:当前删除的文件  files:删除之后的文件
				},
				onSuccess : function(file, response) {
					 fileName = file.name;// 文件上传成功的回调方法
					if (file.name.indexOf("+") >= 0) {
						fileName = file.name.replace("+", "_");
					}
					if (filePath == "") {
					    filePath = fileName;
					    allPath = response;
						$('#label_filepath').html(filePath);
					} else {
						filePath += "<br>" + fileName;
						allPath += "||" + response;
						$('#label_filepath').html(filePath);
					}
					
				},
				onFailure : function(file, response) { // 文件上传失败的回调方法
				},
				onComplete : function(response) { // 上传完成的回调方法
				}
			});
		});
		function save(){
		      $.ajax({  //绑定数据
			   type: "POST",
			   url: "<%=path%>/upload/save",
			   data:"allPath="+allPath,
			   dataType:"text",  
		       error:function(e) { 
					alert("保存文件出错，请重试！"); 
				},
			   success: function(data){
			      $("#hcp_title_file").val(data);
			   }
			});
		};
		function chaxun(){
         var hsptName = $("#hsptName").val();
         if(hsptName==null||$.trim(hsptName)==""){
           alert("请输入医院名进行查询！");
           return false;
         }
		   $.ajax({  //绑定数据
			   type: "POST",
			   url: "<%=path%>/hospital/list/query/"+encodeURI(hsptName),
			   dataType:"json",  
		       error:function(e) { 
					alert("请输入合法的医院名称！"); 
				},
			   success: function(data){
			       $('#label_tishi').html("");
				   $('#hspt_div').html("");
				   if(data==""){
				    	$('#label_tishi').html("医院不存在,请重新查询或者添加医院。");
				    	$("#add_hospital").show();
				   }
				   $(data).each(function(index,item){
					   var row ="   <div class='col-md-6 col-sm-6 col-xs-6'>";
						   row += "<label><input name='hsptId' type='radio' value='"+item.id+"' onclick='radio_click(\""+item.id+"\",\""+item.hsptName+"\")'/> "+item.hsptName+" </label> ";
				           row += "</div>";
						 $('#hspt_div').append(row);
						 
						 if(index==9){
						     $('#label_tishi').html("匹配结果过多，请精确查询条件。");
						   }
					});
			   }
			});
	   };
	   function radio_click(id,name){
	      $("#hspt_label").html(name);
	      $("#hspt_input").val(id);
	   };
	   function check(){
	     if($("#hspt_label").html()=="尚未选择")
	     {
	       alert("请选择医院！");
	       return false;
	     }
	     $('#myModal').modal('hide')
	   };
	function getName(){
	  $("#bankCardName").val($("#hcpName").val());
	};
	function add(){
	  window.open("<%=path%>/hospital/add");
	};
	function hcpClass1_change(){
		$('#hcpClass2').html("");
			  $.ajax({  //获取HCP级别
					   type: "GET",
					   url: "<%=path%>/Info/city/7/"+$('#hcpClass1').val(),
					   dataType:"json",
					   success: function(data){
						   $(data).each(function(index,el){
								 $('#hcpClass2').append("<option value='"+el.id+"'>"+el.content+"</option>");
							});
					   }
					}); 
	};
	function verify(){
		var idCard = $('#idCard').val();
		var cd = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;//定义身份证匹配算法
		 if(idCard != null && idCard !=""){//运用匹配方法直接判断
			if (cd.test(idCard) == false) {
				alert("身份证号不合法!");
				return false;
			}
		}else{
			alert("身份证号码不能为空!")
		}
		if(idCard!="")
		  $.ajax({  
			   type: "POST",
			   url: "<%=path%>/hcp/staff/verify/idCard",
			   data:"idCard="+idCard,
			   dataType:"json",
			   success: function(data){
				   if(data.flag==true){
				     $('#label_hcp').html("该医生已经存在!");
				   }else{
				    $('#label_hcp').html("");
				   }
			   }
			});
	};
	function check1(){
		$('#hcpClass').val($('#hcpClass1 option:selected').text());/* +":"+$('#hcpClass2 option:selected').text()); */
		if($('#label_hcp').html()=="该医生已经存在"){
			alert("该医生已经存在,无法提交！");
			return false;		
		}else if($("#hspt_label").html()=="尚未选择")
	     {
	       alert("请选择医院！");
	       return false;
	     }else if($('#label_filepath').html()==""){
			alert("必须上传职称证明文件！");
			return false;
		}else{
			var bankName = $('#bankName').val();
			if(bankName.indexOf('支行')==-1&&bankName.indexOf('分行')==-1){
				alert("必须输入开户银行和支行名！");
				return false;
			}
		}
		return true;
	};
    </script>
	<!-- mother grid end here-->
</body>
</html>
