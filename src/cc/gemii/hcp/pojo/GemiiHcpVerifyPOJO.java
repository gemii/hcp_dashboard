package cc.gemii.hcp.pojo;

import java.util.Date;

public class GemiiHcpVerifyPOJO {
	String id;
	String verifyId;
	String hcpName;
	String hcpPhone;
	String hsptName;
	String staffName;
	String areaName;
	String cTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVerifyId() {
		return verifyId;
	}

	public void setVerifyId(String verifyIdString) {
		this.verifyId = verifyIdString;
	}

	public String getHcpName() {
		return hcpName;
	}

	public void setHcpName(String hcpName) {
		this.hcpName = hcpName;
	}

	public String getHcpPhone() {
		return hcpPhone;
	}

	public void setHcpPhone(String hcpPhone) {
		this.hcpPhone = hcpPhone;
	}

	public String getHsptName() {
		return hsptName;
	}

	public void setHsptName(String hsptName) {
		this.hsptName = hsptName;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getcTime() {
		return cTime;
	}

	public void setcTime(String cTime) {
		this.cTime = cTime.substring(0,cTime.length()-2);
	}

	@Override
	public String toString() {
		return "GemiiHcpVerifyPOJO [id=" + id + ", verifyId=" + verifyId
				+ ", hcpName=" + hcpName + ", hcpPhone="
				+ hcpPhone + ", hsptName=" + hsptName + ", staffName="
				+ staffName + ", areaName=" + areaName + ", cTime=" + cTime
				+ "]";
	}

}
