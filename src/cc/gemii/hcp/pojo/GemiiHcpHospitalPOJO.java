package cc.gemii.hcp.pojo;

import cc.gemii.hcp.po.GemiiHcpHospital;

public class GemiiHcpHospitalPOJO extends GemiiHcpHospital {
	
	public GemiiHcpHospitalPOJO(String cityName, String hsptType,
			String hsptLevel) {
		super();
		this.cityName = cityName;
		this.hsptType = hsptType;
		this.hsptLevel = hsptLevel;
	}
	public GemiiHcpHospitalPOJO(){
		super();
	} 
	private String cityName;
	private String hsptType;
	private String hsptLevel;
	private String areaName;

	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getHsptType() {
		return hsptType;
	}

	public void setHsptType(String hsptType) {
		this.hsptType = hsptType;
	}

	public String getHsptLevel() {
		return hsptLevel;
	}

	public void setHsptLevel(String hsptLevel) {
		this.hsptLevel = hsptLevel;
	}

	@Override
	public String toString() {
		return "GemiiHcpHospitalPOJO [cityName=" + cityName + ", hsptType="
				+ hsptType + ", hsptLevel=" + hsptLevel + ", areaName="
				+ areaName + "]";
	}
	
	

}
