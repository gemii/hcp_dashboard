package cc.gemii.hcp.pojo;

import cc.gemii.hcp.po.GemiiHcpActivity;

public class GemiiHcpActivityPOJO extends GemiiHcpActivity {

	private String hcpName;
	private String hcpClass;
	private String oaName;
	private String oaPhone;
	private String contractId;
	private String contractState;
	private String feedRemark;
	private String groupName;
	private String areaName;
	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getHcpClass() {
		return hcpClass;
	}

	public void setHcpClass(String hcpClass) {
		this.hcpClass = hcpClass;
	}
	public String getHcpName() {
		return hcpName;
	}

	public void setHcpName(String hcpName) {
		this.hcpName = hcpName;
	}

	public String getOaName() {
		return oaName;
	}

	public void setOaName(String oaName) {
		this.oaName = oaName;
	}

	public String getOaPhone() {
		return oaPhone;
	}

	public void setOaPhone(String oaPhone) {
		this.oaPhone = oaPhone;
	}

	public String getContractState() {
		return contractState;
	}

	public void setContractState(String contractState) {
		this.contractState = contractState;
	}

	public String getFeedRemark() {
		return feedRemark;
	}

	public void setFeedRemark(String feedRemark) {
		this.feedRemark = feedRemark;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	@Override
	public String toString() {
		return "GemiiHcpActivityPOJO [hcpName=" + hcpName + ", hcpClass="
				+ hcpClass + ", oaName=" + oaName + ", oaPhone=" + oaPhone
				+ ", contractId=" + contractId + ", contractState="
				+ contractState + ", feedRemark=" + feedRemark + ", groupName="
				+ groupName + ", areaName=" + areaName + "]";
	}

	
}
