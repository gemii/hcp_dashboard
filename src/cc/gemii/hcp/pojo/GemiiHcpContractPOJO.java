package cc.gemii.hcp.pojo;

import cc.gemii.hcp.po.GemiiHcpContract;

public class GemiiHcpContractPOJO extends GemiiHcpContract {

	private String hcpName;
	private String staffName;
	public String getHcpName() {
		return hcpName;
	}
	public void setHcpName(String hcpName) {
		this.hcpName = hcpName;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	
}
