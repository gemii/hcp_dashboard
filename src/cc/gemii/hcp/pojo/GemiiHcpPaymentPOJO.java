package cc.gemii.hcp.pojo;

import java.util.Date;

public class GemiiHcpPaymentPOJO {
	private String activityId;
	private String contractId;
	private String feedPaid;
	private String staffName;
	private String hcpName;
	private String actSubject;
	private String actRealEnd;
	private Date payToThirdTime;
	private Date payTime;
	private String payRemark;
	private String paymentId;
	private String bank;
	private String bankCardName;
	private String bankCardNumber;
	private String payState;
	private String payToThird;
	private String payFile;
	private String cityId;
	private Date verfiryTime;
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getFeedPaid() {
		return feedPaid;
	}
	public void setFeedPaid(String feedPaid) {
		this.feedPaid = feedPaid;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getHcpName() {
		return hcpName;
	}
	public void setHcpName(String hcpName) {
		this.hcpName = hcpName;
	}
	public String getActSubject() {
		return actSubject;
	}
	public void setActSubject(String actSubject) {
		this.actSubject = actSubject;
	}
	public String getActRealEnd() {
		return actRealEnd;
	}
	public void setActRealEnd(String actRealEnd) {
		this.actRealEnd = actRealEnd;
	}
	public Date getPayTime() {
		return payTime;
	}
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	public String getPayRemark() {
		return payRemark;
	}
	public void setPayRemark(String payRemark) {
		this.payRemark = payRemark;
	}
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getBankCardName() {
		return bankCardName;
	}
	public void setBankCardName(String bankCardName) {
		this.bankCardName = bankCardName;
	}
	public String getBankCardNumber() {
		return bankCardNumber;
	}
	public void setBankCardNumber(String bankCardNumber) {
		this.bankCardNumber = bankCardNumber;
	}
	public String getPayState() {
		return payState;
	}
	public void setPayState(String payState) {
		this.payState = payState;
	}
	public Date getPayToThirdTime() {
		return payToThirdTime;
	}
	public void setPayToThirdTime(Date payToThirdTime) {
		this.payToThirdTime = payToThirdTime;
	}
	public String getPayToThird() {
		return payToThird;
	}
	public void setPayToThird(String payToThird) {
		this.payToThird = payToThird;
	}
	public String getPayFile() {
		return payFile;
	}
	public void setPayFile(String payFile) {
		this.payFile = payFile;
	}
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	
	public Date getVerfiryTime() {
		return verfiryTime;
	}
	public void setVerfiryTime(Date verfiryTime) {
		this.verfiryTime = verfiryTime;
	}
	@Override
	public String toString() {
		return "GemiiHcpPaymentPOJO [activityId=" + activityId
				+ ", contractId=" + contractId + ", feedPaid=" + feedPaid
				+ ", staffName=" + staffName + ", hcpName=" + hcpName
				+ ", actSubject=" + actSubject + ", actRealEnd=" + actRealEnd
				+ ", payToThirdTime=" + payToThirdTime + ", payTime=" + payTime
				+ ", payRemark=" + payRemark + ", paymentId=" + paymentId
				+ ", bank=" + bank + ", bankCardName=" + bankCardName
				+ ", bankCardNumber=" + bankCardNumber + ", payState="
				+ payState + ", payToThird=" + payToThird + ", payFile="
				+ payFile + ", cityId=" + cityId + ", verfiryTime="
				+ verfiryTime + "]";
	}
	
}
