package cc.gemii.hcp.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.xml.crypto.Data;

/**
 * 
 * class_name：TimeUtils description： 用于返回时间值以及截取时间等操作 editor：yunfei.han date：Jul
 * 27, 2016 1:50:02 PM
 * 
 * @version V1.0
 *
 */
public class TimeUtils {

	/**
	 * 
	 * @return 纯数字类型的当前时间
	 * @throws ParseException
	 */
	public static String getTime() {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");// 设置日期格式
		return df.format(new Date());
	}

	public static Date getDate() {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");// 设置日期格式
		try {
			return df.parse(df.format(new Date()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param 时间类型
	 *            如：yyyy-mm-dd HH:mm:ss
	 * @return 格式化后的当前时间
	 */
	public static String getTime(String type) {
		SimpleDateFormat df = new SimpleDateFormat(type);// 设置日期格式
		return df.format(new Date());
	}

	public static Date getDate(String type) {
		SimpleDateFormat df = new SimpleDateFormat(type);// 设置日期格式
		try {
			return df.parse(df.format(new Date()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @Title: 日期类型的转换
	 * @Description: TODO
	 * @param @param 要转化的美式日期 如：Tue Aug 02 11:00:18 CST 2016
	 * @return 转化后的日期 如2016-08-02 11:00:18
	 * @throws
	 */
	public static String changeDate(Date date) {
		try {
			/*if (date.matches(".*[a-zA-Z]+.*") == true) {
				SimpleDateFormat sdf = new SimpleDateFormat(
						"EEE MMM dd HH:mm:ss z yyyy", Locale.US);
				Date d = sdf.parse(date);*/
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				return sdf.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date.toString();
	}
	/**
	 * 
	* @Title 获取时间差
	* @param  要比较的时间
	* @return 该时间距离今天还有多少天 负数代表该天已经过去
	* @throws
	 */
	public static long dateDiff(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date1 = TimeUtils.getDate();
		 long l=date.getTime()-date1.getTime();
		 long day=l/(24*60*60*1000);
		return day;
	}
	/**
	 *   判断当前时间是否在指定日期之间
	 * @param 开始星期X
	 * @param 开始时间
	 * @param 结束星期X
	 * @param 结束时间
	 * @return  true|fasle
	 */
    public static boolean isTimeArea(String timeArr){
//    	System.out.println(weekStart+" "+hourStart+" "+weekEnd+" "+hourEnd);
    	Calendar cal=Calendar.getInstance();
    	cal.setTime(new Date()); 
    	int week=cal.get(Calendar.DAY_OF_WEEK)-1;
    	week = week==0 ? 7:week;
    	if (timeArr.contains(String.valueOf(week))) {
			return false;
		}
    	return true;
    	//int hour = cal.get(Calendar.HOUR_OF_DAY);
    	/*if(weekStart<weekEnd){   //处理周一至周五类型  
    		if(week<weekStart||week>weekEnd){   //不在周一至周五之间
    			return false;
    		}
        	if(week==weekStart&&hour>=hourStart){  //等于周一而且当前时间大于等于开始时间
        		return true;
        	}
        	if(week==weekEnd&&hour<=hourEnd){   //等于周五而且当前时间小于等于结束时间
        		return true;
        	}
        	if(week>weekStart&&week<weekEnd){   //在周一至周五之间  如周二
        		return true;
        	}
        	return false;  //其他情况返回false
    	}else if(weekStart>weekEnd){  //处理周五至下周一此类情况
    		if(week>weekEnd&&week<weekStart){  //周二这样的直接返回false
    			return false;
    		}
    		if(week==weekStart&&hour>=hourStart){  //周五的判断当前时间
        		return true;
        	}
        	if(week==weekEnd&&hour<=hourEnd){  //周一的判断当前时间
        		return true;
        	}
    		if(week>weekStart){  //周六
        		return true;
        	}
        		return true;
    	}else{  //开始结束星期为同一天
    		if(hourStart>hourEnd){    //周五12点至周五8点情况  跨越一周
    			if(week==weekEnd&&hour>hourEnd&&hour<hourStart){  //只要周五八点到十二点之间才返回false
    				return false;
    			}else{
    				return true;
    			}
    		}else{
    			if(week==weekEnd&&hour>=hourStart&&hour<=hourEnd){
    				return true;
    			}else{
    				return false;
    			}
    		}
    	} */
    }
    /**
     * 
    * @Title  字符串转化为日期类型
    * @param  日期类型
    * @return yyyy-MM-dd类型的日期
    * @throws
     */
    public static Date StringToDate(String string){
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
	    Date date = new Date();
		try {
			date = sdf.parse(string);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    return date;
    }
    /**
     * 
    * @Title  字符串转化为日期类型
    * @param  日期类型
    * @return 全类型的日期
    * @throws
     */
    public static Date StringToDateAll(String string){
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	    Date date = new Date();
		try {
			date = sdf.parse(string);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    return date;
    }
    /**
     *  获取N天前的日期
     * @param day  天数
     * @return 日期  yyyy-mm-dd
     */
    public static String GetDayBegin(int day){
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    Date date = new Date();
	    Calendar c = Calendar.getInstance();  
        c.setTime(date);   //设置当前日期  
        c.add(Calendar.DATE, -day); //日期减1天  
        date = c.getTime(); 
    	return sdf.format(date);
    }
	public static void main(String[] args) throws ParseException {
		String hcpTime = "3";
		boolean flag = isTimeArea(hcpTime);
		System.out.println(GetDayBegin(0));
	}
}
