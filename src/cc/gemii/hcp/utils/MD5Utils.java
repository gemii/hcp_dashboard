package cc.gemii.hcp.utils;

import java.security.MessageDigest;

import org.apache.commons.codec.digest.Md5Crypt;

public class MD5Utils {
	public static void main(String[] args) {
		String str = "上海景栗信息科技-HCP";
		
		System.out.println(md5Encode(str));
		System.out.println(md5Encode(md5Encode("rrYYNC_2016-08-30 04:27:09")));
		System.out.println(md5Encode("ca4585d65b4c86a5a7f897f8c75d5262"));
	}
	    /*** 
	     * MD5加密 生成32位md5码
	     * @param 待加密字符串
	     * @return 返回32位md5码
	     */
	    public static String md5Encode(String inStr) {
	     try{
		        MessageDigest md5 = null;
		        try {
		            md5 = MessageDigest.getInstance("MD5");
		        } catch (Exception e) {
		            System.out.println(e.toString());
		            e.printStackTrace();
		            return "";
		        }
	
		        byte[] byteArray = inStr.getBytes("UTF-8");
		        byte[] md5Bytes = md5.digest(byteArray);
		        StringBuffer hexValue = new StringBuffer();
		        for (int i = 0; i < md5Bytes.length; i++) {
		            int val = ((int) md5Bytes[i]) & 0xff;
		            if (val < 16) {
		                hexValue.append("0");
		            }
		            hexValue.append(Integer.toHexString(val));
		        }
	             return hexValue.toString();
	     }catch(Exception e){
	        	e.printStackTrace();
	      }
	     return inStr;
	    }
}
