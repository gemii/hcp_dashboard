package cc.gemii.hcp.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class FileUtil {
	/**
	 * 
	 * @param 要截取的文件名
	 *            类型：时间_文件名
	 * @return 截取后的文件名
	 */
	public static String subStringFileName(String fileName) {
		fileName = fileName.substring(fileName.indexOf('_') + 1);
		return fileName;
	}
	/**
	 * 
	* @Title 截取文件名
	* @param  截取的文件类型
	* @return 文件类型 如：.jpg
	* @throws
	 */
	public  static String getFileType(String fileName){
		String type = fileName.substring(fileName.lastIndexOf("."));
		return type;
	}
	/**
	 * 将String保存到指定的目录下，文件名为当前时间 ，目录不存在会自动新建
	 * @param 文件地址
	 * @param 文件要保存的内容
	 * @return 保存结果 success 成功 其他 错误信息
	 */
	public static String saveFile(String path,String content){
		byte[] bytexml = content.getBytes();  
        
        try {  
      	  if (!(new File(path).isDirectory())) {  //判断log文件夹是否存在，不存在则新建
      		    new File(path).mkdir();
      	}
            OutputStream os = new FileOutputStream(new File(path+"/"+TimeUtils.getTime()+".txt"));  
            os.write(bytexml);  
            os.flush();  
            os.close();  
        } catch (FileNotFoundException e) {  
            e.printStackTrace();  
            return e.getMessage();
        } catch (IOException e) {  
            e.printStackTrace();  
            return e.getMessage();
        }  
		return "success";
	}
	public static void main(String[] args) {
		String pathString = "111.mp4";
		System.out.println(getFileType(pathString));
	}
}
