package cc.gemii.hcp.utils;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;


public class JsonUtils<T> {

	public String ObjectToJson(List<T> list){
		ObjectMapper mapper = new ObjectMapper();
		String string = null;
		try {
			string = mapper.writeValueAsString(list);
			System.out.println(string);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return string;
	}
}
