package cc.gemii.hcp.utils;

import java.util.Map;

public class InterfaceUtils {

	private String reason;  //返回说明
	private Integer error_code; //返回码
	private Map<String, Object> result;
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Integer getError_code() {
		return error_code;
	}
	public void setError_code(Integer error_code) {
		this.error_code = error_code;
	}
	public Map<String, Object> getResult() {
		return result;
	}
	public void setResult(Map<String, Object> result) {
		this.result = result;
	}
	@Override
	public String toString() {
		return "InterfaceUtils [reason=" + reason + ", error_code="
				+ error_code + ", result=" + result + "]";
	}
	
}
