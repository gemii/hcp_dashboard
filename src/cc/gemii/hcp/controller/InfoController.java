package cc.gemii.hcp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.hcp.po.GemiiHcpInfo;
import cc.gemii.hcp.service.InfoService;
/**
 * 
 * class_name：InfoController   
 * description：   信息控制器
 * editor：yunfei.han
 * date：Aug 19, 2016 10:30:07 AM      
 * @version  V1.0
 *
 */
@Controller
@RequestMapping("/Info")
public class InfoController {
	@Autowired
	private InfoService infoService;

	@RequestMapping("/city/{dID}/{pID}")
	public @ResponseBody List<GemiiHcpInfo> getCityMessage(
			@PathVariable Integer dID, @PathVariable Integer pID) {
		return infoService.getInfoByDid(dID, pID);
	}
}
