package cc.gemii.hcp.controller.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.hcp.po.GemiiHcpContract;
import cc.gemii.hcp.po.GemiiHcpDoctor;
import cc.gemii.hcp.po.GemiiHcpVerify;
import cc.gemii.hcp.pojo.GemiiHcpVerifyPOJO;
import cc.gemii.hcp.service.ActivityService;
import cc.gemii.hcp.service.ContractService;
import cc.gemii.hcp.service.HcpService;
import cc.gemii.hcp.service.InfoService;
import cc.gemii.hcp.service.UploadTableService;
import cc.gemii.hcp.utils.AESUtil;
import cc.gemii.hcp.utils.TimeUtils;

/**
 * class_name：Application   
 * description：处理审核员的Hcp请求
 * editor：yunfei.han
 * date：Jul 26, 2016 11:19:49 AM      
 * @version  V1.0
 */
@Controller
@RequestMapping("/hcp/admin")
public class AdminDoctorController {
	 @Autowired
	 private HcpService hcpService;
	 @Autowired
	 private InfoService infoService;
	 @Autowired
	 private UploadTableService uploadTableService;
	 @Autowired
	 private ContractService contractService;
	 @Autowired
	 private ActivityService activityService;
    
     @RequestMapping("/view")
   	 public String view(){
   		 return "hcp/admin/view";
   	 }
     @RequestMapping("/verify")
   	 public String verify(){
   		 return "hcp/admin/verifyList";
   	 }
     @RequestMapping("/save")
	 public String save(GemiiHcpDoctor doctor){
    	 doctor.setUpdateRemark("");
		 hcpService.updeteDoctor(doctor);
		 activityService.updatePayError(doctor.getId());
		return "redirect:/hcp/admin/verify";
	 }
     @RequestMapping("/verifylist")
     public @ResponseBody List<GemiiHcpVerifyPOJO> list(){
  		return hcpService.selectByNotVerify();
     }
     @RequestMapping("/list")
     public @ResponseBody List<GemiiHcpDoctor> list(String hsptName,String hcpName){
    	 HashMap<String, Object> map = new HashMap<String, Object>();
    	 if(hsptName!=null&&hsptName.length()>0)
      		 map.put("hsptName", hsptName);
      	if(hcpName!=null&&hcpName.length()>0)
    	 map.put("hcpName", AESUtil.encrypt2Str(hcpName));
    	 return hcpService.selectByVerify(map);
     }
     @RequestMapping("/show/{id}")
     @ResponseBody
     public Map<String, Object> show(@PathVariable String id){
    	 Map<String, Object> map = new HashMap<String, Object>();
    	 GemiiHcpDoctor doctor = hcpService.selectByDoctorId(id);
    	 doctor.setRemark(TimeUtils.changeDate(doctor.getHcpCtime()));
    	 doctor.setHcpPost(infoService.getInfoById(doctor.getHcpPost()).getContent());
    	 doctor.setHcpTitle(infoService.getInfoById(doctor.getHcpTitle()).getContent());
    	 map.put("doctor", doctor);
    	 if(doctor.getHcpTitleFile()!=null)
    	   map.put("file",uploadTableService.selectById(doctor.getHcpTitleFile()));
    	 ArrayList<HashMap<String, Object>> contractMap = new ArrayList<HashMap<String,Object>>();
		 List<GemiiHcpContract> contractList = contractService.selectByHcpId(id);
		 for (GemiiHcpContract gemiiHcpContract : contractList) {
			 HashMap<String, Object> contractMap2 = new HashMap<String, Object>();
			 contractMap2.put("id", gemiiHcpContract.getId());
			 contractMap2.put("date", "");
			 contractMap2.put("tp_name", gemiiHcpContract.getRemark());
	        	String flag = gemiiHcpContract.getState();
	        		 switch (flag) {
	     			case "5":
	     				long day = TimeUtils.dateDiff(gemiiHcpContract.getContractEndTime());
	     				if(day>=0){
	     					 contractMap2.put("state","通过");
	     					 contractMap2.put("date", day+"天");
	     				}
	     				else {
	     					contractMap2.put("state","已失效");
	     				}
	     				break;
	     			case "0":
	     				contractMap2.put("state","审核中");
	     				break;
	     			case "1":
	     				contractMap2.put("state","待签章");
	     				break;
	     			case"2":
	     				contractMap2.put("state","初审未通过");
	     				break;
	     			case "3":
	     				contractMap2.put("state","待上传");
	     				break;
	     			case "4":
	     				contractMap2.put("state","待终审");
	     				break;
	     			case"6":
	     				contractMap2.put("state","终审未通过");
	     				break;
	    			}
	        	contractMap.add(contractMap2); 
		}
		
	 map.put("contract", contractMap);
    	 return map;
     }
}
