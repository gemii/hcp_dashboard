package cc.gemii.hcp.controller.admin;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cc.gemii.hcp.po.GemiiHcpContract;
import cc.gemii.hcp.po.GemiiHcpContractThird;
import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.pojo.GemiiHcpContractPOJO;
import cc.gemii.hcp.service.ContractService;
import cc.gemii.hcp.service.HcpService;
import cc.gemii.hcp.service.UploadTableService;
import cc.gemii.hcp.service.UserService;
import cc.gemii.hcp.utils.TimeUtils;

@Controller
@RequestMapping("/contract/admin")
public class AdminContractController {
	@Autowired
	private ContractService contractService;
	@Autowired
	private HcpService hcpService;
	@Autowired
	private UploadTableService uploadTableService;
	@Autowired
	private UserService userService;
	
	@RequestMapping("/check/{contractId}")
	public void check(@PathVariable String contractId){
		System.out.println("--"+contractId);
	}
	@RequestMapping("/view")
	public String view(){
		return "contract/admin/view";
	}
	@RequestMapping("/list/{tp_id}")
	@ResponseBody
	public Map<String, Object> list(HttpServletRequest request,@PathVariable Integer tp_id){
  		HashMap<String, Object> param = new HashMap<String, Object>();
  		param.put("third", tp_id);
  		List<GemiiHcpContractPOJO> list =  contractService.getContractList(param);
  		Map<String, Object> map = new HashMap<String, Object>();
  		map.put("list", list);
  		map.put("time", TimeUtils.getDate("yyyy-MM-dd"));
  		return map;
	}
	@RequestMapping("/show/{id}")
	@ResponseBody
	public Map<String, Object> show(@PathVariable String id){
		Map<String, Object> map = new HashMap<String, Object>();
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("id", id);
		GemiiHcpContractPOJO contractPOJO = contractService.getContractById(param);
		map.put("contract", contractPOJO);
		//map.put("file", uploadTableService.selectById(contractPOJO.getFileId()));
		map.put("time", TimeUtils.getDate("yyyy-MM-dd"));
		GemiiHcpContractThird third = contractService.selectByContractThirdId(id);
		map.put("third", third);
		GemiiHcpUser user = userService.selectByUserId(third.getTpId());
		map.put("tp_name", user.getName());
		if(third!=null){
			if(third.getFileId()!=null)
			map.put("file1", uploadTableService.selectById(third.getFileId()));
		}
		return map;
	}
	@RequestMapping("/save")
	public void save(HttpServletResponse response,GemiiHcpContract contract,HttpServletRequest request) throws IOException{
		response.setContentType("text/html;charset=utf-8");
		int count = contractService.updateContract(contract);
 		if (count > 0) {
	 			response.getWriter()
	 					.write("<script>alert('审核成功！');window.location='"+request.getContextPath()+"/contract/admin/view';</script>");
	 		} else {
	 			response.getWriter().write("<script>alert('审核失败，请重试！');window.location='"+request.getContextPath()+"/contract/admin/view';</script>");
	 		}
	}
	@RequestMapping("/third")
	public void third(HttpServletResponse response,GemiiHcpContractThird third,HttpServletRequest request) throws IOException{
		response.setContentType("text/html;charset=utf-8");
			third.setCtime(TimeUtils.getDate());
			third.setState(0);
			int count = contractService.updateContractThird(third);
			GemiiHcpContract contract = new GemiiHcpContract();
			contract.setId(third.getContractId());
			contract.setState("3");
			contractService.updateContract(contract);//更新合同状态
			if(count<1){
				response.getWriter().write("<script>alert('操作失败，请重试！');window.location='"+request.getContextPath()+"/contract/admin/view';</script>");
				return;
			}
		response.getWriter()
			.write("<script>alert('操作成功！');window.location='"+request.getContextPath()+"/contract/admin/view';</script>");
	}
	@RequestMapping("/detail/{id}")
	public ModelAndView detail(ModelAndView mav,@PathVariable String id){
		mav.addObject("id", id);
		mav.setViewName("contract/admin/detail");
		return mav;
	}
	@RequestMapping("/last")
	public void last(HttpServletResponse response,GemiiHcpContractThird third,String ctId,HttpServletRequest request) throws IOException{
		response.setContentType("text/html;charset=utf-8");
		int count = contractService.updateContractThird(third);
		if (count > 0) {
 			GemiiHcpContract contract = new GemiiHcpContract();
 			contract.setId(ctId);
 			if(third.getState()==2){
 				contract.setState("5");
 			}
 			else {
 				contract.setState("6");
 				contract.setFeedRemark(third.getRemark3());
			}
 			contractService.updateContract(contract); //更新状态
	 			response.getWriter()
	 					.write("<script>alert('提交成功！');window.close();</script>");
	 		} else {
	 			response.getWriter().write("<script>alert('审核失败，请重试！');window.location='"+request.getContextPath()+"/contract/admin/detail/"+ctId+"';</script>");
	 		}
	}
	@RequestMapping("/getTPN/{id}")
	@ResponseBody
	public Map<String, Object> getTP_Name(@PathVariable String id){
		Map<String, Object> map = new HashMap<String, Object>();
		GemiiHcpContractThird third = contractService.selectByContractThirdId(id);
		GemiiHcpUser user = userService.selectByUserId(third.getTpId());
		map.put("tp_name", user.getName());
		return map;
	}
}
