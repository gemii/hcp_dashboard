package cc.gemii.hcp.controller.admin;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.hcp.po.GemiiHcpGroup;
import cc.gemii.hcp.service.AreaService;
import cc.gemii.hcp.service.GroupService;
import cc.gemii.hcp.service.InfoService;
/**
 * 
 * class_name：AdminGroupController   
 * description：管理员操作微信群的控制器
 * editor：yunfei.han
 * date：Aug 13, 2016 10:33:26 AM      
 * @version  V1.0
 *
 */
@Controller
@RequestMapping("/group/admin")
public class AdminGroupController {

	@Autowired
	private GroupService groupService;
	@Autowired
	private InfoService infoService;
	@Autowired
	private AreaService areaService;
	/**
	 * 
	* @Title 负责从菜单栏跳转至微信群列表
	* @return 
	* @throws
	 */
	@RequestMapping("/view")
	public String view(){
		return "group/GroupAdminView";
	}
	@RequestMapping("/list")
	@ResponseBody
	public Map<String, Object> list(String groupName){
		Map<String, Object> map = new HashMap<String, Object>();
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("groupName", groupName);
	    map.put("group", groupService.selectByGroupName(param));
		return map;
	}
	@RequestMapping("/show")
	@ResponseBody
	public Map<String, Object> show(String id){
		Map<String, Object> map = new HashMap<String, Object>();
		GemiiHcpGroup group = groupService.selectByGroupId(id);
		map.put("group", group);
		
		return map;
	}
}
