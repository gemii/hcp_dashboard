package cc.gemii.hcp.controller.admin;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.hcp.po.GemiiHcpActivity;
import cc.gemii.hcp.po.GemiiHcpActivityLog;
import cc.gemii.hcp.po.GemiiHcpContract;
import cc.gemii.hcp.po.GemiiHcpContractThird;
import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.pojo.GemiiHcpActivityPOJO;
import cc.gemii.hcp.service.ActivityLogService;
import cc.gemii.hcp.service.ActivityService;
import cc.gemii.hcp.service.ContractService;
import cc.gemii.hcp.service.HcpService;
import cc.gemii.hcp.service.UploadTableService;
import cc.gemii.hcp.service.UserService;
import cc.gemii.hcp.utils.AESUtil;
import cc.gemii.hcp.utils.FileUtil;
import cc.gemii.hcp.utils.MailUtils;
import cc.gemii.hcp.utils.TimeUtils;

/**
 * 
 * class_name：ActivityController 
 * description： 活动控制器-审核员 
 * editor：yunfei.han 
 * date：Aug 4, 2016 3:04:31 PM
 * @version V1.0
 *
 */
@Controller
@RequestMapping("/activity/admin")
public class AdminActivityController {

	@Autowired
	private ActivityService activityService;
	@Autowired
	private ContractService contractService;
	@Autowired
	private HcpService hcpService;
	@Autowired
	private UploadTableService uploadTableService;
	@Autowired
	private UserService userService;
	
	@RequestMapping("/view")
	public String View(){
		return "activity/admin/view";
	}
	@RequestMapping("/list")
	@ResponseBody
	public List<GemiiHcpActivity> list(String actSubject,String actStartTime,String actEndTime){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("actSubject", actSubject);
		if(actStartTime!=null&&actStartTime!="")
		   map.put("actStartTime", actStartTime+" 00:00:00");
		if(actEndTime!=null&&actEndTime!="")
			map.put("actEndTime", actEndTime+" 23:59:59");
		return activityService.selectByActSubject(map);
	}
	@RequestMapping("/show/{id}")
	@ResponseBody
	public Map<String, Object> Show(@PathVariable String id){
		Map<String, Object> map = new HashMap<String, Object>();
		GemiiHcpActivityPOJO activityPOJO =  activityService.selectByActivityId(id);
		if(activityPOJO.getContractId()!=null){
			GemiiHcpContract contract = contractService.selectByContractId(activityPOJO.getContractId());
			activityPOJO.setContractState(contract.getState());
			activityPOJO.setFeedRemark(contract.getFeedRemark());
		}
		if(activityPOJO.getState()!=0&&activityPOJO.getState()!=4){
			map.put("feedbackTime", TimeUtils.changeDate(activityPOJO.getFeedbackTime()));
			map.put("actRealStart", TimeUtils.changeDate(activityPOJO.getActRealStart()));
			map.put("actRealEnd", TimeUtils.changeDate(activityPOJO.getActRealEnd()));
			if(activityPOJO.getState()!=1)
				map.put("verfiryTime", TimeUtils.changeDate(activityPOJO.getVerfiryTime()));
			if(activityPOJO.getActFile()!=null)
				map.put("file", uploadTableService.selectById(activityPOJO.getActFile()));
		}
		if(activityPOJO.getPayFile()!=null)
			map.put("file1", uploadTableService.selectById(activityPOJO.getPayFile()));
		map.put("activity", activityPOJO);
		map.put("startTime", TimeUtils.changeDate(activityPOJO.getActStartTime()));
		map.put("endTime", TimeUtils.changeDate(activityPOJO.getActEndTime()));
		map.put("actCtime", TimeUtils.changeDate(activityPOJO.getActCtime()));
	
		return map;
	}
	@RequestMapping("/verify/{id}/{hcpId}")
	public ModelAndView verify(ModelAndView mav, @PathVariable String id,@PathVariable String hcpId){
		mav.addObject("id", id);
		mav.addObject("hcpId", hcpId);
		mav.setViewName("activity/admin/verify");
		return mav;
	}
	@RequestMapping("/verify/data/{id}/{hcpId}")
	@ResponseBody
	public Map<String, Object> getData(@PathVariable String id,@PathVariable String hcpId){
		Map<String, Object> map = new HashMap<String, Object>();
		GemiiHcpActivityPOJO activityPOJO =  activityService.selectByActivityId(id);
		map.put("feedbackTime", TimeUtils.changeDate(activityPOJO.getFeedbackTime()));
		map.put("actRealStart", TimeUtils.changeDate(activityPOJO.getActRealStart()));
		map.put("actRealEnd", TimeUtils.changeDate(activityPOJO.getActRealEnd()));
		
		map.put("activity", activityPOJO);
		map.put("startTime", TimeUtils.changeDate(activityPOJO.getActStartTime()));
		map.put("endTime", TimeUtils.changeDate(activityPOJO.getActEndTime()));
		map.put("actCtime", TimeUtils.changeDate(activityPOJO.getActCtime()));
		
		String contractId = hcpService.selectByDoctorId(hcpId).getContractId();
		GemiiHcpContract contract = contractService.selectByContractId(contractId);
		if(activityPOJO.getActFile()!=null)
			map.put("file", uploadTableService.selectById(activityPOJO.getActFile()));
		map.put("contract", contract);
		return map;
	}
	@RequestMapping("/verify/contract/{id}")
	@ResponseBody
	public Map<String, Object> getContract(@PathVariable String id){
		Map<String, Object> map = new HashMap<String, Object>();
		GemiiHcpContract contract = contractService.selectByContractId(id);
		//map.put("file", uploadTableService.selectById(contract.getFileId()));
		map.put("contract", contract);
		map.put("ctime", TimeUtils.changeDate(contract.getCtime()));
		map.put("contractStartTime", TimeUtils.changeDate(contract.getContractStartTime()).replace(" 00:00:00", ""));
		map.put("contractEndTime", TimeUtils.changeDate(contract.getContractEndTime()).replace(" 00:00:00", ""));
		GemiiHcpContractThird third = contractService.selectByContractThirdId(id);
		GemiiHcpUser user = userService.selectByUserId(third.getTpId());
		map.put("tp_name", user.getName());
		return map;
	}
	@RequestMapping("/save/{aId}/{hcpId}")
	public void save(HttpServletResponse response,HttpServletRequest request,GemiiHcpContract contract, @PathVariable String aId,@PathVariable String hcpId) throws IOException{
		response.setContentType("text/html;charset=utf-8");
		contract.setHcpId(null);
		int count = contractService.updateContract(contract);
 		if (count > 0) {
	 			response.getWriter()
	 					.write("<script>alert('审核成功！');window.location='"+request.getContextPath()+"/activity/admin/verify/"+aId+"/"+hcpId+"';</script>");
	 		} else {
	 			response.getWriter().write("<script>alert('审核失败，请重试！');window.location='"+request.getContextPath()+"/activity/admin/verify/"+aId+"/"+hcpId+"';</script>");
	 		}
	}
	@RequestMapping("/update")
	@ResponseBody
	public Map<String, Object> update(GemiiHcpActivity activity) throws IOException{
		Map<String, Object> map = new HashMap<String, Object>();
		activity.setVerfiryTime(TimeUtils.getDate());
		int count = activityService.updateActivity(activity);
		map.put("count", count);
		return map;
	}
	@RequestMapping("/contract/{id}")
	@ResponseBody
	public ArrayList<HashMap<String, Object>> contract(@PathVariable String id){
		ArrayList<HashMap<String, Object>> contractMap = new ArrayList<HashMap<String,Object>>();
		 List<GemiiHcpContract> contractList = contractService.selectByHcpId(id);
		 for (GemiiHcpContract gemiiHcpContract : contractList) {
			 HashMap<String, Object> contractMap2 = new HashMap<String, Object>();
			 contractMap2.put("id", gemiiHcpContract.getId());
			 contractMap2.put("contractStartTime", gemiiHcpContract.getContractStartTime());
			 contractMap2.put("contractEndTime", gemiiHcpContract.getContractEndTime());
			 contractMap2.put("tp_name", gemiiHcpContract.getRemark());
	        	String flag = gemiiHcpContract.getState();
	        		 switch (flag) {
	     			case "5":
	     				long day = TimeUtils.dateDiff(gemiiHcpContract.getContractEndTime());
	     				if(day>=0){
	     					 contractMap2.put("state","通过");
	     					 contractMap2.put("date", day+"天");
	     				}
	     				else {
	     					contractMap2.put("state","已失效");
	     				}
	     				break;
	     			case "0":
	     				contractMap2.put("state","审核中");
	     				break;
	     			case "1":
	     				contractMap2.put("state","待签章");
	     				break;
	     			case"2":
	     				contractMap2.put("state","初审未通过");
	     				break;
	     			case "3":
	     				contractMap2.put("state","待上传");
	     				break;
	     			case "4":
	     				contractMap2.put("state","待终审");
	     				break;
	     			case"6":
	     				contractMap2.put("state","终审未通过");
	     				break;
	    			}
	        	contractMap.add(contractMap2); 
		}
   	 return contractMap;
	}
	
	@Autowired
	private ActivityLogService activityLogService;
	@Value("${logPath}")
	private String logPath;
//	@Value("${activity_api_day}")
//	private String activity_api_day;
  /**
   * 每天22点30启动任务
   */
  @RequestMapping("/import/{day_number}")
  @ResponseBody
  public Map<String, Object> test1(String day_number)
  {
	  DefaultHttpClient httpClient = new DefaultHttpClient();  
	    String soapRequestData = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +  
	            "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"  +  
	              "<soap12:Body>" +  
	               " <GetTodayScheduling xmlns=\"http://tempuri.org/\">" +  
	                 " <key>12E07D22-5B56-41A6-A2A2-64EA9152FC07</key>" +  
	            //     "<confirmStartDate>"+TimeUtils.GetDayBegin(Integer.valueOf(day_number))+"</confirmStartDate>"+
	              //   "<confirmEndDate>"+TimeUtils.GetDayBegin(Integer.valueOf(0))+"</confirmEndDate>"+
	                "</GetTodayScheduling>" +  
	             " </soap12:Body>" +  
	            "</soap12:Envelope>" ; 
	  Map<String,Object> returnMap= new HashMap<String, Object>();
      HttpPost httppost = new HttpPost("http://54.223.84.125:8099/SchedulingWebService.asmx"); 
      GemiiHcpActivityLog activityLog = new GemiiHcpActivityLog();
      String textString = "",mailMessage = "",adminString = "";
      int successCount = 0, dupCount = 0, errorCount = 0;
      /*把Soap请求数据添加到PostMethod*/  
      //byte[] b = soapRequestData.getBytes("utf-8");  
      //InputStream is = new ByteArrayInputStream(b,0,b.length);  
      try {  
          HttpEntity re = new StringEntity(soapRequestData,HTTP.UTF_8);  
          httppost.setHeader("Content-Type","application/soap+xml; charset=utf-8");  
          //httppost.setHeader("Content-Length", String.valueOf(soapRequestData.length()));  
          httppost.setEntity(re);           
          HttpResponse response = httpClient.execute(httppost);  
         // System.out.println(EntityUtils.toString(httppost.getEntity()));  
          int state = response.getStatusLine().getStatusCode();
          String string = EntityUtils.toString(response.getEntity());
          activityLog.setState(state+"");
          activityLog.setContent(string);
          
          mailMessage +="state:"+state;
          
          if(state==200){
              String json = string.substring(string.indexOf("{"),string.lastIndexOf("}")+1);
              Map<String, Object> map = (Map<String, Object>) JSONObject.parse(json);
              Boolean flag = (Boolean) map.get("IsPass");
              mailMessage += "_flag:"+flag;
              if(flag==true){  //成功
           	      String Obj = AESUtil.Decrypt(map.get("Obj").toString(),"745732822d2c0b6e");
                  Integer ReordCount = (Integer) map.get("ReordCount");
                  List<Map<String, Object>> activityList = (List<Map<String, Object>>)JSONObject.parse(Obj);
                  for (Map<String, Object> activityMap : activityList) {
					GemiiHcpActivity activity = new GemiiHcpActivity();
					try{
						activity.setId(activityMap.get("id").toString());
						activity.setGroupId(String.valueOf(activityMap.get("group_id")));
						activity.setHcpId(String.valueOf(activityMap.get("hcp_id")));
						activity.setActStartTime(activityMap.get("act_start_time")==null?null:new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(activityMap.get("act_start_time").toString()+":00"));
						activity.setActEndTime(activityMap.get("act_end_time")==null?null:new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(activityMap.get("act_end_time").toString()+":00"));
						activity.setActPeriod( activityMap.get("act_period")==null?null:Integer.parseInt(activityMap.get("act_period").toString().replace(".0", "")));
						activity.setActReview(String.valueOf(activityMap.get("act_review")));
						activity.setActPaid( activityMap.get("act_paid")==null?null:Long.parseLong(activityMap.get("act_paid").toString().replace(".00", "")));
						activity.setActCourseware(String.valueOf(activityMap.get("act_courseware")));
						activity.setActSubject(activityMap.get("act_subject")==null? "":activityMap.get("act_subject").toString());
						activity.setActSubject(activityMap.get("hcp_wx_name")==null? "":activityMap.get("hcp_wx_name").toString());
						activity.setActSubject(activityMap.get("hcp_wx_nick")==null? "":activityMap.get("hcp_wx_nick").toString());
						activity.setActSubject(activityMap.get("remark")==null? "":activityMap.get("remark").toString());
						activity.setActSubject(activityMap.get("act_subject")==null? "":activityMap.get("act_subject").toString());
					    activity.setStaffId(activityMap.get("staff_id")==null?null:activityMap.get("staff_id").toString());
						
					    activity.setActCtime(activity.getActStartTime());
					    int num = 0;
					    if(activity.getActStartTime().after(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2016-09-21 00:00:00")))
					    	try{
					    		num = activityService.insertActivity(activity);
					    		successCount++;
					    	}catch (Exception e) {
								e.printStackTrace();
								String errorMessage = e.getMessage();
								if(errorMessage.contains("Duplicate entry")){
									dupCount++;
								}
								else {
									errorCount++;
									textString += activityMap.toString() +"\r\n"+e.getMessage() +"\r\n";
								}
							}
					}catch(Exception e){
						e.printStackTrace();
						errorCount++;
						textString += activityMap.toString() +"\r\n"+e.getMessage() +"\r\n";
					}
				}
                  adminString +="共"+ReordCount+"条记录,导入成功"+successCount+"条,重复记录"+dupCount+"条,出错记录:"+errorCount+"条";
                  textString += adminString +"\r\n";
                  mailMessage +="_ReordCount:"+ReordCount+"_success:"+successCount+"_duplicate:"+dupCount+"_error:"+errorCount;
              }else{  //失败
            	  textString += json +"\r\n";
              }
          }else{  //不是200的时候
        	  textString += string +"\r\n";
          }
      } catch (UnsupportedEncodingException e) {  
    	  textString += e.getMessage() +"\r\n";
          e.printStackTrace();  
      } catch (ClientProtocolException e) {  
    	  textString += e.getMessage() +"\r\n";
          e.printStackTrace();  
      } catch (Exception e) {  
    	  textString += e.getMessage() +"\r\n";
          e.printStackTrace();  
      }finally{  
    	  returnMap.put("result", adminString);
    	  activityLog.setCtime(TimeUtils.getDate());
    	  try{
    	    activityLogService.insertActivityLog(activityLog);
    	  }catch(Exception e){
    		  e.printStackTrace();
    		  textString += activityLog.toString();
    	  }
          httpClient.getConnectionManager().shutdown();  
          textString += "\r\n邮件内容：" + mailMessage;
          String returnString = FileUtil.saveFile(logPath+"/activity", textString);
          System.out.println("保存文件："+returnString);
          MailUtils.sendMessage(mailMessage);
      }
      return returnMap;
  }
	
}
