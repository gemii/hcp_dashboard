package cc.gemii.hcp.controller.third;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cc.gemii.hcp.po.GemiiHcpContract;
import cc.gemii.hcp.po.GemiiHcpContractThird;
import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.pojo.GemiiHcpContractPOJO;
import cc.gemii.hcp.service.ContractService;
import cc.gemii.hcp.service.HcpService;
import cc.gemii.hcp.service.UploadTableService;
import cc.gemii.hcp.utils.TimeUtils;

@Controller
@RequestMapping("/contract/third")
public class ThirdContractController {
	@Autowired
	private ContractService contractService;
	@Autowired
	private HcpService hcpService;
	 @Autowired
	 private UploadTableService uploadTableService;
	@RequestMapping("/view")
	public String view(){
		return "contract/third/view";
	}
	@RequestMapping("/list")
	@ResponseBody
	public Map<String, Object> list(HttpServletRequest request){
		//判断session
  		HttpSession session  = request.getSession();
  		//从session中取出用户身份信息
  		GemiiHcpUser third = (GemiiHcpUser) session.getAttribute("user");
  		
  		HashMap<String, Object> param = new HashMap<String, Object>();
  		param.put("third", third.getId());
  		List<GemiiHcpContractPOJO> list =  contractService.getContractList(param);
  		Map<String, Object> map = new HashMap<String, Object>();
  		map.put("list", list);
  		map.put("time", TimeUtils.getDate("yyyy-MM-dd"));
  		return map;
	}
	@RequestMapping("/show/{id}")
	@ResponseBody
	public Map<String, Object> show(@PathVariable String id){
		Map<String, Object> map = new HashMap<String, Object>();
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("id", id);
		GemiiHcpContractPOJO contractPOJO = contractService.getContractById(param);
		map.put("contract", contractPOJO);
		//map.put("file", uploadTableService.selectById(contractPOJO.getFileId()));
		GemiiHcpContractThird third = contractService.selectByContractThirdId(id);
		map.put("third", third);
		if(third!=null){
			if(third.getFileId()!=null)
			map.put("file1", uploadTableService.selectById(third.getFileId()));
		}
		return map;
	}
	@RequestMapping("/save")
	public void save(HttpServletResponse response,HttpServletRequest request,GemiiHcpContractThird third,String ctId) throws IOException{
		response.setContentType("text/html;charset=utf-8");
		third.setState(1);
		third.setFtime(TimeUtils.getDate());
		int count = contractService.updateContractThird(third);
 		if (count > 0) {
 			GemiiHcpContract contract = new GemiiHcpContract();
 			contract.setId(ctId);
 			contract.setState("4");
 			contractService.updateContract(contract); //更新状态
	 			response.getWriter()
	 					.write("<script>alert('提交成功！');window.close();</script>");
	 		} else {
	 			response.getWriter().write("<script>alert('审核失败，请重试！');window.location='"+request.getContextPath()+"/contract/third/detail/"+ctId+"';</script>");
	 		}
	}
	@RequestMapping("/detail/{id}")
	public ModelAndView detail(ModelAndView mav,@PathVariable String id){
		mav.addObject("id", id);
		mav.setViewName("contract/third/detail");
		return mav;
	}
}
