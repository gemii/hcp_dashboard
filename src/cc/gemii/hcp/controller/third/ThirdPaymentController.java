package cc.gemii.hcp.controller.third;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cc.gemii.hcp.po.GemiiHcpActivity;
import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.pojo.GemiiHcpPaymentPOJO;
import cc.gemii.hcp.service.ActivityService;
import cc.gemii.hcp.service.InfoService;
import cc.gemii.hcp.service.PaymentService;
import cc.gemii.hcp.service.UploadTableService;
import cc.gemii.hcp.utils.TimeUtils;
/**
 * 
 * class_name：ThirdPaymentController   
 * description：  处理第三方支付的请求
 * editor：yunfei.han
 * date：Aug 16, 2016 5:05:20 PM      
 * @version  V1.0
 *
 */
@Controller
@RequestMapping("/payment/third")
public class ThirdPaymentController {

	@Autowired
	private PaymentService payService;
	@Autowired
	private ActivityService activityService;
	@Autowired
	private UploadTableService uploadTableService;
	@Autowired
	private InfoService infoService;
	
	@RequestMapping("/view/{state}")
	public ModelAndView view(ModelAndView mav,@PathVariable Integer state,HttpServletRequest request){
		
		String div_h3 = "待支付列表";
		 if(state==1){
			div_h3 = "支付成功列表";
		}else if(state==2){
			div_h3 = "支付失败列表";
		}else{  //其余情况均设置成1
			state = 0;
		}
		request.getSession().setAttribute("state", state);
		
		mav.addObject("div_h3", div_h3);
		mav.setViewName("payment/third/payList");
		return mav;
	}
	@RequestMapping("/getList")
	@ResponseBody
	public Map<String, Object> payList(HttpServletRequest request,String activityId,String startTime,String endTime){
		Map<String, Object> map = new HashMap<String, Object>();
		//判断session
  		HttpSession session  = request.getSession();
  		//从session中取出用户身份信息
  		GemiiHcpUser third = (GemiiHcpUser) session.getAttribute("user");
  		
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("tp_id", third.getId());
		param.put("aState", request.getSession().getAttribute("state"));
		param.put("activityId", activityId);
		if(startTime!=null&&startTime!="")
			param.put("startTime", startTime+" 00:00:00");
		if(endTime!=null&&endTime!="")
			param.put("endTime", endTime+" 23:59:59");
		map = payService.getNotPayList(param);
		List<GemiiHcpPaymentPOJO> list = (List<GemiiHcpPaymentPOJO>) map.get("payment");
		if(list.size()==1){
			GemiiHcpPaymentPOJO pojo = list.get(0);
			if(pojo.getPayFile()!=null)
				map.put("file", uploadTableService.selectById(pojo.getPayFile()));
			pojo.setCityId(infoService.getInfoTree(Integer.parseInt(pojo.getCityId())));
		}
		map.put("payment", list);
		return map;
	}
	
	@RequestMapping("/save")
	public void Savepoint(HttpServletResponse response,HttpServletRequest request,GemiiHcpActivity activity) throws IOException{
		response.setContentType("text/html;charset=utf-8");
		activity.setPayTime(TimeUtils.getDate());
		if(activity.getPayState()==1){
			activity.setPaymentId("P"+TimeUtils.getTime());
			activity.setPayRemark(null);
		}
		activityService.updateActivity(activity);
		response.getWriter().write("<script>alert('更新支付状态成功！');window.location='"+request.getContextPath()+"/payment/third/view/1';</script>");
	}

	
	@RequestMapping("/list")
	@ResponseBody
	public Map<String, Object> list(){
		return null;
	}
}
