package cc.gemii.hcp.controller.task;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import cc.gemii.hcp.po.GemiiHcpActivity;
import cc.gemii.hcp.po.GemiiHcpActivityLog;
import cc.gemii.hcp.service.ActivityLogService;
import cc.gemii.hcp.service.ActivityService;
import cc.gemii.hcp.utils.AESUtil;
import cc.gemii.hcp.utils.FileUtil;
import cc.gemii.hcp.utils.MailUtils;
import cc.gemii.hcp.utils.TimeUtils;

import com.alibaba.fastjson.JSONObject;

/**
 * 
 * 类名称：TimerTask   
 * 类描述：定时器任务
 * 创建人：yunfei.han
 * 创建时间：Aug 29, 2016 10:56:27 AM      
 * @version  V1.0
 *
 */
@Component
public class TimerTask {
	@Autowired
	private ActivityLogService activityLogService;
	@Autowired
	private ActivityService activityService;
	@Value("${logPath}")
	private String logPath;
	@Value("${activity_api_day}")
	private String activity_api_day;
  /**
   * 每天22点30启动任务
   */
  @Scheduled(cron = "0 30 22 ? * *")
  public void test1()
  {
	  DefaultHttpClient httpClient = new DefaultHttpClient();  
	    String soapRequestData = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +  
	            "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"  +  
	              "<soap12:Body>" +  
	               " <GetTodayScheduling xmlns=\"http://tempuri.org/\">" +  
	                 "<key>12E07D22-5B56-41A6-A2A2-64EA9152FC07</key>" + 
	               //  "<confirmStartDate>"+TimeUtils.GetDayBegin(Integer.valueOf(activity_api_day))+"</confirmStartDate>"+
	                 //"<confirmEndDate>"+TimeUtils.GetDayBegin(Integer.valueOf(0))+"</confirmEndDate>"+
	                "</GetTodayScheduling>" +  
	             " </soap12:Body>" +  
	            "</soap12:Envelope>" ; 
        
      HttpPost httppost = new HttpPost("http://54.223.84.125:8099/SchedulingWebService.asmx"); 
      GemiiHcpActivityLog activityLog = new GemiiHcpActivityLog();
      String textString = "",mailMessage = "";
      int successCount = 0, dupCount = 0, errorCount = 0;
      /*把Soap请求数据添加到PostMethod*/  
      //byte[] b = soapRequestData.getBytes("utf-8");  
      //InputStream is = new ByteArrayInputStream(b,0,b.length);  
      try {  
          HttpEntity re = new StringEntity(soapRequestData,HTTP.UTF_8);  
          httppost.setHeader("Content-Type","application/soap+xml; charset=utf-8");  
          //httppost.setHeader("Content-Length", String.valueOf(soapRequestData.length()));  
          httppost.setEntity(re);           
          HttpResponse response = httpClient.execute(httppost);  
         // System.out.println(EntityUtils.toString(httppost.getEntity()));  
          int state = response.getStatusLine().getStatusCode();
          String string = EntityUtils.toString(response.getEntity());
          activityLog.setState(state+"");
          activityLog.setContent(string);
          
          mailMessage +="state:"+state;
          
          if(state==200){
              String json = string.substring(string.indexOf("{"),string.lastIndexOf("}")+1);
              Map<String, Object> map = (Map<String, Object>) JSONObject.parse(json);
              Boolean flag = (Boolean) map.get("IsPass");
              mailMessage += "_flag:"+flag;
              if(flag==true){  //成功
           	      String Obj = AESUtil.Decrypt(map.get("Obj").toString(),"745732822d2c0b6e");
                  Integer ReordCount = (Integer) map.get("ReordCount");
                  List<Map<String, Object>> activityList = (List<Map<String, Object>>)JSONObject.parse(Obj);
                  for (Map<String, Object> activityMap : activityList) {
					GemiiHcpActivity activity = new GemiiHcpActivity();
					try{
						activity.setId(activityMap.get("id").toString());
						activity.setGroupId(String.valueOf(activityMap.get("group_id")));
						activity.setHcpId(String.valueOf(activityMap.get("hcp_id")));
						activity.setActStartTime(activityMap.get("act_start_time")==null?null:new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(activityMap.get("act_start_time").toString()+":00"));
						activity.setActEndTime(activityMap.get("act_end_time")==null?null:new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(activityMap.get("act_end_time").toString()+":00"));
						activity.setActPeriod( activityMap.get("act_period")==null?null:Integer.parseInt(activityMap.get("act_period").toString().replace(".0", "")));
						activity.setActReview(String.valueOf(activityMap.get("act_review")));
						activity.setActPaid( activityMap.get("act_paid")==null?null:Long.parseLong(activityMap.get("act_paid").toString().replace(".00", "")));
						activity.setActCourseware(String.valueOf(activityMap.get("act_courseware")));
						activity.setActSubject(activityMap.get("act_subject")==null? "":activityMap.get("act_subject").toString());
						activity.setActSubject(activityMap.get("hcp_wx_name")==null? "":activityMap.get("hcp_wx_name").toString());
						activity.setActSubject(activityMap.get("hcp_wx_nick")==null? "":activityMap.get("hcp_wx_nick").toString());
						activity.setActSubject(activityMap.get("remark")==null? "":activityMap.get("remark").toString());
						activity.setActSubject(activityMap.get("act_subject")==null? "":activityMap.get("act_subject").toString());
					    activity.setStaffId(activityMap.get("staff_id")==null?null:activityMap.get("staff_id").toString());
						
					    activity.setActCtime(activity.getActStartTime());
					    int num = 0;
					    if(activity.getActStartTime().after(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2016-09-21 00:00:00")))
					    	try{
					    		num = activityService.insertActivity(activity);
					    		successCount++;
					    	}catch (Exception e) {
								e.printStackTrace();
								String errorMessage = e.getMessage();
								if(errorMessage.contains("Duplicate entry")){
									dupCount++;
								}
								else {
									errorCount++;
								}
								textString += activityMap.toString() +"\r\n"+e.getMessage() +"\r\n";
							}
					}catch(Exception e){
						e.printStackTrace();
						errorCount++;
						textString += activityMap.toString() +"\r\n"+e.getMessage() +"\r\n";
					}
				}
                  textString +="共"+ReordCount+"条记录,导入成功"+successCount+"条,重复记录"+dupCount+"条,出错记录:"+errorCount+"条。\r\n" ;
                  mailMessage +="_ReordCount:"+ReordCount+"_success:"+successCount+"_duplicate:"+dupCount+"_error:"+errorCount;
              }else{  //失败
            	  textString += json +"\r\n";
              }
          }else{  //不是200的时候
        	  textString += string +"\r\n";
          }
      } catch (UnsupportedEncodingException e) {  
    	  textString += e.getMessage() +"\r\n";
          e.printStackTrace();  
      } catch (ClientProtocolException e) {  
    	  textString += e.getMessage() +"\r\n";
          e.printStackTrace();  
      } catch (Exception e) {  
    	  textString += e.getMessage() +"\r\n";
          e.printStackTrace();  
      }finally{  
    	  activityLog.setCtime(TimeUtils.getDate());
    	  try{
    	    activityLogService.insertActivityLog(activityLog);
    	  }catch(Exception e){
    		  e.printStackTrace();
    		  textString += activityLog.toString();
    	  }
          httpClient.getConnectionManager().shutdown();  
          textString += "\r\n邮件内容：" + mailMessage;
          String returnString = FileUtil.saveFile(logPath+"/activity", textString);
          System.out.println("保存文件："+returnString);
          MailUtils.sendMessage(mailMessage);
      }  
  } 
  
//  @Scheduled(cron = "0 58 13 * * ?")//每隔5秒隔行一次 
//  public void test2()
//  {
//      System.out.println("job2 开始执行");
//  } 
  
}
