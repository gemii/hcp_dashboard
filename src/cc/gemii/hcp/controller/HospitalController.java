package cc.gemii.hcp.controller;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cc.gemii.hcp.po.GemiiHcpHospital;
import cc.gemii.hcp.po.GemiiHcpHospitalExample;
import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.pojo.GemiiHcpHospitalPOJO;
import cc.gemii.hcp.service.HospitalService;
import cc.gemii.hcp.utils.TimeUtils;
import cc.gemii.hcp.utils.UUIDUtils;

/**
 * 
 * class_name：HospitalController description：医院信息控制器 editor：yunfei.han date：Jul
 * 27, 2016 4:21:56 PM
 * 
 * @version V1.0
 *
 */
@Controller
@RequestMapping("/hospital")
public class HospitalController {
	@Autowired
	private HospitalService hospitalService;

	@RequestMapping("/add")
	public ModelAndView Index(HttpServletRequest request,ModelAndView mav) {
		//判断session
  		HttpSession session  = request.getSession();
  		//从session中取出用户身份信息
  		GemiiHcpUser staff = (GemiiHcpUser) session.getAttribute("user");
  		
  		if(staff.getType().equals("业务员")){
  			mav.addObject("title", "HCP-业务员");
  			mav.setViewName("hospital/staff/add");
  		}else{
  			mav.addObject("title", "HCP-审核员");
  			mav.setViewName("hospital/admin/add");
  		}
		return mav;
	}

	@RequestMapping("/save")
	public void test(HttpServletResponse response, GemiiHcpHospital hospital,HttpServletRequest request)
			throws IOException {
		//判断session
		String id = hospital.getHsptCode(); // 获取当前时间作为医院ID
		hospital.setId(id);
		int count = hospitalService.addHospital(hospital);
		response.setContentType("text/html;charset=utf-8");
		if (count > 0) {
			response.getWriter()
					.write("<script>alert('添加成功！');window.location='"+request.getContextPath()+"/hospital/view';</script>");
		} else {
			response.getWriter().write("<script>alert('添加失败，请重试！');</script>");
		}
	}

	@RequestMapping("/view")
	public ModelAndView view(HttpServletRequest request,ModelAndView mav) {
		//判断session
  		HttpSession session  = request.getSession();
  		//从session中取出用户身份信息
  		GemiiHcpUser staff = (GemiiHcpUser) session.getAttribute("user");
  		
  		if(staff.getType().equals("业务员")){
  			mav.addObject("title", "HCP-业务员");
  			mav.setViewName("hospital/staff/view");
  		}else{
  			mav.addObject("title", "HCP-审核员");
  			mav.setViewName("hospital/admin/view");
  		}
		return mav;
	}
	@RequestMapping("/list/{hsptName}")
	@ResponseBody
	public List<GemiiHcpHospitalPOJO> list(@PathVariable String hsptName) {
		GemiiHcpHospitalExample example = new GemiiHcpHospitalExample();
		hsptName = URLDecoder.decode(hsptName);
		if (!hsptName.equals("1")) {
			example.createCriteria().andHsptNameLike("%"+hsptName+"%");
		}
		return hospitalService.getAllHsopital(example);
	}
	
	@RequestMapping("/list/query/{hsptName}")
	@ResponseBody
	public List<GemiiHcpHospital> query(@PathVariable String hsptName) {
		GemiiHcpHospitalExample example = new GemiiHcpHospitalExample();
		example.createCriteria().andHsptNameLike("%"+URLDecoder.decode(hsptName)+"%");
		example.setOrderByClause("hspt_name limit 10");
		return hospitalService.getHospitalsName(example);
	}
	@RequestMapping("/show/{id}")
	@ResponseBody
	public GemiiHcpHospital show(@PathVariable String id) {
		return hospitalService.selectByHospitalId(id);
	}
	@RequestMapping("/verify")
	@ResponseBody
	public Map<String, Object> verify(String hsptCode){
		Map<String, Object> map = new HashMap<String, Object>();
		GemiiHcpHospitalExample example = new GemiiHcpHospitalExample();
		example.createCriteria().andHsptCodeEqualTo(hsptCode);
		List<GemiiHcpHospitalPOJO> list = hospitalService.getAllHsopital(example);
		map.put("count", list.size());
		return map;
	}
	
}
