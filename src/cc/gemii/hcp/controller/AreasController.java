package cc.gemii.hcp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.hcp.po.GemiiHcpAreas;
import cc.gemii.hcp.service.AreaService;

@Controller
@RequestMapping("/areas")
public class AreasController {

	@Autowired
	private AreaService areaService;
	@RequestMapping("/getALl")
	@ResponseBody
	public List<GemiiHcpAreas> getAll(){
		return areaService.getAll();
	}
}
