package cc.gemii.hcp.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cc.gemii.hcp.po.GemiiHcpUpload;
import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.service.UploadService;
import cc.gemii.hcp.service.UploadTableService;
import cc.gemii.hcp.utils.FileUtil;
import cc.gemii.hcp.utils.TimeUtils;
import cc.gemii.hcp.utils.UUIDUtils;

/**
 * 
 * @author yunfei.han
 * Description TODO 上传文件Controller
 * 2016年5月18日
 *
 */
@Controller
public class UploadController {

	@Autowired
	private UploadService uploadService;
	@Autowired
	private UploadTableService uploadTableService;
	
	/**
	 * 
	 * @param request
	 * @param type 上传类型,邮箱附件上传为mail,用户管理上传user,
	 * @return
	 * String
	 */
	@RequestMapping("/uploadFile")
	@ResponseBody
	public String uploadTest(HttpServletRequest request){
	    GemiiHcpUser staff = (GemiiHcpUser) request.getSession().getAttribute("user");
	    if(staff==null){
	    	staff = new GemiiHcpUser();
	    	staff.setId(1001);
	    }
		String path = uploadService.uploadFile(request,staff);
		return path;
	}
	@RequestMapping("/upload/save")
	@ResponseBody
	public String save(HttpServletRequest request,String allPath) throws ParseException{
		//判断session
 		HttpSession session  = request.getSession();
 		//从session中取出用户身份信息
 		GemiiHcpUser staff = (GemiiHcpUser) session.getAttribute("user");
		
		String uuid = UUIDUtils.getId(6);
		String[] files = allPath.split("\\|\\|");
		for (String file : files) {
			String fileName = FileUtil.subStringFileName(file);
			GemiiHcpUpload upload = new GemiiHcpUpload();
			upload.setOutId(uuid);
			upload.setFileName(fileName);
			upload.setPath("/"+staff.getId()+"/"+file);
			upload.setCtime(TimeUtils.getDate());
			upload.setUseage("医生职称");
			upload.setId(UUIDUtils.getId(8));
			uploadTableService.addFile(upload);
		}
		return uuid;
	}
	@RequestMapping("/download/{foled}/{fileName}")  
	public ResponseEntity<byte[]> download(@PathVariable String foled,@PathVariable String fileName) throws IOException { 
	    HttpHeaders headers = new HttpHeaders();  
	    headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);  
	    headers.setContentDispositionFormData("attachment", URLEncoder.encode(fileName,"UTF-8")); 
	    return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(uploadService.downloadFile(foled,fileName)),  
	                                      headers, HttpStatus.CREATED);  
	}
	@RequestMapping("/viewPhoto/{path}/{fileName}")
	public void getPhoto(HttpServletResponse response,
			@PathVariable String path,@PathVariable String fileName){
		try {
			//设置header
		    response.setHeader("Pragma", "No-cache");   
		    response.setHeader("Cache-Control", "no-cache");
		    if (fileName.contains(".pdf")||fileName.contains(".PDF")) {
		    	response.setHeader("Content-Type", "application/pdf");  
			}
		    response.setDateHeader("Expires", 0L);
		    
			OutputStream os = response.getOutputStream();
			os.write(uploadService.viewPhoto(path,fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@RequestMapping("/admin/view/photo/{path}/{fileName}")
	public ModelAndView viewPhoto1(ModelAndView mav,@PathVariable String path,@PathVariable String fileName){
		mav.addObject("path", "/"+path+"/"+fileName);
		mav.setViewName("photo/viewPhoto");
		return mav;
	}
	@RequestMapping("/staff/view/photo/{path}/{fileName}")
	public ModelAndView viewPhoto2(ModelAndView mav,@PathVariable String path,@PathVariable String fileName){
		mav.addObject("path", "/"+path+"/"+fileName);
		mav.setViewName("photo/viewPhoto");
		return mav;
	}
	@RequestMapping("/view/photo/{path}/{fileName}")
	public ModelAndView viewPhoto3(ModelAndView mav,@PathVariable String path,@PathVariable String fileName){
		mav.addObject("path", "/"+path+"/"+fileName);
		mav.setViewName("photo/viewPhoto");
		return mav;
	}
}
