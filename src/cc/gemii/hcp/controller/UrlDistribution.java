package cc.gemii.hcp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 
 * class_name：UrlDistribution   
 * description：负责URL分发   
 * editor：yunfei.han
 * date：Aug 13, 2016 3:00:46 PM      
 * @version  V1.0
 *
 */
@Controller
public class UrlDistribution {

	@RequestMapping("/admin")
	public String admin(){
		return "index/admin/index";
	}
	@RequestMapping("/staff")
	public String staff(){
		return "index/staff/index";
	}
	@RequestMapping("/third")
	public String third(){
		return "index/third/index";
	}
}
