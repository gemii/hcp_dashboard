package cc.gemii.hcp.controller.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.hcp.controller.api.resultType.GroupObject;
import cc.gemii.hcp.controller.api.resultType.HospitalObject;
import cc.gemii.hcp.po.GemiiHcpGroup;
import cc.gemii.hcp.po.GemiiHcpRequest;
import cc.gemii.hcp.po.GemiiHcpToken;
import cc.gemii.hcp.service.GroupService;
import cc.gemii.hcp.utils.AESUtil;
import cc.gemii.hcp.utils.FileUtil;
import cc.gemii.hcp.utils.InterfaceUtils;
import cc.gemii.hcp.utils.JsonUtils;
import cc.gemii.hcp.utils.MailUtils;
import cc.gemii.hcp.utils.TimeUtils;
/**
 * 
 * class_name：GroupController
 * description：微信群接口
 * editor：yunfei.han
 * date：2016-08-26 10:56:59      
 * @version  V1.0
 *
 */
@Controller
@RequestMapping("/api/group")
public class GroupAPIController {

	@Autowired
	private GroupService groupService;
	@Autowired
	private TokenController tokenController;
	@Autowired
	private RequestController requestController;
	@Value("${filterIp}")
	private String filterIp;
	@Value("${logPath}")
	private String logPath;
	
	@RequestMapping(value="/get",method={RequestMethod.POST })
	@ResponseBody
	public InterfaceUtils getGroup(@RequestBody GemiiHcpToken token,HttpServletRequest request){
		String string = tokenController.verToken(token.getToken());
		InterfaceUtils interfaceUtils = JSONObject.parseObject(string, InterfaceUtils.class);
		requestController.insert(request, token.getToken(), null,interfaceUtils.getError_code()+"",string);
		if(interfaceUtils.getError_code()==0){  //Token验证通过
			Map<String, Object> map = new HashMap<String, Object>();
			JsonUtils<GroupObject> jsonUtils = new JsonUtils<GroupObject>();
			map.put("data", AESUtil.Encrypt(jsonUtils.ObjectToJson(groupService.apiGetGroup()), token.getToken()));
			interfaceUtils.setResult(map);
		}
		return interfaceUtils;
	}
	@RequestMapping(value="/import",method={RequestMethod.POST })
	@ResponseBody
	public InterfaceUtils importGroup(@RequestBody List<GemiiHcpGroup> groupList,HttpServletRequest request){
		String ip = request.getRemoteAddr(); 
		InterfaceUtils interfaceUtils = new InterfaceUtils();
		String mailMessage = "Import_group_data_info_",logText="";
		try{
			if(filterIp.contains(ip)){  //ip没有被拦截
				interfaceUtils.setError_code(0);
				groupService.truncateGroup(); //删除原来记录
				int count = 0;
				for (GemiiHcpGroup gemiiHcpGroup : groupList) {
					int state = groupService.insertGroup(gemiiHcpGroup);
					if(state==1){
						count++;
					}else{
						logText += JSONObject.toJSONString(gemiiHcpGroup) +"\r\n";
					}
				}
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("totalCount", groupList.size());
				map.put("successCount", count);
				interfaceUtils.setResult(map);
				logText += JSONObject.toJSONString(map) +"\r\n";
				mailMessage += "_totalCount:"+groupList.size() +"_successCount:"+count;
				interfaceUtils.setReason("导入成功");
			}else{
				interfaceUtils.setError_code(-1);
				interfaceUtils.setReason("您没有权限进行操作。。。");
				mailMessage +="_no_access_and_request_ip:"+ip;
			}
		}catch(Exception e){
			interfaceUtils.setError_code(1);
			interfaceUtils.setReason(e.getMessage());
			e.printStackTrace();
			logText += e.getMessage()+"\r\n";
			mailMessage +="_error";
		}finally{
			System.out.println("-----"+mailMessage);
			logText += "\r\nmailMessage:" + mailMessage;
			FileUtil.saveFile(logPath+"/group", logText);
			MailUtils.sendMessage(mailMessage);
		}
		return interfaceUtils;
	}
}
