package cc.gemii.hcp.controller.api.resultType;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

public class GroupObject {

	@JsonProperty
	private String Id;
	@JsonProperty
	private String GPDID;
	@JsonProperty
	private String GroupName;
	@JsonProperty
	private String GroupType;
	@JsonProperty
	private String FK_HospitalCode;
	@JsonProperty
	private String FK_AnchorCity;
	@JsonProperty
	private String FK_AnchorCityID;
	@JsonProperty
	private String Team;
	@JsonProperty
	private String JoinType;
	@JsonProperty
	private String FK_Key;
	@JsonProperty
	private int ParentId;
	@JsonProperty
	private String Year;
	@JsonProperty
	private String Quarter;
	@JsonProperty
	private String GroupStatus;
	@JsonProperty
	private Date PromoteDate;
	@JsonProperty
	private Date RetainDate;
	@JsonProperty
	private String ExtName;
	@JsonProperty
	private Date ExtDate;
	@JsonProperty
	private int IsRetain;
	@JsonProperty
	private int IsOpen;
	@JsonProperty
	private String MonitorPhone;
	@JsonProperty
	private int PeopleNumber;
	@JsonProperty
	private int WhiteListNumber;
	@JsonIgnore
	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	@JsonIgnore
	public String getGPDID() {
		return GPDID;
	}

	public void setGPDID(String gPDID) {
		GPDID = gPDID;
	}

	@JsonIgnore
	public String getGroupName() {
		return GroupName;
	}

	public void setGroupName(String groupName) {
		GroupName = groupName;
	}

	@JsonIgnore
	public String getGroupType() {
		return GroupType;
	}

	public void setGroupType(String groupType) {
		GroupType = groupType;
	}

	@JsonIgnore
	public String getFK_HospitalCode() {
		return FK_HospitalCode;
	}

	public void setFK_HospitalCode(String fK_HospitalCode) {
		FK_HospitalCode = fK_HospitalCode;
	}

	public String getFK_AnchorCityID() {
		return FK_AnchorCityID;
	}

	public void setFK_AnchorCityID(String fK_AnchorCityID) {
		FK_AnchorCityID = fK_AnchorCityID;
	}

	@JsonIgnore
	public String getFK_AnchorCity() {
		return FK_AnchorCity;
	}

	public void setFK_AnchorCity(String fK_AnchorCity) {
		FK_AnchorCity = fK_AnchorCity;
	}

	@JsonIgnore
	public String getTeam() {
		return Team;
	}

	public void setTeam(String team) {
		Team = team;
	}

	@JsonIgnore
	public String getJoinType() {
		return JoinType;
	}

	public void setJoinType(String joinType) {
		JoinType = joinType;
	}

	@JsonIgnore
	public String getFK_Key() {
		return FK_Key;
	}

	public void setFK_Key(String fK_Key) {
		FK_Key = fK_Key;
	}

	@JsonIgnore
	public int getParentId() {
		return ParentId;
	}

	public void setParentId(int parentId) {
		ParentId = parentId;
	}

	@JsonIgnore
	public String getYear() {
		return Year;
	}

	public void setYear(String year) {
		Year = year;
	}

	@JsonIgnore
	public String getQuarter() {
		return Quarter;
	}

	public void setQuarter(String quarter) {
		Quarter = quarter;
	}

	@JsonIgnore
	public String getGroupStatus() {
		return GroupStatus;
	}

	public void setGroupStatus(String groupStatus) {
		GroupStatus = groupStatus;
	}

	@JsonIgnore
	public Date getPromoteDate() {
		return PromoteDate;
	}

	public void setPromoteDate(Date promoteDate) {
		PromoteDate = promoteDate;
	}

	@JsonIgnore
	public Date getRetainDate() {
		return RetainDate;
	}

	public void setRetainDate(Date retainDate) {
		RetainDate = retainDate;
	}

	@JsonIgnore
	public String getExtName() {
		return ExtName;
	}

	public void setExtName(String extName) {
		ExtName = extName;
	}

	@JsonIgnore
	public Date getExtDate() {
		return ExtDate;
	}

	public void setExtDate(Date extDate) {
		ExtDate = extDate;
	}

	@JsonIgnore
	public int getIsRetain() {
		return IsRetain;
	}

	public void setIsRetain(int isRetain) {
		IsRetain = isRetain;
	}

	@JsonIgnore
	public int getIsOpen() {
		return IsOpen;
	}

	public void setIsOpen(int isOpen) {
		IsOpen = isOpen;
	}

	@JsonIgnore
	public String getMonitorPhone() {
		return MonitorPhone;
	}

	public void setMonitorPhone(String monitorPhone) {
		MonitorPhone = monitorPhone;
	}

	@JsonIgnore
	public int getPeopleNumber() {
		return PeopleNumber;
	}

	public void setPeopleNumber(int peopleNumber) {
		PeopleNumber = peopleNumber;
	}

	@JsonIgnore
	public int getWhiteListNumber() {
		return WhiteListNumber;
	}

	public void setWhiteListNumber(int whiteListNumber) {
		WhiteListNumber = whiteListNumber;
	}

	@Override
	public String toString() {
		return "GroupObject [Id=" + Id + ", GPDID=" + GPDID + ", GroupName="
				+ GroupName + ", GroupType=" + GroupType + ", FK_HospitalCode="
				+ FK_HospitalCode + ", FK_AnchorCity=" + FK_AnchorCity
				+ ", Team=" + Team + ", JoinType=" + JoinType + ", FK_Key="
				+ FK_Key + ", ParentId=" + ParentId + ", Year=" + Year
				+ ", Quarter=" + Quarter + ", GroupStatus=" + GroupStatus
				+ ", PromoteDate=" + PromoteDate + ", RetainDate=" + RetainDate
				+ ", ExtName=" + ExtName + ", ExtDate=" + ExtDate
				+ ", IsRetain=" + IsRetain + ", IsOpen=" + IsOpen
				+ ", MonitorPhone=" + MonitorPhone + ", PeopleNuber="
				+ PeopleNumber + ", WhiteListNumber=" + WhiteListNumber + "]";
	}

}
