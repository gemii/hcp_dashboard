package cc.gemii.hcp.controller.api.resultType;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

public class HCPObject {
	@JsonProperty
	private String HCPName;
	@JsonProperty
	private String HCPCode;
	@JsonProperty
	private int RegionLinkMan;
	@JsonProperty
	private String RegionLinkManName;
	@JsonProperty
	private String HCPMobile;
	@JsonProperty
	private String HospitalName;
	@JsonProperty
	private String HospitalCode;
	@JsonProperty
	private String HCPLevel;
	@JsonProperty
	private String Departments;
	@JsonProperty
	private String Position;
	@JsonProperty
	private String HospitalTitle;
	@JsonProperty
	private String Profile;
	@JsonProperty
	private String BeGoodAt;
	@JsonProperty
	private Date CreateDate;
	@JsonProperty
	private String CreateUser;
	@JsonProperty
	private String ModifiedDate;
	@JsonProperty
	private String ModifiedUser;
	@JsonProperty
	private int IsContract;
	@JsonProperty
	private String Team;
	@JsonProperty
	private String OaName;
	@JsonProperty
	private String OaPhone;
	@JsonIgnore
	public String getHCPName() {
		return HCPName;
	}
	public void setHCPName(String hCPName) {
		HCPName = hCPName;
	}
	@JsonIgnore
	public String getHCPCode() {
		return HCPCode;
	}
	public void setHCPCode(String hCPCode) {
		HCPCode = hCPCode;
	}
	@JsonIgnore
	public int getRegionLinkMan() {
		return RegionLinkMan;
	}
	public void setRegionLinkMan(int regionLinkMan) {
		RegionLinkMan = regionLinkMan;
	}
	@JsonIgnore
	public String getRegionLinkManName() {
		return RegionLinkManName;
	}
	public void setRegionLinkManName(String regionLinkManName) {
		RegionLinkManName = regionLinkManName;
	}
	@JsonIgnore
	public String getHCPMobile() {
		return HCPMobile;
	}
	public void setHCPMobile(String hCPMobile) {
		HCPMobile = hCPMobile;
	}
	@JsonIgnore
	public String getHospitalName() {
		return HospitalName;
	}
	public void setHospitalName(String hospitalName) {
		HospitalName = hospitalName;
	}
	@JsonIgnore
	public String getHospitalCode() {
		return HospitalCode;
	}
	public void setHospitalCode(String hospitalCode) {
		HospitalCode = hospitalCode;
	}
	@JsonIgnore
	public String getHCPLevel() {
		return HCPLevel;
	}
	public void setHCPLevel(String hCPLevel) {
		HCPLevel = hCPLevel;
	}
	@JsonIgnore
	public String getDepartments() {
		return Departments;
	}
	public void setDepartments(String departments) {
		Departments = departments;
	}
	@JsonIgnore
	public String getPosition() {
		return Position;
	}
	public void setPosition(String position) {
		Position = position;
	}
	@JsonIgnore
	public String getHospitalTitle() {
		return HospitalTitle;
	}
	public void setHospitalTitle(String hospitalTitle) {
		HospitalTitle = hospitalTitle;
	}
	@JsonIgnore
	public String getProfile() {
		return Profile;
	}
	public void setProfile(String profile) {
		Profile = profile;
	}
	@JsonIgnore
	public String getBeGoodAt() {
		return BeGoodAt;
	}
	public void setBeGoodAt(String beGoodAt) {
		BeGoodAt = beGoodAt;
	}
	@JsonIgnore
	public Date getCreateDate() {
		return CreateDate;
	}
	public void setCreateDate(Date createDate) {
		CreateDate = createDate;
	}
	@JsonIgnore
	public String getCreateUser() {
		return CreateUser;
	}
	public void setCreateUser(String createUser) {
		CreateUser = createUser;
	}
	@JsonIgnore
	public String getModifiedDate() {
		return ModifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		ModifiedDate = modifiedDate;
	}
	@JsonIgnore
	public String getModifiedUser() {
		return ModifiedUser;
	}
	public void setModifiedUser(String modifiedUser) {
		ModifiedUser = modifiedUser;
	}
	@JsonIgnore
	public int getIsContract() {
		return IsContract;
	}
	public void setIsContract(int isContract) {
		IsContract = isContract;
	}
	@JsonIgnore
	public String getTeam() {
		return Team;
	}
	public void setTeam(String team) {
		Team = team;
	}
	@JsonIgnore
	public String getOaName() {
		return OaName;
	}
	public void setOaName(String oaName) {
		OaName = oaName;
	}
	@JsonIgnore
	public String getOaPhone() {
		return OaPhone;
	}
	public void setOaPhone(String oaPhone) {
		OaPhone = oaPhone;
	}
	@Override
	public String toString() {
		return "HCPObject [HCPName=" + HCPName + ", HCPCode=" + HCPCode
				+ ", RegionLinkMan=" + RegionLinkMan + ", RegionLinkManName="
				+ RegionLinkManName + ", HCPMobile=" + HCPMobile
				+ ", HospitalName=" + HospitalName + ", HospitalCode="
				+ HospitalCode + ", HCPLevel=" + HCPLevel + ", Departments="
				+ Departments + ", Position=" + Position + ", HospitalTitle="
				+ HospitalTitle + ", Profile=" + Profile + ", BeGoodAt="
				+ BeGoodAt + ", CreateDate=" + CreateDate + ", CreateUser="
				+ CreateUser + ", ModifiedDate=" + ModifiedDate
				+ ", ModifiedUser=" + ModifiedUser + ", IsContract="
				+ IsContract + ", Team=" + Team + ", OaName=" + OaName
				+ ", OaPhone=" + OaPhone + "]";
	}
	
}
