package cc.gemii.hcp.controller.api.resultType;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

public class HospitalObject {
	@JsonProperty
	private int Id;
	@JsonProperty
	private String HospitalName;
	@JsonProperty
	private String HospitalCode;
	@JsonProperty
	private String IMF_Region;
	@JsonProperty
	private String Maternal_Region;
	@JsonProperty
	private String Province;
	@JsonProperty
	private String CheckCity;
	@JsonProperty
	private String City;
	@JsonProperty
	private String CityAttribute;
	@JsonProperty
	private String Coveragd;
	@JsonProperty
	private int MonthAvg_NB;
	@JsonProperty
	private String HospitalType;
	@JsonProperty
	private String HospitalLevel;
	@JsonProperty
	private int IsEnable;
	@JsonProperty
	private String CreateUser;
	@JsonProperty
	private Date CreateDate;
	@JsonProperty
	private String ModifiedUser;
	@JsonProperty
	private Date ModifiedDate;
	@JsonIgnore
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	@JsonIgnore
	public String getHospitalName() {
		return HospitalName;
	}
	public void setHospitalName(String hospitalName) {
		HospitalName = hospitalName;
	}
	@JsonIgnore
	public String getHospitalCode() {
		return HospitalCode;
	}
	public void setHospitalCode(String hospitalCode) {
		HospitalCode = hospitalCode;
	}
	@JsonIgnore
	public String getIMF_Region() {
		return IMF_Region;
	}
	public void setIMF_Region(String iMF_Region) {
		IMF_Region = iMF_Region;
	}
	@JsonIgnore
	public String getMaternal_Region() {
		return Maternal_Region;
	}
	public void setMaternal_Region(String maternal_Region) {
		Maternal_Region = maternal_Region;
	}
	@JsonIgnore
	public String getProvince() {
		return Province;
	}
	public void setProvince(String province) {
		Province = province;
	}
	@JsonIgnore
	public String getCheckCity() {
		return CheckCity;
	}
	public void setCheckCity(String checkCity) {
		CheckCity = checkCity;
	}
	@JsonIgnore
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	@JsonIgnore
	public String getCityAttribute() {
		return CityAttribute;
	}
	public void setCityAttribute(String cityAttribute) {
		CityAttribute = cityAttribute;
	}
	@JsonIgnore
	public String getCoveragd() {
		return Coveragd;
	}
	public void setCoveragd(String coveragd) {
		Coveragd = coveragd;
	}
	@JsonIgnore
	public int getMonthAvg_NB() {
		return MonthAvg_NB;
	}
	public void setMonthAvg_NB(int monthAvg_NB) {
		MonthAvg_NB = monthAvg_NB;
	}
	@JsonIgnore
	public String getHospitalType() {
		return HospitalType;
	}
	public void setHospitalType(String hospitalType) {
		HospitalType = hospitalType;
	}
	@JsonIgnore
	public String getHospitalLevel() {
		return HospitalLevel;
	}
	public void setHospitalLevel(String hospitalLevel) {
		HospitalLevel = hospitalLevel;
	}
	@JsonIgnore
	public int getIsEnable() {
		return IsEnable;
	}
	public void setIsEnable(int isEnable) {
		IsEnable = isEnable;
	}
	@JsonIgnore
	public String getCreateUser() {
		return CreateUser;
	}
	public void setCreateUser(String createUser) {
		CreateUser = createUser;
	}
	@JsonIgnore
	public Date getCreateDate() {
		return CreateDate;
	}
	public void setCreateDate(Date createDate) {
		CreateDate = createDate;
	}
	@JsonIgnore
	public String getModifiedUser() {
		return ModifiedUser;
	}
	public void setModifiedUser(String modifiedUser) {
		ModifiedUser = modifiedUser;
	}
	@JsonIgnore
	public Date getModifiedDate() {
		return ModifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		ModifiedDate = modifiedDate;
	}


	@Override
	public String toString() {
		return "HospitalObject [Id=" + Id + ", HospitalName=" + HospitalName
				+ ", HospitalCode=" + HospitalCode + ", IMF_Region="
				+ IMF_Region + ", Maternal_Region=" + Maternal_Region
				+ ", Province=" + Province + ", CheckCity=" + CheckCity
				+ ", City=" + City + ", CityAttribute=" + CityAttribute
				+ ", Coveragd=" + Coveragd + ", MonthAvg_NB=" + MonthAvg_NB
				+ ", HospitalType=" + HospitalType + ", HospitalLevel="
				+ HospitalLevel + ", IsEnable=" + IsEnable + ", CreateUser="
				+ CreateUser + ", CreateDate=" + CreateDate + ", ModifiedUser="
				+ ModifiedUser + ", ModifiedDate=" + ModifiedDate + "]";
	}
	
}
