package cc.gemii.hcp.controller.api;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.hcp.po.GemiiHcpToken;
import cc.gemii.hcp.service.TokenService;
import cc.gemii.hcp.utils.InterfaceUtils;
import cc.gemii.hcp.utils.MD5Utils;
import cc.gemii.hcp.utils.TimeUtils;
import cc.gemii.hcp.utils.UUIDUtils;

/**
 * 
 * class_name：TokenContraller   
 * description：生成和验证Token
 * editor：yunfei.han
 * date：Aug 25, 2016 2:32:44 PM      
 * @version  V1.0
 *
 */
@Controller
@RequestMapping("/api/token")
public class TokenController {
	
	@Value("${token_time}")
	private Integer token_time;
	@Autowired
	private TokenService tokenService;
	@Autowired
	private RequestController requestController;
	/**
	 * 
	 * @param 明文字符串
	 * @param 时间  yyyyMMddHHmmss
	 * @param 密文字符串
	 * @return  生成的token
	 */
	@RequestMapping(value="/get", method = {RequestMethod.POST },consumes = "application/json")
	@ResponseBody
	public InterfaceUtils getToken(@RequestBody GemiiHcpToken token,HttpServletRequest request){
		System.out.println(token.toString());
		InterfaceUtils interfaceUtils = new InterfaceUtils();
		try{
			
			String text = token.getPlain() + "_" + token.getDate();
			text = MD5Utils.md5Encode(text);
			text = MD5Utils.md5Encode(text);
			System.out.println(text);
			if (text.equals(token.getCipher())) {
				String tokenString = "T" + UUIDUtils.getId(15);
				token.setToken(tokenString);
				
				Date nowDate = TimeUtils.getDate();
				Calendar calendar = new GregorianCalendar();
				calendar.setTime(nowDate);
				calendar.add(calendar.MINUTE, token_time);
				
				token.setEtime(calendar.getTime());
				token.setCtime(TimeUtils.getDate());
				
				interfaceUtils.setReason("验证成功");
				interfaceUtils.setError_code(0);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("token", tokenString);
				map.put("validity", token_time);
				interfaceUtils.setResult(map);
				tokenService.insertToken(token);
			}else{
				interfaceUtils.setReason("密钥验证失败");
				interfaceUtils.setError_code(1);
			}
		}catch(Exception e){
			interfaceUtils.setReason("未知异常");
			interfaceUtils.setError_code(2);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("message", e.getMessage());
			interfaceUtils.setResult(map);
		}
		requestController.insert(request, null, token.toString(),interfaceUtils.getError_code()+"",interfaceUtils.toString());
		return interfaceUtils;
	}
	public String verToken(String token){
		InterfaceUtils interfaceUtils = new InterfaceUtils();
		try{
			GemiiHcpToken gemiiHcpToken = tokenService.selectToken(token);
			if(gemiiHcpToken==null){
				interfaceUtils.setReason("Token不存在");
				interfaceUtils.setError_code(-1);
			}else{
				Date nowDate = TimeUtils.getDate();
				if (nowDate.getTime()<gemiiHcpToken.getEtime().getTime()) {
					interfaceUtils.setReason("Token验证成功");
					interfaceUtils.setError_code(0);
				}else{
					interfaceUtils.setReason("Token已过期");
					interfaceUtils.setError_code(1);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			interfaceUtils.setReason("验证Token出现异常");
			interfaceUtils.setError_code(2);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("message", e.getMessage());
			interfaceUtils.setResult(map);
		}
		return JSONObject.toJSONString(interfaceUtils);
	}
}
