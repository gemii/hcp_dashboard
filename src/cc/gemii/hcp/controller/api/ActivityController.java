package cc.gemii.hcp.controller.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.hcp.po.GemiiHcpActivity;
import cc.gemii.hcp.po.GemiiHcpActivityLog;
import cc.gemii.hcp.po.GemiiHcpDoctor;
import cc.gemii.hcp.po.GemiiHcpGroup;
import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.service.ActivityLogService;
import cc.gemii.hcp.service.ActivityService;
import cc.gemii.hcp.service.GroupService;
import cc.gemii.hcp.service.HcpService;
import cc.gemii.hcp.service.UserService;
import cc.gemii.hcp.utils.AESUtil;
import cc.gemii.hcp.utils.FileUtil;
import cc.gemii.hcp.utils.MailUtils;
import cc.gemii.hcp.utils.TimeUtils;
/**
 * 
 * class_name：TPController   
 * description：   为第三方提供的接口
 * editor：yunfei.han
 * date：Aug 9, 2016 4:55:01 PM      
 * @version  V1.0
 *
 */
@Controller
@RequestMapping("/api")
public class ActivityController {
	@Autowired
	private ActivityService activityService;
	@Autowired
	private UserService userService;
	@Autowired
	private GroupService groupService;
	@Autowired
	private HcpService hcpService;
	@Autowired
	private ActivityLogService activityLogService;
	@Value("${logPath}")
	private String logPath;
	
	@RequestMapping(value="/activity/add", method = {RequestMethod.POST })
	@ResponseBody
	public Map<String, Object> add(@RequestBody GemiiHcpActivity activity) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String result = null, content = null;
		GemiiHcpUser user = userService.selectByUserId(Integer
				.parseInt(activity.getStaffId()));
		if (user == null) {
			result = "error";
			content = "业务员不存在";
		}
		GemiiHcpGroup group = groupService.selectByGroupId(activity
				.getGroupId());
		if (group == null) {
			result = "error";
			content = "微信群不存在";
		}
		GemiiHcpDoctor doctor = hcpService
				.selectByDoctorId(activity.getHcpId());
		if (doctor == null) {
			result = "error";
			content = "医生不存在";
		}else if(doctor.getHcpClass()==null||doctor.getHcpClass()==""){
			result = "error";
			content = "医生未通过审核";
		}
		if (result == null){  //result为被修改时，才允许提交
			String idString = "A"+TimeUtils.getTime();
			activity.setId(idString); // 活动Id使用当前时间加活动标识
			int count = activityService.insertActivity(activity);
			if (count == 0) {
				result = "error";
				content = "未知原因出现异常";
			}else{
				result = "success";
				content = "添加成功，活动ID为:"+idString;
			}
		}
		map.put("result", result);
		map.put("content", content);
		return map;
	}
	@RequestMapping("/activity/test")
	@ResponseBody
	public Map<String, Object> test(){
		 DefaultHttpClient httpClient = new DefaultHttpClient();  
		    String soapRequestData = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +  
		            "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"  +  
		              "<soap12:Body>" +  
		               " <GetTodayScheduling xmlns=\"http://tempuri.org/\">" +  
		                 " <key>12E07D22-5B56-41A6-A2A2-64EA9152FC07</key>" +  
		                "</GetTodayScheduling>" +  
		             " </soap12:Body>" +  
		            "</soap12:Envelope>" ; 
	        
	      HttpPost httppost = new HttpPost("http://54.223.84.125:8099/SchedulingWebService.asmx"); 
	      /*把Soap请求数据添加到PostMethod*/  
	      //byte[] b = soapRequestData.getBytes("utf-8");  
	      //InputStream is = new ByteArrayInputStream(b,0,b.length);  
	      try {  
	          HttpEntity re = new StringEntity(soapRequestData,HTTP.UTF_8);  
	          httppost.setHeader("Content-Type","application/soap+xml; charset=utf-8");  
	          //httppost.setHeader("Content-Length", String.valueOf(soapRequestData.length()));  
	          httppost.setEntity(re);           
	          HttpResponse response = httpClient.execute(httppost);  
	         // System.out.println(EntityUtils.toString(httppost.getEntity()));  
	          int state = response.getStatusLine().getStatusCode();
	          String string = EntityUtils.toString(response.getEntity());
	          if(state==200){
	              String json = string.substring(string.indexOf("{"),string.lastIndexOf("}")+1);
	              Map<String, Object> map = (Map<String, Object>) JSONObject.parse(json);
	              Boolean flag = (Boolean) map.get("IsPass");
	              if(flag==true)
	                map.put("Obj", AESUtil.Decrypt(map.get("Obj").toString(),"745732822d2c0b6e"));
	              return map;
	          }
	      } catch (Exception e) {  
	          e.printStackTrace();  
	      }
	      return null;
	}
}
