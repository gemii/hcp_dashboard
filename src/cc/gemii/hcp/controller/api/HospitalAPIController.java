package cc.gemii.hcp.controller.api;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.hcp.controller.api.resultType.HospitalObject;
import cc.gemii.hcp.po.GemiiHcpToken;
import cc.gemii.hcp.service.HospitalService;
import cc.gemii.hcp.utils.AESUtil;
import cc.gemii.hcp.utils.InterfaceUtils;
import cc.gemii.hcp.utils.JsonUtils;
/**
 * 
 * 类名称：HospitalController   
 * 类描述：医院接口
 * 创建人：yunfei.han
 * 创建时间：Aug 27, 2016 2:39:33 PM      
 * @version  V1.0
 *
 */
@Controller
@RequestMapping("/api/hospital")
public class HospitalAPIController {

	@Autowired
	private HospitalService hospitalService;
	@Autowired
	private TokenController tokenController;
	@Autowired
	private RequestController requestController;
	
	@RequestMapping(value="/get",method={RequestMethod.POST })
	@ResponseBody
	public InterfaceUtils getHcp(@RequestBody GemiiHcpToken token,HttpServletRequest request){
		String string = tokenController.verToken(token.getToken());
		InterfaceUtils interfaceUtils = JSONObject.parseObject(string, InterfaceUtils.class);
		requestController.insert(request, token.getToken(), null,interfaceUtils.getError_code()+"",string);
		if(interfaceUtils.getError_code()==0){  //Token验证通过
			Map<String, Object> map = new HashMap<String, Object>();
			JsonUtils<HospitalObject> jsonUtils = new JsonUtils<HospitalObject>();
			map.put("data", AESUtil.Encrypt(jsonUtils.ObjectToJson(hospitalService.apiGetHospital()), token.getToken()));
			interfaceUtils.setResult(map);
		}
		return interfaceUtils;
	}
}
