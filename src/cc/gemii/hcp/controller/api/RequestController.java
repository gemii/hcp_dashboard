package cc.gemii.hcp.controller.api;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cc.gemii.hcp.po.GemiiHcpRequestWithBLOBs;
import cc.gemii.hcp.service.RequestService;
import cc.gemii.hcp.utils.TimeUtils;

@Controller
@RequestMapping("/api/request")
public class RequestController {
	@Autowired
	private RequestService requestService;
	
	public void insert(HttpServletRequest request,String token,String parmas,String state,String error){
		GemiiHcpRequestWithBLOBs gemiiHcpRequestWithBLOBs = new GemiiHcpRequestWithBLOBs(); 
		gemiiHcpRequestWithBLOBs.setRequestUrl(request.getRequestURL().toString());
		gemiiHcpRequestWithBLOBs.setRequestIp(request.getRemoteAddr());
		gemiiHcpRequestWithBLOBs.setToken(token);
		gemiiHcpRequestWithBLOBs.setParams(parmas);
		gemiiHcpRequestWithBLOBs.setCtime(TimeUtils.getDate());
		gemiiHcpRequestWithBLOBs.setResponseState(state);
		gemiiHcpRequestWithBLOBs.setError(error);
		requestService.insertRequest(gemiiHcpRequestWithBLOBs);
	}
}
