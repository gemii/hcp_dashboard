package cc.gemii.hcp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.po.GemiiHcpUserExample;
import cc.gemii.hcp.service.UserService;
import cc.gemii.hcp.utils.MD5Utils;
/**
 * 
 * class_name：UserController   
 * description：用户登陆和退出   
 * editor：yunfei.han
 * date：Aug 13, 2016 1:58:30 PM      
 * @version  V1.0
 *
 */
@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;
	
	@RequestMapping("/login")
	public String login(HttpServletRequest request,String id,String password){
		password = MD5Utils.md5Encode(password);
		GemiiHcpUser user = userService.login(id, password);
		if(user==null){
			request.getSession().setAttribute("error", "用户名或密码错误");
			return "redirect:/";
		}else{
			request.getSession().setAttribute("user", user);
			if(user.getType().equals("管理员")||user.getType().equals("审核员")){
				 return "redirect:/admin";
			}else if(user.getType().equals("业务员")){
				return "redirect:/staff";
			}else if(user.getType().equals("第三方")){
				return "redirect:/third";
			}else {
				return "redirect:/staff";
			}
		}
	}
	@RequestMapping("/quit")
	public String quit(HttpServletRequest request){
		request.removeAttribute("user");
		return "redirect:/";
	}
	@RequestMapping("/getUser")
	@ResponseBody
	public List<GemiiHcpUser> getUser(){
		GemiiHcpUserExample example = new GemiiHcpUserExample();
		example.createCriteria().andTypeEqualTo("第三方");
		return userService.selectByUser(example);
	}
}
