package cc.gemii.hcp.controller.staff;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cc.gemii.hcp.po.GemiiHcpContract;
import cc.gemii.hcp.po.GemiiHcpContractThird;
import cc.gemii.hcp.po.GemiiHcpDoctor;
import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.pojo.GemiiHcpContractPOJO;
import cc.gemii.hcp.service.ContractService;
import cc.gemii.hcp.service.HcpService;
import cc.gemii.hcp.service.UploadTableService;
import cc.gemii.hcp.service.UserService;
import cc.gemii.hcp.utils.TimeUtils;

/**
 * 
 * class_name：ContractController   
 * description：   业务员合同控制器
 * editor：yunfei.han
 * date：Aug 8, 2016 5:01:21 PM      
 * @version  V1.0
 *
 */
@Controller
@RequestMapping("/contract/staff")
public class StaffContractController {

	@Autowired
	private ContractService contractService;
	@Autowired
	private HcpService hcpService;
	@Autowired
	private UploadTableService uploadTableService;
	@Autowired
	private UserService userService;

	@RequestMapping("/add/{hcpId}")
	public ModelAndView add(ModelAndView mav,@PathVariable String hcpId,HttpServletRequest request) throws Exception{
		GemiiHcpDoctor doctor = hcpService.selectByDoctorId(hcpId);
		mav.addObject("hcpId", doctor.getId());
		mav.addObject("hcpName", doctor.getHcpName());
		mav.addObject("hsptName", doctor.getHsptId());
		mav.addObject("hcpPhone", doctor.getHcpPhone());
		mav.addObject("flag", request.getSession().getAttribute("ACFlag"));
		mav.setViewName("contract/staff/application");
		return mav;
	}
	@RequestMapping("/save")
	public void save(HttpServletRequest request,HttpServletResponse response, GemiiHcpContract contract,String startTime,String endTime,Integer tpId) throws IOException{
		//判断session
  		HttpSession session  = request.getSession();
  		//从session中取出用户身份信息
  		GemiiHcpUser staff = (GemiiHcpUser) session.getAttribute("user");
  		
		String contractId = "C"+TimeUtils.getTime();
		contract.setId(contractId);
		contract.setStaffId(staff.getId());
		contract.setCtime(TimeUtils.getDate());
		contract.setContractStartTime(TimeUtils.StringToDate(startTime));
		contract.setContractEndTime(TimeUtils.StringToDate(endTime));
		contract.setType(0);
		int count = contractService.insertContract(contract);
		response.setContentType("text/html;charset=utf-8");
		if (count > 0) {
			GemiiHcpContractThird third = new GemiiHcpContractThird();
			third.setContractId(contractId);
			third.setTpId(tpId);
			contractService.insertContractThird(third);
			response.getWriter()
					.write("<script>alert('添加成功,请等待审核！'); window.location='"+request.getContextPath()+"/hcp/staff/view';</script>");
		} else {
			response.getWriter().write("<script>alert('添加失败，请重试！');</script>");
		}
	}
	@RequestMapping("/view")
	public String view(){
		return "contract/staff/view";
	}
	@RequestMapping("/list/{tp_id}")
	@ResponseBody
	public Map<String, Object> list(HttpServletRequest request,@PathVariable Integer tp_id){
		//判断session
  		HttpSession session  = request.getSession();
  		//从session中取出用户身份信息
  		GemiiHcpUser staff = (GemiiHcpUser) session.getAttribute("user");
  		
  		HashMap<String, Object> param = new HashMap<String, Object>();
  		param.put("id", staff.getId());
  		param.put("third", tp_id);
  		List<GemiiHcpContractPOJO> list =  contractService.getContractList(param);
  		Map<String, Object> map = new HashMap<String, Object>();
  		map.put("list", list);
  		map.put("time", TimeUtils.getDate("yyyy-MM-dd"));
  		return map;
	}
	@RequestMapping("/show/{id}")
	@ResponseBody
	public Map<String, Object> show(@PathVariable String id){
		Map<String, Object> map = new HashMap<String, Object>();
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("id", id);
		GemiiHcpContractPOJO contractPOJO = contractService.getContractById(param);
		map.put("contract", contractPOJO);
		
		//map.put("file", uploadTableService.selectById(contractPOJO.getFileId()));
		map.put("time", TimeUtils.getDate("yyyy-MM-dd"));
		GemiiHcpContractThird third = contractService.selectByContractThirdId(id);
		map.put("third", third);
		GemiiHcpUser user = userService.selectByUserId(third.getTpId());
		map.put("tp_name", user.getName());
		if(third!=null){
			if(third.getFileId()!=null)
			map.put("file1", uploadTableService.selectById(third.getFileId()));
		}
		return map;
	}
	@RequestMapping("/detail/{id}")
	public ModelAndView detail(ModelAndView mav,@PathVariable String id){
		mav.addObject("id", id);
		mav.setViewName("contract/staff/detail");
		return mav;
	}
}
