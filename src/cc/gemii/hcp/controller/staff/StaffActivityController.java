package cc.gemii.hcp.controller.staff;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cc.gemii.hcp.po.GemiiHcpActivity;
import cc.gemii.hcp.po.GemiiHcpContract;
import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.pojo.GemiiHcpActivityPOJO;
import cc.gemii.hcp.service.ActivityService;
import cc.gemii.hcp.service.ContractService;
import cc.gemii.hcp.service.HcpService;
import cc.gemii.hcp.service.UploadTableService;
import cc.gemii.hcp.utils.TimeUtils;
/**
 * 
 * class_name：StaffActivityController   
 * description：  活动控制器－业务员 
 * editor：yunfei.han
 * date：Aug 8, 2016 1:59:34 PM      
 * @version  V1.0
 *
 */
@Controller
@RequestMapping("/activity/staff")
public class StaffActivityController {

	@Autowired
	private ActivityService activityService;
	@Autowired
	private HcpService hcpService;
	@Autowired
	private ContractService contractService;
	 @Autowired
	 private UploadTableService uploadTableService;
	
	@RequestMapping("/view")
	public String View(){
		return "activity/staff/view";
	}
	@RequestMapping("/list")
	@ResponseBody
	public List<GemiiHcpActivity> list(HttpServletRequest request,String actSubject,@RequestParam(required = false)String actStartTime,@RequestParam(required = false)String actEndTime){
		//判断session
  		HttpSession session  = request.getSession();
  		//从session中取出用户身份信息
  		GemiiHcpUser staff = (GemiiHcpUser) session.getAttribute("user");
  		
  		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("actSubject", actSubject);
		map.put("id", staff.getId());
	//	System.out.println("---"+actEndTime.length());
		if(actStartTime!=null&&actStartTime.length()>1)
			   map.put("actStartTime", actStartTime+" 00:00:00");
		if(actEndTime!=null&&actEndTime.length()>1)
			map.put("actEndTime", actEndTime+" 23:59:59");
		return activityService.selectByActSubject(map);
	}
	@RequestMapping("/show/{id}")
	@ResponseBody
	public Map<String, Object> Show(@PathVariable String id){
		Map<String, Object> map = new HashMap<String, Object>();
		GemiiHcpActivityPOJO activityPOJO =  activityService.selectByActivityId(id);
		if(activityPOJO.getContractId()!=null){
			GemiiHcpContract contract = contractService.selectByContractId(activityPOJO.getContractId());
			activityPOJO.setContractState(contract.getState());
			activityPOJO.setFeedRemark(contract.getFeedRemark());
		}
		if(activityPOJO.getState()!=0&&activityPOJO.getState()!=4){
			map.put("feedbackTime", TimeUtils.changeDate(activityPOJO.getFeedbackTime()));
			map.put("actRealStart", TimeUtils.changeDate(activityPOJO.getActRealStart()));
			map.put("actRealEnd", TimeUtils.changeDate(activityPOJO.getActRealEnd()));
			if(activityPOJO.getState()!=1)
				map.put("verfiryTime", TimeUtils.changeDate(activityPOJO.getVerfiryTime()));
			if(activityPOJO.getActFile()!=null)
				map.put("file", uploadTableService.selectById(activityPOJO.getActFile()));
		}
		if(activityPOJO.getPayFile()!=null)
			map.put("file1", uploadTableService.selectById(activityPOJO.getPayFile()));
		map.put("activity", activityPOJO);
		map.put("startTime", TimeUtils.changeDate(activityPOJO.getActStartTime()));
		map.put("endTime", TimeUtils.changeDate(activityPOJO.getActEndTime()));
		map.put("actCtime", TimeUtils.changeDate(activityPOJO.getActCtime()));
	
		return map;
	}
	
	@RequestMapping("/feed/{id}/{hcpId}")
	public ModelAndView feed(ModelAndView mav,@PathVariable String id,@PathVariable String hcpId){
		mav.addObject("id", id);
		mav.setViewName("activity/staff/feed");
		return mav;
	}
	@RequestMapping("/save")
	public void save(HttpServletResponse response,GemiiHcpActivity activity,String startTime,String endTime) throws IOException{
		activity.setFeedbackTime(TimeUtils.getDate());
		activity.setActRealStart(TimeUtils.StringToDateAll(startTime));
		activity.setActRealEnd(TimeUtils.StringToDateAll(endTime));
		activity.setState(1);
		response.setContentType("text/html;charset=utf-8");
		int count = activityService.updateActivity(activity);
 		if (count > 0) {
	 			response.getWriter()
	 					.write("<script>alert('添加成功！'); window.close();</script>");
	 		} else {
	 			response.getWriter().write("<script>alert('添加失败，请重试！');</script>");
	 		}
	}
	@RequestMapping("/update")
	@ResponseBody
	public Map<String, Object> update(GemiiHcpActivity activity){
		Map<String, Object> map = new HashMap<String, Object>();
		int count = activityService.updateActivity(activity);
		map.put("count", count);
		return map;
	}
	
	@RequestMapping("/contract/{id}")
	@ResponseBody
	public ArrayList<HashMap<String, Object>> contract(@PathVariable String id){
		ArrayList<HashMap<String, Object>> contractMap = new ArrayList<HashMap<String,Object>>();
		 List<GemiiHcpContract> contractList = contractService.selectByHcpId(id);
		 for (GemiiHcpContract gemiiHcpContract : contractList) {
			 HashMap<String, Object> contractMap2 = new HashMap<String, Object>();
			 contractMap2.put("id", gemiiHcpContract.getId());
			 contractMap2.put("contractStartTime", gemiiHcpContract.getContractStartTime());
			 contractMap2.put("contractEndTime", gemiiHcpContract.getContractEndTime());
			 contractMap2.put("tp_name", gemiiHcpContract.getRemark());
	        	String flag = gemiiHcpContract.getState();
	        		 switch (flag) {
	     			case "5":
	     				long day = TimeUtils.dateDiff(gemiiHcpContract.getContractEndTime());
	     				if(day>=0){
	     					 contractMap2.put("state","通过");
	     					 contractMap2.put("date", day+"天");
	     				}
	     				else {
	     					contractMap2.put("state","已失效");
	     				}
	     				break;
	     			case "0":
	     				contractMap2.put("state","审核中");
	     				break;
	     			case "1":
	     				contractMap2.put("state","待签章");
	     				break;
	     			case"2":
	     				contractMap2.put("state","初审未通过");
	     				break;
	     			case "3":
	     				contractMap2.put("state","待上传");
	     				break;
	     			case "4":
	     				contractMap2.put("state","待终审");
	     				break;
	     			case"6":
	     				contractMap2.put("state","终审未通过");
	     				break;
	    			}
	        	contractMap.add(contractMap2); 
		}
   	 return contractMap;
	}
	
}
