package cc.gemii.hcp.controller.staff;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.hcp.po.GemiiHcpContract;
import cc.gemii.hcp.po.GemiiHcpDoctor;
import cc.gemii.hcp.po.GemiiHcpDoctorExample;
import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.service.ActivityService;
import cc.gemii.hcp.service.AreaService;
import cc.gemii.hcp.service.ContractService;
import cc.gemii.hcp.service.HcpService;
import cc.gemii.hcp.service.InfoService;
import cc.gemii.hcp.service.UploadTableService;
import cc.gemii.hcp.service.UserService;
import cc.gemii.hcp.utils.AESUtil;
import cc.gemii.hcp.utils.TimeUtils;

/**
 * class_name：Application   
 * description：处理业务员的Hcp请求
 * editor：yunfei.han
 * date：Jul 26, 2016 11:19:49 AM      
 * @version  V1.0
 */
@Controller
@RequestMapping("/hcp/staff")
public class StaffDoctorController {
	
	@Value("${hcp_time_limit}")
	private String hcp_time_limit;
	@Value("${hcp_time}")
	private String hcp_time;
	
	 @Autowired
	 private HcpService hcpService;
	 @Autowired
	 private InfoService infoService;
	 @Autowired
	 private UploadTableService uploadTableService;
	 @Autowired
	 private ContractService contractService;
	 @Autowired
	 private ActivityService activityService;
     @RequestMapping("/add")
	 public String add(HttpServletRequest request){
    	 if(Boolean.parseBoolean(hcp_time_limit)==true){  //如果开启限制，则去进行验证时间 否则，直接允许添加
    			boolean flag = TimeUtils.isTimeArea(hcp_time);
    			 if(flag==false){
    	    		 request.getSession().setAttribute("hcpTime", hcp_time);
    	    		 return "hcp/staff/info";
    	    	 }
    	 }
		 return "hcp/staff/application";
	 }
     @RequestMapping("/view")
   	 public String view(){
   		 return "hcp/staff/view";
   	 }
     @RequestMapping("/save")
	 public void save(HttpServletRequest request,HttpServletResponse response, GemiiHcpDoctor doctor)
				throws Exception {
    	//判断session
 		HttpSession session  = request.getSession();
 		//从session中取出用户身份信息
 		GemiiHcpUser staff = (GemiiHcpUser) session.getAttribute("user");
 		String idString = "P0001";
    	GemiiHcpDoctor maxDoctor =  hcpService.getMaxDoctor();
    	if(maxDoctor!=null){
    		int id = Integer.parseInt(maxDoctor.getId().replace("P", ""));
    		DecimalFormat df = new DecimalFormat("0000");
  		    idString = "P"+df.format(id+1);
    	}
    	doctor.setId(idString);
 		doctor.setHcpCtime(TimeUtils.getDate());
 		doctor.setVerifyTime(TimeUtils.getDate());
 		doctor.setHcpCtime(TimeUtils.getDate());
 		doctor.setStaffId(staff.getId());
 		doctor.setState(0);
 		//加密计算
 		doctor.setHcpName(AESUtil.encrypt2Str(doctor.getHcpName()));
 		doctor.setBankCardNumber(AESUtil.encrypt2Str(doctor.getBankCardNumber()));
 		doctor.setBankCardName(AESUtil.encrypt2Str(doctor.getBankCardName()));
 		doctor.setHcpPhone(AESUtil.encrypt2Str(doctor.getHcpPhone()));
 		doctor.setIdCard(AESUtil.encrypt2Str(doctor.getIdCard()));
 		
		int count = hcpService.save(doctor);
 		response.setContentType("text/html;charset=utf-8");
 		if (count > 0) {
 			response.getWriter()
 					.write("<script>alert('添加成功！'); window.location='"+request.getContextPath()+"/hcp/staff/view';</script>");
 		} else {
 			response.getWriter().write("<script>alert('添加失败，请重试！');</script>");
 		}
	 }
     @RequestMapping("/verify/idCard")
     @ResponseBody
     public Map<String, Object> verify1(String idCard){
    	 idCard = AESUtil.encrypt2Str(idCard);
    	 GemiiHcpDoctorExample example = new GemiiHcpDoctorExample();
    	 example.createCriteria().andIdCardEqualTo(idCard).andStateEqualTo(1);
    	 Map<String, Object> map = new HashMap<String, Object>();
    	 boolean flag = hcpService.isExist(example);  //查看该医生是否已经存在  依据医生姓名和电话号码做唯一标识
    	 map.put("flag", flag);
    	 return map;
     }
     @RequestMapping("/verify/hcpId")
     @ResponseBody
     public Map<String, Object> verify(String hcpId){
    	 Map<String, Object> map = new HashMap<String, Object>();
    	 GemiiHcpDoctorExample example = new GemiiHcpDoctorExample();
    	 example.createCriteria().andIdEqualTo(hcpId);
    	 boolean flag = hcpService.isExist(example);  //查看该医生是否已经存在  依据医生姓名和电话号码做唯一标识
    	 map.put("flag", flag);
    	 return map;
     }
     @RequestMapping("/list")
     public @ResponseBody List<GemiiHcpDoctor> list(HttpServletRequest request,String hsptName,String hcpName){
  		HttpSession session  = request.getSession();
  		GemiiHcpUser staff = (GemiiHcpUser) session.getAttribute("user");
  		
  		HashMap<String, Object> map = new HashMap<String, Object>();
  		if(hsptName!=null&&hsptName.length()>0)
  		 map.put("hsptName", hsptName);
  		if(hcpName!=null&&hcpName.length()>0)
    	 map.put("hcpName", AESUtil.encrypt2Str(hcpName));
  		
  		map.put("areaId", staff.getAreaId());
  	    return hcpService.selectByVerify(map);
     }
     @RequestMapping("/show/{id}")
     @ResponseBody
     public Map<String, Object> show(@PathVariable String id,HttpServletRequest request){
    	 Map<String, Object> map = new HashMap<String, Object>();
    	 GemiiHcpDoctor doctor = hcpService.selectByDoctorId(id);
    	 doctor.setRemark(TimeUtils.changeDate(doctor.getHcpCtime()));
    	 doctor.setHcpPost(infoService.getInfoById(doctor.getHcpPost()).getContent());
    	 doctor.setHcpTitle(infoService.getInfoById(doctor.getHcpTitle()).getContent());
    	 map.put("doctor", doctor);
    	 if(doctor.getHcpTitleFile()!=null)
    	   map.put("file",uploadTableService.selectById(doctor.getHcpTitleFile()));
    	 
    	 ArrayList<HashMap<String, Object>> contractMap = new ArrayList<HashMap<String,Object>>();
    		 List<GemiiHcpContract> contractList = contractService.selectByHcpId(id);
    		 for (GemiiHcpContract gemiiHcpContract : contractList) {
    			 HashMap<String, Object> contractMap2 = new HashMap<String, Object>();
    			 contractMap2.put("id", gemiiHcpContract.getId());
    			 contractMap2.put("date", "");
    			 contractMap2.put("tp_name", gemiiHcpContract.getRemark());
    	        	String flag = gemiiHcpContract.getState();
    	        		 switch (flag) {
    	     			case "5":
    	     				long day = TimeUtils.dateDiff(gemiiHcpContract.getContractEndTime());
    	     				if(day>=0){
    	     					 contractMap2.put("state","通过");
    	     					 contractMap2.put("date", day+"天");
    	     				}
    	     				else {
    	     					contractMap2.put("state","已失效");
    	     				}
    	     				break;
    	     			case "0":
    	     				contractMap2.put("state","审核中");
    	     				break;
    	     			case "1":
    	     				contractMap2.put("state","未签章");
    	     				break;
    	     			case"2":
    	     				contractMap2.put("state","初审未通过");
    	     				break;
    	     			case "3":
    	     				contractMap2.put("state","待签章");
    	     				break;
    	     			case "4":
    	     				contractMap2.put("state","待终审");
    	     				break;
    	     			case"6":
    	     				contractMap2.put("state","终审未通过");
    	     				break;
    	    			}
    	        	contractMap.add(contractMap2); 
			}
    	 map.put("contract", contractMap);
    	 String flag = activityService.selectByHcpId(id);
    	 map.put("flag", flag);
    	 request.getSession().setAttribute("ACFlag", flag);
    	 return map;
     }
     @RequestMapping("/update")
     public void update(GemiiHcpDoctor doctor,HttpServletRequest request,HttpServletResponse response) throws Exception{
    	GemiiHcpUser user =  (GemiiHcpUser) request.getSession().getAttribute("user");
    	 doctor.setStaffId(user.getId());
    	 doctor.setVerifyTime(TimeUtils.getDate());
    	 doctor.setState(3);
    	 
  	 	 doctor.setBankCardNumber(AESUtil.encrypt2Str(doctor.getBankCardNumber()));
  		 doctor.setBankCardName(AESUtil.encrypt2Str(doctor.getBankCardName()));
  		
    	 hcpService.updeteDoctor(doctor);
    	
    	response.setContentType("text/html;charset=utf-8");
    	response.getWriter().write("<script>alert(' 修改成功，请等待重新审核！'); window.location='"+request.getContextPath()+"/hcp/staff/view';</script>");
     }
     @RequestMapping("/goback")
     public @ResponseBody Map<String, Object> goback(String hcpId){
    	 Map<String, Object> map = new HashMap<String, Object>();
    	 try{
    		 int count = hcpService.deleteDoctor(hcpId);
    		 map.put("data", count);
    	 }catch(Exception e){
    		 e.printStackTrace();
    		 map.put("data", e.getMessage());
    	 }
    	 return map;
     }
}
