package cc.gemii.hcp.service;


import cc.gemii.hcp.po.GemiiHcpActivityLog;

public interface ActivityLogService {
	int insertActivityLog(GemiiHcpActivityLog activityLog);
}
