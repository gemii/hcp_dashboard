package cc.gemii.hcp.service.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.hcp.mapper.GemiiHcpContractMapper;
import cc.gemii.hcp.mapper.GemiiHcpContractThirdMapper;
import cc.gemii.hcp.mapper.MSContractMapper;
import cc.gemii.hcp.po.GemiiHcpContract;
import cc.gemii.hcp.po.GemiiHcpContractExample;
import cc.gemii.hcp.po.GemiiHcpContractThird;
import cc.gemii.hcp.po.GemiiHcpContractThirdExample;
import cc.gemii.hcp.pojo.GemiiHcpContractPOJO;
import cc.gemii.hcp.service.ContractService;
import cc.gemii.hcp.utils.AESUtil;
@Service
public class ContractServiceImpl implements ContractService {

	@Autowired
	private GemiiHcpContractMapper contractMapper;
	@Autowired
	private MSContractMapper msContractMapper;
	@Autowired
	private GemiiHcpContractThirdMapper thirdMapper;
	@Override
	public List<GemiiHcpContract> selectByHcpId(String id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("hcpId", id);
		List<GemiiHcpContract> list = msContractMapper.getContractListByHcpID(map);
		return list;
	}
	@Override
	public int insertContract(GemiiHcpContract contract) {
		return contractMapper.insertSelective(contract);
	}
	@Override
	public GemiiHcpContract selectByContractId(String id) {
		return contractMapper.selectByPrimaryKey(id);
	}
	@Override
	public int updateContract(GemiiHcpContract contract) {
		return contractMapper.updateByPrimaryKeySelective(contract);
	}
	@Override
	public List<GemiiHcpContractPOJO> getContractList(HashMap<String, Object> map) {
		List<GemiiHcpContractPOJO> list =  msContractMapper.getContractList(map);
		for (GemiiHcpContractPOJO gemiiHcpContractPOJO : list) {
			gemiiHcpContractPOJO.setHcpName(AESUtil.decrypt2Str(gemiiHcpContractPOJO.getHcpName()));
		}
		return list;
	}
	@Override
	public GemiiHcpContractPOJO getContractById(HashMap<String, Object> map) {
		GemiiHcpContractPOJO gemiiHcpContractPOJO = msContractMapper.getContractById(map);
		gemiiHcpContractPOJO.setHcpName(AESUtil.decrypt2Str(gemiiHcpContractPOJO.getHcpName()));
		return gemiiHcpContractPOJO;
	}
	@Override
	public int insertContractThird(GemiiHcpContractThird third) {
		return thirdMapper.insert(third);
	}
	@Override
	public GemiiHcpContractThird selectByContractThirdId(String id) {
		GemiiHcpContractThirdExample example = new GemiiHcpContractThirdExample();
		example.createCriteria().andContractIdEqualTo(id);
		List<GemiiHcpContractThird> list = thirdMapper.selectByExample(example);
		if(list.isEmpty())
			return null;
		return list.get(0);
	}
	@Override
	public int updateContractThird(GemiiHcpContractThird third) {
		return thirdMapper.updateByPrimaryKeySelective(third);
	}

}
