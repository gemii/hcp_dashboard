package cc.gemii.hcp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.hcp.mapper.GemiiHcpRequestMapper;
import cc.gemii.hcp.po.GemiiHcpRequestExample;
import cc.gemii.hcp.po.GemiiHcpRequestWithBLOBs;
import cc.gemii.hcp.service.RequestService;
@Service
public class RequestServiceImpl implements RequestService {

	@Autowired
	private GemiiHcpRequestMapper requestMapper;
	@Override
	public int insertRequest(GemiiHcpRequestWithBLOBs gemiiHcpRequestWithBLOBs) {
		return requestMapper.insertSelective(gemiiHcpRequestWithBLOBs);
	}

}
