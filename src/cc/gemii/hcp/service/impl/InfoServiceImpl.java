package cc.gemii.hcp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import cc.gemii.hcp.mapper.GemiiHcpInfoMapper;
import cc.gemii.hcp.po.GemiiHcpInfo;
import cc.gemii.hcp.po.GemiiHcpInfoExample;
import cc.gemii.hcp.service.InfoService;

import org.springframework.stereotype.Service;

@Service
public class InfoServiceImpl implements InfoService {

	@Autowired
	private GemiiHcpInfoMapper infoMapper;

	@Override
	public List<GemiiHcpInfo> getInfoByDid(int dId, int pId) {
		GemiiHcpInfoExample infoExample = new GemiiHcpInfoExample();
		infoExample.createCriteria().andDidEqualTo(dId).andPidEqualTo(pId);
		return infoMapper.selectByExample(infoExample);
	}

	@Override
	public String getInfoTree(int id) {
		GemiiHcpInfoExample infoExample = new GemiiHcpInfoExample();
		infoExample.createCriteria().andIdEqualTo(id + "");
		List<GemiiHcpInfo> list = infoMapper.selectByExample(infoExample);
		if (list == null) {
			return null;
		}
		GemiiHcpInfo info = list.get(0);
		int flag = info.getPid();
		String content = "";
		content = info.getContent() ;

		while (flag != 0) { // 循环获取某个节点的所有父节点
			GemiiHcpInfoExample infoExample1 = new GemiiHcpInfoExample();
			infoExample1.createCriteria().andIdEqualTo(flag + "");
			List<GemiiHcpInfo> list1 = infoMapper.selectByExample(infoExample1);
			GemiiHcpInfo info1 = list1.get(0);
			flag = info1.getPid();

			content = info1.getContent() + "/" + content; // 拼接方式如：上海市／虹口区
		}
		return content;
	}

	@Override
	public GemiiHcpInfo getInfoById(String id) {
		return infoMapper.selectByPrimaryKey(id);
	}

}
