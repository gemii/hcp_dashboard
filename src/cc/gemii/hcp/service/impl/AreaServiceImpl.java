package cc.gemii.hcp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.hcp.mapper.GemiiHcpAreasMapper;
import cc.gemii.hcp.po.GemiiHcpAreas;
import cc.gemii.hcp.po.GemiiHcpAreasExample;
import cc.gemii.hcp.service.AreaService;
@Service
public class AreaServiceImpl implements AreaService {

	@Autowired
	private GemiiHcpAreasMapper areasMapper;
	@Override
	public GemiiHcpAreas selectById(Integer id) {
		return areasMapper.selectByPrimaryKey(id);
	}
	@Override
	public List<GemiiHcpAreas> getAll() {
		return areasMapper.selectByExample(null);
	}

}
