package cc.gemii.hcp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.hcp.controller.api.resultType.HCPObject;
import cc.gemii.hcp.mapper.GemiiHcpDoctorMapper;
import cc.gemii.hcp.mapper.GemiiHcpInfoMapper;
import cc.gemii.hcp.mapper.GemiiHcpVerifyMapper;
import cc.gemii.hcp.mapper.MSDoctorMapper;
import cc.gemii.hcp.po.GemiiHcpContract;
import cc.gemii.hcp.po.GemiiHcpDoctor;
import cc.gemii.hcp.po.GemiiHcpDoctorExample;
import cc.gemii.hcp.po.GemiiHcpInfo;
import cc.gemii.hcp.po.GemiiHcpInfoExample;
import cc.gemii.hcp.po.GemiiHcpVerify;
import cc.gemii.hcp.pojo.GemiiHcpVerifyPOJO;
import cc.gemii.hcp.service.ContractService;
import cc.gemii.hcp.service.HcpService;
import cc.gemii.hcp.service.InfoService;
import cc.gemii.hcp.utils.AESUtil;
@Service
public class HcpServiceImpl implements HcpService {

	@Autowired
	private GemiiHcpDoctorMapper doctorMapper;
	@Autowired
	private GemiiHcpVerifyMapper verifyMapper;
	@Autowired
	private MSDoctorMapper msDoctorMapper;
	@Autowired
	private GemiiHcpInfoMapper infoMapper;
	@Autowired
	private ContractService contractService;
	@Override
	public int save(GemiiHcpDoctor doctor) {
		return  doctorMapper.insert(doctor);
	}
	@Override
	public boolean isExist(GemiiHcpDoctorExample example) {
		int count = doctorMapper.countByExample(example);
		if(count>0){
			return true;
		}
		return false;
	}
	@Override
	public int saveVerify(GemiiHcpVerify verify) {
		return verifyMapper.insert(verify);
	}
	@Override
	public List<GemiiHcpDoctor> selectByVerify(HashMap map) {
		 List<GemiiHcpDoctor> list = msDoctorMapper.selectByVerify(map);
		 for (GemiiHcpDoctor gemiiHcpDoctor : list) {
			gemiiHcpDoctor.setHcpName(AESUtil.decrypt2Str(gemiiHcpDoctor.getHcpName()));
			gemiiHcpDoctor.setHcpPhone(AESUtil.decrypt2Str(gemiiHcpDoctor.getHcpPhone()));
		}
		 return list;
	}
	@Override
	public GemiiHcpDoctor selectByDoctorId(String id) {
		GemiiHcpDoctor doctor= msDoctorMapper.selectByDoctorId(id);
		doctor.setHcpName(AESUtil.decrypt2Str(doctor.getHcpName()));
		doctor.setHcpPhone(AESUtil.decrypt2Str(doctor.getHcpPhone()));
		doctor.setBankCardName(AESUtil.decrypt2Str(doctor.getBankCardName()));
		doctor.setBankCardNumber(AESUtil.decrypt2Str(doctor.getBankCardNumber()));
		doctor.setIdCard(AESUtil.decrypt2Str(doctor.getIdCard()));
		return doctor;
	}
	@Override
	public List<GemiiHcpVerifyPOJO> selectByNotVerify() {
		List<GemiiHcpVerifyPOJO> list =  msDoctorMapper.getByNotCheck();
		for (GemiiHcpVerifyPOJO gemiiHcpVerifyPOJO : list) {
			gemiiHcpVerifyPOJO.setHcpName(AESUtil.decrypt2Str(gemiiHcpVerifyPOJO.getHcpName()));
			gemiiHcpVerifyPOJO.setHcpPhone(AESUtil.decrypt2Str(gemiiHcpVerifyPOJO.getHcpPhone()));
		}
		return list;
	}
	@Override
	public void updateVerify(GemiiHcpVerify verify) {
		verifyMapper.updateByPrimaryKeySelective(verify);
	}
	@Override
	public void updeteDoctor(GemiiHcpDoctor doctor) {
		doctorMapper.updateByPrimaryKeySelective(doctor);
	}
	@Override
	public GemiiHcpDoctor getMaxDoctor() {
		GemiiHcpDoctor doctor = msDoctorMapper.getMaxDoctor();
		return doctor;
	}
	@Override
	public List<HCPObject> apiGetDoctor() {
		List<HCPObject> list = msDoctorMapper.apiGetDoctor();
		for (HCPObject hcpObject : list) {
			
			hcpObject.setHCPName(AESUtil.decrypt2Str(hcpObject.getHCPName()));
			hcpObject.setHCPMobile(AESUtil.decrypt2Str(hcpObject.getHCPMobile()));
			
			hcpObject.setHCPLevel(hcpObject.getHCPLevel().split(":")[0]);
			
			List<String> params = new ArrayList<String>();
			params.add(hcpObject.getDepartments());
			params.add(hcpObject.getPosition());
			params.add(hcpObject.getHospitalTitle());
			GemiiHcpInfoExample example = new GemiiHcpInfoExample();
			example.createCriteria().andIdIn(params);
			List<GemiiHcpInfo> infoList = infoMapper.selectByExample(example);
			hcpObject.setDepartments(infoList.get(0).getContent().toString());
			hcpObject.setPosition(infoList.get(1).getContent().toString());
			hcpObject.setHospitalTitle(infoList.get(2).getContent().toString());
			List<GemiiHcpContract> contractList = contractService.selectByHcpId(hcpObject.getHCPCode());
			if (contractList.isEmpty()) {  //为空代表没有合同
				hcpObject.setIsContract(0);
			}else{  //否则代表有合同
				hcpObject.setIsContract(1);
			}
		}
		return list;
	}
	@Override
	public int deleteDoctor(String hcpId) {
		return doctorMapper.deleteByPrimaryKey(hcpId);
	}


}
