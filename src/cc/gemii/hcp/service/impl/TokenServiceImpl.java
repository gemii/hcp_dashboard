package cc.gemii.hcp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.hcp.mapper.GemiiHcpTokenMapper;
import cc.gemii.hcp.po.GemiiHcpToken;
import cc.gemii.hcp.po.GemiiHcpTokenExample;
import cc.gemii.hcp.service.TokenService;
@Service
public class TokenServiceImpl implements TokenService {

	@Autowired
	private GemiiHcpTokenMapper tokenMapper;
	@Override
	public int insertToken(GemiiHcpToken token) {
		return tokenMapper.insertSelective(token);
	}
	@Override
	public GemiiHcpToken selectToken(String token) {
		GemiiHcpTokenExample example =new GemiiHcpTokenExample();
		example.createCriteria().andTokenEqualTo(token);
		List<GemiiHcpToken> list =  tokenMapper.selectByExample(example);
		return list.isEmpty() ? null:list.get(0);
	}

}
