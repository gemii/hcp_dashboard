package cc.gemii.hcp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.hcp.mapper.GemiiHcpUserMapper;
import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.po.GemiiHcpUserExample;
import cc.gemii.hcp.service.UserService;
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private GemiiHcpUserMapper userMapper;
	@Override
	public GemiiHcpUser selectByUserId(Integer userId) {
		return userMapper.selectByPrimaryKey(userId);
	}
	@Override
	public GemiiHcpUser login(String id, String password) {
		GemiiHcpUserExample example = new GemiiHcpUserExample();
		example.createCriteria().andIdEqualTo(Integer.parseInt(id)).andPasswordEqualTo(password);
		List<GemiiHcpUser> list = userMapper.selectByExample(example);
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
	@Override
	public List<GemiiHcpUser> selectByUser(GemiiHcpUserExample example) {
		return userMapper.selectByExample(example);
	}

}
