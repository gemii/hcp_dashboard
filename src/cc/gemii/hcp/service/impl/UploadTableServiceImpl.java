package cc.gemii.hcp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.hcp.mapper.GemiiHcpUploadMapper;
import cc.gemii.hcp.po.GemiiHcpUpload;
import cc.gemii.hcp.po.GemiiHcpUploadExample;
import cc.gemii.hcp.service.UploadTableService;
@Service
public class UploadTableServiceImpl implements UploadTableService {

	@Autowired
	private GemiiHcpUploadMapper uploadMapper;
	@Override
	public int addFile(GemiiHcpUpload upload) {
		return uploadMapper.insert(upload);
	}
	@Override
	public List<GemiiHcpUpload> selectById(String id) {
		GemiiHcpUploadExample example = new GemiiHcpUploadExample();
		example.createCriteria().andOutIdEqualTo(id);
		return uploadMapper.selectByExample(example);
	}
}
