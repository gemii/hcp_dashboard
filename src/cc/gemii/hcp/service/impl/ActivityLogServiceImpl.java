package cc.gemii.hcp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.hcp.mapper.GemiiHcpActivityLogMapper;
import cc.gemii.hcp.po.GemiiHcpActivityLog;
import cc.gemii.hcp.service.ActivityLogService;
@Service
public class ActivityLogServiceImpl implements ActivityLogService {

	@Autowired
	private GemiiHcpActivityLogMapper activityLogServiceImpl;
	@Override
	public int insertActivityLog(GemiiHcpActivityLog activityLog) {
		return activityLogServiceImpl.insertSelective(activityLog);
	}

}
