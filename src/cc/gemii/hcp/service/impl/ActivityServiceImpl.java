package cc.gemii.hcp.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.hcp.mapper.GemiiHcpActivityMapper;
import cc.gemii.hcp.mapper.GemiiHcpContractMapper;
import cc.gemii.hcp.mapper.MSActivityMapper;
import cc.gemii.hcp.po.GemiiHcpActivity;
import cc.gemii.hcp.po.GemiiHcpActivityExample;
import cc.gemii.hcp.po.GemiiHcpContract;
import cc.gemii.hcp.po.GemiiHcpContractExample;
import cc.gemii.hcp.pojo.GemiiHcpActivityPOJO;
import cc.gemii.hcp.service.ActivityService;
import cc.gemii.hcp.utils.AESUtil;
import cc.gemii.hcp.utils.TimeUtils;
@Service
public class ActivityServiceImpl implements ActivityService {

	@Autowired
	private GemiiHcpActivityMapper activityMapper;
	@Autowired
	private MSActivityMapper activityMapperMS;
	@Autowired
	private GemiiHcpContractMapper contractMapper;
	@Override
	public int insertActivity(GemiiHcpActivity activity) throws Exception{
		return activityMapper.insertSelective(activity);
	}
	@Override
	public GemiiHcpActivityPOJO selectByActivityId(String id) {
		GemiiHcpActivityPOJO activityPOJO =  activityMapperMS.selectByActivityId(id);
		activityPOJO.setHcpName(AESUtil.decrypt2Str(activityPOJO.getHcpName()));
		return activityPOJO;
	}
	@Override
	public List<GemiiHcpActivity> selectByActSubject(HashMap<String, Object> map) {
		List<GemiiHcpActivity> list =  activityMapperMS.selectByActSubject(map);
		for (GemiiHcpActivity gemiiHcpActivity : list) {
			gemiiHcpActivity.setHcpId(AESUtil.decrypt2Str(gemiiHcpActivity.getHcpId()));
		}
		return list;
	}
	@Override
	public int updateActivity(GemiiHcpActivity activity) {
		return activityMapper.updateByPrimaryKeySelective(activity);
	}
	@Override
	public String selectByHcpId(String id) {
		GemiiHcpActivityExample example = new GemiiHcpActivityExample();
		example.createCriteria().andHcpIdEqualTo(id);
		List<GemiiHcpActivity> list = activityMapper.selectByExample(example);
		if (list.isEmpty()) {
			return "false";
		}else{
			GemiiHcpContractExample cExample = new GemiiHcpContractExample();
			cExample.createCriteria().andHcpIdEqualTo(id);
			List<GemiiHcpContract> cList = contractMapper.selectByExample(cExample);
			if(cList.isEmpty()){
				Date date = list.get(0).getActStartTime();
				Calendar calendar = new GregorianCalendar();
				calendar.setTime(date);
				calendar.add(calendar.DATE, -1);
				return TimeUtils.changeDate(calendar.getTime());
			}else{
				return "true";
			}
		}
	}
	@Override
	public int updatePayError(String hcpId) {
		GemiiHcpActivityExample example = new GemiiHcpActivityExample();
		example.createCriteria().andHcpIdEqualTo(hcpId);
		GemiiHcpActivity activity = new GemiiHcpActivity();
		activity.setPayState(0);
		return activityMapper.updateByExampleSelective(activity, example);
	}
}
