package cc.gemii.hcp.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.service.UploadService;
import cc.gemii.hcp.utils.TimeUtils;
/**
 * 
 * 
 * Description TODO 上传文件service实现
 * 2016年5月18日
 *
 */
@Service
public class UploadServiceImpl implements UploadService {
	
	@Value("${tempPath}")
	private String tempPath;
	
	@Value("${savePath}")
	private String savePath;
	
	
	
	@Override
	public String uploadFile(HttpServletRequest request,GemiiHcpUser staff) {
		
        //创建文件夹  
        File dirFile = new File(savePath + "/" + staff.getId());  
        if (!dirFile.exists()) {  
            dirFile.mkdirs();  
        }  
        //创建临时文件夹
        File dirTempFile = new File(tempPath + "/" + staff.getId());  
        if (!dirTempFile.exists()) {  
            dirTempFile.mkdirs();  
        }  
        
        DiskFileItemFactory  factory = new DiskFileItemFactory();  
        factory.setSizeThreshold(20 * 1024 * 1024*2); //设定使用内存超过5M时，将产生临时文件并存储于临时目录中。     
        factory.setRepository(new File(tempPath)); //设定存储临时文件的目录。     
        ServletFileUpload upload = new ServletFileUpload(factory);  
        upload.setHeaderEncoding("UTF-8");  
        try {  

        	request.setCharacterEncoding("UTF-8");  
            List<?> items = upload.parseRequest(request);  
            Iterator<?> itr = items.iterator();  
              
            while (itr.hasNext())   
            {  
                FileItem item = (FileItem) itr.next();  
                if (!item.isFormField()) {  //如果是文件
                    try{  
                    	String fileName = item.getName();  
                    	 if(fileName.indexOf("+") >= 0){
                         	fileName = fileName.replaceAll("\\+","_");
                         }
                    	 fileName = TimeUtils.getTime()+"_"+fileName; //加上时间标识，防止重复
                        File uploadedFile = new File(savePath + "/" + staff.getId(),fileName);
                        OutputStream os = new FileOutputStream(uploadedFile);  
                        InputStream is = item.getInputStream();  
                        byte buf[] = new byte[1024];//可以修改 1024 以提高读取速度  
                        int length = 0;    
                        while( (length = is.read(buf)) > 0 ){    
                            os.write(buf, 0, length);    
                        }    
                        //关闭流    
                        os.flush();  
                        os.close();    
                        is.close();
                        path.add(fileName);
                    }catch(Exception e){  
                        e.printStackTrace();  
                    }  
                }          
            }   
            String allPath = StringUtils.join(path, "||");
            path.clear();
            return allPath; 
        } catch (Exception e) {  
            // TODO Auto-generated catch block  
            e.printStackTrace();  
            return "";
        }  
	}
	@Override
	public File downloadFile(String foled,String fileName) {
		File file = new File(savePath +"/" + foled + "/"+ fileName);
		return file;
	}
	@Override
	public byte[] viewPhoto(String path,String fileName) {
		byte[] buffer = null;
	    try {
	        File file = new File(savePath +"/" + path + "/"+ fileName);
	       // System.out.println(savePath + path);
	        FileInputStream fis = new FileInputStream(file);
	        ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
	        byte[] b = new byte[1000];
	        int n;
	        while ((n = fis.read(b)) != -1) {
	            bos.write(b, 0, n);
	        }
	        fis.close();
	        bos.close();
	        buffer = bos.toByteArray();
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return buffer;
	}

}
