package cc.gemii.hcp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.hcp.controller.api.resultType.HospitalObject;
import cc.gemii.hcp.mapper.GemiiHcpHospitalMapper;
import cc.gemii.hcp.mapper.GemiiHcpInfoMapper;
import cc.gemii.hcp.mapper.MSHospitalMapper;
import cc.gemii.hcp.po.GemiiHcpHospital;
import cc.gemii.hcp.po.GemiiHcpHospitalExample;
import cc.gemii.hcp.po.GemiiHcpInfo;
import cc.gemii.hcp.po.GemiiHcpInfoExample;
import cc.gemii.hcp.pojo.GemiiHcpHospitalPOJO;
import cc.gemii.hcp.service.AreaService;
import cc.gemii.hcp.service.HospitalService;
import cc.gemii.hcp.service.InfoService;
@Service
public class HospitalServiceImpl implements HospitalService {

	@Autowired
	private GemiiHcpHospitalMapper hospitalMapper;
	@Autowired
	private InfoService infoService;
	@Autowired
	private MSHospitalMapper msHospitalMapper;
	@Autowired
	private GemiiHcpInfoMapper infoMapper;
	@Autowired
	private AreaService areaService;
	@Override
	public int addHospital(GemiiHcpHospital hospital) {
		return hospitalMapper.insert(hospital);
	}
	@Override
	public List<GemiiHcpHospitalPOJO> getAllHsopital(GemiiHcpHospitalExample example) {
		List<GemiiHcpHospital> list = hospitalMapper.selectByExample(example);
		List<GemiiHcpHospitalPOJO> listPOJO = new ArrayList<GemiiHcpHospitalPOJO>();
		for (GemiiHcpHospital gemiiHcpHospital : list) {
			
		  GemiiHcpHospitalPOJO hospitalPOJO = new GemiiHcpHospitalPOJO();
		  hospitalPOJO.setId(gemiiHcpHospital.getId());
		  hospitalPOJO.setCityId(gemiiHcpHospital.getCityId());
		  hospitalPOJO.setHsptAddress(gemiiHcpHospital.getHsptAddress());
		  hospitalPOJO.setHsptCode(gemiiHcpHospital.getHsptCode());
		  hospitalPOJO.setHsptLevel(gemiiHcpHospital.getHsptLevel());
		  hospitalPOJO.setHsptName(gemiiHcpHospital.getHsptName());
		  hospitalPOJO.setHsptNbm(gemiiHcpHospital.getHsptNbm());
		  hospitalPOJO.setHsptPhone(gemiiHcpHospital.getHsptPhone());
		  hospitalPOJO.setHsptType(gemiiHcpHospital.getHsptType());
		  hospitalPOJO.setHsptUrl(gemiiHcpHospital.getHsptUrl());
		  hospitalPOJO.setHsptVerify(gemiiHcpHospital.getHsptVerify());
		  
		  hospitalPOJO.setCityName(infoService.getInfoTree(gemiiHcpHospital.getCityId()));
		  hospitalPOJO.setHsptType(infoService.getInfoTree(Integer.parseInt(gemiiHcpHospital.getHsptType())));
		  if(gemiiHcpHospital.getHsptLevel()!=null)
			  hospitalPOJO.setHsptLevel(infoService.getInfoTree(Integer.parseInt(gemiiHcpHospital.getHsptLevel())));
		  listPOJO.add(hospitalPOJO);
		}
		return listPOJO;
	}
	@Override
	public List<GemiiHcpHospital> getHospitalsName(
			GemiiHcpHospitalExample example) {
		return hospitalMapper.selectByExample(example);
	}
	@Override
	public GemiiHcpHospitalPOJO selectByHospitalId(String id) {
		GemiiHcpHospital gemiiHcpHospital =  hospitalMapper.selectByPrimaryKey(id);
		GemiiHcpHospitalPOJO hospitalPOJO = new GemiiHcpHospitalPOJO();
		  hospitalPOJO.setId(gemiiHcpHospital.getId());
		  hospitalPOJO.setCityId(gemiiHcpHospital.getCityId());
		  hospitalPOJO.setHsptAddress(gemiiHcpHospital.getHsptAddress());
		  hospitalPOJO.setHsptCode(gemiiHcpHospital.getHsptCode());
		  hospitalPOJO.setHsptLevel(gemiiHcpHospital.getHsptLevel());
		  hospitalPOJO.setHsptName(gemiiHcpHospital.getHsptName());
		  hospitalPOJO.setHsptNbm(gemiiHcpHospital.getHsptNbm());
		  hospitalPOJO.setHsptPhone(gemiiHcpHospital.getHsptPhone());
		  hospitalPOJO.setHsptType(gemiiHcpHospital.getHsptType());
		  hospitalPOJO.setHsptUrl(gemiiHcpHospital.getHsptUrl());
		  hospitalPOJO.setHsptVerify(gemiiHcpHospital.getHsptVerify());
		  hospitalPOJO.setAreaName(areaService.selectById(gemiiHcpHospital.getAreaId()).getAreaName());
		  hospitalPOJO.setCityName(infoService.getInfoTree(gemiiHcpHospital.getCityId()));
		  hospitalPOJO.setHsptType(infoService.getInfoTree(Integer.parseInt(gemiiHcpHospital.getHsptType())));
		  if(gemiiHcpHospital.getHsptLevel()!=null)
			  hospitalPOJO.setHsptLevel(infoService.getInfoTree(Integer.parseInt(gemiiHcpHospital.getHsptLevel())));
		  
		  return hospitalPOJO;
	}
	@Override
	public List<HospitalObject> apiGetHospital() {
		List<HospitalObject> list = msHospitalMapper.apiGetHospital();
		for (HospitalObject hospitalObject : list) {
			hospitalObject.setCoveragd("Maternal覆盖");
			
			List<String> params = new ArrayList<String>();
			params.add(hospitalObject.getHospitalType());
			params.add(hospitalObject.getHospitalLevel());
			GemiiHcpInfoExample example = new GemiiHcpInfoExample();
			example.createCriteria().andIdIn(params);
			List<GemiiHcpInfo> infoList = infoMapper.selectByExample(example);
			hospitalObject.setHospitalType(infoList.get(0).getContent());
			hospitalObject.setHospitalLevel(infoList.get(1).getContent());
			String[] addString = infoService.getInfoTree(Integer.parseInt(hospitalObject.getCity())).split("/");
			hospitalObject.setProvince(addString[0]);
			if("北京市天津市上海市重庆市".contains(addString[0])){
				hospitalObject.setCheckCity(addString[0]);
			}else{
				hospitalObject.setCheckCity(addString[1]);
			}
			hospitalObject.setIsEnable(1);
		}
		return list;
	}

}
