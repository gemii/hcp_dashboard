package cc.gemii.hcp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.hcp.controller.api.resultType.GroupObject;
import cc.gemii.hcp.mapper.GemiiHcpGroupMapper;
import cc.gemii.hcp.mapper.MSGroupMapper;
import cc.gemii.hcp.po.GemiiHcpGroup;
import cc.gemii.hcp.po.GemiiHcpGroupExample;
import cc.gemii.hcp.pojo.GemiiHcpGroupPOJO;
import cc.gemii.hcp.service.GroupService;
import cc.gemii.hcp.service.InfoService;
@Service
public class GroupServiceImpl implements GroupService {

	@Autowired
	private GemiiHcpGroupMapper groupMapper;
	@Autowired
	private MSGroupMapper msGroupMapper;
	@Autowired
	private InfoService infoService;
	@Override
	public GemiiHcpGroup selectByGroupId(String groupId) {
		return groupMapper.selectByPrimaryKey(groupId);
	}
	@Override
	public List<GemiiHcpGroup> selectByGroupName(HashMap<String, Object> map) {
		GemiiHcpGroupExample example = new GemiiHcpGroupExample();
		example.createCriteria().andGroupNameLike("%"+map.get("groupName").toString()+"%");
		List<GemiiHcpGroup> list1 = groupMapper.selectByExample(example);
		return list1;
	}
	@Override
	public List<GroupObject> apiGetGroup() {
		List<GroupObject> list = msGroupMapper.apiGetGroup();
//		for (GroupObject groupObject : list) {
//			
////			String[] addString = infoService.getInfoTree(Integer.parseInt(groupObject.getFK_AnchorCityID())).split("/");
////			if("北京市天津市上海市重庆市".contains(addString[0])){
////				groupObject.setFK_AnchorCity(addString[0]);
////			}else{
////				groupObject.setFK_AnchorCity(addString[1]);
////			}
//			groupObject.setIsOpen(1);
//		}
		return list;
	}
	@Override
	public int insertGroup(GemiiHcpGroup group) {
		return groupMapper.insert(group);
	}
	@Override
	public void truncateGroup() {
		msGroupMapper.truncateGroup();
	}
	
}
