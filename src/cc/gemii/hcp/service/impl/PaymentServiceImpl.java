package cc.gemii.hcp.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.hcp.mapper.MSPaymentMapper;
import cc.gemii.hcp.pojo.GemiiHcpPaymentPOJO;
import cc.gemii.hcp.service.PaymentService;
import cc.gemii.hcp.utils.AESUtil;
@Service
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	private MSPaymentMapper msPaymentMapper;
	@Override
	public Map<String, Object> getNotPayList(HashMap<String, Object> param) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<GemiiHcpPaymentPOJO> list = msPaymentMapper.getNotPayList(param);
		Integer totalCount = 0;
		for (GemiiHcpPaymentPOJO gemiiHcpPaymentPOJO : list) {
			gemiiHcpPaymentPOJO.setHcpName(AESUtil.decrypt2Str(gemiiHcpPaymentPOJO.getHcpName()));
			gemiiHcpPaymentPOJO.setBankCardName(AESUtil.decrypt2Str(gemiiHcpPaymentPOJO.getBankCardName()));
			gemiiHcpPaymentPOJO.setBankCardNumber(AESUtil.decrypt2Str(gemiiHcpPaymentPOJO.getBankCardNumber()));
			totalCount += Integer.parseInt(gemiiHcpPaymentPOJO.getFeedPaid());
		}
		map.put("payment", list);
		map.put("totalCount", totalCount);
		return map;
	}

}
