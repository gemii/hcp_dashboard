package cc.gemii.hcp.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import cc.gemii.hcp.po.GemiiHcpUser;

/**
 * 
 * @author 金自力
 * Description TODO 上传文件service
 * 2016年5月19日
 *
 */
public interface UploadService {
	
	//上传文件的路径,多文件之间路径由,分割
	public static List<String> path = new ArrayList<>();
	/**
	 * 
	 * @param request request请求
	 * @return 上传文件的路径,多个文件路径之间用","分割
	 * String
	 */
	String uploadFile(HttpServletRequest request,GemiiHcpUser staff);
	File downloadFile(String foled,String fileName);
	byte[] viewPhoto(String path,String fileName);
}
