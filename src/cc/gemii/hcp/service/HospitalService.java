package cc.gemii.hcp.service;

import java.util.List;

import cc.gemii.hcp.controller.api.resultType.HospitalObject;
import cc.gemii.hcp.po.GemiiHcpHospital;
import cc.gemii.hcp.po.GemiiHcpHospitalExample;
import cc.gemii.hcp.pojo.GemiiHcpHospitalPOJO;

public interface HospitalService {
	int addHospital(GemiiHcpHospital hospital);
	GemiiHcpHospitalPOJO selectByHospitalId(String id);
	List<GemiiHcpHospitalPOJO> getAllHsopital(GemiiHcpHospitalExample example);
	List<GemiiHcpHospital> getHospitalsName(GemiiHcpHospitalExample example);
	List<HospitalObject> apiGetHospital();
}
