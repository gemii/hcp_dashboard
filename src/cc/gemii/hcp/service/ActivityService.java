package cc.gemii.hcp.service;

import java.util.HashMap;
import java.util.List;

import cc.gemii.hcp.po.GemiiHcpActivity;
import cc.gemii.hcp.pojo.GemiiHcpActivityPOJO;

public interface ActivityService {
  int insertActivity(GemiiHcpActivity activity) throws Exception;
  List<GemiiHcpActivity> selectByActSubject(HashMap<String, Object> map);
  GemiiHcpActivityPOJO selectByActivityId(String id);
  int updateActivity(GemiiHcpActivity activity);
  String selectByHcpId(String id);
  int updatePayError(String hcpId);
}
