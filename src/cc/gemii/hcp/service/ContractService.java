package cc.gemii.hcp.service;

import java.util.HashMap;
import java.util.List;

import cc.gemii.hcp.po.GemiiHcpContract;
import cc.gemii.hcp.po.GemiiHcpContractThird;
import cc.gemii.hcp.pojo.GemiiHcpContractPOJO;

public interface ContractService {
   List<GemiiHcpContract> selectByHcpId(String id);
   GemiiHcpContract selectByContractId(String id);
   int insertContract(GemiiHcpContract contract);
   int updateContract(GemiiHcpContract contract);
   List<GemiiHcpContractPOJO> getContractList(HashMap<String, Object> map);
   GemiiHcpContractPOJO getContractById(HashMap<String, Object> map);
   int insertContractThird(GemiiHcpContractThird third);
   GemiiHcpContractThird selectByContractThirdId(String id);
   int updateContractThird(GemiiHcpContractThird third);
}
