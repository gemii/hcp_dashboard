package cc.gemii.hcp.service;

import java.util.List;

import cc.gemii.hcp.po.GemiiHcpAreas;

public interface AreaService {

	GemiiHcpAreas selectById(Integer id);
	List<GemiiHcpAreas> getAll();
}
