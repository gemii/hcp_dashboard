package cc.gemii.hcp.service;

import cc.gemii.hcp.po.GemiiHcpToken;

public interface TokenService {

	int insertToken(GemiiHcpToken token);
	GemiiHcpToken selectToken(String token);
}
