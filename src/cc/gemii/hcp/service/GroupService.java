package cc.gemii.hcp.service;

import java.util.HashMap;
import java.util.List;

import cc.gemii.hcp.controller.api.resultType.GroupObject;
import cc.gemii.hcp.po.GemiiHcpGroup;
import cc.gemii.hcp.pojo.GemiiHcpGroupPOJO;

public interface GroupService {

	GemiiHcpGroup selectByGroupId(String groupId);
	List<GemiiHcpGroup> selectByGroupName(HashMap<String, Object> map);
	List<GroupObject> apiGetGroup();
	int insertGroup(GemiiHcpGroup group);
	void truncateGroup();
}
