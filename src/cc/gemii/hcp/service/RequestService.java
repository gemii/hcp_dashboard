package cc.gemii.hcp.service;

import cc.gemii.hcp.po.GemiiHcpRequestWithBLOBs;

public interface RequestService {

  int insertRequest(GemiiHcpRequestWithBLOBs gemiiHcpRequestWithBLOBs);	
}
