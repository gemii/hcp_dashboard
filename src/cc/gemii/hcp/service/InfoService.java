package cc.gemii.hcp.service;

import java.util.List;

import cc.gemii.hcp.po.GemiiHcpInfo;

public interface InfoService {
    List<GemiiHcpInfo> getInfoByDid(int dId,int pId);
    String getInfoTree(int id);
    GemiiHcpInfo getInfoById(String id);
}
