package cc.gemii.hcp.service;

import java.util.List;

import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.po.GemiiHcpUserExample;

public interface UserService {

	GemiiHcpUser selectByUserId(Integer userId);
	GemiiHcpUser login(String id,String password);
	List<GemiiHcpUser> selectByUser(GemiiHcpUserExample example);
}
