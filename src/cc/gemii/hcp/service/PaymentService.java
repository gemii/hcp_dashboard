package cc.gemii.hcp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cc.gemii.hcp.pojo.GemiiHcpPaymentPOJO;

public interface PaymentService {

	Map<String, Object> getNotPayList(HashMap<String, Object> param);
}
