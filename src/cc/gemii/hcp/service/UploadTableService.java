package cc.gemii.hcp.service;

import java.util.List;

import cc.gemii.hcp.po.GemiiHcpUpload;

public interface UploadTableService {
   int addFile(GemiiHcpUpload upload);
   List<GemiiHcpUpload> selectById(String id);
}
