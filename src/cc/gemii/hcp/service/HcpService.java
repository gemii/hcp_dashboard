package cc.gemii.hcp.service;

import java.util.HashMap;
import java.util.List;

import cc.gemii.hcp.controller.api.resultType.HCPObject;
import cc.gemii.hcp.po.GemiiHcpDoctor;
import cc.gemii.hcp.po.GemiiHcpDoctorExample;
import cc.gemii.hcp.po.GemiiHcpVerify;
import cc.gemii.hcp.pojo.GemiiHcpVerifyPOJO;

public interface HcpService {
  int save(GemiiHcpDoctor doctor);
  boolean isExist(GemiiHcpDoctorExample example);
  int saveVerify(GemiiHcpVerify verify);
  List<GemiiHcpDoctor> selectByVerify(HashMap map);
  GemiiHcpDoctor selectByDoctorId(String id);
  List<GemiiHcpVerifyPOJO> selectByNotVerify();
  void updateVerify(GemiiHcpVerify verify);
  void updeteDoctor(GemiiHcpDoctor doctor);
  GemiiHcpDoctor getMaxDoctor();
  List<HCPObject> apiGetDoctor();
  int deleteDoctor(String hcpId);
}
