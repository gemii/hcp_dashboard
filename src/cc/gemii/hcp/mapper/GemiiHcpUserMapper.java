package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpUser;
import cc.gemii.hcp.po.GemiiHcpUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpUserMapper {
    int countByExample(GemiiHcpUserExample example);

    int deleteByExample(GemiiHcpUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(GemiiHcpUser record);

    int insertSelective(GemiiHcpUser record);

    List<GemiiHcpUser> selectByExample(GemiiHcpUserExample example);

    GemiiHcpUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") GemiiHcpUser record, @Param("example") GemiiHcpUserExample example);

    int updateByExample(@Param("record") GemiiHcpUser record, @Param("example") GemiiHcpUserExample example);

    int updateByPrimaryKeySelective(GemiiHcpUser record);

    int updateByPrimaryKey(GemiiHcpUser record);
}