package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpGroup;
import cc.gemii.hcp.po.GemiiHcpGroupExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpGroupMapper {
    int countByExample(GemiiHcpGroupExample example);

    int deleteByExample(GemiiHcpGroupExample example);

    int deleteByPrimaryKey(String id);

    int insert(GemiiHcpGroup record);

    int insertSelective(GemiiHcpGroup record);

    List<GemiiHcpGroup> selectByExample(GemiiHcpGroupExample example);

    GemiiHcpGroup selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") GemiiHcpGroup record, @Param("example") GemiiHcpGroupExample example);

    int updateByExample(@Param("record") GemiiHcpGroup record, @Param("example") GemiiHcpGroupExample example);

    int updateByPrimaryKeySelective(GemiiHcpGroup record);

    int updateByPrimaryKey(GemiiHcpGroup record);
}