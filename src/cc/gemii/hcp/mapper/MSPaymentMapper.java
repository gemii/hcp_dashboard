package cc.gemii.hcp.mapper;

import java.util.HashMap;
import java.util.List;

import cc.gemii.hcp.pojo.GemiiHcpPaymentPOJO;

public interface MSPaymentMapper {
	List<GemiiHcpPaymentPOJO> getNotPayList(HashMap<String, Object> param);
}
