package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpUpload;
import cc.gemii.hcp.po.GemiiHcpUploadExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpUploadMapper {
    int countByExample(GemiiHcpUploadExample example);

    int deleteByExample(GemiiHcpUploadExample example);

    int deleteByPrimaryKey(String id);

    int insert(GemiiHcpUpload record);

    int insertSelective(GemiiHcpUpload record);

    List<GemiiHcpUpload> selectByExample(GemiiHcpUploadExample example);

    GemiiHcpUpload selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") GemiiHcpUpload record, @Param("example") GemiiHcpUploadExample example);

    int updateByExample(@Param("record") GemiiHcpUpload record, @Param("example") GemiiHcpUploadExample example);

    int updateByPrimaryKeySelective(GemiiHcpUpload record);

    int updateByPrimaryKey(GemiiHcpUpload record);
}