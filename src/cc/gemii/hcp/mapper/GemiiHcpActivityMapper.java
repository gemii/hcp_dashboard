package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpActivity;
import cc.gemii.hcp.po.GemiiHcpActivityExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpActivityMapper {
    int countByExample(GemiiHcpActivityExample example);

    int deleteByExample(GemiiHcpActivityExample example);

    int deleteByPrimaryKey(String id);

    int insert(GemiiHcpActivity record);

    int insertSelective(GemiiHcpActivity record);

    List<GemiiHcpActivity> selectByExample(GemiiHcpActivityExample example);

    GemiiHcpActivity selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") GemiiHcpActivity record, @Param("example") GemiiHcpActivityExample example);

    int updateByExample(@Param("record") GemiiHcpActivity record, @Param("example") GemiiHcpActivityExample example);

    int updateByPrimaryKeySelective(GemiiHcpActivity record);

    int updateByPrimaryKey(GemiiHcpActivity record);
}