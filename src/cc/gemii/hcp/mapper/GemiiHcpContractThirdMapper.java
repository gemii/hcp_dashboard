package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpContractThird;
import cc.gemii.hcp.po.GemiiHcpContractThirdExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpContractThirdMapper {
    int countByExample(GemiiHcpContractThirdExample example);

    int deleteByExample(GemiiHcpContractThirdExample example);

    int deleteByPrimaryKey(String contractId);

    int insert(GemiiHcpContractThird record);

    int insertSelective(GemiiHcpContractThird record);

    List<GemiiHcpContractThird> selectByExample(GemiiHcpContractThirdExample example);

    GemiiHcpContractThird selectByPrimaryKey(String contractId);

    int updateByExampleSelective(@Param("record") GemiiHcpContractThird record, @Param("example") GemiiHcpContractThirdExample example);

    int updateByExample(@Param("record") GemiiHcpContractThird record, @Param("example") GemiiHcpContractThirdExample example);

    int updateByPrimaryKeySelective(GemiiHcpContractThird record);

    int updateByPrimaryKey(GemiiHcpContractThird record);
}