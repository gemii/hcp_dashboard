package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpAreas;
import cc.gemii.hcp.po.GemiiHcpAreasExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpAreasMapper {
    int countByExample(GemiiHcpAreasExample example);

    int deleteByExample(GemiiHcpAreasExample example);

    int deleteByPrimaryKey(Integer areaId);

    int insert(GemiiHcpAreas record);

    int insertSelective(GemiiHcpAreas record);

    List<GemiiHcpAreas> selectByExample(GemiiHcpAreasExample example);

    GemiiHcpAreas selectByPrimaryKey(Integer areaId);

    int updateByExampleSelective(@Param("record") GemiiHcpAreas record, @Param("example") GemiiHcpAreasExample example);

    int updateByExample(@Param("record") GemiiHcpAreas record, @Param("example") GemiiHcpAreasExample example);

    int updateByPrimaryKeySelective(GemiiHcpAreas record);

    int updateByPrimaryKey(GemiiHcpAreas record);
}