package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpRequest;
import cc.gemii.hcp.po.GemiiHcpRequestExample;
import cc.gemii.hcp.po.GemiiHcpRequestWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpRequestMapper {
    int countByExample(GemiiHcpRequestExample example);

    int deleteByExample(GemiiHcpRequestExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(GemiiHcpRequestWithBLOBs record);

    int insertSelective(GemiiHcpRequestWithBLOBs record);

    List<GemiiHcpRequestWithBLOBs> selectByExampleWithBLOBs(GemiiHcpRequestExample example);

    List<GemiiHcpRequest> selectByExample(GemiiHcpRequestExample example);

    GemiiHcpRequestWithBLOBs selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") GemiiHcpRequestWithBLOBs record, @Param("example") GemiiHcpRequestExample example);

    int updateByExampleWithBLOBs(@Param("record") GemiiHcpRequestWithBLOBs record, @Param("example") GemiiHcpRequestExample example);

    int updateByExample(@Param("record") GemiiHcpRequest record, @Param("example") GemiiHcpRequestExample example);

    int updateByPrimaryKeySelective(GemiiHcpRequestWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(GemiiHcpRequestWithBLOBs record);

    int updateByPrimaryKey(GemiiHcpRequest record);
}