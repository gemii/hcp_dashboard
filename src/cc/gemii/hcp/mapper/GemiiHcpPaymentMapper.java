package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpPayment;
import cc.gemii.hcp.po.GemiiHcpPaymentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpPaymentMapper {
    int countByExample(GemiiHcpPaymentExample example);

    int deleteByExample(GemiiHcpPaymentExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(GemiiHcpPayment record);

    int insertSelective(GemiiHcpPayment record);

    List<GemiiHcpPayment> selectByExample(GemiiHcpPaymentExample example);

    GemiiHcpPayment selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") GemiiHcpPayment record, @Param("example") GemiiHcpPaymentExample example);

    int updateByExample(@Param("record") GemiiHcpPayment record, @Param("example") GemiiHcpPaymentExample example);

    int updateByPrimaryKeySelective(GemiiHcpPayment record);

    int updateByPrimaryKey(GemiiHcpPayment record);
}