package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpCity;
import cc.gemii.hcp.po.GemiiHcpCityExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpCityMapper {
    int countByExample(GemiiHcpCityExample example);

    int deleteByExample(GemiiHcpCityExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(GemiiHcpCity record);

    int insertSelective(GemiiHcpCity record);

    List<GemiiHcpCity> selectByExample(GemiiHcpCityExample example);

    GemiiHcpCity selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") GemiiHcpCity record, @Param("example") GemiiHcpCityExample example);

    int updateByExample(@Param("record") GemiiHcpCity record, @Param("example") GemiiHcpCityExample example);

    int updateByPrimaryKeySelective(GemiiHcpCity record);

    int updateByPrimaryKey(GemiiHcpCity record);
}