package cc.gemii.hcp.mapper;

import java.util.HashMap;
import java.util.List;

import cc.gemii.hcp.controller.api.resultType.GroupObject;
import cc.gemii.hcp.pojo.GemiiHcpGroupPOJO;

public interface MSGroupMapper {
  List<GemiiHcpGroupPOJO> selectByGroupName(HashMap map);
  List<GroupObject> apiGetGroup();
  void truncateGroup();
}
