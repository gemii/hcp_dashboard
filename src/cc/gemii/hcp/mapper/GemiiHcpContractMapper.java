package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpContract;
import cc.gemii.hcp.po.GemiiHcpContractExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpContractMapper {
    int countByExample(GemiiHcpContractExample example);

    int deleteByExample(GemiiHcpContractExample example);

    int deleteByPrimaryKey(String id);

    int insert(GemiiHcpContract record);

    int insertSelective(GemiiHcpContract record);

    List<GemiiHcpContract> selectByExample(GemiiHcpContractExample example);

    GemiiHcpContract selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") GemiiHcpContract record, @Param("example") GemiiHcpContractExample example);

    int updateByExample(@Param("record") GemiiHcpContract record, @Param("example") GemiiHcpContractExample example);

    int updateByPrimaryKeySelective(GemiiHcpContract record);

    int updateByPrimaryKey(GemiiHcpContract record);
}