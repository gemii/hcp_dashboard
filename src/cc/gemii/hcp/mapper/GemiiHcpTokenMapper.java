package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpToken;
import cc.gemii.hcp.po.GemiiHcpTokenExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpTokenMapper {
    int countByExample(GemiiHcpTokenExample example);

    int deleteByExample(GemiiHcpTokenExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(GemiiHcpToken record);

    int insertSelective(GemiiHcpToken record);

    List<GemiiHcpToken> selectByExample(GemiiHcpTokenExample example);

    GemiiHcpToken selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") GemiiHcpToken record, @Param("example") GemiiHcpTokenExample example);

    int updateByExample(@Param("record") GemiiHcpToken record, @Param("example") GemiiHcpTokenExample example);

    int updateByPrimaryKeySelective(GemiiHcpToken record);

    int updateByPrimaryKey(GemiiHcpToken record);
}