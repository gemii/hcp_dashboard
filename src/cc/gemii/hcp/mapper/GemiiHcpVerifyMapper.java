package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpVerify;
import cc.gemii.hcp.po.GemiiHcpVerifyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpVerifyMapper {
    int countByExample(GemiiHcpVerifyExample example);

    int deleteByExample(GemiiHcpVerifyExample example);

    int deleteByPrimaryKey(String id);

    int insert(GemiiHcpVerify record);

    int insertSelective(GemiiHcpVerify record);

    List<GemiiHcpVerify> selectByExample(GemiiHcpVerifyExample example);

    GemiiHcpVerify selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") GemiiHcpVerify record, @Param("example") GemiiHcpVerifyExample example);

    int updateByExample(@Param("record") GemiiHcpVerify record, @Param("example") GemiiHcpVerifyExample example);

    int updateByPrimaryKeySelective(GemiiHcpVerify record);

    int updateByPrimaryKey(GemiiHcpVerify record);
}