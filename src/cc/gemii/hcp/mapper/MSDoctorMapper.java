package cc.gemii.hcp.mapper;

import java.util.HashMap;
import java.util.List;

import cc.gemii.hcp.controller.api.resultType.HCPObject;
import cc.gemii.hcp.po.GemiiHcpDoctor;
import cc.gemii.hcp.pojo.GemiiHcpVerifyPOJO;

public interface MSDoctorMapper {
  List<GemiiHcpDoctor> selectByVerify(HashMap map);
  GemiiHcpDoctor selectByDoctorId(String id);
  List<GemiiHcpVerifyPOJO> getByNotCheck();
  GemiiHcpDoctor getMaxDoctor();
  List<HCPObject> apiGetDoctor();
}
