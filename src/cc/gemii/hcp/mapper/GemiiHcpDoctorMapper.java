package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpDoctor;
import cc.gemii.hcp.po.GemiiHcpDoctorExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpDoctorMapper {
    int countByExample(GemiiHcpDoctorExample example);

    int deleteByExample(GemiiHcpDoctorExample example);

    int deleteByPrimaryKey(String id);

    int insert(GemiiHcpDoctor record);

    int insertSelective(GemiiHcpDoctor record);

    List<GemiiHcpDoctor> selectByExample(GemiiHcpDoctorExample example);

    GemiiHcpDoctor selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") GemiiHcpDoctor record, @Param("example") GemiiHcpDoctorExample example);

    int updateByExample(@Param("record") GemiiHcpDoctor record, @Param("example") GemiiHcpDoctorExample example);

    int updateByPrimaryKeySelective(GemiiHcpDoctor record);

    int updateByPrimaryKey(GemiiHcpDoctor record);
}