package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpActivityLog;
import cc.gemii.hcp.po.GemiiHcpActivityLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpActivityLogMapper {
    int countByExample(GemiiHcpActivityLogExample example);

    int deleteByExample(GemiiHcpActivityLogExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(GemiiHcpActivityLog record);

    int insertSelective(GemiiHcpActivityLog record);

    List<GemiiHcpActivityLog> selectByExampleWithBLOBs(GemiiHcpActivityLogExample example);

    List<GemiiHcpActivityLog> selectByExample(GemiiHcpActivityLogExample example);

    GemiiHcpActivityLog selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") GemiiHcpActivityLog record, @Param("example") GemiiHcpActivityLogExample example);

    int updateByExampleWithBLOBs(@Param("record") GemiiHcpActivityLog record, @Param("example") GemiiHcpActivityLogExample example);

    int updateByExample(@Param("record") GemiiHcpActivityLog record, @Param("example") GemiiHcpActivityLogExample example);

    int updateByPrimaryKeySelective(GemiiHcpActivityLog record);

    int updateByPrimaryKeyWithBLOBs(GemiiHcpActivityLog record);

    int updateByPrimaryKey(GemiiHcpActivityLog record);
}