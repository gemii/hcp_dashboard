package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpInfo;
import cc.gemii.hcp.po.GemiiHcpInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpInfoMapper {
    int countByExample(GemiiHcpInfoExample example);

    int deleteByExample(GemiiHcpInfoExample example);

    int deleteByPrimaryKey(String id);

    int insert(GemiiHcpInfo record);

    int insertSelective(GemiiHcpInfo record);

    List<GemiiHcpInfo> selectByExample(GemiiHcpInfoExample example);

    GemiiHcpInfo selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") GemiiHcpInfo record, @Param("example") GemiiHcpInfoExample example);

    int updateByExample(@Param("record") GemiiHcpInfo record, @Param("example") GemiiHcpInfoExample example);

    int updateByPrimaryKeySelective(GemiiHcpInfo record);

    int updateByPrimaryKey(GemiiHcpInfo record);
}