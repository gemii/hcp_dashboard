package cc.gemii.hcp.mapper;

import java.util.List;

import cc.gemii.hcp.controller.api.resultType.HospitalObject;

public interface MSHospitalMapper {
  List<HospitalObject> apiGetHospital();
}
