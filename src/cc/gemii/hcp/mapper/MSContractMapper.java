package cc.gemii.hcp.mapper;

import java.util.HashMap;
import java.util.List;

import cc.gemii.hcp.po.GemiiHcpContract;
import cc.gemii.hcp.pojo.GemiiHcpContractPOJO;


public interface MSContractMapper {
	List<GemiiHcpContractPOJO> getContractList(HashMap<String, Object> map);
	GemiiHcpContractPOJO getContractById(HashMap<String, Object> map);
	List<GemiiHcpContract> getContractListByHcpID(HashMap<String, Object> map);
}
