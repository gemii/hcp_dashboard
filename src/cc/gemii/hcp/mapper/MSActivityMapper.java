package cc.gemii.hcp.mapper;

import java.util.HashMap;
import java.util.List;

import cc.gemii.hcp.po.GemiiHcpActivity;
import cc.gemii.hcp.pojo.GemiiHcpActivityPOJO;

public interface MSActivityMapper {
  GemiiHcpActivityPOJO selectByActivityId(String id);
  List<GemiiHcpActivity> selectByActSubject(HashMap<String, Object> map);
  
}
