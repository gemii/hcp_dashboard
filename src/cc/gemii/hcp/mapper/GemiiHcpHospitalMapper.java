package cc.gemii.hcp.mapper;

import cc.gemii.hcp.po.GemiiHcpHospital;
import cc.gemii.hcp.po.GemiiHcpHospitalExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GemiiHcpHospitalMapper {
    int countByExample(GemiiHcpHospitalExample example);

    int deleteByExample(GemiiHcpHospitalExample example);

    int deleteByPrimaryKey(String id);

    int insert(GemiiHcpHospital record);

    int insertSelective(GemiiHcpHospital record);

    List<GemiiHcpHospital> selectByExample(GemiiHcpHospitalExample example);

    GemiiHcpHospital selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") GemiiHcpHospital record, @Param("example") GemiiHcpHospitalExample example);

    int updateByExample(@Param("record") GemiiHcpHospital record, @Param("example") GemiiHcpHospitalExample example);

    int updateByPrimaryKeySelective(GemiiHcpHospital record);

    int updateByPrimaryKey(GemiiHcpHospital record);
}