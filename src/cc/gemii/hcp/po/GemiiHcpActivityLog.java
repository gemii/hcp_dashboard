package cc.gemii.hcp.po;

import java.util.Date;

public class GemiiHcpActivityLog {
    private Integer id;

    private Date ctime;

    private String state;

    private String result;

    private String content;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result == null ? null : result.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

	@Override
	public String toString() {
		return "GemiiHcpActivityLog [id=" + id + ", ctime=" + ctime
				+ ", state=" + state + ", result=" + result + ", content="
				+ content + "]";
	}
    
}