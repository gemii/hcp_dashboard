package cc.gemii.hcp.po;

public class GemiiHcpHospital {
    private String id;

    private String hsptName;

    private Integer areaId;

    private Integer cityId;

    private Integer hsptNbm;

    private String hsptType;

    private String hsptLevel;

    private String hsptUrl;

    private String hsptAddress;

    private String hsptPhone;

    private Integer hsptVerify;

    private String hsptCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getHsptName() {
        return hsptName;
    }

    public void setHsptName(String hsptName) {
        this.hsptName = hsptName == null ? null : hsptName.trim();
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getHsptNbm() {
        return hsptNbm;
    }

    public void setHsptNbm(Integer hsptNbm) {
        this.hsptNbm = hsptNbm;
    }

    public String getHsptType() {
        return hsptType;
    }

    public void setHsptType(String hsptType) {
        this.hsptType = hsptType == null ? null : hsptType.trim();
    }

    public String getHsptLevel() {
        return hsptLevel;
    }

    public void setHsptLevel(String hsptLevel) {
        this.hsptLevel = hsptLevel == null ? null : hsptLevel.trim();
    }

    public String getHsptUrl() {
        return hsptUrl;
    }

    public void setHsptUrl(String hsptUrl) {
        this.hsptUrl = hsptUrl == null ? null : hsptUrl.trim();
    }

    public String getHsptAddress() {
        return hsptAddress;
    }

    public void setHsptAddress(String hsptAddress) {
        this.hsptAddress = hsptAddress == null ? null : hsptAddress.trim();
    }

    public String getHsptPhone() {
        return hsptPhone;
    }

    public void setHsptPhone(String hsptPhone) {
        this.hsptPhone = hsptPhone == null ? null : hsptPhone.trim();
    }

    public Integer getHsptVerify() {
        return hsptVerify;
    }

    public void setHsptVerify(Integer hsptVerify) {
        this.hsptVerify = hsptVerify;
    }

    public String getHsptCode() {
        return hsptCode;
    }

    public void setHsptCode(String hsptCode) {
        this.hsptCode = hsptCode == null ? null : hsptCode.trim();
    }
}