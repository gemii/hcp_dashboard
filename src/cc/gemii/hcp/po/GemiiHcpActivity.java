package cc.gemii.hcp.po;

import java.util.Date;

public class GemiiHcpActivity {
    private String id;

    private String groupId;

    private String hcpId;

    private String staffId;

    private Date actCtime;

    private Date actStartTime;

    private Date actEndTime;

    private Integer actPeriod;

    private String actReview;

    private Long actPaid;

    private String actCourseware;

    private String actSubject;

    private String hcpWxName;

    private String hcpWxNick;

    private String remark;

    private Date feedbackTime;

    private Date actRealStart;

    private Date actRealEnd;

    private Byte feedbackPro;

    private Byte feedbackFull;

    private Byte feedbackClear;

    private Byte feedbackInteraction;

    private String feedbackOther;

    private String paymentId;

    private Long feedPaid;

    private Integer state;

    private String feedRemark;

    private String actFile;

    private Integer payState;

    private Integer tpId;

    private Date payToThirdTime;

    private Date payTime;

    private String payRemark;

    private Integer payToThird;

    private String payFile;

    private String contractId;

    private String feedbackRemark;

    private Date verfiryTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId == null ? null : groupId.trim();
    }

    public String getHcpId() {
        return hcpId;
    }

    public void setHcpId(String hcpId) {
        this.hcpId = hcpId == null ? null : hcpId.trim();
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId == null ? null : staffId.trim();
    }

    public Date getActCtime() {
        return actCtime;
    }

    public void setActCtime(Date actCtime) {
        this.actCtime = actCtime;
    }

    public Date getActStartTime() {
        return actStartTime;
    }

    public void setActStartTime(Date actStartTime) {
        this.actStartTime = actStartTime;
    }

    public Date getActEndTime() {
        return actEndTime;
    }

    public void setActEndTime(Date actEndTime) {
        this.actEndTime = actEndTime;
    }

    public Integer getActPeriod() {
        return actPeriod;
    }

    public void setActPeriod(Integer actPeriod) {
        this.actPeriod = actPeriod;
    }

    public String getActReview() {
        return actReview;
    }

    public void setActReview(String actReview) {
        this.actReview = actReview == null ? null : actReview.trim();
    }

    public Long getActPaid() {
        return actPaid;
    }

    public void setActPaid(Long actPaid) {
        this.actPaid = actPaid;
    }

    public String getActCourseware() {
        return actCourseware;
    }

    public void setActCourseware(String actCourseware) {
        this.actCourseware = actCourseware == null ? null : actCourseware.trim();
    }

    public String getActSubject() {
        return actSubject;
    }

    public void setActSubject(String actSubject) {
        this.actSubject = actSubject == null ? null : actSubject.trim();
    }

    public String getHcpWxName() {
        return hcpWxName;
    }

    public void setHcpWxName(String hcpWxName) {
        this.hcpWxName = hcpWxName == null ? null : hcpWxName.trim();
    }

    public String getHcpWxNick() {
        return hcpWxNick;
    }

    public void setHcpWxNick(String hcpWxNick) {
        this.hcpWxNick = hcpWxNick == null ? null : hcpWxNick.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getFeedbackTime() {
        return feedbackTime;
    }

    public void setFeedbackTime(Date feedbackTime) {
        this.feedbackTime = feedbackTime;
    }

    public Date getActRealStart() {
        return actRealStart;
    }

    public void setActRealStart(Date actRealStart) {
        this.actRealStart = actRealStart;
    }

    public Date getActRealEnd() {
        return actRealEnd;
    }

    public void setActRealEnd(Date actRealEnd) {
        this.actRealEnd = actRealEnd;
    }

    public Byte getFeedbackPro() {
        return feedbackPro;
    }

    public void setFeedbackPro(Byte feedbackPro) {
        this.feedbackPro = feedbackPro;
    }

    public Byte getFeedbackFull() {
        return feedbackFull;
    }

    public void setFeedbackFull(Byte feedbackFull) {
        this.feedbackFull = feedbackFull;
    }

    public Byte getFeedbackClear() {
        return feedbackClear;
    }

    public void setFeedbackClear(Byte feedbackClear) {
        this.feedbackClear = feedbackClear;
    }

    public Byte getFeedbackInteraction() {
        return feedbackInteraction;
    }

    public void setFeedbackInteraction(Byte feedbackInteraction) {
        this.feedbackInteraction = feedbackInteraction;
    }

    public String getFeedbackOther() {
        return feedbackOther;
    }

    public void setFeedbackOther(String feedbackOther) {
        this.feedbackOther = feedbackOther == null ? null : feedbackOther.trim();
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId == null ? null : paymentId.trim();
    }

    public Long getFeedPaid() {
        return feedPaid;
    }

    public void setFeedPaid(Long feedPaid) {
        this.feedPaid = feedPaid;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getFeedRemark() {
        return feedRemark;
    }

    public void setFeedRemark(String feedRemark) {
        this.feedRemark = feedRemark == null ? null : feedRemark.trim();
    }

    public String getActFile() {
        return actFile;
    }

    public void setActFile(String actFile) {
        this.actFile = actFile == null ? null : actFile.trim();
    }

    public Integer getPayState() {
        return payState;
    }

    public void setPayState(Integer payState) {
        this.payState = payState;
    }

    public Integer getTpId() {
        return tpId;
    }

    public void setTpId(Integer tpId) {
        this.tpId = tpId;
    }

    public Date getPayToThirdTime() {
        return payToThirdTime;
    }

    public void setPayToThirdTime(Date payToThirdTime) {
        this.payToThirdTime = payToThirdTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public String getPayRemark() {
        return payRemark;
    }

    public void setPayRemark(String payRemark) {
        this.payRemark = payRemark == null ? null : payRemark.trim();
    }

    public Integer getPayToThird() {
        return payToThird;
    }

    public void setPayToThird(Integer payToThird) {
        this.payToThird = payToThird;
    }

    public String getPayFile() {
        return payFile;
    }

    public void setPayFile(String payFile) {
        this.payFile = payFile == null ? null : payFile.trim();
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId == null ? null : contractId.trim();
    }

    public String getFeedbackRemark() {
        return feedbackRemark;
    }

    public void setFeedbackRemark(String feedbackRemark) {
        this.feedbackRemark = feedbackRemark == null ? null : feedbackRemark.trim();
    }

    public Date getVerfiryTime() {
        return verfiryTime;
    }

    public void setVerfiryTime(Date verfiryTime) {
        this.verfiryTime = verfiryTime;
    }
}