package cc.gemii.hcp.po;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GemiiHcpDoctorExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GemiiHcpDoctorExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andHcpClassIsNull() {
            addCriterion("hcp_class is null");
            return (Criteria) this;
        }

        public Criteria andHcpClassIsNotNull() {
            addCriterion("hcp_class is not null");
            return (Criteria) this;
        }

        public Criteria andHcpClassEqualTo(String value) {
            addCriterion("hcp_class =", value, "hcpClass");
            return (Criteria) this;
        }

        public Criteria andHcpClassNotEqualTo(String value) {
            addCriterion("hcp_class <>", value, "hcpClass");
            return (Criteria) this;
        }

        public Criteria andHcpClassGreaterThan(String value) {
            addCriterion("hcp_class >", value, "hcpClass");
            return (Criteria) this;
        }

        public Criteria andHcpClassGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_class >=", value, "hcpClass");
            return (Criteria) this;
        }

        public Criteria andHcpClassLessThan(String value) {
            addCriterion("hcp_class <", value, "hcpClass");
            return (Criteria) this;
        }

        public Criteria andHcpClassLessThanOrEqualTo(String value) {
            addCriterion("hcp_class <=", value, "hcpClass");
            return (Criteria) this;
        }

        public Criteria andHcpClassLike(String value) {
            addCriterion("hcp_class like", value, "hcpClass");
            return (Criteria) this;
        }

        public Criteria andHcpClassNotLike(String value) {
            addCriterion("hcp_class not like", value, "hcpClass");
            return (Criteria) this;
        }

        public Criteria andHcpClassIn(List<String> values) {
            addCriterion("hcp_class in", values, "hcpClass");
            return (Criteria) this;
        }

        public Criteria andHcpClassNotIn(List<String> values) {
            addCriterion("hcp_class not in", values, "hcpClass");
            return (Criteria) this;
        }

        public Criteria andHcpClassBetween(String value1, String value2) {
            addCriterion("hcp_class between", value1, value2, "hcpClass");
            return (Criteria) this;
        }

        public Criteria andHcpClassNotBetween(String value1, String value2) {
            addCriterion("hcp_class not between", value1, value2, "hcpClass");
            return (Criteria) this;
        }

        public Criteria andHcpNameIsNull() {
            addCriterion("hcp_name is null");
            return (Criteria) this;
        }

        public Criteria andHcpNameIsNotNull() {
            addCriterion("hcp_name is not null");
            return (Criteria) this;
        }

        public Criteria andHcpNameEqualTo(String value) {
            addCriterion("hcp_name =", value, "hcpName");
            return (Criteria) this;
        }

        public Criteria andHcpNameNotEqualTo(String value) {
            addCriterion("hcp_name <>", value, "hcpName");
            return (Criteria) this;
        }

        public Criteria andHcpNameGreaterThan(String value) {
            addCriterion("hcp_name >", value, "hcpName");
            return (Criteria) this;
        }

        public Criteria andHcpNameGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_name >=", value, "hcpName");
            return (Criteria) this;
        }

        public Criteria andHcpNameLessThan(String value) {
            addCriterion("hcp_name <", value, "hcpName");
            return (Criteria) this;
        }

        public Criteria andHcpNameLessThanOrEqualTo(String value) {
            addCriterion("hcp_name <=", value, "hcpName");
            return (Criteria) this;
        }

        public Criteria andHcpNameLike(String value) {
            addCriterion("hcp_name like", value, "hcpName");
            return (Criteria) this;
        }

        public Criteria andHcpNameNotLike(String value) {
            addCriterion("hcp_name not like", value, "hcpName");
            return (Criteria) this;
        }

        public Criteria andHcpNameIn(List<String> values) {
            addCriterion("hcp_name in", values, "hcpName");
            return (Criteria) this;
        }

        public Criteria andHcpNameNotIn(List<String> values) {
            addCriterion("hcp_name not in", values, "hcpName");
            return (Criteria) this;
        }

        public Criteria andHcpNameBetween(String value1, String value2) {
            addCriterion("hcp_name between", value1, value2, "hcpName");
            return (Criteria) this;
        }

        public Criteria andHcpNameNotBetween(String value1, String value2) {
            addCriterion("hcp_name not between", value1, value2, "hcpName");
            return (Criteria) this;
        }

        public Criteria andHcpInfoIsNull() {
            addCriterion("hcp_info is null");
            return (Criteria) this;
        }

        public Criteria andHcpInfoIsNotNull() {
            addCriterion("hcp_info is not null");
            return (Criteria) this;
        }

        public Criteria andHcpInfoEqualTo(String value) {
            addCriterion("hcp_info =", value, "hcpInfo");
            return (Criteria) this;
        }

        public Criteria andHcpInfoNotEqualTo(String value) {
            addCriterion("hcp_info <>", value, "hcpInfo");
            return (Criteria) this;
        }

        public Criteria andHcpInfoGreaterThan(String value) {
            addCriterion("hcp_info >", value, "hcpInfo");
            return (Criteria) this;
        }

        public Criteria andHcpInfoGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_info >=", value, "hcpInfo");
            return (Criteria) this;
        }

        public Criteria andHcpInfoLessThan(String value) {
            addCriterion("hcp_info <", value, "hcpInfo");
            return (Criteria) this;
        }

        public Criteria andHcpInfoLessThanOrEqualTo(String value) {
            addCriterion("hcp_info <=", value, "hcpInfo");
            return (Criteria) this;
        }

        public Criteria andHcpInfoLike(String value) {
            addCriterion("hcp_info like", value, "hcpInfo");
            return (Criteria) this;
        }

        public Criteria andHcpInfoNotLike(String value) {
            addCriterion("hcp_info not like", value, "hcpInfo");
            return (Criteria) this;
        }

        public Criteria andHcpInfoIn(List<String> values) {
            addCriterion("hcp_info in", values, "hcpInfo");
            return (Criteria) this;
        }

        public Criteria andHcpInfoNotIn(List<String> values) {
            addCriterion("hcp_info not in", values, "hcpInfo");
            return (Criteria) this;
        }

        public Criteria andHcpInfoBetween(String value1, String value2) {
            addCriterion("hcp_info between", value1, value2, "hcpInfo");
            return (Criteria) this;
        }

        public Criteria andHcpInfoNotBetween(String value1, String value2) {
            addCriterion("hcp_info not between", value1, value2, "hcpInfo");
            return (Criteria) this;
        }

        public Criteria andHcpPhoneIsNull() {
            addCriterion("hcp_phone is null");
            return (Criteria) this;
        }

        public Criteria andHcpPhoneIsNotNull() {
            addCriterion("hcp_phone is not null");
            return (Criteria) this;
        }

        public Criteria andHcpPhoneEqualTo(String value) {
            addCriterion("hcp_phone =", value, "hcpPhone");
            return (Criteria) this;
        }

        public Criteria andHcpPhoneNotEqualTo(String value) {
            addCriterion("hcp_phone <>", value, "hcpPhone");
            return (Criteria) this;
        }

        public Criteria andHcpPhoneGreaterThan(String value) {
            addCriterion("hcp_phone >", value, "hcpPhone");
            return (Criteria) this;
        }

        public Criteria andHcpPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_phone >=", value, "hcpPhone");
            return (Criteria) this;
        }

        public Criteria andHcpPhoneLessThan(String value) {
            addCriterion("hcp_phone <", value, "hcpPhone");
            return (Criteria) this;
        }

        public Criteria andHcpPhoneLessThanOrEqualTo(String value) {
            addCriterion("hcp_phone <=", value, "hcpPhone");
            return (Criteria) this;
        }

        public Criteria andHcpPhoneLike(String value) {
            addCriterion("hcp_phone like", value, "hcpPhone");
            return (Criteria) this;
        }

        public Criteria andHcpPhoneNotLike(String value) {
            addCriterion("hcp_phone not like", value, "hcpPhone");
            return (Criteria) this;
        }

        public Criteria andHcpPhoneIn(List<String> values) {
            addCriterion("hcp_phone in", values, "hcpPhone");
            return (Criteria) this;
        }

        public Criteria andHcpPhoneNotIn(List<String> values) {
            addCriterion("hcp_phone not in", values, "hcpPhone");
            return (Criteria) this;
        }

        public Criteria andHcpPhoneBetween(String value1, String value2) {
            addCriterion("hcp_phone between", value1, value2, "hcpPhone");
            return (Criteria) this;
        }

        public Criteria andHcpPhoneNotBetween(String value1, String value2) {
            addCriterion("hcp_phone not between", value1, value2, "hcpPhone");
            return (Criteria) this;
        }

        public Criteria andHsptIdIsNull() {
            addCriterion("hspt_id is null");
            return (Criteria) this;
        }

        public Criteria andHsptIdIsNotNull() {
            addCriterion("hspt_id is not null");
            return (Criteria) this;
        }

        public Criteria andHsptIdEqualTo(String value) {
            addCriterion("hspt_id =", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdNotEqualTo(String value) {
            addCriterion("hspt_id <>", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdGreaterThan(String value) {
            addCriterion("hspt_id >", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdGreaterThanOrEqualTo(String value) {
            addCriterion("hspt_id >=", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdLessThan(String value) {
            addCriterion("hspt_id <", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdLessThanOrEqualTo(String value) {
            addCriterion("hspt_id <=", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdLike(String value) {
            addCriterion("hspt_id like", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdNotLike(String value) {
            addCriterion("hspt_id not like", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdIn(List<String> values) {
            addCriterion("hspt_id in", values, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdNotIn(List<String> values) {
            addCriterion("hspt_id not in", values, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdBetween(String value1, String value2) {
            addCriterion("hspt_id between", value1, value2, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdNotBetween(String value1, String value2) {
            addCriterion("hspt_id not between", value1, value2, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHcpDepartIsNull() {
            addCriterion("hcp_depart is null");
            return (Criteria) this;
        }

        public Criteria andHcpDepartIsNotNull() {
            addCriterion("hcp_depart is not null");
            return (Criteria) this;
        }

        public Criteria andHcpDepartEqualTo(String value) {
            addCriterion("hcp_depart =", value, "hcpDepart");
            return (Criteria) this;
        }

        public Criteria andHcpDepartNotEqualTo(String value) {
            addCriterion("hcp_depart <>", value, "hcpDepart");
            return (Criteria) this;
        }

        public Criteria andHcpDepartGreaterThan(String value) {
            addCriterion("hcp_depart >", value, "hcpDepart");
            return (Criteria) this;
        }

        public Criteria andHcpDepartGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_depart >=", value, "hcpDepart");
            return (Criteria) this;
        }

        public Criteria andHcpDepartLessThan(String value) {
            addCriterion("hcp_depart <", value, "hcpDepart");
            return (Criteria) this;
        }

        public Criteria andHcpDepartLessThanOrEqualTo(String value) {
            addCriterion("hcp_depart <=", value, "hcpDepart");
            return (Criteria) this;
        }

        public Criteria andHcpDepartLike(String value) {
            addCriterion("hcp_depart like", value, "hcpDepart");
            return (Criteria) this;
        }

        public Criteria andHcpDepartNotLike(String value) {
            addCriterion("hcp_depart not like", value, "hcpDepart");
            return (Criteria) this;
        }

        public Criteria andHcpDepartIn(List<String> values) {
            addCriterion("hcp_depart in", values, "hcpDepart");
            return (Criteria) this;
        }

        public Criteria andHcpDepartNotIn(List<String> values) {
            addCriterion("hcp_depart not in", values, "hcpDepart");
            return (Criteria) this;
        }

        public Criteria andHcpDepartBetween(String value1, String value2) {
            addCriterion("hcp_depart between", value1, value2, "hcpDepart");
            return (Criteria) this;
        }

        public Criteria andHcpDepartNotBetween(String value1, String value2) {
            addCriterion("hcp_depart not between", value1, value2, "hcpDepart");
            return (Criteria) this;
        }

        public Criteria andHcpPostIsNull() {
            addCriterion("hcp_post is null");
            return (Criteria) this;
        }

        public Criteria andHcpPostIsNotNull() {
            addCriterion("hcp_post is not null");
            return (Criteria) this;
        }

        public Criteria andHcpPostEqualTo(String value) {
            addCriterion("hcp_post =", value, "hcpPost");
            return (Criteria) this;
        }

        public Criteria andHcpPostNotEqualTo(String value) {
            addCriterion("hcp_post <>", value, "hcpPost");
            return (Criteria) this;
        }

        public Criteria andHcpPostGreaterThan(String value) {
            addCriterion("hcp_post >", value, "hcpPost");
            return (Criteria) this;
        }

        public Criteria andHcpPostGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_post >=", value, "hcpPost");
            return (Criteria) this;
        }

        public Criteria andHcpPostLessThan(String value) {
            addCriterion("hcp_post <", value, "hcpPost");
            return (Criteria) this;
        }

        public Criteria andHcpPostLessThanOrEqualTo(String value) {
            addCriterion("hcp_post <=", value, "hcpPost");
            return (Criteria) this;
        }

        public Criteria andHcpPostLike(String value) {
            addCriterion("hcp_post like", value, "hcpPost");
            return (Criteria) this;
        }

        public Criteria andHcpPostNotLike(String value) {
            addCriterion("hcp_post not like", value, "hcpPost");
            return (Criteria) this;
        }

        public Criteria andHcpPostIn(List<String> values) {
            addCriterion("hcp_post in", values, "hcpPost");
            return (Criteria) this;
        }

        public Criteria andHcpPostNotIn(List<String> values) {
            addCriterion("hcp_post not in", values, "hcpPost");
            return (Criteria) this;
        }

        public Criteria andHcpPostBetween(String value1, String value2) {
            addCriterion("hcp_post between", value1, value2, "hcpPost");
            return (Criteria) this;
        }

        public Criteria andHcpPostNotBetween(String value1, String value2) {
            addCriterion("hcp_post not between", value1, value2, "hcpPost");
            return (Criteria) this;
        }

        public Criteria andHcpTitleIsNull() {
            addCriterion("hcp_title is null");
            return (Criteria) this;
        }

        public Criteria andHcpTitleIsNotNull() {
            addCriterion("hcp_title is not null");
            return (Criteria) this;
        }

        public Criteria andHcpTitleEqualTo(String value) {
            addCriterion("hcp_title =", value, "hcpTitle");
            return (Criteria) this;
        }

        public Criteria andHcpTitleNotEqualTo(String value) {
            addCriterion("hcp_title <>", value, "hcpTitle");
            return (Criteria) this;
        }

        public Criteria andHcpTitleGreaterThan(String value) {
            addCriterion("hcp_title >", value, "hcpTitle");
            return (Criteria) this;
        }

        public Criteria andHcpTitleGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_title >=", value, "hcpTitle");
            return (Criteria) this;
        }

        public Criteria andHcpTitleLessThan(String value) {
            addCriterion("hcp_title <", value, "hcpTitle");
            return (Criteria) this;
        }

        public Criteria andHcpTitleLessThanOrEqualTo(String value) {
            addCriterion("hcp_title <=", value, "hcpTitle");
            return (Criteria) this;
        }

        public Criteria andHcpTitleLike(String value) {
            addCriterion("hcp_title like", value, "hcpTitle");
            return (Criteria) this;
        }

        public Criteria andHcpTitleNotLike(String value) {
            addCriterion("hcp_title not like", value, "hcpTitle");
            return (Criteria) this;
        }

        public Criteria andHcpTitleIn(List<String> values) {
            addCriterion("hcp_title in", values, "hcpTitle");
            return (Criteria) this;
        }

        public Criteria andHcpTitleNotIn(List<String> values) {
            addCriterion("hcp_title not in", values, "hcpTitle");
            return (Criteria) this;
        }

        public Criteria andHcpTitleBetween(String value1, String value2) {
            addCriterion("hcp_title between", value1, value2, "hcpTitle");
            return (Criteria) this;
        }

        public Criteria andHcpTitleNotBetween(String value1, String value2) {
            addCriterion("hcp_title not between", value1, value2, "hcpTitle");
            return (Criteria) this;
        }

        public Criteria andHcpTitleFileIsNull() {
            addCriterion("hcp_title_file is null");
            return (Criteria) this;
        }

        public Criteria andHcpTitleFileIsNotNull() {
            addCriterion("hcp_title_file is not null");
            return (Criteria) this;
        }

        public Criteria andHcpTitleFileEqualTo(String value) {
            addCriterion("hcp_title_file =", value, "hcpTitleFile");
            return (Criteria) this;
        }

        public Criteria andHcpTitleFileNotEqualTo(String value) {
            addCriterion("hcp_title_file <>", value, "hcpTitleFile");
            return (Criteria) this;
        }

        public Criteria andHcpTitleFileGreaterThan(String value) {
            addCriterion("hcp_title_file >", value, "hcpTitleFile");
            return (Criteria) this;
        }

        public Criteria andHcpTitleFileGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_title_file >=", value, "hcpTitleFile");
            return (Criteria) this;
        }

        public Criteria andHcpTitleFileLessThan(String value) {
            addCriterion("hcp_title_file <", value, "hcpTitleFile");
            return (Criteria) this;
        }

        public Criteria andHcpTitleFileLessThanOrEqualTo(String value) {
            addCriterion("hcp_title_file <=", value, "hcpTitleFile");
            return (Criteria) this;
        }

        public Criteria andHcpTitleFileLike(String value) {
            addCriterion("hcp_title_file like", value, "hcpTitleFile");
            return (Criteria) this;
        }

        public Criteria andHcpTitleFileNotLike(String value) {
            addCriterion("hcp_title_file not like", value, "hcpTitleFile");
            return (Criteria) this;
        }

        public Criteria andHcpTitleFileIn(List<String> values) {
            addCriterion("hcp_title_file in", values, "hcpTitleFile");
            return (Criteria) this;
        }

        public Criteria andHcpTitleFileNotIn(List<String> values) {
            addCriterion("hcp_title_file not in", values, "hcpTitleFile");
            return (Criteria) this;
        }

        public Criteria andHcpTitleFileBetween(String value1, String value2) {
            addCriterion("hcp_title_file between", value1, value2, "hcpTitleFile");
            return (Criteria) this;
        }

        public Criteria andHcpTitleFileNotBetween(String value1, String value2) {
            addCriterion("hcp_title_file not between", value1, value2, "hcpTitleFile");
            return (Criteria) this;
        }

        public Criteria andHcpYearsIsNull() {
            addCriterion("hcp_years is null");
            return (Criteria) this;
        }

        public Criteria andHcpYearsIsNotNull() {
            addCriterion("hcp_years is not null");
            return (Criteria) this;
        }

        public Criteria andHcpYearsEqualTo(Integer value) {
            addCriterion("hcp_years =", value, "hcpYears");
            return (Criteria) this;
        }

        public Criteria andHcpYearsNotEqualTo(Integer value) {
            addCriterion("hcp_years <>", value, "hcpYears");
            return (Criteria) this;
        }

        public Criteria andHcpYearsGreaterThan(Integer value) {
            addCriterion("hcp_years >", value, "hcpYears");
            return (Criteria) this;
        }

        public Criteria andHcpYearsGreaterThanOrEqualTo(Integer value) {
            addCriterion("hcp_years >=", value, "hcpYears");
            return (Criteria) this;
        }

        public Criteria andHcpYearsLessThan(Integer value) {
            addCriterion("hcp_years <", value, "hcpYears");
            return (Criteria) this;
        }

        public Criteria andHcpYearsLessThanOrEqualTo(Integer value) {
            addCriterion("hcp_years <=", value, "hcpYears");
            return (Criteria) this;
        }

        public Criteria andHcpYearsIn(List<Integer> values) {
            addCriterion("hcp_years in", values, "hcpYears");
            return (Criteria) this;
        }

        public Criteria andHcpYearsNotIn(List<Integer> values) {
            addCriterion("hcp_years not in", values, "hcpYears");
            return (Criteria) this;
        }

        public Criteria andHcpYearsBetween(Integer value1, Integer value2) {
            addCriterion("hcp_years between", value1, value2, "hcpYears");
            return (Criteria) this;
        }

        public Criteria andHcpYearsNotBetween(Integer value1, Integer value2) {
            addCriterion("hcp_years not between", value1, value2, "hcpYears");
            return (Criteria) this;
        }

        public Criteria andHcpSocialIsNull() {
            addCriterion("hcp_social is null");
            return (Criteria) this;
        }

        public Criteria andHcpSocialIsNotNull() {
            addCriterion("hcp_social is not null");
            return (Criteria) this;
        }

        public Criteria andHcpSocialEqualTo(String value) {
            addCriterion("hcp_social =", value, "hcpSocial");
            return (Criteria) this;
        }

        public Criteria andHcpSocialNotEqualTo(String value) {
            addCriterion("hcp_social <>", value, "hcpSocial");
            return (Criteria) this;
        }

        public Criteria andHcpSocialGreaterThan(String value) {
            addCriterion("hcp_social >", value, "hcpSocial");
            return (Criteria) this;
        }

        public Criteria andHcpSocialGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_social >=", value, "hcpSocial");
            return (Criteria) this;
        }

        public Criteria andHcpSocialLessThan(String value) {
            addCriterion("hcp_social <", value, "hcpSocial");
            return (Criteria) this;
        }

        public Criteria andHcpSocialLessThanOrEqualTo(String value) {
            addCriterion("hcp_social <=", value, "hcpSocial");
            return (Criteria) this;
        }

        public Criteria andHcpSocialLike(String value) {
            addCriterion("hcp_social like", value, "hcpSocial");
            return (Criteria) this;
        }

        public Criteria andHcpSocialNotLike(String value) {
            addCriterion("hcp_social not like", value, "hcpSocial");
            return (Criteria) this;
        }

        public Criteria andHcpSocialIn(List<String> values) {
            addCriterion("hcp_social in", values, "hcpSocial");
            return (Criteria) this;
        }

        public Criteria andHcpSocialNotIn(List<String> values) {
            addCriterion("hcp_social not in", values, "hcpSocial");
            return (Criteria) this;
        }

        public Criteria andHcpSocialBetween(String value1, String value2) {
            addCriterion("hcp_social between", value1, value2, "hcpSocial");
            return (Criteria) this;
        }

        public Criteria andHcpSocialNotBetween(String value1, String value2) {
            addCriterion("hcp_social not between", value1, value2, "hcpSocial");
            return (Criteria) this;
        }

        public Criteria andHcpSocialDespIsNull() {
            addCriterion("hcp_social_desp is null");
            return (Criteria) this;
        }

        public Criteria andHcpSocialDespIsNotNull() {
            addCriterion("hcp_social_desp is not null");
            return (Criteria) this;
        }

        public Criteria andHcpSocialDespEqualTo(String value) {
            addCriterion("hcp_social_desp =", value, "hcpSocialDesp");
            return (Criteria) this;
        }

        public Criteria andHcpSocialDespNotEqualTo(String value) {
            addCriterion("hcp_social_desp <>", value, "hcpSocialDesp");
            return (Criteria) this;
        }

        public Criteria andHcpSocialDespGreaterThan(String value) {
            addCriterion("hcp_social_desp >", value, "hcpSocialDesp");
            return (Criteria) this;
        }

        public Criteria andHcpSocialDespGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_social_desp >=", value, "hcpSocialDesp");
            return (Criteria) this;
        }

        public Criteria andHcpSocialDespLessThan(String value) {
            addCriterion("hcp_social_desp <", value, "hcpSocialDesp");
            return (Criteria) this;
        }

        public Criteria andHcpSocialDespLessThanOrEqualTo(String value) {
            addCriterion("hcp_social_desp <=", value, "hcpSocialDesp");
            return (Criteria) this;
        }

        public Criteria andHcpSocialDespLike(String value) {
            addCriterion("hcp_social_desp like", value, "hcpSocialDesp");
            return (Criteria) this;
        }

        public Criteria andHcpSocialDespNotLike(String value) {
            addCriterion("hcp_social_desp not like", value, "hcpSocialDesp");
            return (Criteria) this;
        }

        public Criteria andHcpSocialDespIn(List<String> values) {
            addCriterion("hcp_social_desp in", values, "hcpSocialDesp");
            return (Criteria) this;
        }

        public Criteria andHcpSocialDespNotIn(List<String> values) {
            addCriterion("hcp_social_desp not in", values, "hcpSocialDesp");
            return (Criteria) this;
        }

        public Criteria andHcpSocialDespBetween(String value1, String value2) {
            addCriterion("hcp_social_desp between", value1, value2, "hcpSocialDesp");
            return (Criteria) this;
        }

        public Criteria andHcpSocialDespNotBetween(String value1, String value2) {
            addCriterion("hcp_social_desp not between", value1, value2, "hcpSocialDesp");
            return (Criteria) this;
        }

        public Criteria andHcpPreferIsNull() {
            addCriterion("hcp_prefer is null");
            return (Criteria) this;
        }

        public Criteria andHcpPreferIsNotNull() {
            addCriterion("hcp_prefer is not null");
            return (Criteria) this;
        }

        public Criteria andHcpPreferEqualTo(String value) {
            addCriterion("hcp_prefer =", value, "hcpPrefer");
            return (Criteria) this;
        }

        public Criteria andHcpPreferNotEqualTo(String value) {
            addCriterion("hcp_prefer <>", value, "hcpPrefer");
            return (Criteria) this;
        }

        public Criteria andHcpPreferGreaterThan(String value) {
            addCriterion("hcp_prefer >", value, "hcpPrefer");
            return (Criteria) this;
        }

        public Criteria andHcpPreferGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_prefer >=", value, "hcpPrefer");
            return (Criteria) this;
        }

        public Criteria andHcpPreferLessThan(String value) {
            addCriterion("hcp_prefer <", value, "hcpPrefer");
            return (Criteria) this;
        }

        public Criteria andHcpPreferLessThanOrEqualTo(String value) {
            addCriterion("hcp_prefer <=", value, "hcpPrefer");
            return (Criteria) this;
        }

        public Criteria andHcpPreferLike(String value) {
            addCriterion("hcp_prefer like", value, "hcpPrefer");
            return (Criteria) this;
        }

        public Criteria andHcpPreferNotLike(String value) {
            addCriterion("hcp_prefer not like", value, "hcpPrefer");
            return (Criteria) this;
        }

        public Criteria andHcpPreferIn(List<String> values) {
            addCriterion("hcp_prefer in", values, "hcpPrefer");
            return (Criteria) this;
        }

        public Criteria andHcpPreferNotIn(List<String> values) {
            addCriterion("hcp_prefer not in", values, "hcpPrefer");
            return (Criteria) this;
        }

        public Criteria andHcpPreferBetween(String value1, String value2) {
            addCriterion("hcp_prefer between", value1, value2, "hcpPrefer");
            return (Criteria) this;
        }

        public Criteria andHcpPreferNotBetween(String value1, String value2) {
            addCriterion("hcp_prefer not between", value1, value2, "hcpPrefer");
            return (Criteria) this;
        }

        public Criteria andHcpActivityIsNull() {
            addCriterion("hcp_activity is null");
            return (Criteria) this;
        }

        public Criteria andHcpActivityIsNotNull() {
            addCriterion("hcp_activity is not null");
            return (Criteria) this;
        }

        public Criteria andHcpActivityEqualTo(String value) {
            addCriterion("hcp_activity =", value, "hcpActivity");
            return (Criteria) this;
        }

        public Criteria andHcpActivityNotEqualTo(String value) {
            addCriterion("hcp_activity <>", value, "hcpActivity");
            return (Criteria) this;
        }

        public Criteria andHcpActivityGreaterThan(String value) {
            addCriterion("hcp_activity >", value, "hcpActivity");
            return (Criteria) this;
        }

        public Criteria andHcpActivityGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_activity >=", value, "hcpActivity");
            return (Criteria) this;
        }

        public Criteria andHcpActivityLessThan(String value) {
            addCriterion("hcp_activity <", value, "hcpActivity");
            return (Criteria) this;
        }

        public Criteria andHcpActivityLessThanOrEqualTo(String value) {
            addCriterion("hcp_activity <=", value, "hcpActivity");
            return (Criteria) this;
        }

        public Criteria andHcpActivityLike(String value) {
            addCriterion("hcp_activity like", value, "hcpActivity");
            return (Criteria) this;
        }

        public Criteria andHcpActivityNotLike(String value) {
            addCriterion("hcp_activity not like", value, "hcpActivity");
            return (Criteria) this;
        }

        public Criteria andHcpActivityIn(List<String> values) {
            addCriterion("hcp_activity in", values, "hcpActivity");
            return (Criteria) this;
        }

        public Criteria andHcpActivityNotIn(List<String> values) {
            addCriterion("hcp_activity not in", values, "hcpActivity");
            return (Criteria) this;
        }

        public Criteria andHcpActivityBetween(String value1, String value2) {
            addCriterion("hcp_activity between", value1, value2, "hcpActivity");
            return (Criteria) this;
        }

        public Criteria andHcpActivityNotBetween(String value1, String value2) {
            addCriterion("hcp_activity not between", value1, value2, "hcpActivity");
            return (Criteria) this;
        }

        public Criteria andHcpMomTeacherIsNull() {
            addCriterion("hcp_mom_teacher is null");
            return (Criteria) this;
        }

        public Criteria andHcpMomTeacherIsNotNull() {
            addCriterion("hcp_mom_teacher is not null");
            return (Criteria) this;
        }

        public Criteria andHcpMomTeacherEqualTo(Integer value) {
            addCriterion("hcp_mom_teacher =", value, "hcpMomTeacher");
            return (Criteria) this;
        }

        public Criteria andHcpMomTeacherNotEqualTo(Integer value) {
            addCriterion("hcp_mom_teacher <>", value, "hcpMomTeacher");
            return (Criteria) this;
        }

        public Criteria andHcpMomTeacherGreaterThan(Integer value) {
            addCriterion("hcp_mom_teacher >", value, "hcpMomTeacher");
            return (Criteria) this;
        }

        public Criteria andHcpMomTeacherGreaterThanOrEqualTo(Integer value) {
            addCriterion("hcp_mom_teacher >=", value, "hcpMomTeacher");
            return (Criteria) this;
        }

        public Criteria andHcpMomTeacherLessThan(Integer value) {
            addCriterion("hcp_mom_teacher <", value, "hcpMomTeacher");
            return (Criteria) this;
        }

        public Criteria andHcpMomTeacherLessThanOrEqualTo(Integer value) {
            addCriterion("hcp_mom_teacher <=", value, "hcpMomTeacher");
            return (Criteria) this;
        }

        public Criteria andHcpMomTeacherIn(List<Integer> values) {
            addCriterion("hcp_mom_teacher in", values, "hcpMomTeacher");
            return (Criteria) this;
        }

        public Criteria andHcpMomTeacherNotIn(List<Integer> values) {
            addCriterion("hcp_mom_teacher not in", values, "hcpMomTeacher");
            return (Criteria) this;
        }

        public Criteria andHcpMomTeacherBetween(Integer value1, Integer value2) {
            addCriterion("hcp_mom_teacher between", value1, value2, "hcpMomTeacher");
            return (Criteria) this;
        }

        public Criteria andHcpMomTeacherNotBetween(Integer value1, Integer value2) {
            addCriterion("hcp_mom_teacher not between", value1, value2, "hcpMomTeacher");
            return (Criteria) this;
        }

        public Criteria andHcpMomConsultsantIsNull() {
            addCriterion("hcp_mom_consultsant is null");
            return (Criteria) this;
        }

        public Criteria andHcpMomConsultsantIsNotNull() {
            addCriterion("hcp_mom_consultsant is not null");
            return (Criteria) this;
        }

        public Criteria andHcpMomConsultsantEqualTo(Integer value) {
            addCriterion("hcp_mom_consultsant =", value, "hcpMomConsultsant");
            return (Criteria) this;
        }

        public Criteria andHcpMomConsultsantNotEqualTo(Integer value) {
            addCriterion("hcp_mom_consultsant <>", value, "hcpMomConsultsant");
            return (Criteria) this;
        }

        public Criteria andHcpMomConsultsantGreaterThan(Integer value) {
            addCriterion("hcp_mom_consultsant >", value, "hcpMomConsultsant");
            return (Criteria) this;
        }

        public Criteria andHcpMomConsultsantGreaterThanOrEqualTo(Integer value) {
            addCriterion("hcp_mom_consultsant >=", value, "hcpMomConsultsant");
            return (Criteria) this;
        }

        public Criteria andHcpMomConsultsantLessThan(Integer value) {
            addCriterion("hcp_mom_consultsant <", value, "hcpMomConsultsant");
            return (Criteria) this;
        }

        public Criteria andHcpMomConsultsantLessThanOrEqualTo(Integer value) {
            addCriterion("hcp_mom_consultsant <=", value, "hcpMomConsultsant");
            return (Criteria) this;
        }

        public Criteria andHcpMomConsultsantIn(List<Integer> values) {
            addCriterion("hcp_mom_consultsant in", values, "hcpMomConsultsant");
            return (Criteria) this;
        }

        public Criteria andHcpMomConsultsantNotIn(List<Integer> values) {
            addCriterion("hcp_mom_consultsant not in", values, "hcpMomConsultsant");
            return (Criteria) this;
        }

        public Criteria andHcpMomConsultsantBetween(Integer value1, Integer value2) {
            addCriterion("hcp_mom_consultsant between", value1, value2, "hcpMomConsultsant");
            return (Criteria) this;
        }

        public Criteria andHcpMomConsultsantNotBetween(Integer value1, Integer value2) {
            addCriterion("hcp_mom_consultsant not between", value1, value2, "hcpMomConsultsant");
            return (Criteria) this;
        }

        public Criteria andHcpCtimeIsNull() {
            addCriterion("hcp_ctime is null");
            return (Criteria) this;
        }

        public Criteria andHcpCtimeIsNotNull() {
            addCriterion("hcp_ctime is not null");
            return (Criteria) this;
        }

        public Criteria andHcpCtimeEqualTo(Date value) {
            addCriterion("hcp_ctime =", value, "hcpCtime");
            return (Criteria) this;
        }

        public Criteria andHcpCtimeNotEqualTo(Date value) {
            addCriterion("hcp_ctime <>", value, "hcpCtime");
            return (Criteria) this;
        }

        public Criteria andHcpCtimeGreaterThan(Date value) {
            addCriterion("hcp_ctime >", value, "hcpCtime");
            return (Criteria) this;
        }

        public Criteria andHcpCtimeGreaterThanOrEqualTo(Date value) {
            addCriterion("hcp_ctime >=", value, "hcpCtime");
            return (Criteria) this;
        }

        public Criteria andHcpCtimeLessThan(Date value) {
            addCriterion("hcp_ctime <", value, "hcpCtime");
            return (Criteria) this;
        }

        public Criteria andHcpCtimeLessThanOrEqualTo(Date value) {
            addCriterion("hcp_ctime <=", value, "hcpCtime");
            return (Criteria) this;
        }

        public Criteria andHcpCtimeIn(List<Date> values) {
            addCriterion("hcp_ctime in", values, "hcpCtime");
            return (Criteria) this;
        }

        public Criteria andHcpCtimeNotIn(List<Date> values) {
            addCriterion("hcp_ctime not in", values, "hcpCtime");
            return (Criteria) this;
        }

        public Criteria andHcpCtimeBetween(Date value1, Date value2) {
            addCriterion("hcp_ctime between", value1, value2, "hcpCtime");
            return (Criteria) this;
        }

        public Criteria andHcpCtimeNotBetween(Date value1, Date value2) {
            addCriterion("hcp_ctime not between", value1, value2, "hcpCtime");
            return (Criteria) this;
        }

        public Criteria andHcpProfileFullIsNull() {
            addCriterion("hcp_profile_full is null");
            return (Criteria) this;
        }

        public Criteria andHcpProfileFullIsNotNull() {
            addCriterion("hcp_profile_full is not null");
            return (Criteria) this;
        }

        public Criteria andHcpProfileFullEqualTo(Integer value) {
            addCriterion("hcp_profile_full =", value, "hcpProfileFull");
            return (Criteria) this;
        }

        public Criteria andHcpProfileFullNotEqualTo(Integer value) {
            addCriterion("hcp_profile_full <>", value, "hcpProfileFull");
            return (Criteria) this;
        }

        public Criteria andHcpProfileFullGreaterThan(Integer value) {
            addCriterion("hcp_profile_full >", value, "hcpProfileFull");
            return (Criteria) this;
        }

        public Criteria andHcpProfileFullGreaterThanOrEqualTo(Integer value) {
            addCriterion("hcp_profile_full >=", value, "hcpProfileFull");
            return (Criteria) this;
        }

        public Criteria andHcpProfileFullLessThan(Integer value) {
            addCriterion("hcp_profile_full <", value, "hcpProfileFull");
            return (Criteria) this;
        }

        public Criteria andHcpProfileFullLessThanOrEqualTo(Integer value) {
            addCriterion("hcp_profile_full <=", value, "hcpProfileFull");
            return (Criteria) this;
        }

        public Criteria andHcpProfileFullIn(List<Integer> values) {
            addCriterion("hcp_profile_full in", values, "hcpProfileFull");
            return (Criteria) this;
        }

        public Criteria andHcpProfileFullNotIn(List<Integer> values) {
            addCriterion("hcp_profile_full not in", values, "hcpProfileFull");
            return (Criteria) this;
        }

        public Criteria andHcpProfileFullBetween(Integer value1, Integer value2) {
            addCriterion("hcp_profile_full between", value1, value2, "hcpProfileFull");
            return (Criteria) this;
        }

        public Criteria andHcpProfileFullNotBetween(Integer value1, Integer value2) {
            addCriterion("hcp_profile_full not between", value1, value2, "hcpProfileFull");
            return (Criteria) this;
        }

        public Criteria andHcpAdditionIsNull() {
            addCriterion("hcp_addition is null");
            return (Criteria) this;
        }

        public Criteria andHcpAdditionIsNotNull() {
            addCriterion("hcp_addition is not null");
            return (Criteria) this;
        }

        public Criteria andHcpAdditionEqualTo(String value) {
            addCriterion("hcp_addition =", value, "hcpAddition");
            return (Criteria) this;
        }

        public Criteria andHcpAdditionNotEqualTo(String value) {
            addCriterion("hcp_addition <>", value, "hcpAddition");
            return (Criteria) this;
        }

        public Criteria andHcpAdditionGreaterThan(String value) {
            addCriterion("hcp_addition >", value, "hcpAddition");
            return (Criteria) this;
        }

        public Criteria andHcpAdditionGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_addition >=", value, "hcpAddition");
            return (Criteria) this;
        }

        public Criteria andHcpAdditionLessThan(String value) {
            addCriterion("hcp_addition <", value, "hcpAddition");
            return (Criteria) this;
        }

        public Criteria andHcpAdditionLessThanOrEqualTo(String value) {
            addCriterion("hcp_addition <=", value, "hcpAddition");
            return (Criteria) this;
        }

        public Criteria andHcpAdditionLike(String value) {
            addCriterion("hcp_addition like", value, "hcpAddition");
            return (Criteria) this;
        }

        public Criteria andHcpAdditionNotLike(String value) {
            addCriterion("hcp_addition not like", value, "hcpAddition");
            return (Criteria) this;
        }

        public Criteria andHcpAdditionIn(List<String> values) {
            addCriterion("hcp_addition in", values, "hcpAddition");
            return (Criteria) this;
        }

        public Criteria andHcpAdditionNotIn(List<String> values) {
            addCriterion("hcp_addition not in", values, "hcpAddition");
            return (Criteria) this;
        }

        public Criteria andHcpAdditionBetween(String value1, String value2) {
            addCriterion("hcp_addition between", value1, value2, "hcpAddition");
            return (Criteria) this;
        }

        public Criteria andHcpAdditionNotBetween(String value1, String value2) {
            addCriterion("hcp_addition not between", value1, value2, "hcpAddition");
            return (Criteria) this;
        }

        public Criteria andContractIdIsNull() {
            addCriterion("contract_id is null");
            return (Criteria) this;
        }

        public Criteria andContractIdIsNotNull() {
            addCriterion("contract_id is not null");
            return (Criteria) this;
        }

        public Criteria andContractIdEqualTo(String value) {
            addCriterion("contract_id =", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdNotEqualTo(String value) {
            addCriterion("contract_id <>", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdGreaterThan(String value) {
            addCriterion("contract_id >", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdGreaterThanOrEqualTo(String value) {
            addCriterion("contract_id >=", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdLessThan(String value) {
            addCriterion("contract_id <", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdLessThanOrEqualTo(String value) {
            addCriterion("contract_id <=", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdLike(String value) {
            addCriterion("contract_id like", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdNotLike(String value) {
            addCriterion("contract_id not like", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdIn(List<String> values) {
            addCriterion("contract_id in", values, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdNotIn(List<String> values) {
            addCriterion("contract_id not in", values, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdBetween(String value1, String value2) {
            addCriterion("contract_id between", value1, value2, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdNotBetween(String value1, String value2) {
            addCriterion("contract_id not between", value1, value2, "contractId");
            return (Criteria) this;
        }

        public Criteria andOaNameIsNull() {
            addCriterion("oa_name is null");
            return (Criteria) this;
        }

        public Criteria andOaNameIsNotNull() {
            addCriterion("oa_name is not null");
            return (Criteria) this;
        }

        public Criteria andOaNameEqualTo(String value) {
            addCriterion("oa_name =", value, "oaName");
            return (Criteria) this;
        }

        public Criteria andOaNameNotEqualTo(String value) {
            addCriterion("oa_name <>", value, "oaName");
            return (Criteria) this;
        }

        public Criteria andOaNameGreaterThan(String value) {
            addCriterion("oa_name >", value, "oaName");
            return (Criteria) this;
        }

        public Criteria andOaNameGreaterThanOrEqualTo(String value) {
            addCriterion("oa_name >=", value, "oaName");
            return (Criteria) this;
        }

        public Criteria andOaNameLessThan(String value) {
            addCriterion("oa_name <", value, "oaName");
            return (Criteria) this;
        }

        public Criteria andOaNameLessThanOrEqualTo(String value) {
            addCriterion("oa_name <=", value, "oaName");
            return (Criteria) this;
        }

        public Criteria andOaNameLike(String value) {
            addCriterion("oa_name like", value, "oaName");
            return (Criteria) this;
        }

        public Criteria andOaNameNotLike(String value) {
            addCriterion("oa_name not like", value, "oaName");
            return (Criteria) this;
        }

        public Criteria andOaNameIn(List<String> values) {
            addCriterion("oa_name in", values, "oaName");
            return (Criteria) this;
        }

        public Criteria andOaNameNotIn(List<String> values) {
            addCriterion("oa_name not in", values, "oaName");
            return (Criteria) this;
        }

        public Criteria andOaNameBetween(String value1, String value2) {
            addCriterion("oa_name between", value1, value2, "oaName");
            return (Criteria) this;
        }

        public Criteria andOaNameNotBetween(String value1, String value2) {
            addCriterion("oa_name not between", value1, value2, "oaName");
            return (Criteria) this;
        }

        public Criteria andOaPhoneIsNull() {
            addCriterion("oa_phone is null");
            return (Criteria) this;
        }

        public Criteria andOaPhoneIsNotNull() {
            addCriterion("oa_phone is not null");
            return (Criteria) this;
        }

        public Criteria andOaPhoneEqualTo(String value) {
            addCriterion("oa_phone =", value, "oaPhone");
            return (Criteria) this;
        }

        public Criteria andOaPhoneNotEqualTo(String value) {
            addCriterion("oa_phone <>", value, "oaPhone");
            return (Criteria) this;
        }

        public Criteria andOaPhoneGreaterThan(String value) {
            addCriterion("oa_phone >", value, "oaPhone");
            return (Criteria) this;
        }

        public Criteria andOaPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("oa_phone >=", value, "oaPhone");
            return (Criteria) this;
        }

        public Criteria andOaPhoneLessThan(String value) {
            addCriterion("oa_phone <", value, "oaPhone");
            return (Criteria) this;
        }

        public Criteria andOaPhoneLessThanOrEqualTo(String value) {
            addCriterion("oa_phone <=", value, "oaPhone");
            return (Criteria) this;
        }

        public Criteria andOaPhoneLike(String value) {
            addCriterion("oa_phone like", value, "oaPhone");
            return (Criteria) this;
        }

        public Criteria andOaPhoneNotLike(String value) {
            addCriterion("oa_phone not like", value, "oaPhone");
            return (Criteria) this;
        }

        public Criteria andOaPhoneIn(List<String> values) {
            addCriterion("oa_phone in", values, "oaPhone");
            return (Criteria) this;
        }

        public Criteria andOaPhoneNotIn(List<String> values) {
            addCriterion("oa_phone not in", values, "oaPhone");
            return (Criteria) this;
        }

        public Criteria andOaPhoneBetween(String value1, String value2) {
            addCriterion("oa_phone between", value1, value2, "oaPhone");
            return (Criteria) this;
        }

        public Criteria andOaPhoneNotBetween(String value1, String value2) {
            addCriterion("oa_phone not between", value1, value2, "oaPhone");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andBankIsNull() {
            addCriterion("bank is null");
            return (Criteria) this;
        }

        public Criteria andBankIsNotNull() {
            addCriterion("bank is not null");
            return (Criteria) this;
        }

        public Criteria andBankEqualTo(String value) {
            addCriterion("bank =", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotEqualTo(String value) {
            addCriterion("bank <>", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankGreaterThan(String value) {
            addCriterion("bank >", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankGreaterThanOrEqualTo(String value) {
            addCriterion("bank >=", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankLessThan(String value) {
            addCriterion("bank <", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankLessThanOrEqualTo(String value) {
            addCriterion("bank <=", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankLike(String value) {
            addCriterion("bank like", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotLike(String value) {
            addCriterion("bank not like", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankIn(List<String> values) {
            addCriterion("bank in", values, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotIn(List<String> values) {
            addCriterion("bank not in", values, "bank");
            return (Criteria) this;
        }

        public Criteria andBankBetween(String value1, String value2) {
            addCriterion("bank between", value1, value2, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotBetween(String value1, String value2) {
            addCriterion("bank not between", value1, value2, "bank");
            return (Criteria) this;
        }

        public Criteria andBankCardNumberIsNull() {
            addCriterion("bank_card_number is null");
            return (Criteria) this;
        }

        public Criteria andBankCardNumberIsNotNull() {
            addCriterion("bank_card_number is not null");
            return (Criteria) this;
        }

        public Criteria andBankCardNumberEqualTo(String value) {
            addCriterion("bank_card_number =", value, "bankCardNumber");
            return (Criteria) this;
        }

        public Criteria andBankCardNumberNotEqualTo(String value) {
            addCriterion("bank_card_number <>", value, "bankCardNumber");
            return (Criteria) this;
        }

        public Criteria andBankCardNumberGreaterThan(String value) {
            addCriterion("bank_card_number >", value, "bankCardNumber");
            return (Criteria) this;
        }

        public Criteria andBankCardNumberGreaterThanOrEqualTo(String value) {
            addCriterion("bank_card_number >=", value, "bankCardNumber");
            return (Criteria) this;
        }

        public Criteria andBankCardNumberLessThan(String value) {
            addCriterion("bank_card_number <", value, "bankCardNumber");
            return (Criteria) this;
        }

        public Criteria andBankCardNumberLessThanOrEqualTo(String value) {
            addCriterion("bank_card_number <=", value, "bankCardNumber");
            return (Criteria) this;
        }

        public Criteria andBankCardNumberLike(String value) {
            addCriterion("bank_card_number like", value, "bankCardNumber");
            return (Criteria) this;
        }

        public Criteria andBankCardNumberNotLike(String value) {
            addCriterion("bank_card_number not like", value, "bankCardNumber");
            return (Criteria) this;
        }

        public Criteria andBankCardNumberIn(List<String> values) {
            addCriterion("bank_card_number in", values, "bankCardNumber");
            return (Criteria) this;
        }

        public Criteria andBankCardNumberNotIn(List<String> values) {
            addCriterion("bank_card_number not in", values, "bankCardNumber");
            return (Criteria) this;
        }

        public Criteria andBankCardNumberBetween(String value1, String value2) {
            addCriterion("bank_card_number between", value1, value2, "bankCardNumber");
            return (Criteria) this;
        }

        public Criteria andBankCardNumberNotBetween(String value1, String value2) {
            addCriterion("bank_card_number not between", value1, value2, "bankCardNumber");
            return (Criteria) this;
        }

        public Criteria andBankCardNameIsNull() {
            addCriterion("bank_card_name is null");
            return (Criteria) this;
        }

        public Criteria andBankCardNameIsNotNull() {
            addCriterion("bank_card_name is not null");
            return (Criteria) this;
        }

        public Criteria andBankCardNameEqualTo(String value) {
            addCriterion("bank_card_name =", value, "bankCardName");
            return (Criteria) this;
        }

        public Criteria andBankCardNameNotEqualTo(String value) {
            addCriterion("bank_card_name <>", value, "bankCardName");
            return (Criteria) this;
        }

        public Criteria andBankCardNameGreaterThan(String value) {
            addCriterion("bank_card_name >", value, "bankCardName");
            return (Criteria) this;
        }

        public Criteria andBankCardNameGreaterThanOrEqualTo(String value) {
            addCriterion("bank_card_name >=", value, "bankCardName");
            return (Criteria) this;
        }

        public Criteria andBankCardNameLessThan(String value) {
            addCriterion("bank_card_name <", value, "bankCardName");
            return (Criteria) this;
        }

        public Criteria andBankCardNameLessThanOrEqualTo(String value) {
            addCriterion("bank_card_name <=", value, "bankCardName");
            return (Criteria) this;
        }

        public Criteria andBankCardNameLike(String value) {
            addCriterion("bank_card_name like", value, "bankCardName");
            return (Criteria) this;
        }

        public Criteria andBankCardNameNotLike(String value) {
            addCriterion("bank_card_name not like", value, "bankCardName");
            return (Criteria) this;
        }

        public Criteria andBankCardNameIn(List<String> values) {
            addCriterion("bank_card_name in", values, "bankCardName");
            return (Criteria) this;
        }

        public Criteria andBankCardNameNotIn(List<String> values) {
            addCriterion("bank_card_name not in", values, "bankCardName");
            return (Criteria) this;
        }

        public Criteria andBankCardNameBetween(String value1, String value2) {
            addCriterion("bank_card_name between", value1, value2, "bankCardName");
            return (Criteria) this;
        }

        public Criteria andBankCardNameNotBetween(String value1, String value2) {
            addCriterion("bank_card_name not between", value1, value2, "bankCardName");
            return (Criteria) this;
        }

        public Criteria andStaffIdIsNull() {
            addCriterion("staff_id is null");
            return (Criteria) this;
        }

        public Criteria andStaffIdIsNotNull() {
            addCriterion("staff_id is not null");
            return (Criteria) this;
        }

        public Criteria andStaffIdEqualTo(Integer value) {
            addCriterion("staff_id =", value, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdNotEqualTo(Integer value) {
            addCriterion("staff_id <>", value, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdGreaterThan(Integer value) {
            addCriterion("staff_id >", value, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("staff_id >=", value, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdLessThan(Integer value) {
            addCriterion("staff_id <", value, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdLessThanOrEqualTo(Integer value) {
            addCriterion("staff_id <=", value, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdIn(List<Integer> values) {
            addCriterion("staff_id in", values, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdNotIn(List<Integer> values) {
            addCriterion("staff_id not in", values, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdBetween(Integer value1, Integer value2) {
            addCriterion("staff_id between", value1, value2, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdNotBetween(Integer value1, Integer value2) {
            addCriterion("staff_id not between", value1, value2, "staffId");
            return (Criteria) this;
        }

        public Criteria andVerifyRemarkIsNull() {
            addCriterion("verify_remark is null");
            return (Criteria) this;
        }

        public Criteria andVerifyRemarkIsNotNull() {
            addCriterion("verify_remark is not null");
            return (Criteria) this;
        }

        public Criteria andVerifyRemarkEqualTo(String value) {
            addCriterion("verify_remark =", value, "verifyRemark");
            return (Criteria) this;
        }

        public Criteria andVerifyRemarkNotEqualTo(String value) {
            addCriterion("verify_remark <>", value, "verifyRemark");
            return (Criteria) this;
        }

        public Criteria andVerifyRemarkGreaterThan(String value) {
            addCriterion("verify_remark >", value, "verifyRemark");
            return (Criteria) this;
        }

        public Criteria andVerifyRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("verify_remark >=", value, "verifyRemark");
            return (Criteria) this;
        }

        public Criteria andVerifyRemarkLessThan(String value) {
            addCriterion("verify_remark <", value, "verifyRemark");
            return (Criteria) this;
        }

        public Criteria andVerifyRemarkLessThanOrEqualTo(String value) {
            addCriterion("verify_remark <=", value, "verifyRemark");
            return (Criteria) this;
        }

        public Criteria andVerifyRemarkLike(String value) {
            addCriterion("verify_remark like", value, "verifyRemark");
            return (Criteria) this;
        }

        public Criteria andVerifyRemarkNotLike(String value) {
            addCriterion("verify_remark not like", value, "verifyRemark");
            return (Criteria) this;
        }

        public Criteria andVerifyRemarkIn(List<String> values) {
            addCriterion("verify_remark in", values, "verifyRemark");
            return (Criteria) this;
        }

        public Criteria andVerifyRemarkNotIn(List<String> values) {
            addCriterion("verify_remark not in", values, "verifyRemark");
            return (Criteria) this;
        }

        public Criteria andVerifyRemarkBetween(String value1, String value2) {
            addCriterion("verify_remark between", value1, value2, "verifyRemark");
            return (Criteria) this;
        }

        public Criteria andVerifyRemarkNotBetween(String value1, String value2) {
            addCriterion("verify_remark not between", value1, value2, "verifyRemark");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeIsNull() {
            addCriterion("verify_time is null");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeIsNotNull() {
            addCriterion("verify_time is not null");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeEqualTo(Date value) {
            addCriterion("verify_time =", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeNotEqualTo(Date value) {
            addCriterion("verify_time <>", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeGreaterThan(Date value) {
            addCriterion("verify_time >", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("verify_time >=", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeLessThan(Date value) {
            addCriterion("verify_time <", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("verify_time <=", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeIn(List<Date> values) {
            addCriterion("verify_time in", values, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeNotIn(List<Date> values) {
            addCriterion("verify_time not in", values, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeBetween(Date value1, Date value2) {
            addCriterion("verify_time between", value1, value2, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("verify_time not between", value1, value2, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andIdCardIsNull() {
            addCriterion("id_card is null");
            return (Criteria) this;
        }

        public Criteria andIdCardIsNotNull() {
            addCriterion("id_card is not null");
            return (Criteria) this;
        }

        public Criteria andIdCardEqualTo(String value) {
            addCriterion("id_card =", value, "idCard");
            return (Criteria) this;
        }

        public Criteria andIdCardNotEqualTo(String value) {
            addCriterion("id_card <>", value, "idCard");
            return (Criteria) this;
        }

        public Criteria andIdCardGreaterThan(String value) {
            addCriterion("id_card >", value, "idCard");
            return (Criteria) this;
        }

        public Criteria andIdCardGreaterThanOrEqualTo(String value) {
            addCriterion("id_card >=", value, "idCard");
            return (Criteria) this;
        }

        public Criteria andIdCardLessThan(String value) {
            addCriterion("id_card <", value, "idCard");
            return (Criteria) this;
        }

        public Criteria andIdCardLessThanOrEqualTo(String value) {
            addCriterion("id_card <=", value, "idCard");
            return (Criteria) this;
        }

        public Criteria andIdCardLike(String value) {
            addCriterion("id_card like", value, "idCard");
            return (Criteria) this;
        }

        public Criteria andIdCardNotLike(String value) {
            addCriterion("id_card not like", value, "idCard");
            return (Criteria) this;
        }

        public Criteria andIdCardIn(List<String> values) {
            addCriterion("id_card in", values, "idCard");
            return (Criteria) this;
        }

        public Criteria andIdCardNotIn(List<String> values) {
            addCriterion("id_card not in", values, "idCard");
            return (Criteria) this;
        }

        public Criteria andIdCardBetween(String value1, String value2) {
            addCriterion("id_card between", value1, value2, "idCard");
            return (Criteria) this;
        }

        public Criteria andIdCardNotBetween(String value1, String value2) {
            addCriterion("id_card not between", value1, value2, "idCard");
            return (Criteria) this;
        }

        public Criteria andUpdateRemarkIsNull() {
            addCriterion("update_remark is null");
            return (Criteria) this;
        }

        public Criteria andUpdateRemarkIsNotNull() {
            addCriterion("update_remark is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateRemarkEqualTo(String value) {
            addCriterion("update_remark =", value, "updateRemark");
            return (Criteria) this;
        }

        public Criteria andUpdateRemarkNotEqualTo(String value) {
            addCriterion("update_remark <>", value, "updateRemark");
            return (Criteria) this;
        }

        public Criteria andUpdateRemarkGreaterThan(String value) {
            addCriterion("update_remark >", value, "updateRemark");
            return (Criteria) this;
        }

        public Criteria andUpdateRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("update_remark >=", value, "updateRemark");
            return (Criteria) this;
        }

        public Criteria andUpdateRemarkLessThan(String value) {
            addCriterion("update_remark <", value, "updateRemark");
            return (Criteria) this;
        }

        public Criteria andUpdateRemarkLessThanOrEqualTo(String value) {
            addCriterion("update_remark <=", value, "updateRemark");
            return (Criteria) this;
        }

        public Criteria andUpdateRemarkLike(String value) {
            addCriterion("update_remark like", value, "updateRemark");
            return (Criteria) this;
        }

        public Criteria andUpdateRemarkNotLike(String value) {
            addCriterion("update_remark not like", value, "updateRemark");
            return (Criteria) this;
        }

        public Criteria andUpdateRemarkIn(List<String> values) {
            addCriterion("update_remark in", values, "updateRemark");
            return (Criteria) this;
        }

        public Criteria andUpdateRemarkNotIn(List<String> values) {
            addCriterion("update_remark not in", values, "updateRemark");
            return (Criteria) this;
        }

        public Criteria andUpdateRemarkBetween(String value1, String value2) {
            addCriterion("update_remark between", value1, value2, "updateRemark");
            return (Criteria) this;
        }

        public Criteria andUpdateRemarkNotBetween(String value1, String value2) {
            addCriterion("update_remark not between", value1, value2, "updateRemark");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}