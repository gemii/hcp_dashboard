package cc.gemii.hcp.po;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GemiiHcpActivityExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GemiiHcpActivityExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGroupIdIsNull() {
            addCriterion("group_id is null");
            return (Criteria) this;
        }

        public Criteria andGroupIdIsNotNull() {
            addCriterion("group_id is not null");
            return (Criteria) this;
        }

        public Criteria andGroupIdEqualTo(String value) {
            addCriterion("group_id =", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotEqualTo(String value) {
            addCriterion("group_id <>", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdGreaterThan(String value) {
            addCriterion("group_id >", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdGreaterThanOrEqualTo(String value) {
            addCriterion("group_id >=", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdLessThan(String value) {
            addCriterion("group_id <", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdLessThanOrEqualTo(String value) {
            addCriterion("group_id <=", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdLike(String value) {
            addCriterion("group_id like", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotLike(String value) {
            addCriterion("group_id not like", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdIn(List<String> values) {
            addCriterion("group_id in", values, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotIn(List<String> values) {
            addCriterion("group_id not in", values, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdBetween(String value1, String value2) {
            addCriterion("group_id between", value1, value2, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotBetween(String value1, String value2) {
            addCriterion("group_id not between", value1, value2, "groupId");
            return (Criteria) this;
        }

        public Criteria andHcpIdIsNull() {
            addCriterion("hcp_id is null");
            return (Criteria) this;
        }

        public Criteria andHcpIdIsNotNull() {
            addCriterion("hcp_id is not null");
            return (Criteria) this;
        }

        public Criteria andHcpIdEqualTo(String value) {
            addCriterion("hcp_id =", value, "hcpId");
            return (Criteria) this;
        }

        public Criteria andHcpIdNotEqualTo(String value) {
            addCriterion("hcp_id <>", value, "hcpId");
            return (Criteria) this;
        }

        public Criteria andHcpIdGreaterThan(String value) {
            addCriterion("hcp_id >", value, "hcpId");
            return (Criteria) this;
        }

        public Criteria andHcpIdGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_id >=", value, "hcpId");
            return (Criteria) this;
        }

        public Criteria andHcpIdLessThan(String value) {
            addCriterion("hcp_id <", value, "hcpId");
            return (Criteria) this;
        }

        public Criteria andHcpIdLessThanOrEqualTo(String value) {
            addCriterion("hcp_id <=", value, "hcpId");
            return (Criteria) this;
        }

        public Criteria andHcpIdLike(String value) {
            addCriterion("hcp_id like", value, "hcpId");
            return (Criteria) this;
        }

        public Criteria andHcpIdNotLike(String value) {
            addCriterion("hcp_id not like", value, "hcpId");
            return (Criteria) this;
        }

        public Criteria andHcpIdIn(List<String> values) {
            addCriterion("hcp_id in", values, "hcpId");
            return (Criteria) this;
        }

        public Criteria andHcpIdNotIn(List<String> values) {
            addCriterion("hcp_id not in", values, "hcpId");
            return (Criteria) this;
        }

        public Criteria andHcpIdBetween(String value1, String value2) {
            addCriterion("hcp_id between", value1, value2, "hcpId");
            return (Criteria) this;
        }

        public Criteria andHcpIdNotBetween(String value1, String value2) {
            addCriterion("hcp_id not between", value1, value2, "hcpId");
            return (Criteria) this;
        }

        public Criteria andStaffIdIsNull() {
            addCriterion("staff_id is null");
            return (Criteria) this;
        }

        public Criteria andStaffIdIsNotNull() {
            addCriterion("staff_id is not null");
            return (Criteria) this;
        }

        public Criteria andStaffIdEqualTo(String value) {
            addCriterion("staff_id =", value, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdNotEqualTo(String value) {
            addCriterion("staff_id <>", value, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdGreaterThan(String value) {
            addCriterion("staff_id >", value, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdGreaterThanOrEqualTo(String value) {
            addCriterion("staff_id >=", value, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdLessThan(String value) {
            addCriterion("staff_id <", value, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdLessThanOrEqualTo(String value) {
            addCriterion("staff_id <=", value, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdLike(String value) {
            addCriterion("staff_id like", value, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdNotLike(String value) {
            addCriterion("staff_id not like", value, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdIn(List<String> values) {
            addCriterion("staff_id in", values, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdNotIn(List<String> values) {
            addCriterion("staff_id not in", values, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdBetween(String value1, String value2) {
            addCriterion("staff_id between", value1, value2, "staffId");
            return (Criteria) this;
        }

        public Criteria andStaffIdNotBetween(String value1, String value2) {
            addCriterion("staff_id not between", value1, value2, "staffId");
            return (Criteria) this;
        }

        public Criteria andActCtimeIsNull() {
            addCriterion("act_ctime is null");
            return (Criteria) this;
        }

        public Criteria andActCtimeIsNotNull() {
            addCriterion("act_ctime is not null");
            return (Criteria) this;
        }

        public Criteria andActCtimeEqualTo(Date value) {
            addCriterion("act_ctime =", value, "actCtime");
            return (Criteria) this;
        }

        public Criteria andActCtimeNotEqualTo(Date value) {
            addCriterion("act_ctime <>", value, "actCtime");
            return (Criteria) this;
        }

        public Criteria andActCtimeGreaterThan(Date value) {
            addCriterion("act_ctime >", value, "actCtime");
            return (Criteria) this;
        }

        public Criteria andActCtimeGreaterThanOrEqualTo(Date value) {
            addCriterion("act_ctime >=", value, "actCtime");
            return (Criteria) this;
        }

        public Criteria andActCtimeLessThan(Date value) {
            addCriterion("act_ctime <", value, "actCtime");
            return (Criteria) this;
        }

        public Criteria andActCtimeLessThanOrEqualTo(Date value) {
            addCriterion("act_ctime <=", value, "actCtime");
            return (Criteria) this;
        }

        public Criteria andActCtimeIn(List<Date> values) {
            addCriterion("act_ctime in", values, "actCtime");
            return (Criteria) this;
        }

        public Criteria andActCtimeNotIn(List<Date> values) {
            addCriterion("act_ctime not in", values, "actCtime");
            return (Criteria) this;
        }

        public Criteria andActCtimeBetween(Date value1, Date value2) {
            addCriterion("act_ctime between", value1, value2, "actCtime");
            return (Criteria) this;
        }

        public Criteria andActCtimeNotBetween(Date value1, Date value2) {
            addCriterion("act_ctime not between", value1, value2, "actCtime");
            return (Criteria) this;
        }

        public Criteria andActStartTimeIsNull() {
            addCriterion("act_start_time is null");
            return (Criteria) this;
        }

        public Criteria andActStartTimeIsNotNull() {
            addCriterion("act_start_time is not null");
            return (Criteria) this;
        }

        public Criteria andActStartTimeEqualTo(Date value) {
            addCriterion("act_start_time =", value, "actStartTime");
            return (Criteria) this;
        }

        public Criteria andActStartTimeNotEqualTo(Date value) {
            addCriterion("act_start_time <>", value, "actStartTime");
            return (Criteria) this;
        }

        public Criteria andActStartTimeGreaterThan(Date value) {
            addCriterion("act_start_time >", value, "actStartTime");
            return (Criteria) this;
        }

        public Criteria andActStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("act_start_time >=", value, "actStartTime");
            return (Criteria) this;
        }

        public Criteria andActStartTimeLessThan(Date value) {
            addCriterion("act_start_time <", value, "actStartTime");
            return (Criteria) this;
        }

        public Criteria andActStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("act_start_time <=", value, "actStartTime");
            return (Criteria) this;
        }

        public Criteria andActStartTimeIn(List<Date> values) {
            addCriterion("act_start_time in", values, "actStartTime");
            return (Criteria) this;
        }

        public Criteria andActStartTimeNotIn(List<Date> values) {
            addCriterion("act_start_time not in", values, "actStartTime");
            return (Criteria) this;
        }

        public Criteria andActStartTimeBetween(Date value1, Date value2) {
            addCriterion("act_start_time between", value1, value2, "actStartTime");
            return (Criteria) this;
        }

        public Criteria andActStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("act_start_time not between", value1, value2, "actStartTime");
            return (Criteria) this;
        }

        public Criteria andActEndTimeIsNull() {
            addCriterion("act_end_time is null");
            return (Criteria) this;
        }

        public Criteria andActEndTimeIsNotNull() {
            addCriterion("act_end_time is not null");
            return (Criteria) this;
        }

        public Criteria andActEndTimeEqualTo(Date value) {
            addCriterion("act_end_time =", value, "actEndTime");
            return (Criteria) this;
        }

        public Criteria andActEndTimeNotEqualTo(Date value) {
            addCriterion("act_end_time <>", value, "actEndTime");
            return (Criteria) this;
        }

        public Criteria andActEndTimeGreaterThan(Date value) {
            addCriterion("act_end_time >", value, "actEndTime");
            return (Criteria) this;
        }

        public Criteria andActEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("act_end_time >=", value, "actEndTime");
            return (Criteria) this;
        }

        public Criteria andActEndTimeLessThan(Date value) {
            addCriterion("act_end_time <", value, "actEndTime");
            return (Criteria) this;
        }

        public Criteria andActEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("act_end_time <=", value, "actEndTime");
            return (Criteria) this;
        }

        public Criteria andActEndTimeIn(List<Date> values) {
            addCriterion("act_end_time in", values, "actEndTime");
            return (Criteria) this;
        }

        public Criteria andActEndTimeNotIn(List<Date> values) {
            addCriterion("act_end_time not in", values, "actEndTime");
            return (Criteria) this;
        }

        public Criteria andActEndTimeBetween(Date value1, Date value2) {
            addCriterion("act_end_time between", value1, value2, "actEndTime");
            return (Criteria) this;
        }

        public Criteria andActEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("act_end_time not between", value1, value2, "actEndTime");
            return (Criteria) this;
        }

        public Criteria andActPeriodIsNull() {
            addCriterion("act_period is null");
            return (Criteria) this;
        }

        public Criteria andActPeriodIsNotNull() {
            addCriterion("act_period is not null");
            return (Criteria) this;
        }

        public Criteria andActPeriodEqualTo(Integer value) {
            addCriterion("act_period =", value, "actPeriod");
            return (Criteria) this;
        }

        public Criteria andActPeriodNotEqualTo(Integer value) {
            addCriterion("act_period <>", value, "actPeriod");
            return (Criteria) this;
        }

        public Criteria andActPeriodGreaterThan(Integer value) {
            addCriterion("act_period >", value, "actPeriod");
            return (Criteria) this;
        }

        public Criteria andActPeriodGreaterThanOrEqualTo(Integer value) {
            addCriterion("act_period >=", value, "actPeriod");
            return (Criteria) this;
        }

        public Criteria andActPeriodLessThan(Integer value) {
            addCriterion("act_period <", value, "actPeriod");
            return (Criteria) this;
        }

        public Criteria andActPeriodLessThanOrEqualTo(Integer value) {
            addCriterion("act_period <=", value, "actPeriod");
            return (Criteria) this;
        }

        public Criteria andActPeriodIn(List<Integer> values) {
            addCriterion("act_period in", values, "actPeriod");
            return (Criteria) this;
        }

        public Criteria andActPeriodNotIn(List<Integer> values) {
            addCriterion("act_period not in", values, "actPeriod");
            return (Criteria) this;
        }

        public Criteria andActPeriodBetween(Integer value1, Integer value2) {
            addCriterion("act_period between", value1, value2, "actPeriod");
            return (Criteria) this;
        }

        public Criteria andActPeriodNotBetween(Integer value1, Integer value2) {
            addCriterion("act_period not between", value1, value2, "actPeriod");
            return (Criteria) this;
        }

        public Criteria andActReviewIsNull() {
            addCriterion("act_review is null");
            return (Criteria) this;
        }

        public Criteria andActReviewIsNotNull() {
            addCriterion("act_review is not null");
            return (Criteria) this;
        }

        public Criteria andActReviewEqualTo(String value) {
            addCriterion("act_review =", value, "actReview");
            return (Criteria) this;
        }

        public Criteria andActReviewNotEqualTo(String value) {
            addCriterion("act_review <>", value, "actReview");
            return (Criteria) this;
        }

        public Criteria andActReviewGreaterThan(String value) {
            addCriterion("act_review >", value, "actReview");
            return (Criteria) this;
        }

        public Criteria andActReviewGreaterThanOrEqualTo(String value) {
            addCriterion("act_review >=", value, "actReview");
            return (Criteria) this;
        }

        public Criteria andActReviewLessThan(String value) {
            addCriterion("act_review <", value, "actReview");
            return (Criteria) this;
        }

        public Criteria andActReviewLessThanOrEqualTo(String value) {
            addCriterion("act_review <=", value, "actReview");
            return (Criteria) this;
        }

        public Criteria andActReviewLike(String value) {
            addCriterion("act_review like", value, "actReview");
            return (Criteria) this;
        }

        public Criteria andActReviewNotLike(String value) {
            addCriterion("act_review not like", value, "actReview");
            return (Criteria) this;
        }

        public Criteria andActReviewIn(List<String> values) {
            addCriterion("act_review in", values, "actReview");
            return (Criteria) this;
        }

        public Criteria andActReviewNotIn(List<String> values) {
            addCriterion("act_review not in", values, "actReview");
            return (Criteria) this;
        }

        public Criteria andActReviewBetween(String value1, String value2) {
            addCriterion("act_review between", value1, value2, "actReview");
            return (Criteria) this;
        }

        public Criteria andActReviewNotBetween(String value1, String value2) {
            addCriterion("act_review not between", value1, value2, "actReview");
            return (Criteria) this;
        }

        public Criteria andActPaidIsNull() {
            addCriterion("act_paid is null");
            return (Criteria) this;
        }

        public Criteria andActPaidIsNotNull() {
            addCriterion("act_paid is not null");
            return (Criteria) this;
        }

        public Criteria andActPaidEqualTo(Long value) {
            addCriterion("act_paid =", value, "actPaid");
            return (Criteria) this;
        }

        public Criteria andActPaidNotEqualTo(Long value) {
            addCriterion("act_paid <>", value, "actPaid");
            return (Criteria) this;
        }

        public Criteria andActPaidGreaterThan(Long value) {
            addCriterion("act_paid >", value, "actPaid");
            return (Criteria) this;
        }

        public Criteria andActPaidGreaterThanOrEqualTo(Long value) {
            addCriterion("act_paid >=", value, "actPaid");
            return (Criteria) this;
        }

        public Criteria andActPaidLessThan(Long value) {
            addCriterion("act_paid <", value, "actPaid");
            return (Criteria) this;
        }

        public Criteria andActPaidLessThanOrEqualTo(Long value) {
            addCriterion("act_paid <=", value, "actPaid");
            return (Criteria) this;
        }

        public Criteria andActPaidIn(List<Long> values) {
            addCriterion("act_paid in", values, "actPaid");
            return (Criteria) this;
        }

        public Criteria andActPaidNotIn(List<Long> values) {
            addCriterion("act_paid not in", values, "actPaid");
            return (Criteria) this;
        }

        public Criteria andActPaidBetween(Long value1, Long value2) {
            addCriterion("act_paid between", value1, value2, "actPaid");
            return (Criteria) this;
        }

        public Criteria andActPaidNotBetween(Long value1, Long value2) {
            addCriterion("act_paid not between", value1, value2, "actPaid");
            return (Criteria) this;
        }

        public Criteria andActCoursewareIsNull() {
            addCriterion("act_courseware is null");
            return (Criteria) this;
        }

        public Criteria andActCoursewareIsNotNull() {
            addCriterion("act_courseware is not null");
            return (Criteria) this;
        }

        public Criteria andActCoursewareEqualTo(String value) {
            addCriterion("act_courseware =", value, "actCourseware");
            return (Criteria) this;
        }

        public Criteria andActCoursewareNotEqualTo(String value) {
            addCriterion("act_courseware <>", value, "actCourseware");
            return (Criteria) this;
        }

        public Criteria andActCoursewareGreaterThan(String value) {
            addCriterion("act_courseware >", value, "actCourseware");
            return (Criteria) this;
        }

        public Criteria andActCoursewareGreaterThanOrEqualTo(String value) {
            addCriterion("act_courseware >=", value, "actCourseware");
            return (Criteria) this;
        }

        public Criteria andActCoursewareLessThan(String value) {
            addCriterion("act_courseware <", value, "actCourseware");
            return (Criteria) this;
        }

        public Criteria andActCoursewareLessThanOrEqualTo(String value) {
            addCriterion("act_courseware <=", value, "actCourseware");
            return (Criteria) this;
        }

        public Criteria andActCoursewareLike(String value) {
            addCriterion("act_courseware like", value, "actCourseware");
            return (Criteria) this;
        }

        public Criteria andActCoursewareNotLike(String value) {
            addCriterion("act_courseware not like", value, "actCourseware");
            return (Criteria) this;
        }

        public Criteria andActCoursewareIn(List<String> values) {
            addCriterion("act_courseware in", values, "actCourseware");
            return (Criteria) this;
        }

        public Criteria andActCoursewareNotIn(List<String> values) {
            addCriterion("act_courseware not in", values, "actCourseware");
            return (Criteria) this;
        }

        public Criteria andActCoursewareBetween(String value1, String value2) {
            addCriterion("act_courseware between", value1, value2, "actCourseware");
            return (Criteria) this;
        }

        public Criteria andActCoursewareNotBetween(String value1, String value2) {
            addCriterion("act_courseware not between", value1, value2, "actCourseware");
            return (Criteria) this;
        }

        public Criteria andActSubjectIsNull() {
            addCriterion("act_subject is null");
            return (Criteria) this;
        }

        public Criteria andActSubjectIsNotNull() {
            addCriterion("act_subject is not null");
            return (Criteria) this;
        }

        public Criteria andActSubjectEqualTo(String value) {
            addCriterion("act_subject =", value, "actSubject");
            return (Criteria) this;
        }

        public Criteria andActSubjectNotEqualTo(String value) {
            addCriterion("act_subject <>", value, "actSubject");
            return (Criteria) this;
        }

        public Criteria andActSubjectGreaterThan(String value) {
            addCriterion("act_subject >", value, "actSubject");
            return (Criteria) this;
        }

        public Criteria andActSubjectGreaterThanOrEqualTo(String value) {
            addCriterion("act_subject >=", value, "actSubject");
            return (Criteria) this;
        }

        public Criteria andActSubjectLessThan(String value) {
            addCriterion("act_subject <", value, "actSubject");
            return (Criteria) this;
        }

        public Criteria andActSubjectLessThanOrEqualTo(String value) {
            addCriterion("act_subject <=", value, "actSubject");
            return (Criteria) this;
        }

        public Criteria andActSubjectLike(String value) {
            addCriterion("act_subject like", value, "actSubject");
            return (Criteria) this;
        }

        public Criteria andActSubjectNotLike(String value) {
            addCriterion("act_subject not like", value, "actSubject");
            return (Criteria) this;
        }

        public Criteria andActSubjectIn(List<String> values) {
            addCriterion("act_subject in", values, "actSubject");
            return (Criteria) this;
        }

        public Criteria andActSubjectNotIn(List<String> values) {
            addCriterion("act_subject not in", values, "actSubject");
            return (Criteria) this;
        }

        public Criteria andActSubjectBetween(String value1, String value2) {
            addCriterion("act_subject between", value1, value2, "actSubject");
            return (Criteria) this;
        }

        public Criteria andActSubjectNotBetween(String value1, String value2) {
            addCriterion("act_subject not between", value1, value2, "actSubject");
            return (Criteria) this;
        }

        public Criteria andHcpWxNameIsNull() {
            addCriterion("hcp_wx_name is null");
            return (Criteria) this;
        }

        public Criteria andHcpWxNameIsNotNull() {
            addCriterion("hcp_wx_name is not null");
            return (Criteria) this;
        }

        public Criteria andHcpWxNameEqualTo(String value) {
            addCriterion("hcp_wx_name =", value, "hcpWxName");
            return (Criteria) this;
        }

        public Criteria andHcpWxNameNotEqualTo(String value) {
            addCriterion("hcp_wx_name <>", value, "hcpWxName");
            return (Criteria) this;
        }

        public Criteria andHcpWxNameGreaterThan(String value) {
            addCriterion("hcp_wx_name >", value, "hcpWxName");
            return (Criteria) this;
        }

        public Criteria andHcpWxNameGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_wx_name >=", value, "hcpWxName");
            return (Criteria) this;
        }

        public Criteria andHcpWxNameLessThan(String value) {
            addCriterion("hcp_wx_name <", value, "hcpWxName");
            return (Criteria) this;
        }

        public Criteria andHcpWxNameLessThanOrEqualTo(String value) {
            addCriterion("hcp_wx_name <=", value, "hcpWxName");
            return (Criteria) this;
        }

        public Criteria andHcpWxNameLike(String value) {
            addCriterion("hcp_wx_name like", value, "hcpWxName");
            return (Criteria) this;
        }

        public Criteria andHcpWxNameNotLike(String value) {
            addCriterion("hcp_wx_name not like", value, "hcpWxName");
            return (Criteria) this;
        }

        public Criteria andHcpWxNameIn(List<String> values) {
            addCriterion("hcp_wx_name in", values, "hcpWxName");
            return (Criteria) this;
        }

        public Criteria andHcpWxNameNotIn(List<String> values) {
            addCriterion("hcp_wx_name not in", values, "hcpWxName");
            return (Criteria) this;
        }

        public Criteria andHcpWxNameBetween(String value1, String value2) {
            addCriterion("hcp_wx_name between", value1, value2, "hcpWxName");
            return (Criteria) this;
        }

        public Criteria andHcpWxNameNotBetween(String value1, String value2) {
            addCriterion("hcp_wx_name not between", value1, value2, "hcpWxName");
            return (Criteria) this;
        }

        public Criteria andHcpWxNickIsNull() {
            addCriterion("hcp_wx_nick is null");
            return (Criteria) this;
        }

        public Criteria andHcpWxNickIsNotNull() {
            addCriterion("hcp_wx_nick is not null");
            return (Criteria) this;
        }

        public Criteria andHcpWxNickEqualTo(String value) {
            addCriterion("hcp_wx_nick =", value, "hcpWxNick");
            return (Criteria) this;
        }

        public Criteria andHcpWxNickNotEqualTo(String value) {
            addCriterion("hcp_wx_nick <>", value, "hcpWxNick");
            return (Criteria) this;
        }

        public Criteria andHcpWxNickGreaterThan(String value) {
            addCriterion("hcp_wx_nick >", value, "hcpWxNick");
            return (Criteria) this;
        }

        public Criteria andHcpWxNickGreaterThanOrEqualTo(String value) {
            addCriterion("hcp_wx_nick >=", value, "hcpWxNick");
            return (Criteria) this;
        }

        public Criteria andHcpWxNickLessThan(String value) {
            addCriterion("hcp_wx_nick <", value, "hcpWxNick");
            return (Criteria) this;
        }

        public Criteria andHcpWxNickLessThanOrEqualTo(String value) {
            addCriterion("hcp_wx_nick <=", value, "hcpWxNick");
            return (Criteria) this;
        }

        public Criteria andHcpWxNickLike(String value) {
            addCriterion("hcp_wx_nick like", value, "hcpWxNick");
            return (Criteria) this;
        }

        public Criteria andHcpWxNickNotLike(String value) {
            addCriterion("hcp_wx_nick not like", value, "hcpWxNick");
            return (Criteria) this;
        }

        public Criteria andHcpWxNickIn(List<String> values) {
            addCriterion("hcp_wx_nick in", values, "hcpWxNick");
            return (Criteria) this;
        }

        public Criteria andHcpWxNickNotIn(List<String> values) {
            addCriterion("hcp_wx_nick not in", values, "hcpWxNick");
            return (Criteria) this;
        }

        public Criteria andHcpWxNickBetween(String value1, String value2) {
            addCriterion("hcp_wx_nick between", value1, value2, "hcpWxNick");
            return (Criteria) this;
        }

        public Criteria andHcpWxNickNotBetween(String value1, String value2) {
            addCriterion("hcp_wx_nick not between", value1, value2, "hcpWxNick");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andFeedbackTimeIsNull() {
            addCriterion("feedback_time is null");
            return (Criteria) this;
        }

        public Criteria andFeedbackTimeIsNotNull() {
            addCriterion("feedback_time is not null");
            return (Criteria) this;
        }

        public Criteria andFeedbackTimeEqualTo(Date value) {
            addCriterion("feedback_time =", value, "feedbackTime");
            return (Criteria) this;
        }

        public Criteria andFeedbackTimeNotEqualTo(Date value) {
            addCriterion("feedback_time <>", value, "feedbackTime");
            return (Criteria) this;
        }

        public Criteria andFeedbackTimeGreaterThan(Date value) {
            addCriterion("feedback_time >", value, "feedbackTime");
            return (Criteria) this;
        }

        public Criteria andFeedbackTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("feedback_time >=", value, "feedbackTime");
            return (Criteria) this;
        }

        public Criteria andFeedbackTimeLessThan(Date value) {
            addCriterion("feedback_time <", value, "feedbackTime");
            return (Criteria) this;
        }

        public Criteria andFeedbackTimeLessThanOrEqualTo(Date value) {
            addCriterion("feedback_time <=", value, "feedbackTime");
            return (Criteria) this;
        }

        public Criteria andFeedbackTimeIn(List<Date> values) {
            addCriterion("feedback_time in", values, "feedbackTime");
            return (Criteria) this;
        }

        public Criteria andFeedbackTimeNotIn(List<Date> values) {
            addCriterion("feedback_time not in", values, "feedbackTime");
            return (Criteria) this;
        }

        public Criteria andFeedbackTimeBetween(Date value1, Date value2) {
            addCriterion("feedback_time between", value1, value2, "feedbackTime");
            return (Criteria) this;
        }

        public Criteria andFeedbackTimeNotBetween(Date value1, Date value2) {
            addCriterion("feedback_time not between", value1, value2, "feedbackTime");
            return (Criteria) this;
        }

        public Criteria andActRealStartIsNull() {
            addCriterion("act_real_start is null");
            return (Criteria) this;
        }

        public Criteria andActRealStartIsNotNull() {
            addCriterion("act_real_start is not null");
            return (Criteria) this;
        }

        public Criteria andActRealStartEqualTo(Date value) {
            addCriterion("act_real_start =", value, "actRealStart");
            return (Criteria) this;
        }

        public Criteria andActRealStartNotEqualTo(Date value) {
            addCriterion("act_real_start <>", value, "actRealStart");
            return (Criteria) this;
        }

        public Criteria andActRealStartGreaterThan(Date value) {
            addCriterion("act_real_start >", value, "actRealStart");
            return (Criteria) this;
        }

        public Criteria andActRealStartGreaterThanOrEqualTo(Date value) {
            addCriterion("act_real_start >=", value, "actRealStart");
            return (Criteria) this;
        }

        public Criteria andActRealStartLessThan(Date value) {
            addCriterion("act_real_start <", value, "actRealStart");
            return (Criteria) this;
        }

        public Criteria andActRealStartLessThanOrEqualTo(Date value) {
            addCriterion("act_real_start <=", value, "actRealStart");
            return (Criteria) this;
        }

        public Criteria andActRealStartIn(List<Date> values) {
            addCriterion("act_real_start in", values, "actRealStart");
            return (Criteria) this;
        }

        public Criteria andActRealStartNotIn(List<Date> values) {
            addCriterion("act_real_start not in", values, "actRealStart");
            return (Criteria) this;
        }

        public Criteria andActRealStartBetween(Date value1, Date value2) {
            addCriterion("act_real_start between", value1, value2, "actRealStart");
            return (Criteria) this;
        }

        public Criteria andActRealStartNotBetween(Date value1, Date value2) {
            addCriterion("act_real_start not between", value1, value2, "actRealStart");
            return (Criteria) this;
        }

        public Criteria andActRealEndIsNull() {
            addCriterion("act_real_end is null");
            return (Criteria) this;
        }

        public Criteria andActRealEndIsNotNull() {
            addCriterion("act_real_end is not null");
            return (Criteria) this;
        }

        public Criteria andActRealEndEqualTo(Date value) {
            addCriterion("act_real_end =", value, "actRealEnd");
            return (Criteria) this;
        }

        public Criteria andActRealEndNotEqualTo(Date value) {
            addCriterion("act_real_end <>", value, "actRealEnd");
            return (Criteria) this;
        }

        public Criteria andActRealEndGreaterThan(Date value) {
            addCriterion("act_real_end >", value, "actRealEnd");
            return (Criteria) this;
        }

        public Criteria andActRealEndGreaterThanOrEqualTo(Date value) {
            addCriterion("act_real_end >=", value, "actRealEnd");
            return (Criteria) this;
        }

        public Criteria andActRealEndLessThan(Date value) {
            addCriterion("act_real_end <", value, "actRealEnd");
            return (Criteria) this;
        }

        public Criteria andActRealEndLessThanOrEqualTo(Date value) {
            addCriterion("act_real_end <=", value, "actRealEnd");
            return (Criteria) this;
        }

        public Criteria andActRealEndIn(List<Date> values) {
            addCriterion("act_real_end in", values, "actRealEnd");
            return (Criteria) this;
        }

        public Criteria andActRealEndNotIn(List<Date> values) {
            addCriterion("act_real_end not in", values, "actRealEnd");
            return (Criteria) this;
        }

        public Criteria andActRealEndBetween(Date value1, Date value2) {
            addCriterion("act_real_end between", value1, value2, "actRealEnd");
            return (Criteria) this;
        }

        public Criteria andActRealEndNotBetween(Date value1, Date value2) {
            addCriterion("act_real_end not between", value1, value2, "actRealEnd");
            return (Criteria) this;
        }

        public Criteria andFeedbackProIsNull() {
            addCriterion("feedback_pro is null");
            return (Criteria) this;
        }

        public Criteria andFeedbackProIsNotNull() {
            addCriterion("feedback_pro is not null");
            return (Criteria) this;
        }

        public Criteria andFeedbackProEqualTo(Byte value) {
            addCriterion("feedback_pro =", value, "feedbackPro");
            return (Criteria) this;
        }

        public Criteria andFeedbackProNotEqualTo(Byte value) {
            addCriterion("feedback_pro <>", value, "feedbackPro");
            return (Criteria) this;
        }

        public Criteria andFeedbackProGreaterThan(Byte value) {
            addCriterion("feedback_pro >", value, "feedbackPro");
            return (Criteria) this;
        }

        public Criteria andFeedbackProGreaterThanOrEqualTo(Byte value) {
            addCriterion("feedback_pro >=", value, "feedbackPro");
            return (Criteria) this;
        }

        public Criteria andFeedbackProLessThan(Byte value) {
            addCriterion("feedback_pro <", value, "feedbackPro");
            return (Criteria) this;
        }

        public Criteria andFeedbackProLessThanOrEqualTo(Byte value) {
            addCriterion("feedback_pro <=", value, "feedbackPro");
            return (Criteria) this;
        }

        public Criteria andFeedbackProIn(List<Byte> values) {
            addCriterion("feedback_pro in", values, "feedbackPro");
            return (Criteria) this;
        }

        public Criteria andFeedbackProNotIn(List<Byte> values) {
            addCriterion("feedback_pro not in", values, "feedbackPro");
            return (Criteria) this;
        }

        public Criteria andFeedbackProBetween(Byte value1, Byte value2) {
            addCriterion("feedback_pro between", value1, value2, "feedbackPro");
            return (Criteria) this;
        }

        public Criteria andFeedbackProNotBetween(Byte value1, Byte value2) {
            addCriterion("feedback_pro not between", value1, value2, "feedbackPro");
            return (Criteria) this;
        }

        public Criteria andFeedbackFullIsNull() {
            addCriterion("feedback_full is null");
            return (Criteria) this;
        }

        public Criteria andFeedbackFullIsNotNull() {
            addCriterion("feedback_full is not null");
            return (Criteria) this;
        }

        public Criteria andFeedbackFullEqualTo(Byte value) {
            addCriterion("feedback_full =", value, "feedbackFull");
            return (Criteria) this;
        }

        public Criteria andFeedbackFullNotEqualTo(Byte value) {
            addCriterion("feedback_full <>", value, "feedbackFull");
            return (Criteria) this;
        }

        public Criteria andFeedbackFullGreaterThan(Byte value) {
            addCriterion("feedback_full >", value, "feedbackFull");
            return (Criteria) this;
        }

        public Criteria andFeedbackFullGreaterThanOrEqualTo(Byte value) {
            addCriterion("feedback_full >=", value, "feedbackFull");
            return (Criteria) this;
        }

        public Criteria andFeedbackFullLessThan(Byte value) {
            addCriterion("feedback_full <", value, "feedbackFull");
            return (Criteria) this;
        }

        public Criteria andFeedbackFullLessThanOrEqualTo(Byte value) {
            addCriterion("feedback_full <=", value, "feedbackFull");
            return (Criteria) this;
        }

        public Criteria andFeedbackFullIn(List<Byte> values) {
            addCriterion("feedback_full in", values, "feedbackFull");
            return (Criteria) this;
        }

        public Criteria andFeedbackFullNotIn(List<Byte> values) {
            addCriterion("feedback_full not in", values, "feedbackFull");
            return (Criteria) this;
        }

        public Criteria andFeedbackFullBetween(Byte value1, Byte value2) {
            addCriterion("feedback_full between", value1, value2, "feedbackFull");
            return (Criteria) this;
        }

        public Criteria andFeedbackFullNotBetween(Byte value1, Byte value2) {
            addCriterion("feedback_full not between", value1, value2, "feedbackFull");
            return (Criteria) this;
        }

        public Criteria andFeedbackClearIsNull() {
            addCriterion("feedback_clear is null");
            return (Criteria) this;
        }

        public Criteria andFeedbackClearIsNotNull() {
            addCriterion("feedback_clear is not null");
            return (Criteria) this;
        }

        public Criteria andFeedbackClearEqualTo(Byte value) {
            addCriterion("feedback_clear =", value, "feedbackClear");
            return (Criteria) this;
        }

        public Criteria andFeedbackClearNotEqualTo(Byte value) {
            addCriterion("feedback_clear <>", value, "feedbackClear");
            return (Criteria) this;
        }

        public Criteria andFeedbackClearGreaterThan(Byte value) {
            addCriterion("feedback_clear >", value, "feedbackClear");
            return (Criteria) this;
        }

        public Criteria andFeedbackClearGreaterThanOrEqualTo(Byte value) {
            addCriterion("feedback_clear >=", value, "feedbackClear");
            return (Criteria) this;
        }

        public Criteria andFeedbackClearLessThan(Byte value) {
            addCriterion("feedback_clear <", value, "feedbackClear");
            return (Criteria) this;
        }

        public Criteria andFeedbackClearLessThanOrEqualTo(Byte value) {
            addCriterion("feedback_clear <=", value, "feedbackClear");
            return (Criteria) this;
        }

        public Criteria andFeedbackClearIn(List<Byte> values) {
            addCriterion("feedback_clear in", values, "feedbackClear");
            return (Criteria) this;
        }

        public Criteria andFeedbackClearNotIn(List<Byte> values) {
            addCriterion("feedback_clear not in", values, "feedbackClear");
            return (Criteria) this;
        }

        public Criteria andFeedbackClearBetween(Byte value1, Byte value2) {
            addCriterion("feedback_clear between", value1, value2, "feedbackClear");
            return (Criteria) this;
        }

        public Criteria andFeedbackClearNotBetween(Byte value1, Byte value2) {
            addCriterion("feedback_clear not between", value1, value2, "feedbackClear");
            return (Criteria) this;
        }

        public Criteria andFeedbackInteractionIsNull() {
            addCriterion("feedback_interaction is null");
            return (Criteria) this;
        }

        public Criteria andFeedbackInteractionIsNotNull() {
            addCriterion("feedback_interaction is not null");
            return (Criteria) this;
        }

        public Criteria andFeedbackInteractionEqualTo(Byte value) {
            addCriterion("feedback_interaction =", value, "feedbackInteraction");
            return (Criteria) this;
        }

        public Criteria andFeedbackInteractionNotEqualTo(Byte value) {
            addCriterion("feedback_interaction <>", value, "feedbackInteraction");
            return (Criteria) this;
        }

        public Criteria andFeedbackInteractionGreaterThan(Byte value) {
            addCriterion("feedback_interaction >", value, "feedbackInteraction");
            return (Criteria) this;
        }

        public Criteria andFeedbackInteractionGreaterThanOrEqualTo(Byte value) {
            addCriterion("feedback_interaction >=", value, "feedbackInteraction");
            return (Criteria) this;
        }

        public Criteria andFeedbackInteractionLessThan(Byte value) {
            addCriterion("feedback_interaction <", value, "feedbackInteraction");
            return (Criteria) this;
        }

        public Criteria andFeedbackInteractionLessThanOrEqualTo(Byte value) {
            addCriterion("feedback_interaction <=", value, "feedbackInteraction");
            return (Criteria) this;
        }

        public Criteria andFeedbackInteractionIn(List<Byte> values) {
            addCriterion("feedback_interaction in", values, "feedbackInteraction");
            return (Criteria) this;
        }

        public Criteria andFeedbackInteractionNotIn(List<Byte> values) {
            addCriterion("feedback_interaction not in", values, "feedbackInteraction");
            return (Criteria) this;
        }

        public Criteria andFeedbackInteractionBetween(Byte value1, Byte value2) {
            addCriterion("feedback_interaction between", value1, value2, "feedbackInteraction");
            return (Criteria) this;
        }

        public Criteria andFeedbackInteractionNotBetween(Byte value1, Byte value2) {
            addCriterion("feedback_interaction not between", value1, value2, "feedbackInteraction");
            return (Criteria) this;
        }

        public Criteria andFeedbackOtherIsNull() {
            addCriterion("feedback_other is null");
            return (Criteria) this;
        }

        public Criteria andFeedbackOtherIsNotNull() {
            addCriterion("feedback_other is not null");
            return (Criteria) this;
        }

        public Criteria andFeedbackOtherEqualTo(String value) {
            addCriterion("feedback_other =", value, "feedbackOther");
            return (Criteria) this;
        }

        public Criteria andFeedbackOtherNotEqualTo(String value) {
            addCriterion("feedback_other <>", value, "feedbackOther");
            return (Criteria) this;
        }

        public Criteria andFeedbackOtherGreaterThan(String value) {
            addCriterion("feedback_other >", value, "feedbackOther");
            return (Criteria) this;
        }

        public Criteria andFeedbackOtherGreaterThanOrEqualTo(String value) {
            addCriterion("feedback_other >=", value, "feedbackOther");
            return (Criteria) this;
        }

        public Criteria andFeedbackOtherLessThan(String value) {
            addCriterion("feedback_other <", value, "feedbackOther");
            return (Criteria) this;
        }

        public Criteria andFeedbackOtherLessThanOrEqualTo(String value) {
            addCriterion("feedback_other <=", value, "feedbackOther");
            return (Criteria) this;
        }

        public Criteria andFeedbackOtherLike(String value) {
            addCriterion("feedback_other like", value, "feedbackOther");
            return (Criteria) this;
        }

        public Criteria andFeedbackOtherNotLike(String value) {
            addCriterion("feedback_other not like", value, "feedbackOther");
            return (Criteria) this;
        }

        public Criteria andFeedbackOtherIn(List<String> values) {
            addCriterion("feedback_other in", values, "feedbackOther");
            return (Criteria) this;
        }

        public Criteria andFeedbackOtherNotIn(List<String> values) {
            addCriterion("feedback_other not in", values, "feedbackOther");
            return (Criteria) this;
        }

        public Criteria andFeedbackOtherBetween(String value1, String value2) {
            addCriterion("feedback_other between", value1, value2, "feedbackOther");
            return (Criteria) this;
        }

        public Criteria andFeedbackOtherNotBetween(String value1, String value2) {
            addCriterion("feedback_other not between", value1, value2, "feedbackOther");
            return (Criteria) this;
        }

        public Criteria andPaymentIdIsNull() {
            addCriterion("payment_id is null");
            return (Criteria) this;
        }

        public Criteria andPaymentIdIsNotNull() {
            addCriterion("payment_id is not null");
            return (Criteria) this;
        }

        public Criteria andPaymentIdEqualTo(String value) {
            addCriterion("payment_id =", value, "paymentId");
            return (Criteria) this;
        }

        public Criteria andPaymentIdNotEqualTo(String value) {
            addCriterion("payment_id <>", value, "paymentId");
            return (Criteria) this;
        }

        public Criteria andPaymentIdGreaterThan(String value) {
            addCriterion("payment_id >", value, "paymentId");
            return (Criteria) this;
        }

        public Criteria andPaymentIdGreaterThanOrEqualTo(String value) {
            addCriterion("payment_id >=", value, "paymentId");
            return (Criteria) this;
        }

        public Criteria andPaymentIdLessThan(String value) {
            addCriterion("payment_id <", value, "paymentId");
            return (Criteria) this;
        }

        public Criteria andPaymentIdLessThanOrEqualTo(String value) {
            addCriterion("payment_id <=", value, "paymentId");
            return (Criteria) this;
        }

        public Criteria andPaymentIdLike(String value) {
            addCriterion("payment_id like", value, "paymentId");
            return (Criteria) this;
        }

        public Criteria andPaymentIdNotLike(String value) {
            addCriterion("payment_id not like", value, "paymentId");
            return (Criteria) this;
        }

        public Criteria andPaymentIdIn(List<String> values) {
            addCriterion("payment_id in", values, "paymentId");
            return (Criteria) this;
        }

        public Criteria andPaymentIdNotIn(List<String> values) {
            addCriterion("payment_id not in", values, "paymentId");
            return (Criteria) this;
        }

        public Criteria andPaymentIdBetween(String value1, String value2) {
            addCriterion("payment_id between", value1, value2, "paymentId");
            return (Criteria) this;
        }

        public Criteria andPaymentIdNotBetween(String value1, String value2) {
            addCriterion("payment_id not between", value1, value2, "paymentId");
            return (Criteria) this;
        }

        public Criteria andFeedPaidIsNull() {
            addCriterion("feed_paid is null");
            return (Criteria) this;
        }

        public Criteria andFeedPaidIsNotNull() {
            addCriterion("feed_paid is not null");
            return (Criteria) this;
        }

        public Criteria andFeedPaidEqualTo(Long value) {
            addCriterion("feed_paid =", value, "feedPaid");
            return (Criteria) this;
        }

        public Criteria andFeedPaidNotEqualTo(Long value) {
            addCriterion("feed_paid <>", value, "feedPaid");
            return (Criteria) this;
        }

        public Criteria andFeedPaidGreaterThan(Long value) {
            addCriterion("feed_paid >", value, "feedPaid");
            return (Criteria) this;
        }

        public Criteria andFeedPaidGreaterThanOrEqualTo(Long value) {
            addCriterion("feed_paid >=", value, "feedPaid");
            return (Criteria) this;
        }

        public Criteria andFeedPaidLessThan(Long value) {
            addCriterion("feed_paid <", value, "feedPaid");
            return (Criteria) this;
        }

        public Criteria andFeedPaidLessThanOrEqualTo(Long value) {
            addCriterion("feed_paid <=", value, "feedPaid");
            return (Criteria) this;
        }

        public Criteria andFeedPaidIn(List<Long> values) {
            addCriterion("feed_paid in", values, "feedPaid");
            return (Criteria) this;
        }

        public Criteria andFeedPaidNotIn(List<Long> values) {
            addCriterion("feed_paid not in", values, "feedPaid");
            return (Criteria) this;
        }

        public Criteria andFeedPaidBetween(Long value1, Long value2) {
            addCriterion("feed_paid between", value1, value2, "feedPaid");
            return (Criteria) this;
        }

        public Criteria andFeedPaidNotBetween(Long value1, Long value2) {
            addCriterion("feed_paid not between", value1, value2, "feedPaid");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andFeedRemarkIsNull() {
            addCriterion("feed_remark is null");
            return (Criteria) this;
        }

        public Criteria andFeedRemarkIsNotNull() {
            addCriterion("feed_remark is not null");
            return (Criteria) this;
        }

        public Criteria andFeedRemarkEqualTo(String value) {
            addCriterion("feed_remark =", value, "feedRemark");
            return (Criteria) this;
        }

        public Criteria andFeedRemarkNotEqualTo(String value) {
            addCriterion("feed_remark <>", value, "feedRemark");
            return (Criteria) this;
        }

        public Criteria andFeedRemarkGreaterThan(String value) {
            addCriterion("feed_remark >", value, "feedRemark");
            return (Criteria) this;
        }

        public Criteria andFeedRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("feed_remark >=", value, "feedRemark");
            return (Criteria) this;
        }

        public Criteria andFeedRemarkLessThan(String value) {
            addCriterion("feed_remark <", value, "feedRemark");
            return (Criteria) this;
        }

        public Criteria andFeedRemarkLessThanOrEqualTo(String value) {
            addCriterion("feed_remark <=", value, "feedRemark");
            return (Criteria) this;
        }

        public Criteria andFeedRemarkLike(String value) {
            addCriterion("feed_remark like", value, "feedRemark");
            return (Criteria) this;
        }

        public Criteria andFeedRemarkNotLike(String value) {
            addCriterion("feed_remark not like", value, "feedRemark");
            return (Criteria) this;
        }

        public Criteria andFeedRemarkIn(List<String> values) {
            addCriterion("feed_remark in", values, "feedRemark");
            return (Criteria) this;
        }

        public Criteria andFeedRemarkNotIn(List<String> values) {
            addCriterion("feed_remark not in", values, "feedRemark");
            return (Criteria) this;
        }

        public Criteria andFeedRemarkBetween(String value1, String value2) {
            addCriterion("feed_remark between", value1, value2, "feedRemark");
            return (Criteria) this;
        }

        public Criteria andFeedRemarkNotBetween(String value1, String value2) {
            addCriterion("feed_remark not between", value1, value2, "feedRemark");
            return (Criteria) this;
        }

        public Criteria andActFileIsNull() {
            addCriterion("act_file is null");
            return (Criteria) this;
        }

        public Criteria andActFileIsNotNull() {
            addCriterion("act_file is not null");
            return (Criteria) this;
        }

        public Criteria andActFileEqualTo(String value) {
            addCriterion("act_file =", value, "actFile");
            return (Criteria) this;
        }

        public Criteria andActFileNotEqualTo(String value) {
            addCriterion("act_file <>", value, "actFile");
            return (Criteria) this;
        }

        public Criteria andActFileGreaterThan(String value) {
            addCriterion("act_file >", value, "actFile");
            return (Criteria) this;
        }

        public Criteria andActFileGreaterThanOrEqualTo(String value) {
            addCriterion("act_file >=", value, "actFile");
            return (Criteria) this;
        }

        public Criteria andActFileLessThan(String value) {
            addCriterion("act_file <", value, "actFile");
            return (Criteria) this;
        }

        public Criteria andActFileLessThanOrEqualTo(String value) {
            addCriterion("act_file <=", value, "actFile");
            return (Criteria) this;
        }

        public Criteria andActFileLike(String value) {
            addCriterion("act_file like", value, "actFile");
            return (Criteria) this;
        }

        public Criteria andActFileNotLike(String value) {
            addCriterion("act_file not like", value, "actFile");
            return (Criteria) this;
        }

        public Criteria andActFileIn(List<String> values) {
            addCriterion("act_file in", values, "actFile");
            return (Criteria) this;
        }

        public Criteria andActFileNotIn(List<String> values) {
            addCriterion("act_file not in", values, "actFile");
            return (Criteria) this;
        }

        public Criteria andActFileBetween(String value1, String value2) {
            addCriterion("act_file between", value1, value2, "actFile");
            return (Criteria) this;
        }

        public Criteria andActFileNotBetween(String value1, String value2) {
            addCriterion("act_file not between", value1, value2, "actFile");
            return (Criteria) this;
        }

        public Criteria andPayStateIsNull() {
            addCriterion("pay_state is null");
            return (Criteria) this;
        }

        public Criteria andPayStateIsNotNull() {
            addCriterion("pay_state is not null");
            return (Criteria) this;
        }

        public Criteria andPayStateEqualTo(Integer value) {
            addCriterion("pay_state =", value, "payState");
            return (Criteria) this;
        }

        public Criteria andPayStateNotEqualTo(Integer value) {
            addCriterion("pay_state <>", value, "payState");
            return (Criteria) this;
        }

        public Criteria andPayStateGreaterThan(Integer value) {
            addCriterion("pay_state >", value, "payState");
            return (Criteria) this;
        }

        public Criteria andPayStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("pay_state >=", value, "payState");
            return (Criteria) this;
        }

        public Criteria andPayStateLessThan(Integer value) {
            addCriterion("pay_state <", value, "payState");
            return (Criteria) this;
        }

        public Criteria andPayStateLessThanOrEqualTo(Integer value) {
            addCriterion("pay_state <=", value, "payState");
            return (Criteria) this;
        }

        public Criteria andPayStateIn(List<Integer> values) {
            addCriterion("pay_state in", values, "payState");
            return (Criteria) this;
        }

        public Criteria andPayStateNotIn(List<Integer> values) {
            addCriterion("pay_state not in", values, "payState");
            return (Criteria) this;
        }

        public Criteria andPayStateBetween(Integer value1, Integer value2) {
            addCriterion("pay_state between", value1, value2, "payState");
            return (Criteria) this;
        }

        public Criteria andPayStateNotBetween(Integer value1, Integer value2) {
            addCriterion("pay_state not between", value1, value2, "payState");
            return (Criteria) this;
        }

        public Criteria andTpIdIsNull() {
            addCriterion("tp_id is null");
            return (Criteria) this;
        }

        public Criteria andTpIdIsNotNull() {
            addCriterion("tp_id is not null");
            return (Criteria) this;
        }

        public Criteria andTpIdEqualTo(Integer value) {
            addCriterion("tp_id =", value, "tpId");
            return (Criteria) this;
        }

        public Criteria andTpIdNotEqualTo(Integer value) {
            addCriterion("tp_id <>", value, "tpId");
            return (Criteria) this;
        }

        public Criteria andTpIdGreaterThan(Integer value) {
            addCriterion("tp_id >", value, "tpId");
            return (Criteria) this;
        }

        public Criteria andTpIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("tp_id >=", value, "tpId");
            return (Criteria) this;
        }

        public Criteria andTpIdLessThan(Integer value) {
            addCriterion("tp_id <", value, "tpId");
            return (Criteria) this;
        }

        public Criteria andTpIdLessThanOrEqualTo(Integer value) {
            addCriterion("tp_id <=", value, "tpId");
            return (Criteria) this;
        }

        public Criteria andTpIdIn(List<Integer> values) {
            addCriterion("tp_id in", values, "tpId");
            return (Criteria) this;
        }

        public Criteria andTpIdNotIn(List<Integer> values) {
            addCriterion("tp_id not in", values, "tpId");
            return (Criteria) this;
        }

        public Criteria andTpIdBetween(Integer value1, Integer value2) {
            addCriterion("tp_id between", value1, value2, "tpId");
            return (Criteria) this;
        }

        public Criteria andTpIdNotBetween(Integer value1, Integer value2) {
            addCriterion("tp_id not between", value1, value2, "tpId");
            return (Criteria) this;
        }

        public Criteria andPayToThirdTimeIsNull() {
            addCriterion("pay_to_third_time is null");
            return (Criteria) this;
        }

        public Criteria andPayToThirdTimeIsNotNull() {
            addCriterion("pay_to_third_time is not null");
            return (Criteria) this;
        }

        public Criteria andPayToThirdTimeEqualTo(Date value) {
            addCriterion("pay_to_third_time =", value, "payToThirdTime");
            return (Criteria) this;
        }

        public Criteria andPayToThirdTimeNotEqualTo(Date value) {
            addCriterion("pay_to_third_time <>", value, "payToThirdTime");
            return (Criteria) this;
        }

        public Criteria andPayToThirdTimeGreaterThan(Date value) {
            addCriterion("pay_to_third_time >", value, "payToThirdTime");
            return (Criteria) this;
        }

        public Criteria andPayToThirdTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("pay_to_third_time >=", value, "payToThirdTime");
            return (Criteria) this;
        }

        public Criteria andPayToThirdTimeLessThan(Date value) {
            addCriterion("pay_to_third_time <", value, "payToThirdTime");
            return (Criteria) this;
        }

        public Criteria andPayToThirdTimeLessThanOrEqualTo(Date value) {
            addCriterion("pay_to_third_time <=", value, "payToThirdTime");
            return (Criteria) this;
        }

        public Criteria andPayToThirdTimeIn(List<Date> values) {
            addCriterion("pay_to_third_time in", values, "payToThirdTime");
            return (Criteria) this;
        }

        public Criteria andPayToThirdTimeNotIn(List<Date> values) {
            addCriterion("pay_to_third_time not in", values, "payToThirdTime");
            return (Criteria) this;
        }

        public Criteria andPayToThirdTimeBetween(Date value1, Date value2) {
            addCriterion("pay_to_third_time between", value1, value2, "payToThirdTime");
            return (Criteria) this;
        }

        public Criteria andPayToThirdTimeNotBetween(Date value1, Date value2) {
            addCriterion("pay_to_third_time not between", value1, value2, "payToThirdTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeIsNull() {
            addCriterion("pay_time is null");
            return (Criteria) this;
        }

        public Criteria andPayTimeIsNotNull() {
            addCriterion("pay_time is not null");
            return (Criteria) this;
        }

        public Criteria andPayTimeEqualTo(Date value) {
            addCriterion("pay_time =", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotEqualTo(Date value) {
            addCriterion("pay_time <>", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeGreaterThan(Date value) {
            addCriterion("pay_time >", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("pay_time >=", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeLessThan(Date value) {
            addCriterion("pay_time <", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeLessThanOrEqualTo(Date value) {
            addCriterion("pay_time <=", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeIn(List<Date> values) {
            addCriterion("pay_time in", values, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotIn(List<Date> values) {
            addCriterion("pay_time not in", values, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeBetween(Date value1, Date value2) {
            addCriterion("pay_time between", value1, value2, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotBetween(Date value1, Date value2) {
            addCriterion("pay_time not between", value1, value2, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayRemarkIsNull() {
            addCriterion("pay_remark is null");
            return (Criteria) this;
        }

        public Criteria andPayRemarkIsNotNull() {
            addCriterion("pay_remark is not null");
            return (Criteria) this;
        }

        public Criteria andPayRemarkEqualTo(String value) {
            addCriterion("pay_remark =", value, "payRemark");
            return (Criteria) this;
        }

        public Criteria andPayRemarkNotEqualTo(String value) {
            addCriterion("pay_remark <>", value, "payRemark");
            return (Criteria) this;
        }

        public Criteria andPayRemarkGreaterThan(String value) {
            addCriterion("pay_remark >", value, "payRemark");
            return (Criteria) this;
        }

        public Criteria andPayRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("pay_remark >=", value, "payRemark");
            return (Criteria) this;
        }

        public Criteria andPayRemarkLessThan(String value) {
            addCriterion("pay_remark <", value, "payRemark");
            return (Criteria) this;
        }

        public Criteria andPayRemarkLessThanOrEqualTo(String value) {
            addCriterion("pay_remark <=", value, "payRemark");
            return (Criteria) this;
        }

        public Criteria andPayRemarkLike(String value) {
            addCriterion("pay_remark like", value, "payRemark");
            return (Criteria) this;
        }

        public Criteria andPayRemarkNotLike(String value) {
            addCriterion("pay_remark not like", value, "payRemark");
            return (Criteria) this;
        }

        public Criteria andPayRemarkIn(List<String> values) {
            addCriterion("pay_remark in", values, "payRemark");
            return (Criteria) this;
        }

        public Criteria andPayRemarkNotIn(List<String> values) {
            addCriterion("pay_remark not in", values, "payRemark");
            return (Criteria) this;
        }

        public Criteria andPayRemarkBetween(String value1, String value2) {
            addCriterion("pay_remark between", value1, value2, "payRemark");
            return (Criteria) this;
        }

        public Criteria andPayRemarkNotBetween(String value1, String value2) {
            addCriterion("pay_remark not between", value1, value2, "payRemark");
            return (Criteria) this;
        }

        public Criteria andPayToThirdIsNull() {
            addCriterion("pay_to_third is null");
            return (Criteria) this;
        }

        public Criteria andPayToThirdIsNotNull() {
            addCriterion("pay_to_third is not null");
            return (Criteria) this;
        }

        public Criteria andPayToThirdEqualTo(Integer value) {
            addCriterion("pay_to_third =", value, "payToThird");
            return (Criteria) this;
        }

        public Criteria andPayToThirdNotEqualTo(Integer value) {
            addCriterion("pay_to_third <>", value, "payToThird");
            return (Criteria) this;
        }

        public Criteria andPayToThirdGreaterThan(Integer value) {
            addCriterion("pay_to_third >", value, "payToThird");
            return (Criteria) this;
        }

        public Criteria andPayToThirdGreaterThanOrEqualTo(Integer value) {
            addCriterion("pay_to_third >=", value, "payToThird");
            return (Criteria) this;
        }

        public Criteria andPayToThirdLessThan(Integer value) {
            addCriterion("pay_to_third <", value, "payToThird");
            return (Criteria) this;
        }

        public Criteria andPayToThirdLessThanOrEqualTo(Integer value) {
            addCriterion("pay_to_third <=", value, "payToThird");
            return (Criteria) this;
        }

        public Criteria andPayToThirdIn(List<Integer> values) {
            addCriterion("pay_to_third in", values, "payToThird");
            return (Criteria) this;
        }

        public Criteria andPayToThirdNotIn(List<Integer> values) {
            addCriterion("pay_to_third not in", values, "payToThird");
            return (Criteria) this;
        }

        public Criteria andPayToThirdBetween(Integer value1, Integer value2) {
            addCriterion("pay_to_third between", value1, value2, "payToThird");
            return (Criteria) this;
        }

        public Criteria andPayToThirdNotBetween(Integer value1, Integer value2) {
            addCriterion("pay_to_third not between", value1, value2, "payToThird");
            return (Criteria) this;
        }

        public Criteria andPayFileIsNull() {
            addCriterion("pay_file is null");
            return (Criteria) this;
        }

        public Criteria andPayFileIsNotNull() {
            addCriterion("pay_file is not null");
            return (Criteria) this;
        }

        public Criteria andPayFileEqualTo(String value) {
            addCriterion("pay_file =", value, "payFile");
            return (Criteria) this;
        }

        public Criteria andPayFileNotEqualTo(String value) {
            addCriterion("pay_file <>", value, "payFile");
            return (Criteria) this;
        }

        public Criteria andPayFileGreaterThan(String value) {
            addCriterion("pay_file >", value, "payFile");
            return (Criteria) this;
        }

        public Criteria andPayFileGreaterThanOrEqualTo(String value) {
            addCriterion("pay_file >=", value, "payFile");
            return (Criteria) this;
        }

        public Criteria andPayFileLessThan(String value) {
            addCriterion("pay_file <", value, "payFile");
            return (Criteria) this;
        }

        public Criteria andPayFileLessThanOrEqualTo(String value) {
            addCriterion("pay_file <=", value, "payFile");
            return (Criteria) this;
        }

        public Criteria andPayFileLike(String value) {
            addCriterion("pay_file like", value, "payFile");
            return (Criteria) this;
        }

        public Criteria andPayFileNotLike(String value) {
            addCriterion("pay_file not like", value, "payFile");
            return (Criteria) this;
        }

        public Criteria andPayFileIn(List<String> values) {
            addCriterion("pay_file in", values, "payFile");
            return (Criteria) this;
        }

        public Criteria andPayFileNotIn(List<String> values) {
            addCriterion("pay_file not in", values, "payFile");
            return (Criteria) this;
        }

        public Criteria andPayFileBetween(String value1, String value2) {
            addCriterion("pay_file between", value1, value2, "payFile");
            return (Criteria) this;
        }

        public Criteria andPayFileNotBetween(String value1, String value2) {
            addCriterion("pay_file not between", value1, value2, "payFile");
            return (Criteria) this;
        }

        public Criteria andContractIdIsNull() {
            addCriterion("contract_id is null");
            return (Criteria) this;
        }

        public Criteria andContractIdIsNotNull() {
            addCriterion("contract_id is not null");
            return (Criteria) this;
        }

        public Criteria andContractIdEqualTo(String value) {
            addCriterion("contract_id =", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdNotEqualTo(String value) {
            addCriterion("contract_id <>", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdGreaterThan(String value) {
            addCriterion("contract_id >", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdGreaterThanOrEqualTo(String value) {
            addCriterion("contract_id >=", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdLessThan(String value) {
            addCriterion("contract_id <", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdLessThanOrEqualTo(String value) {
            addCriterion("contract_id <=", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdLike(String value) {
            addCriterion("contract_id like", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdNotLike(String value) {
            addCriterion("contract_id not like", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdIn(List<String> values) {
            addCriterion("contract_id in", values, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdNotIn(List<String> values) {
            addCriterion("contract_id not in", values, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdBetween(String value1, String value2) {
            addCriterion("contract_id between", value1, value2, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdNotBetween(String value1, String value2) {
            addCriterion("contract_id not between", value1, value2, "contractId");
            return (Criteria) this;
        }

        public Criteria andFeedbackRemarkIsNull() {
            addCriterion("feedback_remark is null");
            return (Criteria) this;
        }

        public Criteria andFeedbackRemarkIsNotNull() {
            addCriterion("feedback_remark is not null");
            return (Criteria) this;
        }

        public Criteria andFeedbackRemarkEqualTo(String value) {
            addCriterion("feedback_remark =", value, "feedbackRemark");
            return (Criteria) this;
        }

        public Criteria andFeedbackRemarkNotEqualTo(String value) {
            addCriterion("feedback_remark <>", value, "feedbackRemark");
            return (Criteria) this;
        }

        public Criteria andFeedbackRemarkGreaterThan(String value) {
            addCriterion("feedback_remark >", value, "feedbackRemark");
            return (Criteria) this;
        }

        public Criteria andFeedbackRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("feedback_remark >=", value, "feedbackRemark");
            return (Criteria) this;
        }

        public Criteria andFeedbackRemarkLessThan(String value) {
            addCriterion("feedback_remark <", value, "feedbackRemark");
            return (Criteria) this;
        }

        public Criteria andFeedbackRemarkLessThanOrEqualTo(String value) {
            addCriterion("feedback_remark <=", value, "feedbackRemark");
            return (Criteria) this;
        }

        public Criteria andFeedbackRemarkLike(String value) {
            addCriterion("feedback_remark like", value, "feedbackRemark");
            return (Criteria) this;
        }

        public Criteria andFeedbackRemarkNotLike(String value) {
            addCriterion("feedback_remark not like", value, "feedbackRemark");
            return (Criteria) this;
        }

        public Criteria andFeedbackRemarkIn(List<String> values) {
            addCriterion("feedback_remark in", values, "feedbackRemark");
            return (Criteria) this;
        }

        public Criteria andFeedbackRemarkNotIn(List<String> values) {
            addCriterion("feedback_remark not in", values, "feedbackRemark");
            return (Criteria) this;
        }

        public Criteria andFeedbackRemarkBetween(String value1, String value2) {
            addCriterion("feedback_remark between", value1, value2, "feedbackRemark");
            return (Criteria) this;
        }

        public Criteria andFeedbackRemarkNotBetween(String value1, String value2) {
            addCriterion("feedback_remark not between", value1, value2, "feedbackRemark");
            return (Criteria) this;
        }

        public Criteria andVerfiryTimeIsNull() {
            addCriterion("verfiry_time is null");
            return (Criteria) this;
        }

        public Criteria andVerfiryTimeIsNotNull() {
            addCriterion("verfiry_time is not null");
            return (Criteria) this;
        }

        public Criteria andVerfiryTimeEqualTo(Date value) {
            addCriterion("verfiry_time =", value, "verfiryTime");
            return (Criteria) this;
        }

        public Criteria andVerfiryTimeNotEqualTo(Date value) {
            addCriterion("verfiry_time <>", value, "verfiryTime");
            return (Criteria) this;
        }

        public Criteria andVerfiryTimeGreaterThan(Date value) {
            addCriterion("verfiry_time >", value, "verfiryTime");
            return (Criteria) this;
        }

        public Criteria andVerfiryTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("verfiry_time >=", value, "verfiryTime");
            return (Criteria) this;
        }

        public Criteria andVerfiryTimeLessThan(Date value) {
            addCriterion("verfiry_time <", value, "verfiryTime");
            return (Criteria) this;
        }

        public Criteria andVerfiryTimeLessThanOrEqualTo(Date value) {
            addCriterion("verfiry_time <=", value, "verfiryTime");
            return (Criteria) this;
        }

        public Criteria andVerfiryTimeIn(List<Date> values) {
            addCriterion("verfiry_time in", values, "verfiryTime");
            return (Criteria) this;
        }

        public Criteria andVerfiryTimeNotIn(List<Date> values) {
            addCriterion("verfiry_time not in", values, "verfiryTime");
            return (Criteria) this;
        }

        public Criteria andVerfiryTimeBetween(Date value1, Date value2) {
            addCriterion("verfiry_time between", value1, value2, "verfiryTime");
            return (Criteria) this;
        }

        public Criteria andVerfiryTimeNotBetween(Date value1, Date value2) {
            addCriterion("verfiry_time not between", value1, value2, "verfiryTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}