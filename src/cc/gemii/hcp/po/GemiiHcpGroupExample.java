package cc.gemii.hcp.po;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GemiiHcpGroupExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GemiiHcpGroupExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAreaNameIsNull() {
            addCriterion("area_name is null");
            return (Criteria) this;
        }

        public Criteria andAreaNameIsNotNull() {
            addCriterion("area_name is not null");
            return (Criteria) this;
        }

        public Criteria andAreaNameEqualTo(String value) {
            addCriterion("area_name =", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotEqualTo(String value) {
            addCriterion("area_name <>", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameGreaterThan(String value) {
            addCriterion("area_name >", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameGreaterThanOrEqualTo(String value) {
            addCriterion("area_name >=", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameLessThan(String value) {
            addCriterion("area_name <", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameLessThanOrEqualTo(String value) {
            addCriterion("area_name <=", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameLike(String value) {
            addCriterion("area_name like", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotLike(String value) {
            addCriterion("area_name not like", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameIn(List<String> values) {
            addCriterion("area_name in", values, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotIn(List<String> values) {
            addCriterion("area_name not in", values, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameBetween(String value1, String value2) {
            addCriterion("area_name between", value1, value2, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotBetween(String value1, String value2) {
            addCriterion("area_name not between", value1, value2, "areaName");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNull() {
            addCriterion("province is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNotNull() {
            addCriterion("province is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceEqualTo(String value) {
            addCriterion("province =", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotEqualTo(String value) {
            addCriterion("province <>", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThan(String value) {
            addCriterion("province >", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("province >=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThan(String value) {
            addCriterion("province <", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThanOrEqualTo(String value) {
            addCriterion("province <=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLike(String value) {
            addCriterion("province like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotLike(String value) {
            addCriterion("province not like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceIn(List<String> values) {
            addCriterion("province in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotIn(List<String> values) {
            addCriterion("province not in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceBetween(String value1, String value2) {
            addCriterion("province between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotBetween(String value1, String value2) {
            addCriterion("province not between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andCityIsNull() {
            addCriterion("city is null");
            return (Criteria) this;
        }

        public Criteria andCityIsNotNull() {
            addCriterion("city is not null");
            return (Criteria) this;
        }

        public Criteria andCityEqualTo(String value) {
            addCriterion("city =", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotEqualTo(String value) {
            addCriterion("city <>", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThan(String value) {
            addCriterion("city >", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThanOrEqualTo(String value) {
            addCriterion("city >=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThan(String value) {
            addCriterion("city <", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThanOrEqualTo(String value) {
            addCriterion("city <=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLike(String value) {
            addCriterion("city like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotLike(String value) {
            addCriterion("city not like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityIn(List<String> values) {
            addCriterion("city in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotIn(List<String> values) {
            addCriterion("city not in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityBetween(String value1, String value2) {
            addCriterion("city between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotBetween(String value1, String value2) {
            addCriterion("city not between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andDepartmentIsNull() {
            addCriterion("department is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentIsNotNull() {
            addCriterion("department is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentEqualTo(String value) {
            addCriterion("department =", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentNotEqualTo(String value) {
            addCriterion("department <>", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentGreaterThan(String value) {
            addCriterion("department >", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentGreaterThanOrEqualTo(String value) {
            addCriterion("department >=", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentLessThan(String value) {
            addCriterion("department <", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentLessThanOrEqualTo(String value) {
            addCriterion("department <=", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentLike(String value) {
            addCriterion("department like", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentNotLike(String value) {
            addCriterion("department not like", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentIn(List<String> values) {
            addCriterion("department in", values, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentNotIn(List<String> values) {
            addCriterion("department not in", values, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentBetween(String value1, String value2) {
            addCriterion("department between", value1, value2, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentNotBetween(String value1, String value2) {
            addCriterion("department not between", value1, value2, "department");
            return (Criteria) this;
        }

        public Criteria andGroupTypeIsNull() {
            addCriterion("group_type is null");
            return (Criteria) this;
        }

        public Criteria andGroupTypeIsNotNull() {
            addCriterion("group_type is not null");
            return (Criteria) this;
        }

        public Criteria andGroupTypeEqualTo(String value) {
            addCriterion("group_type =", value, "groupType");
            return (Criteria) this;
        }

        public Criteria andGroupTypeNotEqualTo(String value) {
            addCriterion("group_type <>", value, "groupType");
            return (Criteria) this;
        }

        public Criteria andGroupTypeGreaterThan(String value) {
            addCriterion("group_type >", value, "groupType");
            return (Criteria) this;
        }

        public Criteria andGroupTypeGreaterThanOrEqualTo(String value) {
            addCriterion("group_type >=", value, "groupType");
            return (Criteria) this;
        }

        public Criteria andGroupTypeLessThan(String value) {
            addCriterion("group_type <", value, "groupType");
            return (Criteria) this;
        }

        public Criteria andGroupTypeLessThanOrEqualTo(String value) {
            addCriterion("group_type <=", value, "groupType");
            return (Criteria) this;
        }

        public Criteria andGroupTypeLike(String value) {
            addCriterion("group_type like", value, "groupType");
            return (Criteria) this;
        }

        public Criteria andGroupTypeNotLike(String value) {
            addCriterion("group_type not like", value, "groupType");
            return (Criteria) this;
        }

        public Criteria andGroupTypeIn(List<String> values) {
            addCriterion("group_type in", values, "groupType");
            return (Criteria) this;
        }

        public Criteria andGroupTypeNotIn(List<String> values) {
            addCriterion("group_type not in", values, "groupType");
            return (Criteria) this;
        }

        public Criteria andGroupTypeBetween(String value1, String value2) {
            addCriterion("group_type between", value1, value2, "groupType");
            return (Criteria) this;
        }

        public Criteria andGroupTypeNotBetween(String value1, String value2) {
            addCriterion("group_type not between", value1, value2, "groupType");
            return (Criteria) this;
        }

        public Criteria andGroupNameIsNull() {
            addCriterion("group_name is null");
            return (Criteria) this;
        }

        public Criteria andGroupNameIsNotNull() {
            addCriterion("group_name is not null");
            return (Criteria) this;
        }

        public Criteria andGroupNameEqualTo(String value) {
            addCriterion("group_name =", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameNotEqualTo(String value) {
            addCriterion("group_name <>", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameGreaterThan(String value) {
            addCriterion("group_name >", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameGreaterThanOrEqualTo(String value) {
            addCriterion("group_name >=", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameLessThan(String value) {
            addCriterion("group_name <", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameLessThanOrEqualTo(String value) {
            addCriterion("group_name <=", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameLike(String value) {
            addCriterion("group_name like", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameNotLike(String value) {
            addCriterion("group_name not like", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameIn(List<String> values) {
            addCriterion("group_name in", values, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameNotIn(List<String> values) {
            addCriterion("group_name not in", values, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameBetween(String value1, String value2) {
            addCriterion("group_name between", value1, value2, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameNotBetween(String value1, String value2) {
            addCriterion("group_name not between", value1, value2, "groupName");
            return (Criteria) this;
        }

        public Criteria andWyethIdIsNull() {
            addCriterion("wyeth_id is null");
            return (Criteria) this;
        }

        public Criteria andWyethIdIsNotNull() {
            addCriterion("wyeth_id is not null");
            return (Criteria) this;
        }

        public Criteria andWyethIdEqualTo(String value) {
            addCriterion("wyeth_id =", value, "wyethId");
            return (Criteria) this;
        }

        public Criteria andWyethIdNotEqualTo(String value) {
            addCriterion("wyeth_id <>", value, "wyethId");
            return (Criteria) this;
        }

        public Criteria andWyethIdGreaterThan(String value) {
            addCriterion("wyeth_id >", value, "wyethId");
            return (Criteria) this;
        }

        public Criteria andWyethIdGreaterThanOrEqualTo(String value) {
            addCriterion("wyeth_id >=", value, "wyethId");
            return (Criteria) this;
        }

        public Criteria andWyethIdLessThan(String value) {
            addCriterion("wyeth_id <", value, "wyethId");
            return (Criteria) this;
        }

        public Criteria andWyethIdLessThanOrEqualTo(String value) {
            addCriterion("wyeth_id <=", value, "wyethId");
            return (Criteria) this;
        }

        public Criteria andWyethIdLike(String value) {
            addCriterion("wyeth_id like", value, "wyethId");
            return (Criteria) this;
        }

        public Criteria andWyethIdNotLike(String value) {
            addCriterion("wyeth_id not like", value, "wyethId");
            return (Criteria) this;
        }

        public Criteria andWyethIdIn(List<String> values) {
            addCriterion("wyeth_id in", values, "wyethId");
            return (Criteria) this;
        }

        public Criteria andWyethIdNotIn(List<String> values) {
            addCriterion("wyeth_id not in", values, "wyethId");
            return (Criteria) this;
        }

        public Criteria andWyethIdBetween(String value1, String value2) {
            addCriterion("wyeth_id between", value1, value2, "wyethId");
            return (Criteria) this;
        }

        public Criteria andWyethIdNotBetween(String value1, String value2) {
            addCriterion("wyeth_id not between", value1, value2, "wyethId");
            return (Criteria) this;
        }

        public Criteria andHsptIdIsNull() {
            addCriterion("hspt_id is null");
            return (Criteria) this;
        }

        public Criteria andHsptIdIsNotNull() {
            addCriterion("hspt_id is not null");
            return (Criteria) this;
        }

        public Criteria andHsptIdEqualTo(String value) {
            addCriterion("hspt_id =", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdNotEqualTo(String value) {
            addCriterion("hspt_id <>", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdGreaterThan(String value) {
            addCriterion("hspt_id >", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdGreaterThanOrEqualTo(String value) {
            addCriterion("hspt_id >=", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdLessThan(String value) {
            addCriterion("hspt_id <", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdLessThanOrEqualTo(String value) {
            addCriterion("hspt_id <=", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdLike(String value) {
            addCriterion("hspt_id like", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdNotLike(String value) {
            addCriterion("hspt_id not like", value, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdIn(List<String> values) {
            addCriterion("hspt_id in", values, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdNotIn(List<String> values) {
            addCriterion("hspt_id not in", values, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdBetween(String value1, String value2) {
            addCriterion("hspt_id between", value1, value2, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptIdNotBetween(String value1, String value2) {
            addCriterion("hspt_id not between", value1, value2, "hsptId");
            return (Criteria) this;
        }

        public Criteria andHsptNameIsNull() {
            addCriterion("hspt_name is null");
            return (Criteria) this;
        }

        public Criteria andHsptNameIsNotNull() {
            addCriterion("hspt_name is not null");
            return (Criteria) this;
        }

        public Criteria andHsptNameEqualTo(String value) {
            addCriterion("hspt_name =", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameNotEqualTo(String value) {
            addCriterion("hspt_name <>", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameGreaterThan(String value) {
            addCriterion("hspt_name >", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameGreaterThanOrEqualTo(String value) {
            addCriterion("hspt_name >=", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameLessThan(String value) {
            addCriterion("hspt_name <", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameLessThanOrEqualTo(String value) {
            addCriterion("hspt_name <=", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameLike(String value) {
            addCriterion("hspt_name like", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameNotLike(String value) {
            addCriterion("hspt_name not like", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameIn(List<String> values) {
            addCriterion("hspt_name in", values, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameNotIn(List<String> values) {
            addCriterion("hspt_name not in", values, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameBetween(String value1, String value2) {
            addCriterion("hspt_name between", value1, value2, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameNotBetween(String value1, String value2) {
            addCriterion("hspt_name not between", value1, value2, "hsptName");
            return (Criteria) this;
        }

        public Criteria andAnchorcityIsNull() {
            addCriterion("anchorCity is null");
            return (Criteria) this;
        }

        public Criteria andAnchorcityIsNotNull() {
            addCriterion("anchorCity is not null");
            return (Criteria) this;
        }

        public Criteria andAnchorcityEqualTo(String value) {
            addCriterion("anchorCity =", value, "anchorcity");
            return (Criteria) this;
        }

        public Criteria andAnchorcityNotEqualTo(String value) {
            addCriterion("anchorCity <>", value, "anchorcity");
            return (Criteria) this;
        }

        public Criteria andAnchorcityGreaterThan(String value) {
            addCriterion("anchorCity >", value, "anchorcity");
            return (Criteria) this;
        }

        public Criteria andAnchorcityGreaterThanOrEqualTo(String value) {
            addCriterion("anchorCity >=", value, "anchorcity");
            return (Criteria) this;
        }

        public Criteria andAnchorcityLessThan(String value) {
            addCriterion("anchorCity <", value, "anchorcity");
            return (Criteria) this;
        }

        public Criteria andAnchorcityLessThanOrEqualTo(String value) {
            addCriterion("anchorCity <=", value, "anchorcity");
            return (Criteria) this;
        }

        public Criteria andAnchorcityLike(String value) {
            addCriterion("anchorCity like", value, "anchorcity");
            return (Criteria) this;
        }

        public Criteria andAnchorcityNotLike(String value) {
            addCriterion("anchorCity not like", value, "anchorcity");
            return (Criteria) this;
        }

        public Criteria andAnchorcityIn(List<String> values) {
            addCriterion("anchorCity in", values, "anchorcity");
            return (Criteria) this;
        }

        public Criteria andAnchorcityNotIn(List<String> values) {
            addCriterion("anchorCity not in", values, "anchorcity");
            return (Criteria) this;
        }

        public Criteria andAnchorcityBetween(String value1, String value2) {
            addCriterion("anchorCity between", value1, value2, "anchorcity");
            return (Criteria) this;
        }

        public Criteria andAnchorcityNotBetween(String value1, String value2) {
            addCriterion("anchorCity not between", value1, value2, "anchorcity");
            return (Criteria) this;
        }

        public Criteria andClustercityIsNull() {
            addCriterion("clusterCity is null");
            return (Criteria) this;
        }

        public Criteria andClustercityIsNotNull() {
            addCriterion("clusterCity is not null");
            return (Criteria) this;
        }

        public Criteria andClustercityEqualTo(String value) {
            addCriterion("clusterCity =", value, "clustercity");
            return (Criteria) this;
        }

        public Criteria andClustercityNotEqualTo(String value) {
            addCriterion("clusterCity <>", value, "clustercity");
            return (Criteria) this;
        }

        public Criteria andClustercityGreaterThan(String value) {
            addCriterion("clusterCity >", value, "clustercity");
            return (Criteria) this;
        }

        public Criteria andClustercityGreaterThanOrEqualTo(String value) {
            addCriterion("clusterCity >=", value, "clustercity");
            return (Criteria) this;
        }

        public Criteria andClustercityLessThan(String value) {
            addCriterion("clusterCity <", value, "clustercity");
            return (Criteria) this;
        }

        public Criteria andClustercityLessThanOrEqualTo(String value) {
            addCriterion("clusterCity <=", value, "clustercity");
            return (Criteria) this;
        }

        public Criteria andClustercityLike(String value) {
            addCriterion("clusterCity like", value, "clustercity");
            return (Criteria) this;
        }

        public Criteria andClustercityNotLike(String value) {
            addCriterion("clusterCity not like", value, "clustercity");
            return (Criteria) this;
        }

        public Criteria andClustercityIn(List<String> values) {
            addCriterion("clusterCity in", values, "clustercity");
            return (Criteria) this;
        }

        public Criteria andClustercityNotIn(List<String> values) {
            addCriterion("clusterCity not in", values, "clustercity");
            return (Criteria) this;
        }

        public Criteria andClustercityBetween(String value1, String value2) {
            addCriterion("clusterCity between", value1, value2, "clustercity");
            return (Criteria) this;
        }

        public Criteria andClustercityNotBetween(String value1, String value2) {
            addCriterion("clusterCity not between", value1, value2, "clustercity");
            return (Criteria) this;
        }

        public Criteria andCtimeIsNull() {
            addCriterion("ctime is null");
            return (Criteria) this;
        }

        public Criteria andCtimeIsNotNull() {
            addCriterion("ctime is not null");
            return (Criteria) this;
        }

        public Criteria andCtimeEqualTo(Date value) {
            addCriterion("ctime =", value, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeNotEqualTo(Date value) {
            addCriterion("ctime <>", value, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeGreaterThan(Date value) {
            addCriterion("ctime >", value, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeGreaterThanOrEqualTo(Date value) {
            addCriterion("ctime >=", value, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeLessThan(Date value) {
            addCriterion("ctime <", value, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeLessThanOrEqualTo(Date value) {
            addCriterion("ctime <=", value, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeIn(List<Date> values) {
            addCriterion("ctime in", values, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeNotIn(List<Date> values) {
            addCriterion("ctime not in", values, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeBetween(Date value1, Date value2) {
            addCriterion("ctime between", value1, value2, "ctime");
            return (Criteria) this;
        }

        public Criteria andCtimeNotBetween(Date value1, Date value2) {
            addCriterion("ctime not between", value1, value2, "ctime");
            return (Criteria) this;
        }

        public Criteria andSquadNumberIsNull() {
            addCriterion("squad_number is null");
            return (Criteria) this;
        }

        public Criteria andSquadNumberIsNotNull() {
            addCriterion("squad_number is not null");
            return (Criteria) this;
        }

        public Criteria andSquadNumberEqualTo(Integer value) {
            addCriterion("squad_number =", value, "squadNumber");
            return (Criteria) this;
        }

        public Criteria andSquadNumberNotEqualTo(Integer value) {
            addCriterion("squad_number <>", value, "squadNumber");
            return (Criteria) this;
        }

        public Criteria andSquadNumberGreaterThan(Integer value) {
            addCriterion("squad_number >", value, "squadNumber");
            return (Criteria) this;
        }

        public Criteria andSquadNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("squad_number >=", value, "squadNumber");
            return (Criteria) this;
        }

        public Criteria andSquadNumberLessThan(Integer value) {
            addCriterion("squad_number <", value, "squadNumber");
            return (Criteria) this;
        }

        public Criteria andSquadNumberLessThanOrEqualTo(Integer value) {
            addCriterion("squad_number <=", value, "squadNumber");
            return (Criteria) this;
        }

        public Criteria andSquadNumberIn(List<Integer> values) {
            addCriterion("squad_number in", values, "squadNumber");
            return (Criteria) this;
        }

        public Criteria andSquadNumberNotIn(List<Integer> values) {
            addCriterion("squad_number not in", values, "squadNumber");
            return (Criteria) this;
        }

        public Criteria andSquadNumberBetween(Integer value1, Integer value2) {
            addCriterion("squad_number between", value1, value2, "squadNumber");
            return (Criteria) this;
        }

        public Criteria andSquadNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("squad_number not between", value1, value2, "squadNumber");
            return (Criteria) this;
        }

        public Criteria andHelperNumberIsNull() {
            addCriterion("helper_number is null");
            return (Criteria) this;
        }

        public Criteria andHelperNumberIsNotNull() {
            addCriterion("helper_number is not null");
            return (Criteria) this;
        }

        public Criteria andHelperNumberEqualTo(Integer value) {
            addCriterion("helper_number =", value, "helperNumber");
            return (Criteria) this;
        }

        public Criteria andHelperNumberNotEqualTo(Integer value) {
            addCriterion("helper_number <>", value, "helperNumber");
            return (Criteria) this;
        }

        public Criteria andHelperNumberGreaterThan(Integer value) {
            addCriterion("helper_number >", value, "helperNumber");
            return (Criteria) this;
        }

        public Criteria andHelperNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("helper_number >=", value, "helperNumber");
            return (Criteria) this;
        }

        public Criteria andHelperNumberLessThan(Integer value) {
            addCriterion("helper_number <", value, "helperNumber");
            return (Criteria) this;
        }

        public Criteria andHelperNumberLessThanOrEqualTo(Integer value) {
            addCriterion("helper_number <=", value, "helperNumber");
            return (Criteria) this;
        }

        public Criteria andHelperNumberIn(List<Integer> values) {
            addCriterion("helper_number in", values, "helperNumber");
            return (Criteria) this;
        }

        public Criteria andHelperNumberNotIn(List<Integer> values) {
            addCriterion("helper_number not in", values, "helperNumber");
            return (Criteria) this;
        }

        public Criteria andHelperNumberBetween(Integer value1, Integer value2) {
            addCriterion("helper_number between", value1, value2, "helperNumber");
            return (Criteria) this;
        }

        public Criteria andHelperNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("helper_number not between", value1, value2, "helperNumber");
            return (Criteria) this;
        }

        public Criteria andBotNumberIsNull() {
            addCriterion("bot_number is null");
            return (Criteria) this;
        }

        public Criteria andBotNumberIsNotNull() {
            addCriterion("bot_number is not null");
            return (Criteria) this;
        }

        public Criteria andBotNumberEqualTo(Integer value) {
            addCriterion("bot_number =", value, "botNumber");
            return (Criteria) this;
        }

        public Criteria andBotNumberNotEqualTo(Integer value) {
            addCriterion("bot_number <>", value, "botNumber");
            return (Criteria) this;
        }

        public Criteria andBotNumberGreaterThan(Integer value) {
            addCriterion("bot_number >", value, "botNumber");
            return (Criteria) this;
        }

        public Criteria andBotNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("bot_number >=", value, "botNumber");
            return (Criteria) this;
        }

        public Criteria andBotNumberLessThan(Integer value) {
            addCriterion("bot_number <", value, "botNumber");
            return (Criteria) this;
        }

        public Criteria andBotNumberLessThanOrEqualTo(Integer value) {
            addCriterion("bot_number <=", value, "botNumber");
            return (Criteria) this;
        }

        public Criteria andBotNumberIn(List<Integer> values) {
            addCriterion("bot_number in", values, "botNumber");
            return (Criteria) this;
        }

        public Criteria andBotNumberNotIn(List<Integer> values) {
            addCriterion("bot_number not in", values, "botNumber");
            return (Criteria) this;
        }

        public Criteria andBotNumberBetween(Integer value1, Integer value2) {
            addCriterion("bot_number between", value1, value2, "botNumber");
            return (Criteria) this;
        }

        public Criteria andBotNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("bot_number not between", value1, value2, "botNumber");
            return (Criteria) this;
        }

        public Criteria andKomNumberIsNull() {
            addCriterion("kom_number is null");
            return (Criteria) this;
        }

        public Criteria andKomNumberIsNotNull() {
            addCriterion("kom_number is not null");
            return (Criteria) this;
        }

        public Criteria andKomNumberEqualTo(Integer value) {
            addCriterion("kom_number =", value, "komNumber");
            return (Criteria) this;
        }

        public Criteria andKomNumberNotEqualTo(Integer value) {
            addCriterion("kom_number <>", value, "komNumber");
            return (Criteria) this;
        }

        public Criteria andKomNumberGreaterThan(Integer value) {
            addCriterion("kom_number >", value, "komNumber");
            return (Criteria) this;
        }

        public Criteria andKomNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("kom_number >=", value, "komNumber");
            return (Criteria) this;
        }

        public Criteria andKomNumberLessThan(Integer value) {
            addCriterion("kom_number <", value, "komNumber");
            return (Criteria) this;
        }

        public Criteria andKomNumberLessThanOrEqualTo(Integer value) {
            addCriterion("kom_number <=", value, "komNumber");
            return (Criteria) this;
        }

        public Criteria andKomNumberIn(List<Integer> values) {
            addCriterion("kom_number in", values, "komNumber");
            return (Criteria) this;
        }

        public Criteria andKomNumberNotIn(List<Integer> values) {
            addCriterion("kom_number not in", values, "komNumber");
            return (Criteria) this;
        }

        public Criteria andKomNumberBetween(Integer value1, Integer value2) {
            addCriterion("kom_number between", value1, value2, "komNumber");
            return (Criteria) this;
        }

        public Criteria andKomNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("kom_number not between", value1, value2, "komNumber");
            return (Criteria) this;
        }

        public Criteria andAddWhitelistNumberIsNull() {
            addCriterion("add_whitelist_number is null");
            return (Criteria) this;
        }

        public Criteria andAddWhitelistNumberIsNotNull() {
            addCriterion("add_whitelist_number is not null");
            return (Criteria) this;
        }

        public Criteria andAddWhitelistNumberEqualTo(Integer value) {
            addCriterion("add_whitelist_number =", value, "addWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andAddWhitelistNumberNotEqualTo(Integer value) {
            addCriterion("add_whitelist_number <>", value, "addWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andAddWhitelistNumberGreaterThan(Integer value) {
            addCriterion("add_whitelist_number >", value, "addWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andAddWhitelistNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("add_whitelist_number >=", value, "addWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andAddWhitelistNumberLessThan(Integer value) {
            addCriterion("add_whitelist_number <", value, "addWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andAddWhitelistNumberLessThanOrEqualTo(Integer value) {
            addCriterion("add_whitelist_number <=", value, "addWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andAddWhitelistNumberIn(List<Integer> values) {
            addCriterion("add_whitelist_number in", values, "addWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andAddWhitelistNumberNotIn(List<Integer> values) {
            addCriterion("add_whitelist_number not in", values, "addWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andAddWhitelistNumberBetween(Integer value1, Integer value2) {
            addCriterion("add_whitelist_number between", value1, value2, "addWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andAddWhitelistNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("add_whitelist_number not between", value1, value2, "addWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andExitWhitelistNumberIsNull() {
            addCriterion("exit_whitelist_number is null");
            return (Criteria) this;
        }

        public Criteria andExitWhitelistNumberIsNotNull() {
            addCriterion("exit_whitelist_number is not null");
            return (Criteria) this;
        }

        public Criteria andExitWhitelistNumberEqualTo(Integer value) {
            addCriterion("exit_whitelist_number =", value, "exitWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andExitWhitelistNumberNotEqualTo(Integer value) {
            addCriterion("exit_whitelist_number <>", value, "exitWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andExitWhitelistNumberGreaterThan(Integer value) {
            addCriterion("exit_whitelist_number >", value, "exitWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andExitWhitelistNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("exit_whitelist_number >=", value, "exitWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andExitWhitelistNumberLessThan(Integer value) {
            addCriterion("exit_whitelist_number <", value, "exitWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andExitWhitelistNumberLessThanOrEqualTo(Integer value) {
            addCriterion("exit_whitelist_number <=", value, "exitWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andExitWhitelistNumberIn(List<Integer> values) {
            addCriterion("exit_whitelist_number in", values, "exitWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andExitWhitelistNumberNotIn(List<Integer> values) {
            addCriterion("exit_whitelist_number not in", values, "exitWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andExitWhitelistNumberBetween(Integer value1, Integer value2) {
            addCriterion("exit_whitelist_number between", value1, value2, "exitWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andExitWhitelistNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("exit_whitelist_number not between", value1, value2, "exitWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andNowWhitelistNumberIsNull() {
            addCriterion("now_whitelist_number is null");
            return (Criteria) this;
        }

        public Criteria andNowWhitelistNumberIsNotNull() {
            addCriterion("now_whitelist_number is not null");
            return (Criteria) this;
        }

        public Criteria andNowWhitelistNumberEqualTo(Integer value) {
            addCriterion("now_whitelist_number =", value, "nowWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andNowWhitelistNumberNotEqualTo(Integer value) {
            addCriterion("now_whitelist_number <>", value, "nowWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andNowWhitelistNumberGreaterThan(Integer value) {
            addCriterion("now_whitelist_number >", value, "nowWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andNowWhitelistNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("now_whitelist_number >=", value, "nowWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andNowWhitelistNumberLessThan(Integer value) {
            addCriterion("now_whitelist_number <", value, "nowWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andNowWhitelistNumberLessThanOrEqualTo(Integer value) {
            addCriterion("now_whitelist_number <=", value, "nowWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andNowWhitelistNumberIn(List<Integer> values) {
            addCriterion("now_whitelist_number in", values, "nowWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andNowWhitelistNumberNotIn(List<Integer> values) {
            addCriterion("now_whitelist_number not in", values, "nowWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andNowWhitelistNumberBetween(Integer value1, Integer value2) {
            addCriterion("now_whitelist_number between", value1, value2, "nowWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andNowWhitelistNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("now_whitelist_number not between", value1, value2, "nowWhitelistNumber");
            return (Criteria) this;
        }

        public Criteria andTotalNumberIsNull() {
            addCriterion("total_number is null");
            return (Criteria) this;
        }

        public Criteria andTotalNumberIsNotNull() {
            addCriterion("total_number is not null");
            return (Criteria) this;
        }

        public Criteria andTotalNumberEqualTo(Integer value) {
            addCriterion("total_number =", value, "totalNumber");
            return (Criteria) this;
        }

        public Criteria andTotalNumberNotEqualTo(Integer value) {
            addCriterion("total_number <>", value, "totalNumber");
            return (Criteria) this;
        }

        public Criteria andTotalNumberGreaterThan(Integer value) {
            addCriterion("total_number >", value, "totalNumber");
            return (Criteria) this;
        }

        public Criteria andTotalNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("total_number >=", value, "totalNumber");
            return (Criteria) this;
        }

        public Criteria andTotalNumberLessThan(Integer value) {
            addCriterion("total_number <", value, "totalNumber");
            return (Criteria) this;
        }

        public Criteria andTotalNumberLessThanOrEqualTo(Integer value) {
            addCriterion("total_number <=", value, "totalNumber");
            return (Criteria) this;
        }

        public Criteria andTotalNumberIn(List<Integer> values) {
            addCriterion("total_number in", values, "totalNumber");
            return (Criteria) this;
        }

        public Criteria andTotalNumberNotIn(List<Integer> values) {
            addCriterion("total_number not in", values, "totalNumber");
            return (Criteria) this;
        }

        public Criteria andTotalNumberBetween(Integer value1, Integer value2) {
            addCriterion("total_number between", value1, value2, "totalNumber");
            return (Criteria) this;
        }

        public Criteria andTotalNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("total_number not between", value1, value2, "totalNumber");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}