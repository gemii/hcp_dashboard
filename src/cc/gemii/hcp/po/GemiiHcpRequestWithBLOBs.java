package cc.gemii.hcp.po;

public class GemiiHcpRequestWithBLOBs extends GemiiHcpRequest {
    private String params;

    private String responseState;

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params == null ? null : params.trim();
    }

    public String getResponseState() {
        return responseState;
    }

    public void setResponseState(String responseState) {
        this.responseState = responseState == null ? null : responseState.trim();
    }
}