package cc.gemii.hcp.po;

import java.util.Date;

public class GemiiHcpDoctor {
    private String id;

    private String hcpClass;

    private String hcpName;

    private String hcpInfo;

    private String hcpPhone;

    private String hsptId;

    private String hcpDepart;

    private String hcpPost;

    private String hcpTitle;

    private String hcpTitleFile;

    private Integer hcpYears;

    private String hcpSocial;

    private String hcpSocialDesp;

    private String hcpPrefer;

    private String hcpActivity;

    private Integer hcpMomTeacher;

    private Integer hcpMomConsultsant;

    private Date hcpCtime;

    private Integer hcpProfileFull;

    private String hcpAddition;

    private String contractId;

    private String oaName;

    private String oaPhone;

    private String remark;

    private String bank;

    private String bankCardNumber;

    private String bankCardName;

    private Integer staffId;

    private String verifyRemark;

    private Date verifyTime;

    private Integer state;

    private String idCard;

    private String updateRemark;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getHcpClass() {
        return hcpClass;
    }

    public void setHcpClass(String hcpClass) {
        this.hcpClass = hcpClass == null ? null : hcpClass.trim();
    }

    public String getHcpName() {
        return hcpName;
    }

    public void setHcpName(String hcpName) {
        this.hcpName = hcpName == null ? null : hcpName.trim();
    }

    public String getHcpInfo() {
        return hcpInfo;
    }

    public void setHcpInfo(String hcpInfo) {
        this.hcpInfo = hcpInfo == null ? null : hcpInfo.trim();
    }

    public String getHcpPhone() {
        return hcpPhone;
    }

    public void setHcpPhone(String hcpPhone) {
        this.hcpPhone = hcpPhone == null ? null : hcpPhone.trim();
    }

    public String getHsptId() {
        return hsptId;
    }

    public void setHsptId(String hsptId) {
        this.hsptId = hsptId == null ? null : hsptId.trim();
    }

    public String getHcpDepart() {
        return hcpDepart;
    }

    public void setHcpDepart(String hcpDepart) {
        this.hcpDepart = hcpDepart == null ? null : hcpDepart.trim();
    }

    public String getHcpPost() {
        return hcpPost;
    }

    public void setHcpPost(String hcpPost) {
        this.hcpPost = hcpPost == null ? null : hcpPost.trim();
    }

    public String getHcpTitle() {
        return hcpTitle;
    }

    public void setHcpTitle(String hcpTitle) {
        this.hcpTitle = hcpTitle == null ? null : hcpTitle.trim();
    }

    public String getHcpTitleFile() {
        return hcpTitleFile;
    }

    public void setHcpTitleFile(String hcpTitleFile) {
        this.hcpTitleFile = hcpTitleFile == null ? null : hcpTitleFile.trim();
    }

    public Integer getHcpYears() {
        return hcpYears;
    }

    public void setHcpYears(Integer hcpYears) {
        this.hcpYears = hcpYears;
    }

    public String getHcpSocial() {
        return hcpSocial;
    }

    public void setHcpSocial(String hcpSocial) {
        this.hcpSocial = hcpSocial == null ? null : hcpSocial.trim();
    }

    public String getHcpSocialDesp() {
        return hcpSocialDesp;
    }

    public void setHcpSocialDesp(String hcpSocialDesp) {
        this.hcpSocialDesp = hcpSocialDesp == null ? null : hcpSocialDesp.trim();
    }

    public String getHcpPrefer() {
        return hcpPrefer;
    }

    public void setHcpPrefer(String hcpPrefer) {
        this.hcpPrefer = hcpPrefer == null ? null : hcpPrefer.trim();
    }

    public String getHcpActivity() {
        return hcpActivity;
    }

    public void setHcpActivity(String hcpActivity) {
        this.hcpActivity = hcpActivity == null ? null : hcpActivity.trim();
    }

    public Integer getHcpMomTeacher() {
        return hcpMomTeacher;
    }

    public void setHcpMomTeacher(Integer hcpMomTeacher) {
        this.hcpMomTeacher = hcpMomTeacher;
    }

    public Integer getHcpMomConsultsant() {
        return hcpMomConsultsant;
    }

    public void setHcpMomConsultsant(Integer hcpMomConsultsant) {
        this.hcpMomConsultsant = hcpMomConsultsant;
    }

    public Date getHcpCtime() {
        return hcpCtime;
    }

    public void setHcpCtime(Date hcpCtime) {
        this.hcpCtime = hcpCtime;
    }

    public Integer getHcpProfileFull() {
        return hcpProfileFull;
    }

    public void setHcpProfileFull(Integer hcpProfileFull) {
        this.hcpProfileFull = hcpProfileFull;
    }

    public String getHcpAddition() {
        return hcpAddition;
    }

    public void setHcpAddition(String hcpAddition) {
        this.hcpAddition = hcpAddition == null ? null : hcpAddition.trim();
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId == null ? null : contractId.trim();
    }

    public String getOaName() {
        return oaName;
    }

    public void setOaName(String oaName) {
        this.oaName = oaName == null ? null : oaName.trim();
    }

    public String getOaPhone() {
        return oaPhone;
    }

    public void setOaPhone(String oaPhone) {
        this.oaPhone = oaPhone == null ? null : oaPhone.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank == null ? null : bank.trim();
    }

    public String getBankCardNumber() {
        return bankCardNumber;
    }

    public void setBankCardNumber(String bankCardNumber) {
        this.bankCardNumber = bankCardNumber == null ? null : bankCardNumber.trim();
    }

    public String getBankCardName() {
        return bankCardName;
    }

    public void setBankCardName(String bankCardName) {
        this.bankCardName = bankCardName == null ? null : bankCardName.trim();
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public String getVerifyRemark() {
        return verifyRemark;
    }

    public void setVerifyRemark(String verifyRemark) {
        this.verifyRemark = verifyRemark == null ? null : verifyRemark.trim();
    }

    public Date getVerifyTime() {
        return verifyTime;
    }

    public void setVerifyTime(Date verifyTime) {
        this.verifyTime = verifyTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard == null ? null : idCard.trim();
    }

    public String getUpdateRemark() {
        return updateRemark;
    }

    public void setUpdateRemark(String updateRemark) {
        this.updateRemark = updateRemark == null ? null : updateRemark.trim();
    }
}