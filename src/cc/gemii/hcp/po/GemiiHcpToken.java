package cc.gemii.hcp.po;

import java.util.Date;

public class GemiiHcpToken {
    private Integer id;

    private String token;

    private String plain;

    private String date;

    private String cipher;

    private Date ctime;

    private Date etime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token == null ? null : token.trim();
    }

    public String getPlain() {
        return plain;
    }

    public void setPlain(String plain) {
        this.plain = plain == null ? null : plain.trim();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date == null ? null : date.trim();
    }

    public String getCipher() {
        return cipher;
    }

    public void setCipher(String cipher) {
        this.cipher = cipher == null ? null : cipher.trim();
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Date getEtime() {
        return etime;
    }

    public void setEtime(Date etime) {
        this.etime = etime;
    }

	@Override
	public String toString() {
		return "GemiiHcpToken [cipher=" + cipher + ", plain=" + plain + ", date="
				+ date + "]";
	}
    
}