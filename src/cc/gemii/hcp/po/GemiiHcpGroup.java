package cc.gemii.hcp.po;

import java.util.Date;

public class GemiiHcpGroup {
    private String id;

    private String areaName;

    private String province;

    private String city;

    private String department;

    private String groupType;

    private String groupName;

    private String wyethId;

    private String hsptId;

    private String hsptName;

    private String anchorcity;

    private String clustercity;

    private Date ctime;

    private Integer squadNumber;

    private Integer helperNumber;

    private Integer botNumber;

    private Integer komNumber;

    private Integer addWhitelistNumber;

    private Integer exitWhitelistNumber;

    private Integer nowWhitelistNumber;

    private Integer totalNumber;

    private String remark;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName == null ? null : areaName.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department == null ? null : department.trim();
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType == null ? null : groupType.trim();
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName == null ? null : groupName.trim();
    }

    public String getWyethId() {
        return wyethId;
    }

    public void setWyethId(String wyethId) {
        this.wyethId = wyethId == null ? null : wyethId.trim();
    }

    public String getHsptId() {
        return hsptId;
    }

    public void setHsptId(String hsptId) {
        this.hsptId = hsptId == null ? null : hsptId.trim();
    }

    public String getHsptName() {
        return hsptName;
    }

    public void setHsptName(String hsptName) {
        this.hsptName = hsptName == null ? null : hsptName.trim();
    }

    public String getAnchorcity() {
        return anchorcity;
    }

    public void setAnchorcity(String anchorcity) {
        this.anchorcity = anchorcity == null ? null : anchorcity.trim();
    }

    public String getClustercity() {
        return clustercity;
    }

    public void setClustercity(String clustercity) {
        this.clustercity = clustercity == null ? null : clustercity.trim();
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Integer getSquadNumber() {
        return squadNumber;
    }

    public void setSquadNumber(Integer squadNumber) {
        this.squadNumber = squadNumber;
    }

    public Integer getHelperNumber() {
        return helperNumber;
    }

    public void setHelperNumber(Integer helperNumber) {
        this.helperNumber = helperNumber;
    }

    public Integer getBotNumber() {
        return botNumber;
    }

    public void setBotNumber(Integer botNumber) {
        this.botNumber = botNumber;
    }

    public Integer getKomNumber() {
        return komNumber;
    }

    public void setKomNumber(Integer komNumber) {
        this.komNumber = komNumber;
    }

    public Integer getAddWhitelistNumber() {
        return addWhitelistNumber;
    }

    public void setAddWhitelistNumber(Integer addWhitelistNumber) {
        this.addWhitelistNumber = addWhitelistNumber;
    }

    public Integer getExitWhitelistNumber() {
        return exitWhitelistNumber;
    }

    public void setExitWhitelistNumber(Integer exitWhitelistNumber) {
        this.exitWhitelistNumber = exitWhitelistNumber;
    }

    public Integer getNowWhitelistNumber() {
        return nowWhitelistNumber;
    }

    public void setNowWhitelistNumber(Integer nowWhitelistNumber) {
        this.nowWhitelistNumber = nowWhitelistNumber;
    }

    public Integer getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(Integer totalNumber) {
        this.totalNumber = totalNumber;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

	@Override
	public String toString() {
		return "GemiiHcpGroup [id=" + id + ", areaName=" + areaName
				+ ", province=" + province + ", city=" + city + ", department="
				+ department + ", groupType=" + groupType + ", groupName="
				+ groupName + ", wyethId=" + wyethId + ", hsptId=" + hsptId
				+ ", hsptName=" + hsptName + ", anchorcity=" + anchorcity
				+ ", clustercity=" + clustercity + ", ctime=" + ctime
				+ ", squadNumber=" + squadNumber + ", helperNumber="
				+ helperNumber + ", botNumber=" + botNumber + ", komNumber="
				+ komNumber + ", addWhitelistNumber=" + addWhitelistNumber
				+ ", exitWhitelistNumber=" + exitWhitelistNumber
				+ ", nowWhitelistNumber=" + nowWhitelistNumber
				+ ", totalNumber=" + totalNumber + ", remark=" + remark + "]";
	}
    
}