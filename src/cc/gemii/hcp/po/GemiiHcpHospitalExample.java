package cc.gemii.hcp.po;

import java.util.ArrayList;
import java.util.List;

public class GemiiHcpHospitalExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GemiiHcpHospitalExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andHsptNameIsNull() {
            addCriterion("hspt_name is null");
            return (Criteria) this;
        }

        public Criteria andHsptNameIsNotNull() {
            addCriterion("hspt_name is not null");
            return (Criteria) this;
        }

        public Criteria andHsptNameEqualTo(String value) {
            addCriterion("hspt_name =", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameNotEqualTo(String value) {
            addCriterion("hspt_name <>", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameGreaterThan(String value) {
            addCriterion("hspt_name >", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameGreaterThanOrEqualTo(String value) {
            addCriterion("hspt_name >=", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameLessThan(String value) {
            addCriterion("hspt_name <", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameLessThanOrEqualTo(String value) {
            addCriterion("hspt_name <=", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameLike(String value) {
            addCriterion("hspt_name like", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameNotLike(String value) {
            addCriterion("hspt_name not like", value, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameIn(List<String> values) {
            addCriterion("hspt_name in", values, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameNotIn(List<String> values) {
            addCriterion("hspt_name not in", values, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameBetween(String value1, String value2) {
            addCriterion("hspt_name between", value1, value2, "hsptName");
            return (Criteria) this;
        }

        public Criteria andHsptNameNotBetween(String value1, String value2) {
            addCriterion("hspt_name not between", value1, value2, "hsptName");
            return (Criteria) this;
        }

        public Criteria andAreaIdIsNull() {
            addCriterion("area_id is null");
            return (Criteria) this;
        }

        public Criteria andAreaIdIsNotNull() {
            addCriterion("area_id is not null");
            return (Criteria) this;
        }

        public Criteria andAreaIdEqualTo(Integer value) {
            addCriterion("area_id =", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotEqualTo(Integer value) {
            addCriterion("area_id <>", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdGreaterThan(Integer value) {
            addCriterion("area_id >", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("area_id >=", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdLessThan(Integer value) {
            addCriterion("area_id <", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdLessThanOrEqualTo(Integer value) {
            addCriterion("area_id <=", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdIn(List<Integer> values) {
            addCriterion("area_id in", values, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotIn(List<Integer> values) {
            addCriterion("area_id not in", values, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdBetween(Integer value1, Integer value2) {
            addCriterion("area_id between", value1, value2, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotBetween(Integer value1, Integer value2) {
            addCriterion("area_id not between", value1, value2, "areaId");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNull() {
            addCriterion("city_id is null");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNotNull() {
            addCriterion("city_id is not null");
            return (Criteria) this;
        }

        public Criteria andCityIdEqualTo(Integer value) {
            addCriterion("city_id =", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotEqualTo(Integer value) {
            addCriterion("city_id <>", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThan(Integer value) {
            addCriterion("city_id >", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("city_id >=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThan(Integer value) {
            addCriterion("city_id <", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThanOrEqualTo(Integer value) {
            addCriterion("city_id <=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdIn(List<Integer> values) {
            addCriterion("city_id in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotIn(List<Integer> values) {
            addCriterion("city_id not in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdBetween(Integer value1, Integer value2) {
            addCriterion("city_id between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotBetween(Integer value1, Integer value2) {
            addCriterion("city_id not between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andHsptNbmIsNull() {
            addCriterion("hspt_nbm is null");
            return (Criteria) this;
        }

        public Criteria andHsptNbmIsNotNull() {
            addCriterion("hspt_nbm is not null");
            return (Criteria) this;
        }

        public Criteria andHsptNbmEqualTo(Integer value) {
            addCriterion("hspt_nbm =", value, "hsptNbm");
            return (Criteria) this;
        }

        public Criteria andHsptNbmNotEqualTo(Integer value) {
            addCriterion("hspt_nbm <>", value, "hsptNbm");
            return (Criteria) this;
        }

        public Criteria andHsptNbmGreaterThan(Integer value) {
            addCriterion("hspt_nbm >", value, "hsptNbm");
            return (Criteria) this;
        }

        public Criteria andHsptNbmGreaterThanOrEqualTo(Integer value) {
            addCriterion("hspt_nbm >=", value, "hsptNbm");
            return (Criteria) this;
        }

        public Criteria andHsptNbmLessThan(Integer value) {
            addCriterion("hspt_nbm <", value, "hsptNbm");
            return (Criteria) this;
        }

        public Criteria andHsptNbmLessThanOrEqualTo(Integer value) {
            addCriterion("hspt_nbm <=", value, "hsptNbm");
            return (Criteria) this;
        }

        public Criteria andHsptNbmIn(List<Integer> values) {
            addCriterion("hspt_nbm in", values, "hsptNbm");
            return (Criteria) this;
        }

        public Criteria andHsptNbmNotIn(List<Integer> values) {
            addCriterion("hspt_nbm not in", values, "hsptNbm");
            return (Criteria) this;
        }

        public Criteria andHsptNbmBetween(Integer value1, Integer value2) {
            addCriterion("hspt_nbm between", value1, value2, "hsptNbm");
            return (Criteria) this;
        }

        public Criteria andHsptNbmNotBetween(Integer value1, Integer value2) {
            addCriterion("hspt_nbm not between", value1, value2, "hsptNbm");
            return (Criteria) this;
        }

        public Criteria andHsptTypeIsNull() {
            addCriterion("hspt_type is null");
            return (Criteria) this;
        }

        public Criteria andHsptTypeIsNotNull() {
            addCriterion("hspt_type is not null");
            return (Criteria) this;
        }

        public Criteria andHsptTypeEqualTo(String value) {
            addCriterion("hspt_type =", value, "hsptType");
            return (Criteria) this;
        }

        public Criteria andHsptTypeNotEqualTo(String value) {
            addCriterion("hspt_type <>", value, "hsptType");
            return (Criteria) this;
        }

        public Criteria andHsptTypeGreaterThan(String value) {
            addCriterion("hspt_type >", value, "hsptType");
            return (Criteria) this;
        }

        public Criteria andHsptTypeGreaterThanOrEqualTo(String value) {
            addCriterion("hspt_type >=", value, "hsptType");
            return (Criteria) this;
        }

        public Criteria andHsptTypeLessThan(String value) {
            addCriterion("hspt_type <", value, "hsptType");
            return (Criteria) this;
        }

        public Criteria andHsptTypeLessThanOrEqualTo(String value) {
            addCriterion("hspt_type <=", value, "hsptType");
            return (Criteria) this;
        }

        public Criteria andHsptTypeLike(String value) {
            addCriterion("hspt_type like", value, "hsptType");
            return (Criteria) this;
        }

        public Criteria andHsptTypeNotLike(String value) {
            addCriterion("hspt_type not like", value, "hsptType");
            return (Criteria) this;
        }

        public Criteria andHsptTypeIn(List<String> values) {
            addCriterion("hspt_type in", values, "hsptType");
            return (Criteria) this;
        }

        public Criteria andHsptTypeNotIn(List<String> values) {
            addCriterion("hspt_type not in", values, "hsptType");
            return (Criteria) this;
        }

        public Criteria andHsptTypeBetween(String value1, String value2) {
            addCriterion("hspt_type between", value1, value2, "hsptType");
            return (Criteria) this;
        }

        public Criteria andHsptTypeNotBetween(String value1, String value2) {
            addCriterion("hspt_type not between", value1, value2, "hsptType");
            return (Criteria) this;
        }

        public Criteria andHsptLevelIsNull() {
            addCriterion("hspt_level is null");
            return (Criteria) this;
        }

        public Criteria andHsptLevelIsNotNull() {
            addCriterion("hspt_level is not null");
            return (Criteria) this;
        }

        public Criteria andHsptLevelEqualTo(String value) {
            addCriterion("hspt_level =", value, "hsptLevel");
            return (Criteria) this;
        }

        public Criteria andHsptLevelNotEqualTo(String value) {
            addCriterion("hspt_level <>", value, "hsptLevel");
            return (Criteria) this;
        }

        public Criteria andHsptLevelGreaterThan(String value) {
            addCriterion("hspt_level >", value, "hsptLevel");
            return (Criteria) this;
        }

        public Criteria andHsptLevelGreaterThanOrEqualTo(String value) {
            addCriterion("hspt_level >=", value, "hsptLevel");
            return (Criteria) this;
        }

        public Criteria andHsptLevelLessThan(String value) {
            addCriterion("hspt_level <", value, "hsptLevel");
            return (Criteria) this;
        }

        public Criteria andHsptLevelLessThanOrEqualTo(String value) {
            addCriterion("hspt_level <=", value, "hsptLevel");
            return (Criteria) this;
        }

        public Criteria andHsptLevelLike(String value) {
            addCriterion("hspt_level like", value, "hsptLevel");
            return (Criteria) this;
        }

        public Criteria andHsptLevelNotLike(String value) {
            addCriterion("hspt_level not like", value, "hsptLevel");
            return (Criteria) this;
        }

        public Criteria andHsptLevelIn(List<String> values) {
            addCriterion("hspt_level in", values, "hsptLevel");
            return (Criteria) this;
        }

        public Criteria andHsptLevelNotIn(List<String> values) {
            addCriterion("hspt_level not in", values, "hsptLevel");
            return (Criteria) this;
        }

        public Criteria andHsptLevelBetween(String value1, String value2) {
            addCriterion("hspt_level between", value1, value2, "hsptLevel");
            return (Criteria) this;
        }

        public Criteria andHsptLevelNotBetween(String value1, String value2) {
            addCriterion("hspt_level not between", value1, value2, "hsptLevel");
            return (Criteria) this;
        }

        public Criteria andHsptUrlIsNull() {
            addCriterion("hspt_url is null");
            return (Criteria) this;
        }

        public Criteria andHsptUrlIsNotNull() {
            addCriterion("hspt_url is not null");
            return (Criteria) this;
        }

        public Criteria andHsptUrlEqualTo(String value) {
            addCriterion("hspt_url =", value, "hsptUrl");
            return (Criteria) this;
        }

        public Criteria andHsptUrlNotEqualTo(String value) {
            addCriterion("hspt_url <>", value, "hsptUrl");
            return (Criteria) this;
        }

        public Criteria andHsptUrlGreaterThan(String value) {
            addCriterion("hspt_url >", value, "hsptUrl");
            return (Criteria) this;
        }

        public Criteria andHsptUrlGreaterThanOrEqualTo(String value) {
            addCriterion("hspt_url >=", value, "hsptUrl");
            return (Criteria) this;
        }

        public Criteria andHsptUrlLessThan(String value) {
            addCriterion("hspt_url <", value, "hsptUrl");
            return (Criteria) this;
        }

        public Criteria andHsptUrlLessThanOrEqualTo(String value) {
            addCriterion("hspt_url <=", value, "hsptUrl");
            return (Criteria) this;
        }

        public Criteria andHsptUrlLike(String value) {
            addCriterion("hspt_url like", value, "hsptUrl");
            return (Criteria) this;
        }

        public Criteria andHsptUrlNotLike(String value) {
            addCriterion("hspt_url not like", value, "hsptUrl");
            return (Criteria) this;
        }

        public Criteria andHsptUrlIn(List<String> values) {
            addCriterion("hspt_url in", values, "hsptUrl");
            return (Criteria) this;
        }

        public Criteria andHsptUrlNotIn(List<String> values) {
            addCriterion("hspt_url not in", values, "hsptUrl");
            return (Criteria) this;
        }

        public Criteria andHsptUrlBetween(String value1, String value2) {
            addCriterion("hspt_url between", value1, value2, "hsptUrl");
            return (Criteria) this;
        }

        public Criteria andHsptUrlNotBetween(String value1, String value2) {
            addCriterion("hspt_url not between", value1, value2, "hsptUrl");
            return (Criteria) this;
        }

        public Criteria andHsptAddressIsNull() {
            addCriterion("hspt_address is null");
            return (Criteria) this;
        }

        public Criteria andHsptAddressIsNotNull() {
            addCriterion("hspt_address is not null");
            return (Criteria) this;
        }

        public Criteria andHsptAddressEqualTo(String value) {
            addCriterion("hspt_address =", value, "hsptAddress");
            return (Criteria) this;
        }

        public Criteria andHsptAddressNotEqualTo(String value) {
            addCriterion("hspt_address <>", value, "hsptAddress");
            return (Criteria) this;
        }

        public Criteria andHsptAddressGreaterThan(String value) {
            addCriterion("hspt_address >", value, "hsptAddress");
            return (Criteria) this;
        }

        public Criteria andHsptAddressGreaterThanOrEqualTo(String value) {
            addCriterion("hspt_address >=", value, "hsptAddress");
            return (Criteria) this;
        }

        public Criteria andHsptAddressLessThan(String value) {
            addCriterion("hspt_address <", value, "hsptAddress");
            return (Criteria) this;
        }

        public Criteria andHsptAddressLessThanOrEqualTo(String value) {
            addCriterion("hspt_address <=", value, "hsptAddress");
            return (Criteria) this;
        }

        public Criteria andHsptAddressLike(String value) {
            addCriterion("hspt_address like", value, "hsptAddress");
            return (Criteria) this;
        }

        public Criteria andHsptAddressNotLike(String value) {
            addCriterion("hspt_address not like", value, "hsptAddress");
            return (Criteria) this;
        }

        public Criteria andHsptAddressIn(List<String> values) {
            addCriterion("hspt_address in", values, "hsptAddress");
            return (Criteria) this;
        }

        public Criteria andHsptAddressNotIn(List<String> values) {
            addCriterion("hspt_address not in", values, "hsptAddress");
            return (Criteria) this;
        }

        public Criteria andHsptAddressBetween(String value1, String value2) {
            addCriterion("hspt_address between", value1, value2, "hsptAddress");
            return (Criteria) this;
        }

        public Criteria andHsptAddressNotBetween(String value1, String value2) {
            addCriterion("hspt_address not between", value1, value2, "hsptAddress");
            return (Criteria) this;
        }

        public Criteria andHsptPhoneIsNull() {
            addCriterion("hspt_phone is null");
            return (Criteria) this;
        }

        public Criteria andHsptPhoneIsNotNull() {
            addCriterion("hspt_phone is not null");
            return (Criteria) this;
        }

        public Criteria andHsptPhoneEqualTo(String value) {
            addCriterion("hspt_phone =", value, "hsptPhone");
            return (Criteria) this;
        }

        public Criteria andHsptPhoneNotEqualTo(String value) {
            addCriterion("hspt_phone <>", value, "hsptPhone");
            return (Criteria) this;
        }

        public Criteria andHsptPhoneGreaterThan(String value) {
            addCriterion("hspt_phone >", value, "hsptPhone");
            return (Criteria) this;
        }

        public Criteria andHsptPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("hspt_phone >=", value, "hsptPhone");
            return (Criteria) this;
        }

        public Criteria andHsptPhoneLessThan(String value) {
            addCriterion("hspt_phone <", value, "hsptPhone");
            return (Criteria) this;
        }

        public Criteria andHsptPhoneLessThanOrEqualTo(String value) {
            addCriterion("hspt_phone <=", value, "hsptPhone");
            return (Criteria) this;
        }

        public Criteria andHsptPhoneLike(String value) {
            addCriterion("hspt_phone like", value, "hsptPhone");
            return (Criteria) this;
        }

        public Criteria andHsptPhoneNotLike(String value) {
            addCriterion("hspt_phone not like", value, "hsptPhone");
            return (Criteria) this;
        }

        public Criteria andHsptPhoneIn(List<String> values) {
            addCriterion("hspt_phone in", values, "hsptPhone");
            return (Criteria) this;
        }

        public Criteria andHsptPhoneNotIn(List<String> values) {
            addCriterion("hspt_phone not in", values, "hsptPhone");
            return (Criteria) this;
        }

        public Criteria andHsptPhoneBetween(String value1, String value2) {
            addCriterion("hspt_phone between", value1, value2, "hsptPhone");
            return (Criteria) this;
        }

        public Criteria andHsptPhoneNotBetween(String value1, String value2) {
            addCriterion("hspt_phone not between", value1, value2, "hsptPhone");
            return (Criteria) this;
        }

        public Criteria andHsptVerifyIsNull() {
            addCriterion("hspt_verify is null");
            return (Criteria) this;
        }

        public Criteria andHsptVerifyIsNotNull() {
            addCriterion("hspt_verify is not null");
            return (Criteria) this;
        }

        public Criteria andHsptVerifyEqualTo(Integer value) {
            addCriterion("hspt_verify =", value, "hsptVerify");
            return (Criteria) this;
        }

        public Criteria andHsptVerifyNotEqualTo(Integer value) {
            addCriterion("hspt_verify <>", value, "hsptVerify");
            return (Criteria) this;
        }

        public Criteria andHsptVerifyGreaterThan(Integer value) {
            addCriterion("hspt_verify >", value, "hsptVerify");
            return (Criteria) this;
        }

        public Criteria andHsptVerifyGreaterThanOrEqualTo(Integer value) {
            addCriterion("hspt_verify >=", value, "hsptVerify");
            return (Criteria) this;
        }

        public Criteria andHsptVerifyLessThan(Integer value) {
            addCriterion("hspt_verify <", value, "hsptVerify");
            return (Criteria) this;
        }

        public Criteria andHsptVerifyLessThanOrEqualTo(Integer value) {
            addCriterion("hspt_verify <=", value, "hsptVerify");
            return (Criteria) this;
        }

        public Criteria andHsptVerifyIn(List<Integer> values) {
            addCriterion("hspt_verify in", values, "hsptVerify");
            return (Criteria) this;
        }

        public Criteria andHsptVerifyNotIn(List<Integer> values) {
            addCriterion("hspt_verify not in", values, "hsptVerify");
            return (Criteria) this;
        }

        public Criteria andHsptVerifyBetween(Integer value1, Integer value2) {
            addCriterion("hspt_verify between", value1, value2, "hsptVerify");
            return (Criteria) this;
        }

        public Criteria andHsptVerifyNotBetween(Integer value1, Integer value2) {
            addCriterion("hspt_verify not between", value1, value2, "hsptVerify");
            return (Criteria) this;
        }

        public Criteria andHsptCodeIsNull() {
            addCriterion("hspt_code is null");
            return (Criteria) this;
        }

        public Criteria andHsptCodeIsNotNull() {
            addCriterion("hspt_code is not null");
            return (Criteria) this;
        }

        public Criteria andHsptCodeEqualTo(String value) {
            addCriterion("hspt_code =", value, "hsptCode");
            return (Criteria) this;
        }

        public Criteria andHsptCodeNotEqualTo(String value) {
            addCriterion("hspt_code <>", value, "hsptCode");
            return (Criteria) this;
        }

        public Criteria andHsptCodeGreaterThan(String value) {
            addCriterion("hspt_code >", value, "hsptCode");
            return (Criteria) this;
        }

        public Criteria andHsptCodeGreaterThanOrEqualTo(String value) {
            addCriterion("hspt_code >=", value, "hsptCode");
            return (Criteria) this;
        }

        public Criteria andHsptCodeLessThan(String value) {
            addCriterion("hspt_code <", value, "hsptCode");
            return (Criteria) this;
        }

        public Criteria andHsptCodeLessThanOrEqualTo(String value) {
            addCriterion("hspt_code <=", value, "hsptCode");
            return (Criteria) this;
        }

        public Criteria andHsptCodeLike(String value) {
            addCriterion("hspt_code like", value, "hsptCode");
            return (Criteria) this;
        }

        public Criteria andHsptCodeNotLike(String value) {
            addCriterion("hspt_code not like", value, "hsptCode");
            return (Criteria) this;
        }

        public Criteria andHsptCodeIn(List<String> values) {
            addCriterion("hspt_code in", values, "hsptCode");
            return (Criteria) this;
        }

        public Criteria andHsptCodeNotIn(List<String> values) {
            addCriterion("hspt_code not in", values, "hsptCode");
            return (Criteria) this;
        }

        public Criteria andHsptCodeBetween(String value1, String value2) {
            addCriterion("hspt_code between", value1, value2, "hsptCode");
            return (Criteria) this;
        }

        public Criteria andHsptCodeNotBetween(String value1, String value2) {
            addCriterion("hspt_code not between", value1, value2, "hsptCode");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}