package cc.gemii.hcp.po;

import java.util.Date;

import cc.gemii.hcp.utils.TimeUtils;
import cc.gemii.hcp.utils.UUIDUtils;

public class GemiiHcpVerify {
    private String id;

    private Integer staffId;

    private String doctorId;

    private String doctorName;

    private String doctorPhone;

    private Date createTime;

    private Date handleTime;

    private Integer state;

    private String remark;

    public String getId() {
        return id;
    }

    public GemiiHcpVerify() {
		super();
	}

	public GemiiHcpVerify(Integer staffId, String doctorId, String doctorName,
			String doctorPhone) {
		super();
		this.staffId = staffId;
		this.doctorId = doctorId;
		this.doctorName = doctorName;
		this.doctorPhone = doctorPhone;
		
		this.createTime = TimeUtils.getDate("yyyy-MM-dd HH:mm:ss");
	}

	public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId == null ? null : doctorId.trim();
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName == null ? null : doctorName.trim();
    }

    public String getDoctorPhone() {
        return doctorPhone;
    }

    public void setDoctorPhone(String doctorPhone) {
        this.doctorPhone = doctorPhone == null ? null : doctorPhone.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getHandleTime() {
        return handleTime;
    }

    public void setHandleTime(Date handleTime) {
        this.handleTime = handleTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

	@Override
	public String toString() {
		return "GemiiHcpVerify [id=" + id + ", staffId=" + staffId
				+ ", doctorId=" + doctorId + ", doctorName=" + doctorName
				+ ", doctorPhone=" + doctorPhone + ", createTime=" + createTime
				+ ", handleTime=" + handleTime + ", state=" + state
				+ ", remark=" + remark + "]";
	}
    
}